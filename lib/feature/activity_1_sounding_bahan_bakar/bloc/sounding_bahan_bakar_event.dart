part of 'sounding_bahan_bakar_bloc.dart';

abstract class SoundingBahanBakarEvent extends Equatable {
  const SoundingBahanBakarEvent();

  @override
  List<Object> get props => [];
}

class PostSoundingBahanBakarEvent extends SoundingBahanBakarEvent {
  final PostSoundingBahanBakar postSoundingBahanBakar;
  const PostSoundingBahanBakarEvent(this.postSoundingBahanBakar);
}

class UpdateStatusSoundingBahanBakarEvent extends SoundingBahanBakarEvent {
  final int id;
  final String date;
  final Approve approve;
  const UpdateStatusSoundingBahanBakarEvent(this.id, this.date, this.approve);
}

class GetHistorySoundingBahanBakarEvent extends SoundingBahanBakarEvent {
  final int id;
  final String date;
  const GetHistorySoundingBahanBakarEvent(this.id, this.date);
}
