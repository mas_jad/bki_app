part of 'sounding_bahan_bakar_bloc.dart';

abstract class SoundingBahanBakarState extends Equatable {
  const SoundingBahanBakarState();

  @override
  List<Object> get props => [];
}

class SoundingBahanBakarInitial extends SoundingBahanBakarState {}

// HISTORY
abstract class HistorySoundingBahanBakarState extends SoundingBahanBakarState {}

class HistorySoundingBahanBakarSuccess extends HistorySoundingBahanBakarState {
  final Map<MasterTangkiBbm, HistorySoundingBahanBakar?> map;
  final Approve approve;
  final String? revisedDate;
  HistorySoundingBahanBakarSuccess(this.map, this.revisedDate, this.approve);
}

class HistorySoundingBahanBakarError extends HistorySoundingBahanBakarState {
  final String message;
  HistorySoundingBahanBakarError(this.message);
}

class HistorySoundingBahanBakarLoading extends HistorySoundingBahanBakarState {}

// POST
abstract class PostSoundingBahanBakarState extends SoundingBahanBakarState {}

class PostSoundingBahanBakarSuccess extends PostSoundingBahanBakarState {}

class PostSoundingBahanBakarError extends PostSoundingBahanBakarState {
  final String message;
  PostSoundingBahanBakarError(this.message);
}

class PostSoundingBahanBakarLoading extends PostSoundingBahanBakarState {}

// UPDATE STATUS
abstract class UpdateStatusSoundingBahanBakarState
    extends SoundingBahanBakarState {}

class UpdateStatusSoundingBahanBakarSuccess
    extends UpdateStatusSoundingBahanBakarState {}

class UpdateStatusSoundingBahanBakarError
    extends UpdateStatusSoundingBahanBakarState {
  final String message;
  UpdateStatusSoundingBahanBakarError(this.message);
}

class UpdateStatusSoundingBahanBakarLoading
    extends UpdateStatusSoundingBahanBakarState {}
