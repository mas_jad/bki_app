import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/data/model/post_sounding_bahan_bakar.dart';

import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_sounding_bahan_bakar.dart';
import '../data/repository/sounding_bahan_bakar_repository.dart';

part 'sounding_bahan_bakar_event.dart';
part 'sounding_bahan_bakar_state.dart';

class SoundingBahanBakarBloc
    extends Bloc<SoundingBahanBakarEvent, SoundingBahanBakarState> {
  final SoundingBahanBakarRepository _soundingBahanBakarRepository;
  final UserRepository _userRepository;

  SoundingBahanBakarBloc(
      this._soundingBahanBakarRepository, this._userRepository)
      : super(SoundingBahanBakarInitial()) {
    on<PostSoundingBahanBakarEvent>((event, emit) async {
      try {
        emit(PostSoundingBahanBakarLoading());
        final token = await _userRepository.getToken();
        final data = await _soundingBahanBakarRepository.postSoundingBahanBakar(
            token, event.postSoundingBahanBakar);
        emit(PostSoundingBahanBakarSuccess());
        emit(HistorySoundingBahanBakarSuccess(
          data,
          _soundingBahanBakarRepository.getRevisedDate(),
          _soundingBahanBakarRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostSoundingBahanBakarError(e.message));
        } else {
          emit(PostSoundingBahanBakarError(somethingErrorHappen));
        }
      }
    });

    on<GetHistorySoundingBahanBakarEvent>((event, emit) async {
      try {
        emit(HistorySoundingBahanBakarLoading());
        final token = await _userRepository.getToken();
        final data = await _soundingBahanBakarRepository
            .getHistorySoundingBahanBakar(event.id, token, event.date);
        emit(HistorySoundingBahanBakarSuccess(
          data,
          _soundingBahanBakarRepository.getRevisedDate(),
          _soundingBahanBakarRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistorySoundingBahanBakarError(e.message));
        } else {
          emit(HistorySoundingBahanBakarError(somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusSoundingBahanBakarEvent>((event, emit) async {
      try {
        emit(UpdateStatusSoundingBahanBakarLoading());
        final token = await _userRepository.getToken();
        final data =
            await _soundingBahanBakarRepository.updateStatusSoundingBahanBakar(
                event.id, token, event.date, event.approve);

        emit(UpdateStatusSoundingBahanBakarSuccess());
        emit(HistorySoundingBahanBakarSuccess(
          data,
          _soundingBahanBakarRepository.getRevisedDate(),
          _soundingBahanBakarRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(UpdateStatusSoundingBahanBakarError(e.message));
        } else {
          emit(UpdateStatusSoundingBahanBakarError(somethingErrorHappen));
        }
      }
    });
  }
}
