import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/data/model/history_sounding_bahan_bakar.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/data/source/sounding_bahan_bakar_remote_data_source.dart';

import '../../../../core/util/request.dart';
import '../model/post_sounding_bahan_bakar.dart';

class SoundingBahanBakarRepository {
  final SoundingBahanBakarRemoteDataSource _soundingBahanBakarRemoteDataSource;
  SoundingBahanBakarRepository(this._soundingBahanBakarRemoteDataSource);

  String? revisedDate;
  Approve? approve;

  String? getRevisedDate() {
    return revisedDate;
  }

  Approve getApprove() {
    return approve ??
        Approve(
            approved1: null, approved2: null, approved3: null, approved4: null);
  }

  Map<MasterTangkiBbm, HistorySoundingBahanBakar?> _relatingTheDataToMaster(
      HistorySoundingBahanBakarResponse response) {
    final Map<MasterTangkiBbm, HistorySoundingBahanBakar?> map = {};
    final listHistoryBahanBakar = response.data ?? [];
    final listTankiKapal = response.masterTangkiBbm ?? [];
    // Relating Tanki To Data History
    for (var element in listTankiKapal) {
      var isThere = false;
      for (var element2 in listHistoryBahanBakar) {
        if (element.id == element2.tangkiBbmId) {
          map.addAll({element: element2});
          isThere = true;
          break;
        }
      }
      if (!isThere) {
        map.addAll({element: null});
      }
    }
    return map;
  }

  Future<Map<MasterTangkiBbm, HistorySoundingBahanBakar?>>
      getHistorySoundingBahanBakar(int id, String token, String date) async {
    return RequestServer<Map<MasterTangkiBbm, HistorySoundingBahanBakar?>>()
        .requestServer(computation: () async {
      final response = await _soundingBahanBakarRemoteDataSource
          .getHistorySoundingBahanBakar(id, token, date);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingTheDataToMaster(response);
    });
  }

  Future<Map<MasterTangkiBbm, HistorySoundingBahanBakar?>>
      postSoundingBahanBakar(
          String token, PostSoundingBahanBakar soundingBahanBakar) async {
    return RequestServer<Map<MasterTangkiBbm, HistorySoundingBahanBakar?>>()
        .requestServer(computation: () async {
      final response = await _soundingBahanBakarRemoteDataSource
          .postSoundingBahanBakar(token, soundingBahanBakar);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingTheDataToMaster(response);
    });
  }

  Future<Map<MasterTangkiBbm, HistorySoundingBahanBakar?>>
      updateStatusSoundingBahanBakar(
          int id, String token, String date, Approve approved) async {
    return RequestServer<Map<MasterTangkiBbm, HistorySoundingBahanBakar?>>()
        .requestServer(computation: () async {
      final response = await _soundingBahanBakarRemoteDataSource
          .updateStatusSoundingBahanBakar(id, token, date, approved);
      revisedDate = response.tanggalMundur;

      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);

      return _relatingTheDataToMaster(response);
    });
  }
}
