import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/data/model/history_sounding_bahan_bakar.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/data/model/post_sounding_bahan_bakar.dart';

import '../../../../core/service/location/location_service.dart';
import '../../../../core/util/user_role.dart';

class SoundingBahanBakarRemoteDataSource {
  Future<HistorySoundingBahanBakarResponse> getHistorySoundingBahanBakar(
      int id, String token, String date) async {
    final endpoint = dotenv.get(dataSBBPerTanggalEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {"kapal_id": id, "date": date},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistorySoundingBahanBakarResponse.fromJson(response.data);
  }

  Future<HistorySoundingBahanBakarResponse> postSoundingBahanBakar(
      String token, PostSoundingBahanBakar soundingBahanBakar) async {
    final endpoint = dotenv.get(simpanDataSBBEndpoint);
    final position = await locator<LocationService>().getLastPosition();
    final data = await soundingBahanBakar.toJson(position);
    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistorySoundingBahanBakarResponse.fromJson(response.data);
  }

  Future<HistorySoundingBahanBakarResponse> updateStatusSoundingBahanBakar(
      int id, String token, String date, Approve approve) async {
    final endpoint = dotenv.get(ubahStatusSBBEndpoint);
    final response = await locator<Dio>().post(
      "$endpoint${locator<UserRole>().getUpdateStatusURL(true, approve)}",
      data: {"kapal_id": id, "date": date},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistorySoundingBahanBakarResponse.fromJson(response.data);
  }
}
