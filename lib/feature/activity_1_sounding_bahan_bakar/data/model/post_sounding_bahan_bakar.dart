import 'package:dio/dio.dart';

class PostSoundingBahanBakar {
  int? kapalId;
  String? date;
  int? tangkiBbmId;
  int? soundingDepth;
  int? soundingCap;
  int? trimKapal;
  String? image;

  PostSoundingBahanBakar(
      {this.kapalId,
      this.date,
      this.tangkiBbmId,
      this.soundingDepth,
      this.soundingCap,
      this.trimKapal,
      this.image});

  Future<FormData> toJson(String? location) async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kapal_id'] = kapalId;
    data['date'] = date;
    data['tangki_bbm_id'] = tangkiBbmId;
    data['sounding_depth'] = soundingDepth;
    data['sounding_cap'] = soundingCap;
    data['trim_kapal'] = trimKapal;
    data['location'] = location;
    data['image'] = image != null
        ? await MultipartFile.fromFile(
            image!,
            filename: image!.split('/').last,
          )
        : null;
    return FormData.fromMap(data);
  }
}
