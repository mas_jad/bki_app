class HistorySoundingBahanBakarResponse {
  String? message;
  int? code;
  List<HistorySoundingBahanBakar>? data;
  String? date;
  String? tanggalMundur;
  List<MasterTangkiBbm>? masterTangkiBbm;
  String? approved1;
  String? approved2;
  String? approved3;
  String? approved4;

  HistorySoundingBahanBakarResponse(
      {this.message,
      this.code,
      this.data,
      this.date,
      this.masterTangkiBbm,
      this.tanggalMundur,
      this.approved1,
      this.approved2,
      this.approved3,
      this.approved4});

  HistorySoundingBahanBakarResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    if (json['data'] != null) {
      data = <HistorySoundingBahanBakar>[];
      json['data'].forEach((v) {
        data!.add(HistorySoundingBahanBakar.fromJson(v));
      });
    }
    date = json['date'];
    approved1 = json['approved_1'];
    approved2 = json['approved_2'];
    approved3 = json['approved_3'];
    approved4 = json['approved_4'];
    tanggalMundur = json['tanggal_mundur'];
    if (json['master_tangki_bbm'] != null) {
      masterTangkiBbm = <MasterTangkiBbm>[];
      json['master_tangki_bbm'].forEach((v) {
        masterTangkiBbm!.add(MasterTangkiBbm.fromJson(v));
      });
    }
  }
}

class HistorySoundingBahanBakar {
  int? soundingDepth;
  int? soundingCap;
  int? trimKapal;
  int? tangkiBbmId;
  String? image;
  String? statusProses;

  HistorySoundingBahanBakar(
      {this.soundingDepth,
      this.soundingCap,
      this.trimKapal,
      this.tangkiBbmId,
      this.image,
      this.statusProses});

  HistorySoundingBahanBakar.fromJson(Map<String, dynamic> json) {
    soundingDepth = json['sounding_depth'];
    soundingCap = json['sounding_cap'];
    trimKapal = json['trim_kapal'];
    tangkiBbmId = json['tangki_bbm_id'];
    image = json['image'];
    statusProses = json['status_proses'];
  }
}

class MasterTangkiBbm {
  int? id;
  String? posisi;
  int? frameStart;
  int? frameTo;

  MasterTangkiBbm({this.id, this.posisi, this.frameStart, this.frameTo});

  MasterTangkiBbm.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    posisi = json['posisi'];
    frameStart = json['frame_start'];
    frameTo = json['frame_to'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['posisi'] = posisi;
    data['frame_start'] = frameStart;
    data['frame_to'] = frameTo;
    return data;
  }
}
