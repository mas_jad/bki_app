import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/feature/home/view/home_page.dart';

import '../../login/view/login_page.dart';
import '../../ship_activity/view/ship_activity_page.dart';
import '../bloc/splash_bloc.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    run();
  }

  run() {
    BlocProvider.of<SplashBloc>(context).add(SplashLoginEvent());
  }

  toLogin() async {
    await Future.delayed(const Duration(seconds: 2));
    if (mounted) {
      Navigator.pushReplacementNamed(context, LoginPage.route);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state is SplashSuccess) {
          if (state.list.length > 1) {
            Navigator.pushReplacementNamed(context, HomePage.route,
                arguments: [state.list, state.profile]);
          } else {
            Navigator.pushReplacementNamed(context, ShipActivity.route,
                arguments: [state.list.first, state.profile]);
          }
        } else if (state is SplashError) {
          toLogin();
        }
      },
      child: Scaffold(
        backgroundColor: white,
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Image.asset(
              imageLogo2,
              width: MediaQuery.of(context).size.width / 1.2,
            ),
          ),
        ),
      ),
    );
  }
}
