part of 'splash_bloc.dart';

abstract class SplashState extends Equatable {
  const SplashState();

  @override
  List<Object> get props => [];
}

class SplashInitial extends SplashState {}

class SplashSuccess extends SplashState {
  final Profile profile;
  final List<DataKapal> list;
  const SplashSuccess(this.profile, this.list);
}

class SplashLoading extends SplashState {}

class SplashError extends SplashState {
  final String message;
  const SplashError(this.message);
}
