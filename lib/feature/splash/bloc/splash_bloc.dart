import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/profile.dart';

import '../../../core/data/model/data_kapal.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../../../core/util/locator.dart';
import '../../../core/util/network_info.dart';

part 'splash_event.dart';
part 'splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final UserRepository _userRepository;
  SplashBloc(this._userRepository) : super(SplashInitial()) {
    on<SplashLoginEvent>((event, emit) async {
      try {
        emit(SplashLoading());
        if (await locator<NetworkInfoImpl>().isConnected) {
          final response = await _userRepository.initialLogin();
          if (response.dataKapal!.isNotEmpty) {
            emit(SplashSuccess(response.data!, response.dataKapal!));
          } else {
            emit(const SplashError(somethingErrorHappen));
          }
        } else {
          emit(const SplashError(connectionError));
        }
      } catch (e) {
        if (e is ErrorResponse) {
          emit(SplashError(e.message));
        } else {
          emit(const SplashError(somethingErrorHappen));
        }
      }
    });
  }
}
