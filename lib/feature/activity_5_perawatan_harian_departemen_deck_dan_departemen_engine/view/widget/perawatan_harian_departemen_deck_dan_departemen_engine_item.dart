import 'package:flutter/material.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/size.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/other/custom_image_network.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';

class PerawatanHarianDepartemenDeckDanDepartemenEngineItem
    extends StatelessWidget {
  const PerawatanHarianDepartemenDeckDanDepartemenEngineItem(
      {required this.callback,
      required this.title,
      this.icon,
      required this.image,
      super.key});
  final Function(BuildContext context) callback;
  final String title;
  final Icon? icon;
  final String? image;

  @override
  Widget build(BuildContext context) {
    return PopWidget(
        onTap: () {
          callback(context);
        },
        child: Stack(
          children: [
            Container(
              width: sizeGridWidth,
              height: sizeGridHeight,
              decoration: const BoxDecoration(
                color: secondary,
                borderRadius: cardBorderRadius,
              ),
            ),
            if (image != null)
              CustomImageNetwork(
                  width: sizeGridWidth,
                  height: sizeGridHeight,
                  borderRadius: cardBorderRadius,
                  imageUrl: image),
            Container(
              width: sizeGridWidth,
              height: sizeGridHeight,
              padding: const EdgeInsets.all(20),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(itemImage1), fit: BoxFit.cover),
                borderRadius: cardBorderRadius,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    title,
                    style: ptSansTextStyle.copyWith(
                        color: white, fontWeight: bold),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ));
  }
}
