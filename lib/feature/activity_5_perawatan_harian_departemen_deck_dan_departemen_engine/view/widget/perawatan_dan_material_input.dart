import 'package:flutter/material.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/view/widget/material_input.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/util/const.dart';
import '../../../../core/widget/button/custom_button_widget.dart';
import '../../../../core/widget/textfield/custom_textfield_widget.dart';
import '../../data/model/history_perawatan_harian_departemen_deck_dan_departemen_engine.dart';

class PerawatanDanMaterialDanInput extends StatefulWidget {
  const PerawatanDanMaterialDanInput(
      {required this.masterMaterial,
      required this.listUraianPerawatan,
      required this.sugestion,
      this.index,
      required this.isUserPermitedToSave,
      super.key});
  final List<MasterMaterial> masterMaterial;
  final List<String> sugestion;
  final int? index;
  final ValueNotifier<List<UraianPerawatan>> listUraianPerawatan;
  final bool isUserPermitedToSave;
  @override
  State<PerawatanDanMaterialDanInput> createState() =>
      _PerawatanDanMaterialDanInputState();
}

class _PerawatanDanMaterialDanInputState
    extends State<PerawatanDanMaterialDanInput> {
  showChooseMaterial() async {
    await showDialog(
        context: context,
        builder: ((context) => MaterialInput(
              masterMaterial: widget.masterMaterial,
              listMaterialUsed: materialTextEditingController,
            )));
  }

  TextEditingController perawatanTextEditingController =
      TextEditingController();
  TextEditingController materialTextEditingController = TextEditingController();
  TextEditingController hasilTextEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    perawatanTextEditingController.text = widget.index != null
        ? widget.listUraianPerawatan.value[widget.index!].uraian ?? empty
        : empty;
    materialTextEditingController.text = getMaterialFromUser();
    hasilTextEditingController.text = widget.index != null
        ? widget.listUraianPerawatan.value[widget.index!].hasil ?? empty
        : empty;
  }

  String getMaterialFromUser() {
    if (widget.index != null) {
      return widget.listUraianPerawatan.value[widget.index!].material ?? empty;
    } else {
      return empty;
    }
  }

  changeUraianPerawatan() {
    try {
      if (perawatanTextEditingController.text == empty) {
        MyToast.showError("Perawatan belum di isi", context);
      } else {
        if (widget.index != null) {
          widget.listUraianPerawatan.value[widget.index!].uraian =
              perawatanTextEditingController.text;
          widget.listUraianPerawatan.value[widget.index!].material =
              materialTextEditingController.text;
          widget.listUraianPerawatan.value[widget.index!].hasil =
              hasilTextEditingController.text;
        } else {
          widget.listUraianPerawatan.value.add(UraianPerawatan(
              uraian: perawatanTextEditingController.text,
              material: materialTextEditingController.text,
              hasil: hasilTextEditingController.text));
        }
        Navigator.pop(context);
      }

      if (mounted) {
        // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
        widget.listUraianPerawatan.notifyListeners();
      }
    } catch (e) {
      MyToast.showError(somethingErrorHappen, context);
    }
  }

  deleteUraianPerawatan() {
    if (widget.index != null) {
      try {
        widget.listUraianPerawatan.value.removeAt(widget.index!);
        // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
        widget.listUraianPerawatan.notifyListeners();
      } catch (e) {
        MyToast.showError(somethingErrorHappen, context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      width: MediaQuery.of(context).size.width,
      decoration:
          const BoxDecoration(color: white, borderRadius: cardBorderRadius),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: const BoxDecoration(
                color: secondaryAccent, borderRadius: cardBorderRadius),
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.index != null ? "Edit Perawatan" : "Tambah Perawatan",
                  style: ptSansTextStyle.copyWith(color: white, fontSize: 16),
                ),
                widget.index != null
                    ? IconButton(
                        onPressed: () {
                          deleteUraianPerawatan();
                          Navigator.pop(context);
                        },
                        icon: const Icon(
                          Icons.delete,
                          color: white,
                        ))
                    : const SizedBox(),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Autocomplete<String>(
            initialValue: TextEditingValue(
              text: perawatanTextEditingController.text,
            ),
            optionsBuilder: (TextEditingValue textEditingValue) {
              perawatanTextEditingController.text = textEditingValue.text;
              if (textEditingValue.text == empty) {
                return widget.sugestion;
              }
              return widget.sugestion.where((String option) {
                return option.toLowerCase().contains(
                      textEditingValue.text.toLowerCase(),
                    );
              });
            },
            optionsViewBuilder: (
              BuildContext context,
              AutocompleteOnSelected<String> onSelected,
              Iterable<String> options,
            ) {
              return Align(
                alignment: Alignment.topLeft,
                child: Material(
                  child: Container(
                    height: 60.0 * options.length,
                    decoration: const BoxDecoration(
                      color: white,
                      boxShadow: [
                        shadowCard,
                      ],
                    ),
                    child: ListView.builder(
                      padding: const EdgeInsets.all(10.0),
                      itemCount: options.length,
                      itemBuilder: (
                        BuildContext context,
                        int index,
                      ) {
                        final String option = options.elementAt(index);
                        return GestureDetector(
                          onTap: () {
                            onSelected(option);
                          },
                          child: ListTile(
                            title: Text(
                              option,
                              style: ptSansTextStyle.copyWith(
                                color: black68,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              );
            },
            fieldViewBuilder: (
              BuildContext context,
              TextEditingController fieldTextEditingController,
              FocusNode fieldFocusNode,
              VoidCallback onFieldSubmitted,
            ) {
              return CustomTextFieldWidget(
                label: "Perawatan",
                focusNode: fieldFocusNode,
                style: ptSansTextStyle.copyWith(
                    color: black68, fontWeight: medium),
                textEditingController: fieldTextEditingController,
                textInputType: TextInputType.text,
              );
            },
            onSelected: (String selection) {
              perawatanTextEditingController.text = selection;
            },
          ),
          const SizedBox(
            height: 16,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Flexible(
                child: CustomTextFieldWidget(
                  label: "Material",
                  style: ptSansTextStyle.copyWith(
                      color: black68, fontWeight: medium),
                  textEditingController: materialTextEditingController,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              GestureDetector(
                onTap: () {
                  showChooseMaterial();
                },
                child: Container(
                  padding: const EdgeInsets.all(16),
                  decoration: const BoxDecoration(
                      color: primary,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      boxShadow: [shadowCard]),
                  child: iconMaterial.copyWith(color: white, size: 24),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          CustomTextFieldWidget(
            label: "Hasil",
            style: ptSansTextStyle.copyWith(color: black68, fontWeight: medium),
            textEditingController: hasilTextEditingController,
          ),
          const SizedBox(
            height: 40,
          ),
          SizedBox(
            child: Row(
              children: [
                Expanded(
                  child: CustomButtonWidgetBorderSide(
                    callback: () {
                      Navigator.pop(context);
                    },
                    title: "Back",
                  ),
                ),
                if (widget.isUserPermitedToSave)
                  const SizedBox(
                    width: 10,
                  ),
                if (widget.isUserPermitedToSave)
                  Expanded(
                    child: CustomButtonWidget(
                      callback: () {
                        changeUraianPerawatan();
                      },
                      title: save,
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
