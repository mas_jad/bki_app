import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/util/global_function.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/core/widget/dialog/custom_alert_dialog.dart';
import 'package:pms_pcm/core/widget/other/error_layout.dart';
import 'package:pms_pcm/core/widget/other/loading_widget.dart';
import 'package:pms_pcm/feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/view/widget/perawatan_harian_departemen_deck_dan_departemen_engine_input.dart';
import 'package:pms_pcm/feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/view/widget/perawatan_harian_departemen_deck_dan_departemen_engine_item.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/util/locator.dart';
import '../../../../core/util/user_role.dart';
import '../../bloc/perawatan_harian_departemen_deck_dan_departemen_engine_bloc.dart';

class PerawatanHarianDepartemenDeckDanDepartemenEngineWidget
    extends StatefulWidget {
  const PerawatanHarianDepartemenDeckDanDepartemenEngineWidget(
      {required this.idDepartemen,
      required this.idKapal,
      required this.date,
      required this.isDeck,
      super.key});

  final int idKapal;
  final int idDepartemen;
  final ValueNotifier<DateTime> date;
  final bool isDeck;
  @override
  State<PerawatanHarianDepartemenDeckDanDepartemenEngineWidget> createState() =>
      _PerawatanHarianDepartemenDeckDanDepartemenEngineWidgetState();
}

class _PerawatanHarianDepartemenDeckDanDepartemenEngineWidgetState
    extends State<PerawatanHarianDepartemenDeckDanDepartemenEngineWidget> {
  DateTime? revisedDate;
  Approve approveStatus = Approve(
      approved1: null, approved2: null, approved3: null, approved4: null);
  bool isDateToSaveNotExpired = false;
  bool isUserPermitedToApprove = false;
  bool isUserPermitedToSave = false;

  getHistory() async {
    final now = locator<MyDate>().getDateToday();
    final result = await showMyDatePicker(context, widget.date.value, now);
    if (result != null) {
      if (result != widget.date.value) {
        widget.date.value = result;
        final dateFormated =
            locator<MyDate>().getDateWithInputInString(widget.date.value);
        if (mounted) {
          BlocProvider.of<PerawatanHarianDepartemenDeckDanDepartemenEngineBloc>(
                  context)
              .add(
                  GetHistoryPerawatanHarianDepartemenDeckDanDepartemenEngineEvent(
                      widget.idKapal, dateFormated, widget.idDepartemen));
        }
      }
    }
  }

  transformDataToWidget(
      HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess state) {
    // GET REVISED DATE
    revisedDate = locator<MyDate>().getDateWithInputString(state.revisedDate);

    // GET TODAY
    final List<Widget> list = [];
    isDateToSaveNotExpired = !widget.date.value
        .isBefore(revisedDate ?? locator<MyDate>().getDateToday());

    // GET STATUS DATA
    approveStatus = state.approve;
    isUserPermitedToApprove =
        locator<UserRole>().isUserPermitedToApprove(state.approve);
    isUserPermitedToSave = locator<UserRole>()
        .isUserPermitedToSave(state.approve, isDateToSaveNotExpired);

    state.historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse?.data
        ?.forEach((e) {
      list.add(PerawatanHarianDepartemenDeckDanDepartemenEngineItem(
        image: e.image,
        callback: (contextPerawatanHarianDepartemenDeckDanDepartemenEngine) {
          Navigator.push(context, MaterialPageRoute(builder: (context2) {
            return BlocProvider.value(
              value: BlocProvider.of<
                      PerawatanHarianDepartemenDeckDanDepartemenEngineBloc>(
                  context),
              child: PerawatanHarianDepartemenDeckDanDepartemenEngineInput(
                isDeck: widget.isDeck,
                departmenId: widget.idDepartemen,
                date: widget.date.value,
                historyPerawatanHarianDepartemenDeckDanDepartemenEngine: e,
                kapalId: widget.idKapal,
                sugestion: state
                    .historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
                    ?.sugestion,
                masterBagian: state
                        .historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
                        ?.masterBagian ??
                    [],
                masterMaterial: state
                        .historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
                        ?.masterMaterial ??
                    [],
                isUserPermitedToSave: isUserPermitedToSave,
              ),
            );
          }));
        },
        title: e.bagian?.bagian ?? empty,
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PerawatanHarianDepartemenDeckDanDepartemenEngineBloc,
        PerawatanHarianDepartemenDeckDanDepartemenEngineState>(
      listener: (context, state) {
        if (state
            is UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess) {
          MyToast.showSuccess(success, context);
        } else if (state
            is UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: BlocBuilder<
              PerawatanHarianDepartemenDeckDanDepartemenEngineBloc,
              PerawatanHarianDepartemenDeckDanDepartemenEngineState>(
            buildWhen: (previous, current) {
              return current
                  is HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineState;
            },
            builder: (context, state) {
              if (state
                  is HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess) {
                final List list = transformDataToWidget(state);
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.vertical(
                              bottom: Radius.circular(12)),
                          boxShadow: [shadowCard]),
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(approveStatus.getStatus(),
                                    style: ptSansTextStyle.copyWith(
                                      color: statusColor(
                                          approveStatus.getStatus()),
                                      fontWeight: semiBold,
                                      fontSize: 16,
                                    )),
                                approveStatus.getName() != null
                                    ? Text(
                                        "by ${approveStatus.getName() ?? empty}",
                                        style: ptSansTextStyle.copyWith(
                                          color: lightText,
                                          fontWeight: reguler,
                                          fontSize: 12,
                                        ))
                                    : const SizedBox(),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          GestureDetector(
                            onTap: () {
                              getHistory();
                            },
                            child: Text(
                              locator<MyDate>().getDateWithInputIndonesiaFormat(
                                  widget.date.value),
                              maxLines: 1,
                              style: ptSansTextStyle.copyWith(
                                  color: black54,
                                  fontWeight: semiBold,
                                  fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: list.isNotEmpty
                            ? GridView.builder(
                                padding: const EdgeInsets.all(20),
                                gridDelegate:
                                    const SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: 200,
                                  childAspectRatio: 1,
                                  crossAxisSpacing: 20,
                                  mainAxisSpacing: 20,
                                ),
                                itemCount: list.length,
                                itemBuilder: (BuildContext ctx, index) {
                                  return list[index];
                                },
                              )
                            : const ErrorLayout(message: dataEmpty)),
                  ],
                );
              } else if (state
                  is HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineError) {
                return ErrorLayout(message: state.message);
              }
              return const Center(child: LoadingWidget());
            },
          ),
        ),
        floatingActionButton: HeroMode(
          enabled: false,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              BlocBuilder<PerawatanHarianDepartemenDeckDanDepartemenEngineBloc,
                  PerawatanHarianDepartemenDeckDanDepartemenEngineState>(
                buildWhen: ((previous, current) => current
                    is HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineState),
                builder: (context, state) {
                  if (isUserPermitedToSave) {
                    return FloatingActionButton(
                      elevation: elevationFAB,
                      backgroundColor: primary,
                      onPressed: () {
                        if (state
                            is HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess) {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context2) {
                            return BlocProvider.value(
                              value: BlocProvider.of<
                                      PerawatanHarianDepartemenDeckDanDepartemenEngineBloc>(
                                  context),
                              child:
                                  PerawatanHarianDepartemenDeckDanDepartemenEngineInput(
                                date: widget.date.value,
                                isDeck: widget.isDeck,
                                departmenId: widget.idDepartemen,
                                sugestion: state
                                    .historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
                                    ?.sugestion,
                                masterBagian: state
                                        .historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
                                        ?.masterBagian ??
                                    [],
                                masterMaterial: state
                                        .historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
                                        ?.masterMaterial ??
                                    [],
                                historyPerawatanHarianDepartemenDeckDanDepartemenEngine:
                                    null,
                                kapalId: widget.idKapal,
                                isUserPermitedToSave: isUserPermitedToSave,
                              ),
                            );
                          }));
                        }
                      },
                      child: Container(
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                    itemImage4,
                                  ),
                                  fit: BoxFit.fill)),
                          child: const Center(child: iconAdd)),
                    );
                  } else {
                    return const SizedBox();
                  }
                },
              ),
              const SizedBox(
                height: 12,
              ),
              BlocBuilder<PerawatanHarianDepartemenDeckDanDepartemenEngineBloc,
                  PerawatanHarianDepartemenDeckDanDepartemenEngineState>(
                builder: (context, state) {
                  if (isUserPermitedToApprove) {
                    if (state
                        is UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineLoading) {
                      return FloatingActionButton(
                        elevation: 12,
                        backgroundColor: secondary,
                        onPressed: () {},
                        child: Container(
                            decoration: const BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                      itemImage4,
                                    ),
                                    fit: BoxFit.fill)),
                            child: const Center(
                                child: LoadingWidget(
                              color: white,
                            ))),
                      );
                    }

                    return FloatingActionButton(
                      elevation: elevationFAB,
                      backgroundColor: primary,
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: ((dialogContext) => BlocProvider.value(
                                  value: BlocProvider.of<
                                          PerawatanHarianDepartemenDeckDanDepartemenEngineBloc>(
                                      context),
                                  child: CustomAlertDialog(
                                      description: dataCannotChangeAgain,
                                      title: locator<UserRole>()
                                          .getTitle(approveStatus),
                                      voidCallback: () {
                                        BlocProvider.of<
                                                    PerawatanHarianDepartemenDeckDanDepartemenEngineBloc>(
                                                context)
                                            .add(UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineEvent(
                                                widget.idKapal,
                                                locator<MyDate>()
                                                    .getDateWithInputInString(
                                                        widget.date.value),
                                                widget.idDepartemen,
                                                approveStatus));
                                      }),
                                )));
                      },
                      child: Container(
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                    itemImage4,
                                  ),
                                  fit: BoxFit.fill)),
                          child: Center(
                              child:
                                  locator<UserRole>().getIcon(approveStatus))),
                    );
                  } else {
                    return const SizedBox();
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
