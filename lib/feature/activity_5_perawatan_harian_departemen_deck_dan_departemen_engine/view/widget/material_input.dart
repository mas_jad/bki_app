import 'package:flutter/material.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/util/const.dart';
import '../../../../core/widget/button/custom_button_widget.dart';
import '../../data/model/history_perawatan_harian_departemen_deck_dan_departemen_engine.dart';

class MaterialInput extends StatefulWidget {
  const MaterialInput(
      {required this.masterMaterial,
      required this.listMaterialUsed,
      super.key});
  final List<MasterMaterial>? masterMaterial;
  final TextEditingController listMaterialUsed;

  @override
  State<MaterialInput> createState() => _MaterialInputState();
}

class _MaterialInputState extends State<MaterialInput> {
  List<String> temporaryMaterial = [];

  @override
  void initState() {
    super.initState();
    temporaryMaterial = widget.listMaterialUsed.text.split(",");
    temporaryMaterial.removeWhere(
      (element) => element == empty,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: white,
      shape: const RoundedRectangleBorder(borderRadius: cardBorderRadius),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.8,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                  borderRadius: cardBorderRadius,
                  color: secondary,
                ),
                padding: const EdgeInsets.all(20),
                child: Center(
                  child: Text(
                    "List Material",
                    style: ptSansTextStyle.copyWith(
                        color: white, fontWeight: semiBold, fontSize: 16),
                  ),
                ),
              ),
              Expanded(
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  children: [
                    ...widget.masterMaterial
                            ?.map((e) => CheckboxListTile(
                                  title: Text(
                                    e.material ?? empty,
                                    style: ptSansTextStyle.copyWith(
                                        color: black54),
                                  ),
                                  onChanged: (isChecked) {
                                    try {
                                      setState(() {
                                        if (isChecked ?? false) {
                                          if (e.material != null) {
                                            temporaryMaterial
                                                .add(e.material!.trim());
                                          }
                                        } else {
                                          temporaryMaterial
                                              .removeWhere((element) {
                                            return element.trim().contains(
                                                e.material?.trim() ?? empty);
                                          });
                                        }
                                      });
                                    } catch (e) {
                                      // DO NOTHING
                                    }
                                  },
                                  value: temporaryMaterial
                                      .toString()
                                      .contains(e.material ?? empty),
                                ))
                            .toList() ??
                        []
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 36,
                child: Row(
                  children: [
                    Expanded(
                      child: CustomButtonWidgetBorderSide(
                          callback: () {
                            Navigator.pop(context, 'Cancel');
                          },
                          title: 'Back'),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: CustomButtonWidget(
                          callback: () {
                            widget.listMaterialUsed.text = temporaryMaterial
                                .toString()
                                .replaceFirst("[", empty)
                                .replaceFirst("]", empty);

                            Navigator.pop(context, 'OK');
                          },
                          title: 'Ok'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
