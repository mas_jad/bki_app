import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/image/image_container_picker.dart';
import 'package:pms_pcm/feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/view/widget/perawatan_dan_material_input.dart';

import '../../../../core/service/date/date.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/util/locator.dart';
import '../../../../core/util/toast.dart';
import '../../../../core/widget/button/custom_button_widget.dart';
import '../../../../core/widget/textfield/custom_textfield_widget.dart';
import '../../bloc/perawatan_harian_departemen_deck_dan_departemen_engine_bloc.dart';
import '../../data/model/history_perawatan_harian_departemen_deck_dan_departemen_engine.dart';
import '../../data/model/post_perawatan_harian_departemen_deck_dan_departemen_engine.dart';

class PerawatanHarianDepartemenDeckDanDepartemenEngineInput
    extends StatefulWidget {
  const PerawatanHarianDepartemenDeckDanDepartemenEngineInput(
      {this.historyPerawatanHarianDepartemenDeckDanDepartemenEngine,
      required this.kapalId,
      required this.date,
      required this.isUserPermitedToSave,
      required this.departmenId,
      required this.isDeck,
      required this.masterBagian,
      required this.masterMaterial,
      required this.sugestion,
      super.key});
  final int kapalId;
  final int departmenId;
  final DateTime date;
  final bool isUserPermitedToSave;
  final bool isDeck;
  // Master
  final List<MasterBagian>? masterBagian;
  final List<MasterMaterial>? masterMaterial;
  final List<Sugestion>? sugestion;

  // User Data
  final HistoryPerawatanHarianDepartemenDeckDanDepartemenEngine?
      historyPerawatanHarianDepartemenDeckDanDepartemenEngine;

  @override
  State<PerawatanHarianDepartemenDeckDanDepartemenEngineInput> createState() =>
      _PerawatanHarianDepartemenDeckDanDepartemenEngineInputState();
}

class _PerawatanHarianDepartemenDeckDanDepartemenEngineInputState
    extends State<PerawatanHarianDepartemenDeckDanDepartemenEngineInput> {
  // Master
  final List<MasterBagian> listMasterBagian = [];

  // User Data
  late ValueNotifier<String?> imageValueNotifier;
  late ValueNotifier<MasterBagian?> bagianValue;
  late ValueNotifier<List<UraianPerawatan>> listUraianValue;

  @override
  void dispose() {
    imageValueNotifier.dispose();
    bagianValue.dispose();
    listUraianValue.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // MASTER
    listMasterBagian.addAll(widget.masterBagian ?? []);

    // INITIATED VALUE
    imageValueNotifier = ValueNotifier<String?>(null);
    bagianValue = ValueNotifier<MasterBagian?>(
        widget.historyPerawatanHarianDepartemenDeckDanDepartemenEngine?.bagian);
    listUraianValue = ValueNotifier<List<UraianPerawatan>>([]);
    final myList = (widget
                .historyPerawatanHarianDepartemenDeckDanDepartemenEngine
                ?.uraianPerawatan ??
            [])
        .map((e) {
      return UraianPerawatan(
          uraian: e.uraian, material: e.material ?? empty, hasil: e.hasil);
    });
    listUraianValue.value.addAll(myList);
  }

  postPerawatanHarianDepartemenDeckDanDepartemenEngine() async {
    if (bagianValue.value == null) {
      MyToast.showError("Bagian Belum Dipilih", context);
      return;
    }
    if (listUraianValue.value.isEmpty) {
      MyToast.showError("Uraian Perawatan Belum Diisi", context);
      return;
    }
    final isOld =
        widget.historyPerawatanHarianDepartemenDeckDanDepartemenEngine != null;
    final oldId = widget
        .historyPerawatanHarianDepartemenDeckDanDepartemenEngine?.bagian?.id;

    BlocProvider.of<PerawatanHarianDepartemenDeckDanDepartemenEngineBloc>(
            context)
        .add(PostPerawatanHarianDepartemenDeckDanDepartemenEngineEvent(
      PostPerawatanHarianDepartemenDeckDanDepartemenEngine(
        kapalId: widget.kapalId,
        departemenId: widget.departmenId,
        departemenBagianId: bagianValue.value?.id,
        departemenBagianIdOld: isOld ? oldId : null,
        date: locator<MyDate>().getDateWithInputInString(widget.date),
        uraianPerawatan: listUraianValue.value,
        image: imageValueNotifier.value,
      ),
    ));
  }

  List<Widget> uraianContainer() {
    final List<Widget> list = [];
    for (var i = 0; i < listUraianValue.value.length; i++) {
      list.add(GestureDetector(
        onTap: () {
          showModalBottomSheet(
              isDismissible: false,
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              isScrollControlled: true,
              context: context,
              builder: (context) {
                return Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: PerawatanDanMaterialDanInput(
                    isUserPermitedToSave: widget.isUserPermitedToSave,
                    masterMaterial: widget.masterMaterial ?? [],
                    sugestion: (widget.sugestion ?? [])
                        .map((e) => e.text ?? empty)
                        .toList(),
                    index: i,
                    listUraianPerawatan: listUraianValue,
                  ),
                );
              });
        },
        child: Container(
          padding: const EdgeInsets.all(12),
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: const BoxDecoration(
              color: grey200,
              borderRadius: BorderRadius.all(Radius.circular(14))),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  listUraianValue.value[i].uraian ?? empty,
                  style: ptSansTextStyle.copyWith(color: black54, fontSize: 14),
                ),
                listUraianValue.value[i].material?.isNotEmpty ?? false
                    ? const SizedBox(
                        height: 12,
                      )
                    : const SizedBox(),
                listUraianValue.value[i].material?.isNotEmpty ?? false
                    ? Text(
                        "Material : ${(listUraianValue.value[i].material ?? empty)}",
                        style: ptSansTextStyle.copyWith(
                            color: black54, fontSize: 12),
                      )
                    : const SizedBox(),
                listUraianValue.value[i].hasil?.isNotEmpty ?? false
                    ? const SizedBox(
                        height: 10,
                      )
                    : const SizedBox(),
                listUraianValue.value[i].hasil?.isNotEmpty ?? false
                    ? Text(
                        "Hasil      : ${(listUraianValue.value[i].hasil ?? empty)}",
                        style: ptSansTextStyle.copyWith(
                            color: black54, fontSize: 12),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ),
      ));
    }
    if (widget.isUserPermitedToSave) {
      list.add(Container(
        padding: const EdgeInsets.all(6),
        margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
        decoration: const BoxDecoration(
            color: grey200,
            borderRadius: BorderRadius.all(Radius.circular(14))),
        child: Center(
          child: IconButton(
              onPressed: () {
                showModalBottomSheet(
                    isDismissible: false,
                    enableDrag: false,
                    shape: const RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(20))),
                    isScrollControlled: true,
                    context: context,
                    builder: (context) {
                      return Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: PerawatanDanMaterialDanInput(
                          isUserPermitedToSave: widget.isUserPermitedToSave,
                          masterMaterial: widget.masterMaterial ?? [],
                          sugestion: (widget.sugestion ?? [])
                              .map((e) => e.text ?? empty)
                              .toList(),
                          index: null,
                          listUraianPerawatan: listUraianValue,
                        ),
                      );
                    });
              },
              icon: iconAdd.copyWith(color: black54)),
        ),
      ));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PerawatanHarianDepartemenDeckDanDepartemenEngineBloc,
        PerawatanHarianDepartemenDeckDanDepartemenEngineState>(
      listener: (context, state) {
        if (state
            is PostPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess) {
          MyToast.showSuccess(success, context);
          Navigator.pop(context);
        } else if (state
            is PostPerawatanHarianDepartemenDeckDanDepartemenEngineError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Text(
                          "Perawatan Harian",
                          style: ptSansTextStyle.copyWith(
                              color: white, fontWeight: bold, fontSize: 22),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8),
                            child: iconArrow.copyWith(color: transparent)),
                      ],
                    ),
                    Text(
                      widget.isDeck ? "Deck" : "Engine",
                      style: ptSansTextStyle.copyWith(
                          color: white, fontWeight: medium, fontSize: 18),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Center(
                          child: ImageContainerPicker(
                        image: widget
                            .historyPerawatanHarianDepartemenDeckDanDepartemenEngine
                            ?.image,
                        imageValueNotifier: imageValueNotifier,
                      )),
                    ),
                  ],
                ),
              ),
              // Master Bagian
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Autocomplete<MasterBagian>(
                      initialValue: TextEditingValue(
                        text: bagianValue.value?.bagian ?? empty,
                      ),
                      optionsBuilder: (TextEditingValue textEditingValue) {
                        if (textEditingValue.text == empty) {
                          return listMasterBagian;
                        }

                        return listMasterBagian.where((MasterBagian option) {
                          return (option.bagian?.toLowerCase() ?? empty)
                              .contains(
                            textEditingValue.text.toLowerCase(),
                          );
                        });
                      },
                      optionsViewBuilder: (
                        BuildContext context,
                        AutocompleteOnSelected<MasterBagian> onSelected,
                        Iterable<MasterBagian> options,
                      ) {
                        return Align(
                          alignment: Alignment.topLeft,
                          child: Material(
                            child: Container(
                              height: 60.0 * options.length,
                              decoration: const BoxDecoration(
                                color: white,
                                boxShadow: [
                                  shadowCard,
                                ],
                              ),
                              child: ListView.builder(
                                padding: const EdgeInsets.all(10.0),
                                itemCount: options.length,
                                itemBuilder: (
                                  BuildContext context,
                                  int index,
                                ) {
                                  final MasterBagian option =
                                      options.elementAt(index);
                                  return GestureDetector(
                                    onTap: () {
                                      onSelected(option);
                                    },
                                    child: ListTile(
                                      title: Text(
                                        option.bagian ?? empty,
                                        style: ptSansTextStyle.copyWith(
                                          color: black68,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                      fieldViewBuilder: (
                        BuildContext context,
                        TextEditingController fieldTextEditingController,
                        FocusNode fieldFocusNode,
                        VoidCallback onFieldSubmitted,
                      ) {
                        return CustomTextFieldWidget(
                          label: "Nama Bagian",
                          focusNode: fieldFocusNode,
                          textEditingController: fieldTextEditingController,
                          textInputType: TextInputType.text,
                        );
                      },
                      onSelected: (MasterBagian selection) {
                        bagianValue.value = selection;
                      },
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    Text(
                      "Uraian Perawatan",
                      style: ptSansTextStyle.copyWith(color: primary),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: white,
                          border: Border.all(color: black26, width: 0.5),
                          borderRadius: cardBorderRadius),
                      child: Stack(
                        children: [
                          ValueListenableBuilder(
                              valueListenable: listUraianValue,
                              builder: (context, value, child) {
                                return Column(
                                  children: [
                                    ...uraianContainer(),
                                    const SizedBox(
                                      height: 12,
                                    )
                                  ],
                                );
                              }),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    if (widget.isUserPermitedToSave)
                      BlocBuilder<
                          PerawatanHarianDepartemenDeckDanDepartemenEngineBloc,
                          PerawatanHarianDepartemenDeckDanDepartemenEngineState>(
                        builder: (context, state) {
                          return CustomButtonWidget(
                              isActive: state
                                      is PostPerawatanHarianDepartemenDeckDanDepartemenEngineLoading
                                  ? false
                                  : true,
                              callback: () {
                                postPerawatanHarianDepartemenDeckDanDepartemenEngine();
                              },
                              title: save);
                        },
                      ),
                    const SizedBox(
                      height: 200,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
