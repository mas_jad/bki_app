import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/util/user_role.dart';
import 'package:pms_pcm/feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/bloc/perawatan_harian_departemen_deck_dan_departemen_engine_bloc.dart';
import 'package:pms_pcm/feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/view/widget/perawatan_harian_departemen_deck_dan_departemen_engine_widget.dart';

import '../../../core/data/repository/user_repository.dart';
import '../../../core/service/date/date.dart';
import '../../../core/util/locator.dart';
import '../data/repository/perawatan_harian_departemen_deck_dan_departemen_engine_repository.dart';

class PerawatanHarianDepartemenDeckDanDepartemenEnginePage
    extends StatefulWidget {
  const PerawatanHarianDepartemenDeckDanDepartemenEnginePage(
      {required this.dataKapal, super.key});
  static const route =
      "/activity_perawatan_harian_departemen_deck_dan_departemen_engine_page";
  final DataKapal dataKapal;

  @override
  State<PerawatanHarianDepartemenDeckDanDepartemenEnginePage> createState() =>
      _PerawatanHarianDepartemenDeckDanDepartemenEnginePageState();
}

class _PerawatanHarianDepartemenDeckDanDepartemenEnginePageState
    extends State<PerawatanHarianDepartemenDeckDanDepartemenEnginePage>
    with SingleTickerProviderStateMixin {
  int? idDeck;
  int? idEngine;
  late ValueNotifier<DateTime> deckDate;
  late ValueNotifier<DateTime> engineDate;

  final deckBloc = PerawatanHarianDepartemenDeckDanDepartemenEngineBloc(
      locator<PerawatanHarianDepartemenDeckDanDepartemenEngineRepository>(),
      locator<UserRepository>());

  final engineBloc = PerawatanHarianDepartemenDeckDanDepartemenEngineBloc(
      locator<PerawatanHarianDepartemenDeckDanDepartemenEngineRepository>(),
      locator<UserRepository>());

  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    // DEPARTEMEN
    widget.dataKapal.departemen?.forEach((element) {
      if (element.nama == ENGINE || element.nama == ENGINE2) {
        idEngine = element.id;
      }
      if (element.nama == DECK) {
        idDeck = element.id;
      }
    });
    final initialIndex = locator<UserRole>().isEngine() ? 1 : 0;
    _tabController =
        TabController(length: 2, vsync: this, initialIndex: initialIndex);

    // GET DATE
    deckDate = ValueNotifier(locator<MyDate>().getDateToday());
    engineDate = ValueNotifier(locator<MyDate>().getDateToday());

    // ADD EVENT
    deckBloc.add(
        GetHistoryPerawatanHarianDepartemenDeckDanDepartemenEngineEvent(
            widget.dataKapal.idKapal ?? -1,
            locator<MyDate>().getDateTodayInString(),
            idDeck ?? -1));
    engineBloc.add(
        GetHistoryPerawatanHarianDepartemenDeckDanDepartemenEngineEvent(
            widget.dataKapal.idKapal ?? -1,
            locator<MyDate>().getDateTodayInString(),
            idEngine ?? -1));
  }

  @override
  void dispose() {
    deckBloc.close();
    engineBloc.close();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: secondary,
          bottom: TabBar(
            controller: _tabController,
            indicatorWeight: 2.5,
            indicatorColor: primaryAccent,
            tabs: [
              Tab(
                child: Text(
                  "Deck",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
              Tab(
                child: Text(
                  "Engine",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
            ],
          ),
          elevation: 0,
          title: Text(
            "Perawatan Harian",
            style: ptSansTextStyle.copyWith(color: white),
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            BlocProvider.value(
              value: deckBloc,
              child: PerawatanHarianDepartemenDeckDanDepartemenEngineWidget(
                idDepartemen: idDeck ?? -1,
                idKapal: widget.dataKapal.idKapal ?? -1,
                date: deckDate,
                isDeck: true,
              ),
            ),
            BlocProvider.value(
              value: engineBloc,
              child: PerawatanHarianDepartemenDeckDanDepartemenEngineWidget(
                idDepartemen: idEngine ?? -1,
                idKapal: widget.dataKapal.idKapal ?? -1,
                date: engineDate,
                isDeck: false,
              ),
            )
          ],
        ),
      ),
    );
  }
}
