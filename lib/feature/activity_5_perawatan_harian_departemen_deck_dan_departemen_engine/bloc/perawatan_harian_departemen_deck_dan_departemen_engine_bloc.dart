import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_perawatan_harian_departemen_deck_dan_departemen_engine.dart';
import '../data/model/post_perawatan_harian_departemen_deck_dan_departemen_engine.dart';
import '../data/repository/perawatan_harian_departemen_deck_dan_departemen_engine_repository.dart';

part 'perawatan_harian_departemen_deck_dan_departemen_engine_event.dart';
part 'perawatan_harian_departemen_deck_dan_departemen_engine_state.dart';

class PerawatanHarianDepartemenDeckDanDepartemenEngineBloc extends Bloc<
    PerawatanHarianDepartemenDeckDanDepartemenEngineEvent,
    PerawatanHarianDepartemenDeckDanDepartemenEngineState> {
  final PerawatanHarianDepartemenDeckDanDepartemenEngineRepository
      _perawatanHarianDepartemenDeckDanDepartemenEngineRepository;
  final UserRepository _userRepository;

  PerawatanHarianDepartemenDeckDanDepartemenEngineBloc(
      this._perawatanHarianDepartemenDeckDanDepartemenEngineRepository,
      this._userRepository)
      : super(PerawatanHarianDepartemenDeckDanDepartemenEngineInitial()) {
    on<PostPerawatanHarianDepartemenDeckDanDepartemenEngineEvent>(
        (event, emit) async {
      try {
        emit(PostPerawatanHarianDepartemenDeckDanDepartemenEngineLoading());
        final token = await _userRepository.getToken();
        final data =
            await _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
                .postPerawatanHarianDepartemenDeckDanDepartemenEngine(
          token,
          event.postPerawatanHarianDepartemenDeckDanDepartemenEngine,
        );

        emit(PostPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess());
        emit(HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess(
          data,
          _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
              .getRevisedDate(),
          _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
              .getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostPerawatanHarianDepartemenDeckDanDepartemenEngineError(
              e.message));
        } else {
          emit(PostPerawatanHarianDepartemenDeckDanDepartemenEngineError(
              somethingErrorHappen));
        }
      }
    });

    on<GetHistoryPerawatanHarianDepartemenDeckDanDepartemenEngineEvent>(
        (event, emit) async {
      try {
        emit(HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineLoading());
        final token = await _userRepository.getToken();
        final data =
            await _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
                .getHistoryPerawatanHarianDepartemenDeckDanDepartemenEngine(
                    event.id, token, event.date, event.departmenId);

        emit(HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess(
          data,
          _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
              .getRevisedDate(),
          _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
              .getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineError(
              e.message));
        } else {
          emit(HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineError(
              somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineEvent>(
        (event, emit) async {
      try {
        emit(
            UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineLoading());
        final token = await _userRepository.getToken();
        final data =
            await _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
                .updateStatusPerawatanHarianDepartemenDeckDanDepartemenEngine(
                    event.id,
                    token,
                    event.date,
                    event.departmenId,
                    event.approve);

        emit(
            UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess());
        emit(HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess(
          data,
          _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
              .getRevisedDate(),
          _perawatanHarianDepartemenDeckDanDepartemenEngineRepository
              .getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(
              UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineError(
                  e.message));
        } else {
          emit(
              UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineError(
                  somethingErrorHappen));
        }
      }
    });
  }
}
