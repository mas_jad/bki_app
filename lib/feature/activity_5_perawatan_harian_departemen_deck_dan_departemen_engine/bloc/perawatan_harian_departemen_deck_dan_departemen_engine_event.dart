part of 'perawatan_harian_departemen_deck_dan_departemen_engine_bloc.dart';

abstract class PerawatanHarianDepartemenDeckDanDepartemenEngineEvent
    extends Equatable {
  const PerawatanHarianDepartemenDeckDanDepartemenEngineEvent();

  @override
  List<Object> get props => [];
}

class PostPerawatanHarianDepartemenDeckDanDepartemenEngineEvent
    extends PerawatanHarianDepartemenDeckDanDepartemenEngineEvent {
  final PostPerawatanHarianDepartemenDeckDanDepartemenEngine
      postPerawatanHarianDepartemenDeckDanDepartemenEngine;
  const PostPerawatanHarianDepartemenDeckDanDepartemenEngineEvent(
      this.postPerawatanHarianDepartemenDeckDanDepartemenEngine);
}

class UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineEvent
    extends PerawatanHarianDepartemenDeckDanDepartemenEngineEvent {
  final int id;
  final String date;
  final Approve approve;
  final int departmenId;
  const UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineEvent(
      this.id, this.date, this.departmenId, this.approve);
}

class GetHistoryPerawatanHarianDepartemenDeckDanDepartemenEngineEvent
    extends PerawatanHarianDepartemenDeckDanDepartemenEngineEvent {
  final int id;
  final String date;
  final int departmenId;
  const GetHistoryPerawatanHarianDepartemenDeckDanDepartemenEngineEvent(
      this.id, this.date, this.departmenId);
}
