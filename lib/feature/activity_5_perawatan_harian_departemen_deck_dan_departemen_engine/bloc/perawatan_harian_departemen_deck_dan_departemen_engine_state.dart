part of 'perawatan_harian_departemen_deck_dan_departemen_engine_bloc.dart';

abstract class PerawatanHarianDepartemenDeckDanDepartemenEngineState
    extends Equatable {
  const PerawatanHarianDepartemenDeckDanDepartemenEngineState();

  @override
  List<Object> get props => [];
}

class PerawatanHarianDepartemenDeckDanDepartemenEngineInitial
    extends PerawatanHarianDepartemenDeckDanDepartemenEngineState {}

// HISTORY
abstract class HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineState
    extends PerawatanHarianDepartemenDeckDanDepartemenEngineState {}

class HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess
    extends HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineState {
  final HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse?
      historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse;
  final String? revisedDate;
  final Approve approve;
  HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess(
      this.historyPerawatanHarianDepartemenDeckDanDepartemenEngineResponse,
      this.revisedDate,
      this.approve);
}

class HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineError
    extends HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineState {
  final String message;
  HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineError(this.message);
}

class HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineLoading
    extends HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineState {}

// POST
abstract class PostPerawatanHarianDepartemenDeckDanDepartemenEngineState
    extends PerawatanHarianDepartemenDeckDanDepartemenEngineState {}

class PostPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess
    extends PostPerawatanHarianDepartemenDeckDanDepartemenEngineState {}

class PostPerawatanHarianDepartemenDeckDanDepartemenEngineError
    extends PostPerawatanHarianDepartemenDeckDanDepartemenEngineState {
  final String message;
  PostPerawatanHarianDepartemenDeckDanDepartemenEngineError(this.message);
}

class PostPerawatanHarianDepartemenDeckDanDepartemenEngineLoading
    extends PostPerawatanHarianDepartemenDeckDanDepartemenEngineState {}

// UPDATE STATUS
abstract class UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineState
    extends PerawatanHarianDepartemenDeckDanDepartemenEngineState {}

class UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineSuccess
    extends UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineState {}

class UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineError
    extends UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineState {
  final String message;
  UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineError(
      this.message);
}

class UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineLoading
    extends UpdateStatusPerawatanHarianDepartemenDeckDanDepartemenEngineState {}
