import '../../../../core/data/model/approve.dart';
import '../../../../core/util/request.dart';
import '../model/history_perawatan_harian_departemen_deck_dan_departemen_engine.dart';
import '../model/post_perawatan_harian_departemen_deck_dan_departemen_engine.dart';
import '../source/perawatan_harian_departemen_deck_dan_departemen_engine_remote_data_source.dart';

class PerawatanHarianDepartemenDeckDanDepartemenEngineRepository {
  final PerawatanHarianDepartemenDeckDanDepartemenEngineRemoteDataSource
      _perawatanHarianDepartemenDeckDanDepartemenEngineRemoteDataSource;
  PerawatanHarianDepartemenDeckDanDepartemenEngineRepository(
      this._perawatanHarianDepartemenDeckDanDepartemenEngineRemoteDataSource);

  String? revisedDate;

  Approve? approve;

  String? getRevisedDate() {
    return revisedDate;
  }

  Approve getApprove() {
    return approve ??
        Approve(
            approved1: null, approved2: null, approved3: null, approved4: null);
  }

  Future<HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse?>
      getHistoryPerawatanHarianDepartemenDeckDanDepartemenEngine(
          int id, String token, String date, int departementId) async {
    return RequestServer<
            HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse>()
        .requestServer(computation: () async {
      final response =
          await _perawatanHarianDepartemenDeckDanDepartemenEngineRemoteDataSource
              .getHistoryPerawatanHarianDepartemenDeckDanDepartemenEngine(
                  id, token, date, departementId);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return response;
    });
  }

  Future<HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse?>
      postPerawatanHarianDepartemenDeckDanDepartemenEngine(
    String token,
    PostPerawatanHarianDepartemenDeckDanDepartemenEngine
        perawatanHarianDepartemenDeckDanDepartemenEngine,
  ) async {
    return RequestServer<
            HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse?>()
        .requestServer(computation: () async {
      final response =
          await _perawatanHarianDepartemenDeckDanDepartemenEngineRemoteDataSource
              .postPerawatanHarianDepartemenDeckDanDepartemenEngine(
        token,
        perawatanHarianDepartemenDeckDanDepartemenEngine,
      );
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return response;
    });
  }

  Future<HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse?>
      updateStatusPerawatanHarianDepartemenDeckDanDepartemenEngine(
          int id,
          String token,
          String date,
          int departementId,
          Approve approved) async {
    return RequestServer<
            HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse?>()
        .requestServer(computation: () async {
      final response =
          await _perawatanHarianDepartemenDeckDanDepartemenEngineRemoteDataSource
              .updateStatusPerawatanHarianDepartemenDeckDanDepartemenEngine(
                  id, token, date, departementId, approved);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return response;
    });
  }
}
