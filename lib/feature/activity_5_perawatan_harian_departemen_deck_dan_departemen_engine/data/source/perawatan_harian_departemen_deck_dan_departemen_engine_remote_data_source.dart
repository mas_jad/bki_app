import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/service/location/location_service.dart';
import '../../../../core/util/user_role.dart';
import '../model/history_perawatan_harian_departemen_deck_dan_departemen_engine.dart';
import '../model/post_perawatan_harian_departemen_deck_dan_departemen_engine.dart';

class PerawatanHarianDepartemenDeckDanDepartemenEngineRemoteDataSource {
  Future<HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse>
      getHistoryPerawatanHarianDepartemenDeckDanDepartemenEngine(
          int id, String token, String date, int departementId) async {
    final endpoint = dotenv.get(dataPHDPerTanggalEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {"kapal_id": id, "date": date, "departemen_id": departementId},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
        .fromJson(response.data);
  }

  Future<HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse>
      postPerawatanHarianDepartemenDeckDanDepartemenEngine(
    String token,
    PostPerawatanHarianDepartemenDeckDanDepartemenEngine
        perawatanHarianDepartemenDeckDanDepartemenEngine,
  ) async {
    final endpoint = dotenv.get(perawatanHarianDepartemenDeckDanDepartemenEngine
                .departemenBagianIdOld ==
            null
        ? simpanDataPHDEndpoint
        : updateDataPHDEndpoint);
    final position = await locator<LocationService>().getLastPosition();
    final data =
        await perawatanHarianDepartemenDeckDanDepartemenEngine.toJson(position);
    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
        .fromJson(response.data);
  }

  Future<HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse>
      updateStatusPerawatanHarianDepartemenDeckDanDepartemenEngine(int id,
          String token, String date, int departementId, Approve approve) async {
    final endpoint = dotenv.get(ubahStatusPHDEndpoint);
    final response = await locator<Dio>().post(
      "$endpoint${locator<UserRole>().getUpdateStatusURL(false, approve)}",
      data: {"kapal_id": id, "date": date, "departemen_id": departementId},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse
        .fromJson(response.data);
  }
}
