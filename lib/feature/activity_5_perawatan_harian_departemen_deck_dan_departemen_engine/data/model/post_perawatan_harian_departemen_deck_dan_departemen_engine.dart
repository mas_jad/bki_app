import 'package:dio/dio.dart';

import 'history_perawatan_harian_departemen_deck_dan_departemen_engine.dart';

class PostPerawatanHarianDepartemenDeckDanDepartemenEngine {
  int? kapalId;
  int? departemenId;
  String? date;
  int? departemenBagianId;
  int? departemenBagianIdOld;
  List<UraianPerawatan>? uraianPerawatan;
  String? image;
  String? location;

  PostPerawatanHarianDepartemenDeckDanDepartemenEngine({
    this.kapalId,
    this.departemenId,
    this.date,
    this.departemenBagianId,
    this.uraianPerawatan,
    this.departemenBagianIdOld,
    this.image,
  });

  Future<FormData> toJson(String? location) async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kapal_id'] = kapalId;
    data['departemen_id'] = departemenId;
    data['date'] = date;
    data['location'] = location;
    data['departemen_bagian_id'] = departemenBagianId;
    if (uraianPerawatan != null) {
      data['uraian_perawatan'] =
          uraianPerawatan!.map((v) => v.toJson()).toList();
    }
    data['departemen_bagian_id_old'] = departemenBagianIdOld;

    data['image'] = image != null
        ? await MultipartFile.fromFile(
            image!,
            filename: image!.split('/').last,
          )
        : null;
    return FormData.fromMap(data);
  }
}
