import 'package:pms_pcm/core/util/const.dart';

class HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse {
  String? message;
  int? code;
  String? tanggalMundur;
  List<HistoryPerawatanHarianDepartemenDeckDanDepartemenEngine>? data;
  List<Sugestion>? sugestion;
  List<MasterBagian>? masterBagian;
  List<MasterMaterial>? masterMaterial;
  String? approved1;
  String? approved2;
  String? approved3;
  String? approved4;

  HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse(
      {this.message,
      this.code,
      this.tanggalMundur,
      this.data,
      this.sugestion,
      this.masterBagian,
      this.masterMaterial,
      this.approved1,
      this.approved2,
      this.approved3,
      this.approved4});

  HistoryPerawatanHarianDepartemenDeckDanDepartemenEngineResponse.fromJson(
      Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    tanggalMundur = json['tanggal_mundur'];
    approved1 = json['approved_1'];
    approved2 = json['approved_2'];
    approved3 = json['approved_3'];
    approved4 = json['approved_4'];
    if (json['data'] != null) {
      data = <HistoryPerawatanHarianDepartemenDeckDanDepartemenEngine>[];
      json['data'].forEach((v) {
        data!.add(
            HistoryPerawatanHarianDepartemenDeckDanDepartemenEngine.fromJson(
                v));
      });
    }
    if (json['sugestion'] != null) {
      sugestion = <Sugestion>[];
      json['sugestion'].forEach((v) {
        sugestion!.add(Sugestion.fromJson(v));
      });
    }
    if (json['master_bagian'] != null) {
      masterBagian = <MasterBagian>[];
      json['master_bagian'].forEach((v) {
        masterBagian!.add(MasterBagian.fromJson(v));
      });
    }
    if (json['master_material'] != null) {
      masterMaterial = <MasterMaterial>[];
      json['master_material'].forEach((v) {
        masterMaterial!.add(MasterMaterial.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    data['tanggal_mundur'] = tanggalMundur;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    if (sugestion != null) {
      data['sugestion'] = sugestion!.map((v) => v.toJson()).toList();
    }
    if (masterBagian != null) {
      data['master_bagian'] = masterBagian!.map((v) => v.toJson()).toList();
    }
    if (masterMaterial != null) {
      data['master_material'] = masterMaterial!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HistoryPerawatanHarianDepartemenDeckDanDepartemenEngine {
  int? id;
  String? statusProses;
  String? date;
  List<UraianPerawatan>? uraianPerawatan;
  MasterBagian? bagian;
  String? image;

  HistoryPerawatanHarianDepartemenDeckDanDepartemenEngine(
      {this.id,
      this.statusProses,
      this.date,
      this.uraianPerawatan,
      this.bagian,
      this.image});

  HistoryPerawatanHarianDepartemenDeckDanDepartemenEngine.fromJson(
      Map<String, dynamic> json) {
    id = json['id'];
    statusProses = json['status_proses'];
    date = json['date'];
    if (json['uraian_perawatan'] != null) {
      uraianPerawatan = <UraianPerawatan>[];
      json['uraian_perawatan'].forEach((v) {
        uraianPerawatan!.add(UraianPerawatan.fromJson(v));
      });
    }
    bagian =
        json['bagian'] != null ? MasterBagian.fromJson(json['bagian']) : null;
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['status_proses'] = statusProses;
    data['date'] = date;
    if (uraianPerawatan != null) {
      data['uraian_perawatan'] =
          uraianPerawatan!.map((v) => v.toJson()).toList();
    }
    if (bagian != null) {
      data['bagian'] = bagian!.toJson();
    }
    data['image'] = image;
    return data;
  }
}

class UraianPerawatan {
  String? uraian;
  String? material;
  String? hasil;

  UraianPerawatan({this.uraian, this.material, this.hasil});

  UraianPerawatan.fromJson(Map<String, dynamic> json) {
    uraian = json['uraian'];
    material = json['material'];
    hasil = json['hasil'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['uraian'] = uraian;
    data['material'] = material;
    data['hasil'] = hasil;
    return data;
  }
}

class MasterBagian {
  int? id;
  String? bagian;

  MasterBagian({this.id, this.bagian});

  @override
  String toString() {
    return bagian ?? empty;
  }

  MasterBagian.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bagian = json['bagian'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['bagian'] = bagian;
    return data;
  }
}

class Sugestion {
  String? text;

  Sugestion({this.text});

  Sugestion.fromJson(Map<String, dynamic> json) {
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['text'] = text;
    return data;
  }
}

class MasterMaterial {
  int? id;
  String? material;

  MasterMaterial({this.id, this.material});

  MasterMaterial.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    material = json['material'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['material'] = material;
    return data;
  }
}
