import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/data/model/post_terima_barang.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_permintaan_barang_dan_jasa.dart';
import '../data/model/post_permintaan_barang_dan_jasa.dart';
import '../data/repository/permintaan_barang_dan_jasa_repository.dart';

part 'permintaan_barang_dan_jasa_event.dart';
part 'permintaan_barang_dan_jasa_state.dart';

class PermintaanBarangDanJasaBloc
    extends Bloc<PermintaanBarangDanJasaEvent, PermintaanBarangDanJasaState> {
  final PermintaanBarangDanJasaRepository _permintaanBarangDanJasaRepository;
  final UserRepository _userRepository;

  PermintaanBarangDanJasaBloc(
      this._permintaanBarangDanJasaRepository, this._userRepository)
      : super(PermintaanBarangDanJasaInitial()) {
    on<PostPermintaanBarangDanJasaEvent>((event, emit) async {
      try {
        emit(PostPermintaanBarangDanJasaLoading());
        final token = await _userRepository.getToken();
        final data = await _permintaanBarangDanJasaRepository
            .postPermintaanBarangDanJasa(
                token, event.postPermintaanBarangDanJasa);
        emit(PostPermintaanBarangDanJasaSuccess());
        emit(HistoryPermintaanBarangDanJasaSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostPermintaanBarangDanJasaError(e.message));
        } else {
          emit(PostPermintaanBarangDanJasaError(somethingErrorHappen));
        }
      }
    });

    on<GetHistoryPermintaanBarangDanJasaEvent>((event, emit) async {
      try {
        emit(HistoryPermintaanBarangDanJasaLoading());
        final token = await _userRepository.getToken();
        final data = await _permintaanBarangDanJasaRepository
            .getHistoryPermintaanBarangDanJasa(
                event.id, token, event.date, event.departemenId);
        emit(HistoryPermintaanBarangDanJasaSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryPermintaanBarangDanJasaError(e.message));
        } else {
          emit(HistoryPermintaanBarangDanJasaError(somethingErrorHappen));
        }
      }
    });

    on<GetAllHistoryPermintaanBarangDanJasaEvent>((event, emit) async {
      try {
        emit(HistoryPermintaanBarangDanJasaLoading());
        final token = await _userRepository.getToken();
        final data = await _permintaanBarangDanJasaRepository
            .getAllHistoryPermintaanBarangDanJasa(event.id, token,
                event.dateFrom, event.dateTo, event.departemenId, event.search);
        emit(HistoryPermintaanBarangDanJasaSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryPermintaanBarangDanJasaError(e.message));
        } else {
          emit(HistoryPermintaanBarangDanJasaError(somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusPermintaanBarangDanJasaEvent>((event, emit) async {
      try {
        emit(UpdateStatusPermintaanBarangDanJasaLoading());
        final token = await _userRepository.getToken();
        final data = await _permintaanBarangDanJasaRepository
            .updateStatusPermintaanBarangDanJasa(event.id, token, event.date,
                event.departemenId, event.noPbj, event.approve);

        emit(UpdateStatusPermintaanBarangDanJasaSuccess());
        emit(HistoryPermintaanBarangDanJasaSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(UpdateStatusPermintaanBarangDanJasaError(e.message));
        } else {
          emit(UpdateStatusPermintaanBarangDanJasaError(somethingErrorHappen));
        }
      }
    });

    on<KirimBarangEvent>((event, emit) async {
      try {
        emit(KirimBarangLoading());
        final token = await _userRepository.getToken();
        final data = await _permintaanBarangDanJasaRepository.kirimBarang(
            event.id, token, event.date, event.departemenId, event.noPbj);

        emit(KirimBarangSuccess());
        emit(HistoryPermintaanBarangDanJasaSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(KirimBarangError(e.message));
        } else {
          emit(KirimBarangError(somethingErrorHappen));
        }
      }
    });

    on<TerimaBarangEvent>((event, emit) async {
      try {
        emit(TerimaBarangLoading());
        final token = await _userRepository.getToken();
        final data = await _permintaanBarangDanJasaRepository.terimaBarang(
            event.postTerimaBarang, token);

        emit(TerimaBarangSuccess());
        emit(HistoryPermintaanBarangDanJasaSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(TerimaBarangError(e.message));
        } else {
          emit(TerimaBarangError(somethingErrorHappen));
        }
      }
    });
  }
}
