part of 'permintaan_barang_dan_jasa_bloc.dart';

abstract class PermintaanBarangDanJasaState extends Equatable {
  const PermintaanBarangDanJasaState();

  @override
  List<Object> get props => [];
}

class PermintaanBarangDanJasaInitial extends PermintaanBarangDanJasaState {}

// HISTORY
abstract class HistoryPermintaanBarangDanJasaState
    extends PermintaanBarangDanJasaState {}

class HistoryPermintaanBarangDanJasaSuccess
    extends HistoryPermintaanBarangDanJasaState {
  final HistoryPermintaanBarangDanJasaResponse
      historyPermintaanBarangDanJasaResponse;
  HistoryPermintaanBarangDanJasaSuccess(
      this.historyPermintaanBarangDanJasaResponse);
}

class HistoryPermintaanBarangDanJasaError
    extends HistoryPermintaanBarangDanJasaState {
  final String message;
  HistoryPermintaanBarangDanJasaError(this.message);
}

class HistoryPermintaanBarangDanJasaLoading
    extends HistoryPermintaanBarangDanJasaState {}

// POST
abstract class PostPermintaanBarangDanJasaState
    extends PermintaanBarangDanJasaState {}

class PostPermintaanBarangDanJasaSuccess
    extends PostPermintaanBarangDanJasaState {}

class PostPermintaanBarangDanJasaError
    extends PostPermintaanBarangDanJasaState {
  final String message;
  PostPermintaanBarangDanJasaError(this.message);
}

class PostPermintaanBarangDanJasaLoading
    extends PostPermintaanBarangDanJasaState {}

// UPDATE STATUS
abstract class UpdateStatusPermintaanBarangDanJasaState
    extends PermintaanBarangDanJasaState {}

class UpdateStatusPermintaanBarangDanJasaSuccess
    extends UpdateStatusPermintaanBarangDanJasaState {}

class UpdateStatusPermintaanBarangDanJasaError
    extends UpdateStatusPermintaanBarangDanJasaState {
  final String message;
  UpdateStatusPermintaanBarangDanJasaError(this.message);
}

class UpdateStatusPermintaanBarangDanJasaLoading
    extends UpdateStatusPermintaanBarangDanJasaState {}

// KIRIM BARANG
abstract class KirimBarangState extends PermintaanBarangDanJasaState {}

class KirimBarangSuccess extends KirimBarangState {}

class KirimBarangError extends KirimBarangState {
  final String message;
  KirimBarangError(this.message);
}

class KirimBarangLoading extends KirimBarangState {}

// TERIMA BARANG
abstract class TerimaBarangState extends PermintaanBarangDanJasaState {}

class TerimaBarangSuccess extends KirimBarangState {}

class TerimaBarangError extends KirimBarangState {
  final String message;
  TerimaBarangError(this.message);
}

class TerimaBarangLoading extends KirimBarangState {}
