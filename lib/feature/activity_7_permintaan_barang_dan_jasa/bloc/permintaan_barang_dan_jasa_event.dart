part of 'permintaan_barang_dan_jasa_bloc.dart';

abstract class PermintaanBarangDanJasaEvent extends Equatable {
  const PermintaanBarangDanJasaEvent();

  @override
  List<Object> get props => [];
}

class PostPermintaanBarangDanJasaEvent extends PermintaanBarangDanJasaEvent {
  final PostPermintaanBarangDanJasa postPermintaanBarangDanJasa;
  const PostPermintaanBarangDanJasaEvent(this.postPermintaanBarangDanJasa);
}

class UpdateStatusPermintaanBarangDanJasaEvent
    extends PermintaanBarangDanJasaEvent {
  final int id;
  final String date;
  final int departemenId;
  final String noPbj;
  final Approve approve;
  const UpdateStatusPermintaanBarangDanJasaEvent(
      this.id, this.date, this.departemenId, this.noPbj, this.approve);
}

class KirimBarangEvent extends PermintaanBarangDanJasaEvent {
  final int id;
  final String date;
  final int departemenId;
  final String noPbj;
  const KirimBarangEvent(this.id, this.date, this.departemenId, this.noPbj);
}

class TerimaBarangEvent extends PermintaanBarangDanJasaEvent {
  final PostTerimaBarang postTerimaBarang;
  const TerimaBarangEvent(this.postTerimaBarang);
}

class GetHistoryPermintaanBarangDanJasaEvent
    extends PermintaanBarangDanJasaEvent {
  final int id;
  final String date;
  final int departemenId;
  const GetHistoryPermintaanBarangDanJasaEvent(
      this.id, this.date, this.departemenId);
}

class GetAllHistoryPermintaanBarangDanJasaEvent
    extends PermintaanBarangDanJasaEvent {
  final int id;
  final String? dateFrom;
  final String? dateTo;
  final String? search;
  final int departemenId;
  const GetAllHistoryPermintaanBarangDanJasaEvent(
      this.id, this.dateFrom, this.dateTo, this.departemenId, this.search);
}
