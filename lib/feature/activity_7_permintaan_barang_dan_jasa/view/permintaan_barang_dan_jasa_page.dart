import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/a_permintaan_barang_dan_jasa_widget.dart';

import '../../../core/data/repository/user_repository.dart';
import '../../../core/service/date/date.dart';
import '../../../core/util/locator.dart';
import '../../../core/util/user_role.dart';
import '../bloc/permintaan_barang_dan_jasa_bloc.dart';
import '../data/repository/permintaan_barang_dan_jasa_repository.dart';
import 'history_permintaan_barang_dan_jasa_page.dart';

class PermintaanBarangDanJasaPage extends StatefulWidget {
  const PermintaanBarangDanJasaPage({required this.dataKapal, super.key});
  static const route = "/activity_permintaan_barang_dan_jasa_page";
  final DataKapal dataKapal;

  @override
  State<PermintaanBarangDanJasaPage> createState() =>
      _PermintaanBarangDanJasaPageState();
}

class _PermintaanBarangDanJasaPageState
    extends State<PermintaanBarangDanJasaPage>
    with SingleTickerProviderStateMixin {
  int? idDeck;
  int? idEngine;
  late ValueNotifier<DateTime> deckDate;
  late ValueNotifier<DateTime> engineDate;

  final deckBloc = PermintaanBarangDanJasaBloc(
      locator<PermintaanBarangDanJasaRepository>(), locator<UserRepository>());

  final engineBloc = PermintaanBarangDanJasaBloc(
      locator<PermintaanBarangDanJasaRepository>(), locator<UserRepository>());

  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    // DEPARTEMEN
    widget.dataKapal.departemen?.forEach((element) {
      if (element.nama == ENGINE || element.nama == ENGINE2) {
        idEngine = element.id;
      }
      if (element.nama == DECK) {
        idDeck = element.id;
      }
    });

    // GET DATE
    deckDate = ValueNotifier(locator<MyDate>().getDateToday());
    engineDate = ValueNotifier(locator<MyDate>().getDateToday());

    // ADD EVENT
    deckBloc.add(GetHistoryPermintaanBarangDanJasaEvent(
        widget.dataKapal.idKapal ?? -1,
        locator<MyDate>().getDateTodayInString(),
        idDeck ?? -1));
    engineBloc.add(GetHistoryPermintaanBarangDanJasaEvent(
        widget.dataKapal.idKapal ?? -1,
        locator<MyDate>().getDateTodayInString(),
        idEngine ?? -1));

    final initialIndex = locator<UserRole>().isEngine() ? 1 : 0;
    _tabController =
        TabController(length: 2, vsync: this, initialIndex: initialIndex);
  }

  @override
  void dispose() {
    deckBloc.close();
    engineBloc.close();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: secondary,
          bottom: TabBar(
            indicatorWeight: 2.5,
            controller: _tabController,
            indicatorColor: primaryAccent,
            tabs: [
              Tab(
                child: Text(
                  "Deck",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
              Tab(
                child: Text(
                  "Engine",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
            ],
          ),
          elevation: 0,
          title: Text(
            "Permintaan Barang Dan Jasa",
            style: ptSansTextStyle.copyWith(color: white),
          ),
          actions: [
            IconButton(
                onPressed: () async {
                  await Navigator.pushNamed(
                    context,
                    HistoryPermintaanBarangDanJasaPage.route,
                    arguments: widget.dataKapal,
                  );
                  deckBloc.add(GetHistoryPermintaanBarangDanJasaEvent(
                      widget.dataKapal.idKapal ?? -1,
                      locator<MyDate>()
                          .getDateWithInputInString(deckDate.value),
                      idDeck ?? -1));
                  engineBloc.add(GetHistoryPermintaanBarangDanJasaEvent(
                      widget.dataKapal.idKapal ?? -1,
                      locator<MyDate>()
                          .getDateWithInputInString(engineDate.value),
                      idEngine ?? -1));
                },
                icon: const Icon(
                  Icons.history,
                  color: white,
                ))
          ],
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            BlocProvider.value(
              value: deckBloc,
              child: PermintaanBarangDanJasaWidget(
                idDepartemen: idDeck ?? -1,
                idKapal: widget.dataKapal.idKapal ?? -1,
                date: deckDate,
                isDeck: true,
              ),
            ),
            BlocProvider.value(
              value: engineBloc,
              child: PermintaanBarangDanJasaWidget(
                idDepartemen: idEngine ?? -1,
                idKapal: widget.dataKapal.idKapal ?? -1,
                date: engineDate,
                isDeck: false,
              ),
            )
          ],
        ),
      ),
    );
  }
}
