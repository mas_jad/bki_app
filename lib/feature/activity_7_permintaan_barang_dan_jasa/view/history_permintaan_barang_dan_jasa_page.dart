import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/a_history_permintaan_barang_dan_jasa_widget.dart';

import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/locator.dart';
import '../../../core/util/user_role.dart';
import '../bloc/permintaan_barang_dan_jasa_bloc.dart';
import '../data/repository/permintaan_barang_dan_jasa_repository.dart';

class HistoryPermintaanBarangDanJasaPage extends StatefulWidget {
  const HistoryPermintaanBarangDanJasaPage(
      {required this.dataKapal, super.key});
  static const route = "/activity_history_permintaan_barang_dan_jasa_page";
  final DataKapal dataKapal;

  @override
  State<HistoryPermintaanBarangDanJasaPage> createState() =>
      _HistoryPermintaanBarangDanJasaPageState();
}

class _HistoryPermintaanBarangDanJasaPageState
    extends State<HistoryPermintaanBarangDanJasaPage>
    with SingleTickerProviderStateMixin {
  int? idDeck;
  int? idEngine;
  late ValueNotifier<DateTimeRange?> deckDate;
  late ValueNotifier<DateTimeRange?> engineDate;

  late ValueNotifier<String?> deckSearch;
  late ValueNotifier<String?> engineSearch;

  final deckBloc = PermintaanBarangDanJasaBloc(
      locator<PermintaanBarangDanJasaRepository>(), locator<UserRepository>());

  final engineBloc = PermintaanBarangDanJasaBloc(
      locator<PermintaanBarangDanJasaRepository>(), locator<UserRepository>());

  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    // DEPARTEMEN
    widget.dataKapal.departemen?.forEach((element) {
      if (element.nama == ENGINE || element.nama == ENGINE2) {
        idEngine = element.id;
      }
      if (element.nama == DECK) {
        idDeck = element.id;
      }
    });

    // GET DATE
    deckDate = ValueNotifier(null);
    engineDate = ValueNotifier(null);

    // GET DATE
    deckSearch = ValueNotifier(null);
    engineSearch = ValueNotifier(null);

    // ADD EVENT
    deckBloc.add(GetAllHistoryPermintaanBarangDanJasaEvent(
        widget.dataKapal.idKapal ?? -1, null, null, idDeck ?? -1, null));
    engineBloc.add(GetAllHistoryPermintaanBarangDanJasaEvent(
        widget.dataKapal.idKapal ?? -1, null, null, idEngine ?? -1, null));

    final initialIndex = locator<UserRole>().isEngine() ? 1 : 0;
    _tabController =
        TabController(length: 2, vsync: this, initialIndex: initialIndex);
  }

  @override
  void dispose() {
    deckBloc.close();
    engineBloc.close();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: secondary,
          bottom: TabBar(
            controller: _tabController,
            indicatorWeight: 2.5,
            indicatorColor: primaryAccent,
            tabs: [
              Tab(
                child: Text(
                  "Deck",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
              Tab(
                child: Text(
                  "Engine",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
            ],
          ),
          elevation: 0,
          title: Text(
            "History Permintaan Barang Dan Jasa",
            style: ptSansTextStyle.copyWith(color: white),
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            BlocProvider.value(
              value: deckBloc,
              child: HistoryPermintaanBarangDanJasaWidget(
                departemenId: idDeck ?? -1,
                kapalId: widget.dataKapal.idKapal ?? -1,
                dateRange: deckDate,
                search: deckSearch,
                isDeck: true,
              ),
            ),
            BlocProvider.value(
              value: engineBloc,
              child: HistoryPermintaanBarangDanJasaWidget(
                departemenId: idEngine ?? -1,
                kapalId: widget.dataKapal.idKapal ?? -1,
                dateRange: engineDate,
                isDeck: false,
                search: engineSearch,
              ),
            )
          ],
        ),
      ),
    );
  }
}
