import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/bloc/master_barang_dan_jasa_bloc.dart';
import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/core/data/model/master_barang_dan_jasa.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/data/model/post_terima_barang.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/permintaan_barang_dan_jasa_input_widget/bukti_penerimaan_barang_dan_jasa.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/permintaan_barang_dan_jasa_input_widget/header_permintaan_barang_dan_jasa.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/permintaan_barang_dan_jasa_input_widget/jenis_permintaan_barang_dan_jasa.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/permintaan_barang_dan_jasa_input_widget/kualifikasi_permintaan_barang_dan_jasa.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/permintaan_barang_dan_jasa_input_widget/list_permintaan_barang.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/permintaan_barang_dan_jasa_input_widget/list_permintaan_jasa.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/permintaan_barang_dan_jasa_input_widget/nomor_permintaan_barang_dan_jasa.dart';

import '../../../../core/bloc/master_barang_oli_bloc.dart';
import '../../../../core/data/model/alert_request_oli.dart';
import '../../../../core/service/date/date.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/util/enum.dart';
import '../../../../core/util/locator.dart';
import '../../../../core/util/toast.dart';
import '../../../../core/util/user_role.dart';
import '../../../../core/widget/button/custom_button_widget.dart';
import '../../../../core/widget/dialog/custom_alert_dialog.dart';
import '../../../../core/widget/other/loading_widget.dart';
import '../../bloc/permintaan_barang_dan_jasa_bloc.dart';
import '../../data/model/history_permintaan_barang_dan_jasa.dart';
import '../../data/model/post_permintaan_barang_dan_jasa.dart';

class PermintaanBarangDanJasaInput extends StatefulWidget {
  const PermintaanBarangDanJasaInput(
      {this.historyPermintaanBarangDanJasa,
      required this.kapalId,
      required this.date,
      required this.departmenId,
      required this.isDeck,
      this.dateRange,
      this.isDateToSaveNotExpired = false,
      this.parameterStok,
      this.search,
      super.key});
  final int kapalId;

  final int departmenId;
  final DateTime? date;
  final bool isDeck;
  final bool isDateToSaveNotExpired;

  // FOR UPDATE STOCK
  final ParameterStok? parameterStok;

  final ValueNotifier<DateTimeRange?>? dateRange;
  final ValueNotifier<String?>? search;

  // User Data
  final HistoryPermintaanBarangDanJasa? historyPermintaanBarangDanJasa;

  @override
  State<PermintaanBarangDanJasaInput> createState() =>
      _PermintaanBarangDanJasaInputState();
}

class _PermintaanBarangDanJasaInputState
    extends State<PermintaanBarangDanJasaInput> {
  bool isUserPermitedToApprove = false;
  bool isUserPermitedToSave = false;
  bool isUserPermitedToKirim = false;
  bool isUserPermitedToTerima = false;

  bool isBarangDanJasaSudahDiApprovePortEngginer = false;
  bool isDone = false;

  // MASTER
  MasterBarangDanJasa? masterBarangDanJasa;
  List<AlertRequestOli>? listMasterBarangOli;

  // USER DATA
  late ValueNotifier<List<Barang>> listBarangValue;
  late ValueNotifier<List<Jasa>> listJasaValue;
  late ValueNotifier<String> kualifikasiValue;
  late ValueNotifier<String?> imageValueNotifier;
  late TextEditingController catatanPenerimaTextEditingController;
  late TextEditingController namaPengirimTextEditingController;
  final ValueNotifier<ParameterStok> parameterStokValue =
      ValueNotifier(ParameterStok.lainnya);

  @override
  void dispose() {
    listBarangValue.dispose();
    listJasaValue.dispose();
    catatanPenerimaTextEditingController.dispose();
    namaPengirimTextEditingController.dispose();
    imageValueNotifier.dispose();
    kualifikasiValue.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (widget.parameterStok != null) {
      parameterStokValue.value = widget.parameterStok!;
    }
    try {
      if (widget.historyPermintaanBarangDanJasa!.barang!.first.paramStok !=
          null) {
        parameterStokValue.value = convertStringToParamterStok(
            widget.historyPermintaanBarangDanJasa!.barang!.first.paramStok);
      }
    } catch (e) {
      // DO NOTHING
    }

    final stateMasterBarangDanJasa =
        BlocProvider.of<MasterBarangDanJasaBloc>(context).state;
    masterBarangDanJasa = stateMasterBarangDanJasa is MasterBarangDanJasaSuccess
        ? stateMasterBarangDanJasa.masterBarangDanJasa
        : null;

    final stateMasterBarangOli =
        BlocProvider.of<MasterBarangOliBloc>(context).state;
    listMasterBarangOli = stateMasterBarangOli is MasterBarangOliSuccess
        ? stateMasterBarangOli.listMasterOli
        : null;

    isUserPermitedToApprove = widget.historyPermintaanBarangDanJasa == null
        ? false
        : locator<UserRole>().isUserPermitedToApprove(
            widget.historyPermintaanBarangDanJasa?.approve ?? Approve());
    isUserPermitedToSave = locator<UserRole>().isUserPermitedToSave(
        widget.historyPermintaanBarangDanJasa?.approve ?? Approve(),
        widget.isDateToSaveNotExpired);
    isUserPermitedToKirim = locator<UserRole>().isUserPermitedToKirim(
      widget.historyPermintaanBarangDanJasa?.approve ?? Approve(),
    );
    isUserPermitedToTerima = locator<UserRole>().isUserPermitedToTerima(
      widget.historyPermintaanBarangDanJasa?.approve ?? Approve(),
    );
    isBarangDanJasaSudahDiApprovePortEngginer =
        locator<UserRole>().isSudahDiApprovePortEngginer(
      widget.historyPermintaanBarangDanJasa?.approve ?? Approve(),
    );
    isDone = locator<UserRole>().isUserDoneBarang(
      widget.historyPermintaanBarangDanJasa?.approve ?? Approve(),
    );
    // INITIATE VALUE
    listBarangValue = ValueNotifier<List<Barang>>([]);
    listJasaValue = ValueNotifier<List<Jasa>>([]);
    final renewListBarang =
        (widget.historyPermintaanBarangDanJasa?.barang ?? []).map((e) {
      return Barang(
          kode: e.kode,
          kodefikasi: e.kodefikasi,
          spesifikasi: e.spesifikasi,
          namaBarang: e.namaBarang,
          satuan: e.satuan,
          jumlah: e.jumlah,
          paramStok: e.paramStok,
          tglDigunakan: e.tglDigunakan,
          jumlahApproved: e.jumlahApproved,
          idKapalItemPesawatPenggerak: e.idKapalItemPesawatPenggerak,
          keterangan: e.keterangan,
          idKapalItemTangkiAir: e.idKapalItemTangkiAir,
          idKapalItemTangkiBbm: e.idKapalItemTangkiBbm,
          isUpdateStok: e.isUpdateStok,
          isReject: e.isReject);
    });
    listBarangValue.value.addAll(renewListBarang);

    final renewListJasa =
        (widget.historyPermintaanBarangDanJasa?.jasa ?? []).map((e) {
      return Jasa(
          id: e.id,
          jasa: e.jasa,
          penyebab: e.penyebab,
          keterangan: e.keterangan);
    });
    listJasaValue.value.addAll(renewListJasa);
    kualifikasiValue = ValueNotifier(listKualifikasi.first);

    imageValueNotifier = ValueNotifier(null);
    namaPengirimTextEditingController = TextEditingController(
        text: widget.historyPermintaanBarangDanJasa?.kurir ?? empty);
    catatanPenerimaTextEditingController = TextEditingController(
        text: widget.historyPermintaanBarangDanJasa?.catatanDariPenerima ??
            empty);
  }

  postPermintaanBarangDanJasa() async {
    if (listBarangValue.value.isEmpty && listJasaValue.value.isEmpty) {
      MyToast.showError(
        "Barang atau Jasa Belum Diisi",
        context,
      );
      return;
    }
    final oldId = widget.historyPermintaanBarangDanJasa?.noPbj;
    if (widget.date != null) {
      BlocProvider.of<PermintaanBarangDanJasaBloc>(context)
          .add(PostPermintaanBarangDanJasaEvent(
        PostPermintaanBarangDanJasa(
            kapalId: widget.kapalId,
            departemenId: widget.departmenId,
            date: locator<MyDate>().getDateWithInputInString(widget.date!),
            noPbj: oldId,
            kualifikasi: kualifikasiValue.value,
            barang: listBarangValue.value,
            jasa: listJasaValue.value),
      ));
    }
  }

  postKirimBarang() async {
    final oldId = widget.historyPermintaanBarangDanJasa?.noPbj;

    BlocProvider.of<PermintaanBarangDanJasaBloc>(context).add(KirimBarangEvent(
      widget.kapalId,
      locator<MyDate>().getDateTodayInString(),
      widget.departmenId,
      oldId ?? empty,
    ));
  }

  postTerimaBarang() async {
    final oldId = widget.historyPermintaanBarangDanJasa?.noPbj;
    BlocProvider.of<PermintaanBarangDanJasaBloc>(context).add(TerimaBarangEvent(
        PostTerimaBarang(
            noPbj: oldId,
            kapalId: widget.kapalId,
            departemenId: widget.departmenId,
            date: locator<MyDate>().getDateTodayInString(),
            namaKurir: namaPengirimTextEditingController.text,
            catatanPenerima: catatanPenerimaTextEditingController.text,
            fotoBukti: imageValueNotifier.value)));
  }

  // FOR HISTORY
  getHistory() {
    if (widget.dateRange != null && widget.search != null) {
      final dateFormatedFrom = widget.dateRange?.value?.start != null
          ? locator<MyDate>()
              .getDateWithInputInString(widget.dateRange!.value!.start)
          : null;
      final dateFormatedTo = widget.dateRange?.value?.end != null
          ? locator<MyDate>()
              .getDateWithInputInString(widget.dateRange!.value!.end)
          : null;

      if (mounted) {
        BlocProvider.of<PermintaanBarangDanJasaBloc>(context).add(
            GetAllHistoryPermintaanBarangDanJasaEvent(
                widget.kapalId,
                dateFormatedFrom,
                dateFormatedTo,
                widget.departmenId,
                widget.search?.value ?? empty));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PermintaanBarangDanJasaBloc,
        PermintaanBarangDanJasaState>(
      listener: (context, state) {
        if (state is PostPermintaanBarangDanJasaSuccess) {
          MyToast.showSuccess(success, context);
          Navigator.pop(context);
        } else if (state is PostPermintaanBarangDanJasaError) {
          MyToast.showError(state.message, context);
        }
        if (state is UpdateStatusPermintaanBarangDanJasaSuccess) {
          MyToast.showSuccess(success, context);
          getHistory();
          Navigator.pop(context);
        } else if (state is UpdateStatusPermintaanBarangDanJasaError) {
          MyToast.showError(state.message, context);
        }
        if (state is KirimBarangSuccess) {
          MyToast.showSuccess(success, context);
          getHistory();
          Navigator.pop(context);
        } else if (state is KirimBarangError) {
          MyToast.showError(state.message, context);
        }

        if (state is TerimaBarangSuccess) {
          MyToast.showSuccess(success, context);
          getHistory();
          Navigator.pop(context);
        } else if (state is TerimaBarangError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: ValueListenableBuilder(
              valueListenable: parameterStokValue,
              builder: (context, value, child) {
                return Column(
                  children: [
                    // HEADER
                    HeaderPermintaanBarangDanJasa(
                        parameterStok: parameterStokValue.value),
                    // NO PBJ
                    if (widget.historyPermintaanBarangDanJasa != null)
                      NomorPermintaanBarangDanJasa(
                        historyPermintaanBarangDanJasa:
                            widget.historyPermintaanBarangDanJasa,
                        isDone: isDone,
                      ),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // KUALIFIKASI
                          KualifikasiPermintaanBarangDanJasa(
                              kualifikasiValue: kualifikasiValue),
                          const SizedBox(
                            height: 24,
                          ),
                          // JENIS PERMINTAAN
                          JenisPermintaanBarangDanJasa(
                              parameterStokValue: parameterStokValue,
                              callback: () {
                                listBarangValue.value.clear();
                                listJasaValue.value.clear();
                              }),
                          const SizedBox(
                            height: 24,
                          ),

                          // LIST BARANG
                          if (listBarangValue.value.isNotEmpty ||
                              isUserPermitedToSave)
                            Padding(
                              padding: const EdgeInsets.only(bottom: 24),
                              child: ListPermintaanBarang(
                                parameterStok: parameterStokValue.value,
                                listBarangValue: listBarangValue,
                                isBarangSudahDiApprovePortEngginer:
                                    isBarangDanJasaSudahDiApprovePortEngginer,
                                isUserPermitedToSave: isUserPermitedToSave,
                                masterBarangDanJasa: masterBarangDanJasa,
                                listMasterBarangOli: listMasterBarangOli,
                              ),
                            ),

                          // LIST JASA
                          if (parameterStokValue.value == ParameterStok.lainnya)
                            if (listJasaValue.value.isNotEmpty ||
                                isUserPermitedToSave)
                              Padding(
                                padding: const EdgeInsets.only(bottom: 16),
                                child: ListPermintaanJasa(
                                  listJasaValue: listJasaValue,
                                  isJasaSudahDiApprovePortEngginer:
                                      isBarangDanJasaSudahDiApprovePortEngginer,
                                  isUserPermitedToSave: isUserPermitedToSave,
                                  masterBarangDanJasa: masterBarangDanJasa,
                                ),
                              ),

                          // BUKTI PENERIMAAN
                          if (isUserPermitedToTerima || isDone)
                            BuktiPenerimaanBarangDanJasa(
                                isUserPermitedToTerima: isUserPermitedToTerima,
                                widget: widget,
                                imageValueNotifier: imageValueNotifier,
                                namaPengirimTextEditingController:
                                    namaPengirimTextEditingController,
                                catatanPenerimaTextEditingController:
                                    catatanPenerimaTextEditingController),
                          const SizedBox(
                            height: 32,
                          ),
                          if (isUserPermitedToTerima)
                            BlocBuilder<PermintaanBarangDanJasaBloc,
                                PermintaanBarangDanJasaState>(
                              builder: (context, state) {
                                return CustomButtonWidgetBorderSide(
                                    isActive: state is TerimaBarangLoading
                                        ? false
                                        : true,
                                    callback: () {
                                      postTerimaBarang();
                                    },
                                    title: "Terima");
                              },
                            ),
                          if (isUserPermitedToKirim)
                            BlocBuilder<PermintaanBarangDanJasaBloc,
                                PermintaanBarangDanJasaState>(
                              builder: (context, state) {
                                return CustomButtonWidgetBorderSide(
                                    isActive: state is KirimBarangLoading
                                        ? false
                                        : true,
                                    callback: () {
                                      postKirimBarang();
                                    },
                                    title: "Kirim");
                              },
                            ),
                          if (isUserPermitedToSave)
                            BlocBuilder<PermintaanBarangDanJasaBloc,
                                PermintaanBarangDanJasaState>(
                              builder: (context, state) {
                                return CustomButtonWidget(
                                    isActive: state
                                            is PostPermintaanBarangDanJasaLoading
                                        ? false
                                        : true,
                                    callback: () {
                                      postPermintaanBarangDanJasa();
                                    },
                                    title: save);
                              },
                            ),
                          const SizedBox(
                            height: 200,
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              }),
        ),
        // FLOATING ACTION BUTTON FOR UPDATE STATUS
        floatingActionButton: BlocBuilder<PermintaanBarangDanJasaBloc,
            PermintaanBarangDanJasaState>(
          builder: (context, state) {
            if (isUserPermitedToApprove) {
              if (state is UpdateStatusPermintaanBarangDanJasaLoading) {
                return FloatingActionButton(
                  elevation: elevationFAB,
                  backgroundColor: secondary,
                  onPressed: () {},
                  child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                itemImage4,
                              ),
                              fit: BoxFit.fill)),
                      child: const Center(
                          child: LoadingWidget(
                        color: white,
                      ))),
                );
              }

              return FloatingActionButton(
                elevation: elevationFAB,
                backgroundColor: primary,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: ((dialogContext) => BlocProvider.value(
                            value: BlocProvider.of<PermintaanBarangDanJasaBloc>(
                                context),
                            child: CustomAlertDialog(
                                description: dataCannotChangeAgain,
                                title: locator<UserRole>().getTitle(widget
                                        .historyPermintaanBarangDanJasa
                                        ?.approve ??
                                    Approve()),
                                voidCallback: () {
                                  BlocProvider.of<PermintaanBarangDanJasaBloc>(
                                          context)
                                      .add(UpdateStatusPermintaanBarangDanJasaEvent(
                                          widget.kapalId,
                                          locator<MyDate>()
                                              .getDateTodayInString(),
                                          widget.departmenId,
                                          widget.historyPermintaanBarangDanJasa
                                                  ?.noPbj ??
                                              empty,
                                          widget.historyPermintaanBarangDanJasa
                                                  ?.approve ??
                                              Approve()));
                                }),
                          )));
                },
                child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              itemImage4,
                            ),
                            fit: BoxFit.fill)),
                    child: Center(
                        child: locator<UserRole>().getIcon(
                            widget.historyPermintaanBarangDanJasa?.approve ??
                                Approve()))),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}
