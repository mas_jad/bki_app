import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/util/global_function.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/core/widget/other/error_layout.dart';
import 'package:pms_pcm/core/widget/other/loading_widget.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/b_permintaan_barang_dan_jasa_item.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/c_permintaan_barang_dan_jasa_input.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/util/locator.dart';
import '../../../../core/util/user_role.dart';
import '../../bloc/permintaan_barang_dan_jasa_bloc.dart';

class PermintaanBarangDanJasaWidget extends StatefulWidget {
  const PermintaanBarangDanJasaWidget(
      {required this.idDepartemen,
      required this.idKapal,
      required this.date,
      required this.isDeck,
      super.key});

  final int idKapal;
  final int idDepartemen;
  final ValueNotifier<DateTime> date;
  final bool isDeck;
  @override
  State<PermintaanBarangDanJasaWidget> createState() =>
      _PermintaanBarangDanJasaWidgetState();
}

class _PermintaanBarangDanJasaWidgetState
    extends State<PermintaanBarangDanJasaWidget> {
  bool isDateToSaveNotExpired = false;

  getHistory() async {
    final now = locator<MyDate>().getDateToday();
    final result = await showMyDatePicker(context, widget.date.value, now);
    if (result != null) {
      if (result != widget.date.value) {
        widget.date.value = result;
        refreshDate();
        final dateFormated =
            locator<MyDate>().getDateWithInputInString(widget.date.value);
        if (mounted) {
          BlocProvider.of<PermintaanBarangDanJasaBloc>(context).add(
              GetHistoryPermintaanBarangDanJasaEvent(
                  widget.idKapal, dateFormated, widget.idDepartemen));
        }
      }
    }
  }

  @override
  initState() {
    super.initState();
    refreshDate();
  }

  refreshDate() {
    // GET REVISED DATE
    final revisedDate =
        locator<MyDate>().getDateToday().subtract(const Duration(days: 3));
    if (widget.date.value.isBefore(revisedDate)) {
      isDateToSaveNotExpired = false;
    } else {
      isDateToSaveNotExpired = true;
    }
  }

  transformDataToWidget(HistoryPermintaanBarangDanJasaSuccess state) {
    final List<Widget> list = [];
    state.historyPermintaanBarangDanJasaResponse.data?.forEach((e) {
      list.add(PermintaanBarangDanJasaItem(
        date: e.date,
        approve: e.approve ?? Approve(),
        callback: (contextPermintaanBarangDanJasa) {
          Navigator.push(context, MaterialPageRoute(builder: (context2) {
            return BlocProvider.value(
                value: BlocProvider.of<PermintaanBarangDanJasaBloc>(context),
                child: PermintaanBarangDanJasaInput(
                  kapalId: widget.idKapal,
                  isDateToSaveNotExpired: isDateToSaveNotExpired,
                  date: widget.date.value,
                  departmenId: widget.idDepartemen,
                  isDeck: widget.isDeck,
                  historyPermintaanBarangDanJasa: e,
                ));
          }));
        },
        noPbj: e.noPbj ?? empty,
        kualifikasi: e.kualifikasi ?? empty,
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PermintaanBarangDanJasaBloc,
        PermintaanBarangDanJasaState>(
      listener: (context, state) {
        if (state is UpdateStatusPermintaanBarangDanJasaSuccess) {
          MyToast.showSuccess(success, context);
        } else if (state is UpdateStatusPermintaanBarangDanJasaError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: BlocBuilder<PermintaanBarangDanJasaBloc,
              PermintaanBarangDanJasaState>(
            buildWhen: (previous, current) {
              return current is HistoryPermintaanBarangDanJasaState;
            },
            builder: (context, state) {
              if (state is HistoryPermintaanBarangDanJasaSuccess) {
                final List list = transformDataToWidget(state);
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.vertical(
                              bottom: Radius.circular(12)),
                          boxShadow: [shadowCard]),
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () {
                              getHistory();
                            },
                            child: Text(
                              locator<MyDate>().getDateWithInputIndonesiaFormat(
                                  widget.date.value),
                              maxLines: 1,
                              style: ptSansTextStyle.copyWith(
                                  color: black54,
                                  fontWeight: semiBold,
                                  fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: list.isNotEmpty
                            ? ListView.builder(
                                padding: const EdgeInsets.all(20),
                                itemCount: list.length,
                                itemBuilder: (BuildContext ctx, index) {
                                  return list[index];
                                },
                              )
                            : const ErrorLayout(message: dataEmpty)),
                  ],
                );
              } else if (state is HistoryPermintaanBarangDanJasaError) {
                return ErrorLayout(message: state.message);
              }
              return const Center(child: LoadingWidget());
            },
          ),
        ),
        floatingActionButton: BlocBuilder<PermintaanBarangDanJasaBloc,
            PermintaanBarangDanJasaState>(
          buildWhen: ((previous, current) =>
              current is HistoryPermintaanBarangDanJasaState),
          builder: (context, state) {
            if (isDateToSaveNotExpired &&
                locator<UserRole>().isUserPermitedToCreate()) {
              return FloatingActionButton(
                elevation: elevationFAB,
                backgroundColor: primary,
                onPressed: () {
                  if (state is HistoryPermintaanBarangDanJasaSuccess) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context2) {
                      return BlocProvider.value(
                          value: BlocProvider.of<PermintaanBarangDanJasaBloc>(
                              context),
                          child: PermintaanBarangDanJasaInput(
                            kapalId: widget.idKapal,
                            isDateToSaveNotExpired: isDateToSaveNotExpired,
                            date: widget.date.value,
                            departmenId: widget.idDepartemen,
                            isDeck: widget.isDeck,
                          ));
                    }));
                  }
                },
                child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              itemImage4,
                            ),
                            fit: BoxFit.fill)),
                    child: const Center(child: iconAdd)),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}
