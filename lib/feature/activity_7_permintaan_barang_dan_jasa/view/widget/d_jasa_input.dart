import 'package:flutter/material.dart';
import 'package:pms_pcm/core/data/model/master_barang_dan_jasa.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/data/model/history_permintaan_barang_dan_jasa.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/util/const.dart';
import '../../../../core/widget/button/custom_button_widget.dart';
import '../../../../core/widget/textfield/custom_textfield_widget.dart';

class JasaInput extends StatefulWidget {
  const JasaInput(
      {this.jasa,
      required this.listMasterJasa,
      required this.isUserPermitedToSave,
      required this.isJasaSudahDiApprovePortEngginer,
      super.key});
  final Jasa? jasa;
  final bool isJasaSudahDiApprovePortEngginer;
  final List<MasterJasa> listMasterJasa;
  final bool isUserPermitedToSave;
  @override
  State<JasaInput> createState() => _JasaInputState();
}

class _JasaInputState extends State<JasaInput> {
  final TextEditingController namaJasaTextEditingController =
      TextEditingController();
  final TextEditingController penyebabTextEditingController =
      TextEditingController();
  final TextEditingController keteranganTextEditingController =
      TextEditingController();
  late ValueNotifier<bool> isAccepted;

  @override
  void initState() {
    super.initState();
    namaJasaTextEditingController.text = widget.jasa?.jasa ?? empty;
    penyebabTextEditingController.text = widget.jasa?.penyebab ?? empty;
    keteranganTextEditingController.text = widget.jasa?.keterangan ?? empty;
    isAccepted = ValueNotifier(widget.jasa?.isReject == 1 ? false : true);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Container(
        width: width,
        decoration: const BoxDecoration(
            color: white,
            boxShadow: [shadowCard],
            borderRadius: cardBorderRadius),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Text(
                          "Pilih Jasa",
                          style: ptSansTextStyle.copyWith(
                              color: white, fontWeight: bold, fontSize: 22),
                        ),
                        Visibility(
                          visible: widget.jasa != null ? true : false,
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                          child: IconButton(
                              onPressed: (() {
                                Navigator.pop(context, true);
                              }),
                              icon: iconDelete.copyWith(color: white)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Autocomplete<MasterJasa>(
                      initialValue: TextEditingValue(
                        text: widget.jasa?.jasa ?? empty,
                      ),
                      optionsBuilder: (TextEditingValue textEditingValue) {
                        if (textEditingValue.text == empty) {
                          return widget.listMasterJasa;
                        }
                        namaJasaTextEditingController.text =
                            textEditingValue.text;
                        return widget.listMasterJasa.where((MasterJasa option) {
                          return (option.namaJasa?.toLowerCase() ?? empty)
                              .contains(
                            textEditingValue.text.toLowerCase(),
                          );
                        });
                      },
                      optionsViewBuilder: (
                        BuildContext context,
                        AutocompleteOnSelected<MasterJasa> onSelected,
                        Iterable<MasterJasa> options,
                      ) {
                        return Align(
                          alignment: Alignment.topLeft,
                          child: Material(
                            child: Container(
                              height: 80.0 * options.length,
                              decoration: const BoxDecoration(
                                color: white,
                                boxShadow: [
                                  shadowCard,
                                ],
                              ),
                              child: ListView.builder(
                                padding: const EdgeInsets.all(10.0),
                                itemCount: options.length,
                                itemBuilder: (
                                  BuildContext context,
                                  int index,
                                ) {
                                  final MasterJasa option =
                                      options.elementAt(index);
                                  return GestureDetector(
                                    onTap: () {
                                      onSelected(option);
                                    },
                                    child: ListTile(
                                      title: Text(
                                        option.namaJasa ?? empty,
                                        style: ptSansTextStyle.copyWith(
                                          color: black68,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                      fieldViewBuilder: (
                        BuildContext context,
                        TextEditingController fieldTextEditingController,
                        FocusNode fieldFocusNode,
                        VoidCallback onFieldSubmitted,
                      ) {
                        return CustomTextFieldWidget(
                          label: "Nama Jasa",
                          focusNode: fieldFocusNode,
                          textEditingController: fieldTextEditingController,
                          textInputType: TextInputType.text,
                        );
                      },
                      onSelected: (MasterJasa selection) {
                        namaJasaTextEditingController.text =
                            selection.namaJasa ?? empty;
                      },
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextFieldWidget(
                      label: "Penyebab",
                      textEditingController: penyebabTextEditingController,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextFieldWidget(
                      label: "Keterangan",
                      maxlines: 6,
                      textEditingController: keteranganTextEditingController,
                    ),
                    if (widget.isJasaSudahDiApprovePortEngginer)
                      ValueListenableBuilder(
                          valueListenable: isAccepted,
                          builder: (context, _, child) {
                            return CheckboxListTile(
                                checkColor: white,
                                activeColor: primary,
                                contentPadding:
                                    const EdgeInsets.only(bottom: 10),
                                title: Text(
                                  "Accept This Service",
                                  style: ptSansTextStyle,
                                ),
                                subtitle: Text(
                                  "uncheck this to reject service",
                                  style: ptSansTextStyle.copyWith(color: red),
                                ),
                                value: isAccepted.value,
                                onChanged: (value) {
                                  isAccepted.value = value ?? false;
                                });
                          }),
                    const SizedBox(
                      height: 30,
                    ),
                    CustomButtonWidget(
                        callback: () {
                          if (namaJasaTextEditingController.text.isNotEmpty) {
                            Navigator.pop(
                                context,
                                Jasa(
                                    jasa: namaJasaTextEditingController.text,
                                    penyebab:
                                        penyebabTextEditingController.text,
                                    keterangan:
                                        keteranganTextEditingController.text,
                                    isReject: isAccepted.value ? 0 : 1));
                          } else {
                            MyToast.showError("Jasa belum dipilih", context);
                          }
                        },
                        title: save)
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
