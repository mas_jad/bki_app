import 'package:flutter/material.dart';
import 'package:pms_pcm/core/data/model/master_barang_dan_jasa.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/enum.dart';
import 'package:pms_pcm/core/util/global_function.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/data/model/history_permintaan_barang_dan_jasa.dart';

import '../../../../core/data/model/alert_request_oli.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/util/const.dart';
import '../../../../core/widget/button/custom_button_widget.dart';
import '../../../../core/widget/textfield/custom_textfield_widget.dart';

class BarangInput extends StatefulWidget {
  const BarangInput(
      {this.barang,
      required this.listMasterBarang,
      required this.isBarangSudahDiApprovePortEngginer,
      required this.isUserPermitedToSave,
      required this.parameterStok,
      required this.listMasterBarangOli,
      super.key});
  final Barang? barang;
  final bool isBarangSudahDiApprovePortEngginer;
  final List<MasterBarang> listMasterBarang;
  final List<AlertRequestOli> listMasterBarangOli;
  final bool isUserPermitedToSave;
  final ParameterStok? parameterStok;
  @override
  State<BarangInput> createState() => _BarangInputState();
}

class _BarangInputState extends State<BarangInput> {
  final TextEditingController namaBarangEditingController =
      TextEditingController();
  final TextEditingController jumlahEditingController = TextEditingController();
  final TextEditingController jumlahApprovedEditingController =
      TextEditingController();
  final TextEditingController satuanEditingController = TextEditingController();

  final TextEditingController tanggalDigunakanController =
      TextEditingController();
  final TextEditingController tanggalDigunakanIndonesiaController =
      TextEditingController();
  final TextEditingController spesifikasiEditingController =
      TextEditingController();

  final TextEditingController keteranganTextEditingController =
      TextEditingController();

  final TextEditingController kodePartEditingController =
      TextEditingController();
  late ValueNotifier<String?> kodefikasiBarang;
  late ValueNotifier<int?> idPesawatPenggerak;
  late ValueNotifier<bool> isAccepted;

  @override
  void initState() {
    super.initState();

    jumlahEditingController.text = widget.barang?.jumlah?.toString() ?? empty;
    namaBarangEditingController.text =
        widget.barang?.namaBarang?.toString() ?? empty;
    spesifikasiEditingController.text =
        widget.barang?.spesifikasi?.toString() ?? empty;
    satuanEditingController.text = widget.barang?.satuan ?? empty;
    tanggalDigunakanController.text = widget.barang?.tglDigunakan ?? empty;
    tanggalDigunakanIndonesiaController.text = locator<MyDate>()
            .getDateWithInputStringIndonesiaFormat(
                widget.barang?.tglDigunakan) ??
        empty;
    kodePartEditingController.text = widget.barang?.kode ?? empty;
    keteranganTextEditingController.text = widget.barang?.keterangan ?? empty;
    kodefikasiBarang = ValueNotifier(widget.barang?.kodefikasi);
    isAccepted = ValueNotifier(widget.barang?.isReject == 1 ? false : true);
    idPesawatPenggerak =
        ValueNotifier(widget.barang?.idKapalItemPesawatPenggerak);
    jumlahApprovedEditingController.text =
        widget.barang?.jumlahApproved?.toString() ?? empty;
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Container(
        width: width,
        decoration: const BoxDecoration(
            color: white,
            boxShadow: [shadowCard],
            borderRadius: cardBorderRadius),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Text(
                          "Pilih Barang",
                          style: ptSansTextStyle.copyWith(
                              color: white, fontWeight: bold, fontSize: 22),
                        ),
                        Visibility(
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                          visible: widget.barang != null ? true : false,
                          child: IconButton(
                              onPressed: (() {
                                Navigator.pop(context, true);
                              }),
                              icon: iconDelete.copyWith(color: white)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    if (widget.parameterStok == ParameterStok.oli)
                      Autocomplete<AlertRequestOli>(
                        initialValue: TextEditingValue(
                          text: widget.barang?.namaBarang ?? empty,
                        ),
                        optionsBuilder: (TextEditingValue textEditingValue) {
                          if (textEditingValue.text == empty) {
                            return widget.listMasterBarangOli;
                          }

                          return widget.listMasterBarangOli
                              .where((AlertRequestOli option) {
                            return (option.namaJenisOliYgDigunakan
                                        ?.toLowerCase() ??
                                    empty)
                                .contains(
                              textEditingValue.text.toLowerCase(),
                            );
                          });
                        },
                        optionsViewBuilder: (
                          BuildContext context,
                          AutocompleteOnSelected<AlertRequestOli> onSelected,
                          Iterable<AlertRequestOli> options,
                        ) {
                          return Align(
                            alignment: Alignment.topLeft,
                            child: Material(
                              child: Container(
                                height: 80.0 * options.length,
                                decoration: const BoxDecoration(
                                  color: white,
                                  boxShadow: [
                                    shadowCard,
                                  ],
                                ),
                                child: ListView.builder(
                                  padding: const EdgeInsets.all(10.0),
                                  itemCount: options.length,
                                  itemBuilder: (
                                    BuildContext context,
                                    int index,
                                  ) {
                                    final AlertRequestOli option =
                                        options.elementAt(index);
                                    return GestureDetector(
                                      onTap: () {
                                        onSelected(option);
                                      },
                                      child: ListTile(
                                        title: Text(
                                          option.namaJenisOliYgDigunakan ??
                                              empty,
                                          style: ptSansTextStyle.copyWith(
                                            color: primary,
                                            fontSize: 14,
                                          ),
                                        ),
                                        subtitle: Text(
                                          option.namaPesawatPenggerak ?? empty,
                                          style: ptSansTextStyle.copyWith(
                                              color: black54, fontSize: 12),
                                        ),
                                        trailing: Padding(
                                          padding:
                                              const EdgeInsets.only(right: 20),
                                          child: Text(
                                            option.merekOliYgDigunakan ?? empty,
                                            style: ptSansTextStyle.copyWith(
                                                color: quaternary,
                                                fontSize: 12),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                        fieldViewBuilder: (
                          BuildContext context,
                          TextEditingController fieldTextEditingController,
                          FocusNode fieldFocusNode,
                          VoidCallback onFieldSubmitted,
                        ) {
                          return CustomTextFieldWidget(
                            label: "Nama Oli",
                            focusNode: fieldFocusNode,
                            textEditingController: fieldTextEditingController,
                            textInputType: TextInputType.text,
                          );
                        },
                        onSelected: (AlertRequestOli selection) {
                          namaBarangEditingController.text =
                              selection.namaJenisOliYgDigunakan ?? empty;
                          satuanEditingController.text = "LITER";
                          idPesawatPenggerak.value =
                              selection.idPesawatPenggerak;
                        },
                      ),
                    if (widget.parameterStok != ParameterStok.oli)
                      Autocomplete<MasterBarang>(
                        initialValue: TextEditingValue(
                          text: widget.barang?.namaBarang ?? empty,
                        ),
                        optionsBuilder: (TextEditingValue textEditingValue) {
                          if (textEditingValue.text == empty) {
                            if (widget.parameterStok != ParameterStok.lainnya) {
                              return widget.listMasterBarang.where((element) {
                                return element.jenis ==
                                    getJenisParameter(widget.parameterStok);
                              });
                            }

                            return widget.listMasterBarang;
                          }

                          return widget.listMasterBarang.where((element) {
                            if (widget.parameterStok != ParameterStok.lainnya) {
                              return element.jenis ==
                                  getJenisParameter(widget.parameterStok);
                            } else {
                              return true;
                            }
                          }).where((MasterBarang option) {
                            return (option.namaBarang?.toLowerCase() ?? empty)
                                .contains(
                              textEditingValue.text.toLowerCase(),
                            );
                          });
                        },
                        optionsViewBuilder: (
                          BuildContext context,
                          AutocompleteOnSelected<MasterBarang> onSelected,
                          Iterable<MasterBarang> options,
                        ) {
                          return Align(
                            alignment: Alignment.topLeft,
                            child: Material(
                              child: Container(
                                height: 80.0 * options.length,
                                decoration: const BoxDecoration(
                                  color: white,
                                  boxShadow: [
                                    shadowCard,
                                  ],
                                ),
                                child: ListView.builder(
                                  padding: const EdgeInsets.all(10.0),
                                  itemCount: options.length,
                                  itemBuilder: (
                                    BuildContext context,
                                    int index,
                                  ) {
                                    final MasterBarang option =
                                        options.elementAt(index);
                                    return GestureDetector(
                                      onTap: () {
                                        onSelected(option);
                                      },
                                      child: ListTile(
                                        title: Text(
                                          option.namaBarang ?? empty,
                                          style: ptSansTextStyle.copyWith(
                                            color: black68,
                                            fontSize: 14,
                                          ),
                                        ),
                                        subtitle: Text(
                                          option.kodePart ?? empty,
                                          style: ptSansTextStyle.copyWith(
                                            color: black54,
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                        fieldViewBuilder: (
                          BuildContext context,
                          TextEditingController fieldTextEditingController,
                          FocusNode fieldFocusNode,
                          VoidCallback onFieldSubmitted,
                        ) {
                          return CustomTextFieldWidget(
                            label: "Nama Barang",
                            focusNode: fieldFocusNode,
                            textEditingController: fieldTextEditingController,
                            textInputType: TextInputType.text,
                          );
                        },
                        onSelected: (MasterBarang selection) {
                          namaBarangEditingController.text =
                              selection.namaBarang ?? empty;
                          kodePartEditingController.text =
                              selection.kodePart ?? empty;
                          satuanEditingController.text =
                              selection.satuan ?? empty;
                          spesifikasiEditingController.text =
                              selection.spesifikasi ?? empty;
                          kodefikasiBarang.value = selection.kodefikasi;
                        },
                      ),
                    const SizedBox(
                      height: 16,
                    ),
                    if (widget.parameterStok != ParameterStok.oli)
                      Padding(
                        padding: const EdgeInsets.only(bottom: 16),
                        child: CustomTextFieldWidget(
                          label: "Kode Part",
                          textEditingController: kodePartEditingController,
                          isEnable: false,
                        ),
                      ),
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: CustomTextFieldWidget(
                            label: "Jumlah Barang",
                            textEditingController: jumlahEditingController,
                            textInputType: TextInputType.number,
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          flex: 1,
                          child: CustomTextFieldWidget(
                            label: "Satuan",
                            isEnable: false,
                            textEditingController: satuanEditingController,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    if (widget.isBarangSudahDiApprovePortEngginer)
                      Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: CustomTextFieldWidget(
                                  label: "Jumlah Approved",
                                  textEditingController:
                                      jumlahApprovedEditingController,
                                  textInputType: TextInputType.number,
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                flex: 1,
                                child: CustomTextFieldWidget(
                                  label: "Satuan",
                                  isEnable: false,
                                  textEditingController:
                                      satuanEditingController,
                                ),
                              ),
                            ],
                          ),
                          ValueListenableBuilder(
                              valueListenable: isAccepted,
                              builder: (context, _, child) {
                                return CheckboxListTile(
                                    checkColor: white,
                                    activeColor: primary,
                                    contentPadding:
                                        const EdgeInsets.only(bottom: 10),
                                    title: Text(
                                      "Accept This Item",
                                      style: ptSansTextStyle,
                                    ),
                                    subtitle: Text(
                                      "uncheck this to reject items",
                                      style:
                                          ptSansTextStyle.copyWith(color: red),
                                    ),
                                    value: isAccepted.value,
                                    onChanged: (value) {
                                      isAccepted.value = value ?? false;
                                    });
                              }),
                        ],
                      ),
                    Stack(
                      children: [
                        CustomTextFieldWidget(
                          label: "Tanggal Di Gunakan",
                          textEditingController:
                              tanggalDigunakanIndonesiaController,
                          isEnable: false,
                        ),
                        SizedBox(
                          height: 85,
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: IconButton(
                              onPressed: () async {
                                final date = await showMyDatePicker(
                                    context, DateTime.now(), DateTime(3100));
                                if (date != null) {
                                  tanggalDigunakanController.text =
                                      locator<MyDate>()
                                          .getDateWithInputInString(date);

                                  tanggalDigunakanIndonesiaController.text =
                                      locator<MyDate>()
                                          .getDateWithInputIndonesiaFormat(
                                              date);
                                }
                              },
                              icon: const Icon(
                                Icons.date_range,
                                color: primary,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    if (widget.parameterStok != ParameterStok.oli)
                      Padding(
                        padding: const EdgeInsets.only(bottom: 16),
                        child: CustomTextFieldWidget(
                          label: "Spesifikasi",
                          maxlines: 6,
                          isEnable: false,
                          textEditingController: spesifikasiEditingController,
                        ),
                      ),
                    CustomTextFieldWidget(
                      label: "Keterangan",
                      maxlines: 6,
                      textEditingController: keteranganTextEditingController,
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    if (widget.isUserPermitedToSave)
                      CustomButtonWidget(
                          callback: () {
                            if (jumlahEditingController.text.isEmpty) {
                              MyToast.showError(
                                  "Jumlah belum ditentukan", context);
                            } else {
                              Navigator.pop(
                                  context,
                                  Barang(
                                      kode:
                                          kodePartEditingController.text.isEmpty
                                              ? null
                                              : kodePartEditingController.text,
                                      kodefikasi: kodefikasiBarang.value,
                                      namaBarang:
                                          namaBarangEditingController.text,
                                      satuan: satuanEditingController.text,
                                      jumlah: int.tryParse(
                                              jumlahEditingController.text) ??
                                          0,
                                      jumlahApproved: int.tryParse(
                                          jumlahApprovedEditingController.text),
                                      idKapalItemPesawatPenggerak:
                                          idPesawatPenggerak.value,
                                      idKapalItemTangkiAir: null,
                                      idKapalItemTangkiBbm: null,
                                      keterangan:
                                          keteranganTextEditingController.text,
                                      spesifikasi: spesifikasiEditingController
                                              .text.isEmpty
                                          ? null
                                          : spesifikasiEditingController.text,
                                      tglDigunakan:
                                          tanggalDigunakanController.text,
                                      isUpdateStok: widget.parameterStok ==
                                              ParameterStok.lainnya
                                          ? 0
                                          : 1,
                                      paramStok: widget.parameterStok?.name,
                                      isReject: isAccepted.value ? 0 : 1));
                            }
                          },
                          title: save)
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
