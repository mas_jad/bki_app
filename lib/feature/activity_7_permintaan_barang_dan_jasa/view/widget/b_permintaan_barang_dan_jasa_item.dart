import 'package:flutter/material.dart';
import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/size.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';

import '../../../../core/theme/shadow.dart';

class PermintaanBarangDanJasaItem extends StatelessWidget {
  const PermintaanBarangDanJasaItem(
      {required this.callback,
      required this.kualifikasi,
      required this.noPbj,
      required this.approve,
      required this.date,
      super.key});
  final Function(BuildContext context) callback;

  final String? noPbj;
  final String? kualifikasi;
  final String? date;
  final Approve approve;

  @override
  Widget build(BuildContext context) {
    return PopWidget(
        onTap: () {
          callback(context);
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(bottom: 16),
          padding: const EdgeInsets.all(20),
          decoration: const BoxDecoration(
              color: white,
              boxShadow: [shadowCard],
              borderRadius: cardBorderRadius),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    noPbj ?? empty,
                    style: ptSansTextStyle.copyWith(
                        color: secondary, fontWeight: light, fontSize: 16),
                  ),
                  Text(
                    kualifikasi ?? empty,
                    style: ptSansTextStyle.copyWith(
                        color: kualifikasi == SEGERA ? red : lightText,
                        fontWeight: bold,
                        fontSize: 12),
                  ),
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Text(
                    approve.getStatus(),
                    style: ptSansTextStyle.copyWith(
                        color: statusColor(approve.getStatus()),
                        fontWeight: semiBold,
                        fontSize: 14),
                  ),
                  if (approve.getName() != null)
                    Text(
                      " by ${approve.getName()}",
                      style: ptSansTextStyle.copyWith(
                        color: lightText,
                        fontWeight: reguler,
                        fontSize: 14,
                      ),
                    ),
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Text(
                  locator<MyDate>()
                          .getDateWithInputStringIndonesiaFormat(date) ??
                      empty,
                  style: ptSansTextStyle.copyWith(
                      color: black38, fontWeight: light, fontSize: 14),
                ),
              )
            ],
          ),
        ));
  }
}
