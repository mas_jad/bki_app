import 'package:flutter/material.dart';

import '../../../../../core/data/model/master_barang_dan_jasa.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../data/model/history_permintaan_barang_dan_jasa.dart';
import '../d_jasa_input.dart';

class ListPermintaanJasa extends StatefulWidget {
  const ListPermintaanJasa({
    Key? key,
    required this.listJasaValue,
    required this.isUserPermitedToSave,
    required this.isJasaSudahDiApprovePortEngginer,
    this.masterBarangDanJasa,
  }) : super(key: key);

  final ValueNotifier<List<Jasa>> listJasaValue;
  final bool isUserPermitedToSave;
  final bool isJasaSudahDiApprovePortEngginer;
  final MasterBarangDanJasa? masterBarangDanJasa;

  @override
  State<ListPermintaanJasa> createState() => _ListPermintaanJasaState();
}

class _ListPermintaanJasaState extends State<ListPermintaanJasa> {
  // LIST JASA
  List<Widget> jasaContainer() {
    final List<Widget> list = [];
    for (var i = 0; i < widget.listJasaValue.value.length; i++) {
      list.add(GestureDetector(
        onTap: () async {
          final jasa = await showModalBottomSheet(
              isDismissible: false,
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              isScrollControlled: true,
              context: context,
              builder: (context) {
                return Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: JasaInput(
                      isJasaSudahDiApprovePortEngginer:
                          widget.isJasaSudahDiApprovePortEngginer,
                      isUserPermitedToSave: widget.isUserPermitedToSave,
                      jasa: widget.listJasaValue.value[0],
                      listMasterJasa:
                          widget.masterBarangDanJasa?.masterJasa ?? [],
                    ));
              });
          if (jasa is Jasa) {
            widget.listJasaValue.value.removeAt(i);
            widget.listJasaValue.value.insert(i, jasa);
            final tmp = [...widget.listJasaValue.value];
            widget.listJasaValue.value = tmp;
          } else if (jasa is bool) {
            widget.listJasaValue.value.removeAt(i);
            final tmp = [...widget.listJasaValue.value];
            widget.listJasaValue.value = tmp;
          }
        },
        child: Container(
          padding: const EdgeInsets.all(12),
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: const BoxDecoration(
              color: grey200,
              borderRadius: BorderRadius.all(Radius.circular(14))),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.listJasaValue.value[i].jasa ?? empty,
                  style: ptSansTextStyle.copyWith(color: black54, fontSize: 16),
                ),
                widget.listJasaValue.value[i].penyebab?.isNotEmpty ?? false
                    ? const SizedBox(
                        height: 10,
                      )
                    : const SizedBox(),
                widget.listJasaValue.value[i].penyebab?.isNotEmpty ?? false
                    ? Text(
                        "Penyebab : ${(widget.listJasaValue.value[i].penyebab ?? empty)}",
                        style: ptSansTextStyle.copyWith(
                            color: black54, fontSize: 14),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ),
      ));
    }
    if (widget.isUserPermitedToSave) {
      list.add(Container(
        padding: const EdgeInsets.all(6),
        margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
        decoration: const BoxDecoration(
            color: grey200,
            borderRadius: BorderRadius.all(Radius.circular(14))),
        child: Center(
          child: IconButton(
              onPressed: () async {
                final jasa = await showModalBottomSheet(
                    isDismissible: false,
                    enableDrag: false,
                    shape: const RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(20))),
                    isScrollControlled: true,
                    context: context,
                    builder: (context) {
                      return Padding(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: JasaInput(
                            isJasaSudahDiApprovePortEngginer:
                                widget.isJasaSudahDiApprovePortEngginer,
                            isUserPermitedToSave: widget.isUserPermitedToSave,
                            listMasterJasa:
                                widget.masterBarangDanJasa?.masterJasa ?? [],
                          ));
                    });
                if (jasa is Jasa) {
                  final tmp = [...widget.listJasaValue.value, jasa];
                  widget.listJasaValue.value = tmp;
                }
              },
              icon: iconAdd.copyWith(color: black54)),
        ),
      ));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "List Jasa",
          style: ptSansTextStyle.copyWith(color: primary),
        ),
        const SizedBox(
          height: 16,
        ),
        Container(
          decoration: BoxDecoration(
              color: white,
              border: Border.all(color: black26, width: 0.5),
              borderRadius: cardBorderRadius),
          child: Stack(
            children: [
              ValueListenableBuilder(
                  valueListenable: widget.listJasaValue,
                  builder: (context, value, child) {
                    return Column(
                      children: [
                        ...jasaContainer(),
                        const SizedBox(
                          height: 12,
                        )
                      ],
                    );
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
