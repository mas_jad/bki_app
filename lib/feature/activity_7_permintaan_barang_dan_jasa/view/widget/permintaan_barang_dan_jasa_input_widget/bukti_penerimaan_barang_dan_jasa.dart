import 'package:flutter/material.dart';

import '../../../../../core/theme/theme.dart';
import '../../../../../core/widget/image/image_container_picker.dart';
import '../../../../../core/widget/textfield/custom_textfield_widget.dart';
import '../c_permintaan_barang_dan_jasa_input.dart';

class BuktiPenerimaanBarangDanJasa extends StatelessWidget {
  const BuktiPenerimaanBarangDanJasa({
    Key? key,
    required this.isUserPermitedToTerima,
    required this.widget,
    required this.imageValueNotifier,
    required this.namaPengirimTextEditingController,
    required this.catatanPenerimaTextEditingController,
  }) : super(key: key);

  final bool isUserPermitedToTerima;
  final PermintaanBarangDanJasaInput widget;
  final ValueNotifier<String?> imageValueNotifier;
  final TextEditingController namaPengirimTextEditingController;
  final TextEditingController catatanPenerimaTextEditingController;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 16),
            child: Text(
              "Bukti Penerimaan",
              style: ptSansTextStyle.copyWith(
                  fontWeight: light, color: primary, fontSize: 14),
            )),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Center(
              child: ImageContainerPicker(
            ratioToWidth: 1,
            isEnable: isUserPermitedToTerima,
            image: widget.historyPermintaanBarangDanJasa?.fotoBuktiTerima,
            imageValueNotifier: imageValueNotifier,
          )),
        ),
        const SizedBox(
          height: 16,
        ),
        CustomTextFieldWidget(
          label: "Nama Pengirim",
          maxlines: 1,
          textEditingController: namaPengirimTextEditingController,
          isEnable: isUserPermitedToTerima,
        ),
        const SizedBox(
          height: 16,
        ),
        CustomTextFieldWidget(
          label: "Catatan Penerima",
          maxlines: 6,
          textEditingController: catatanPenerimaTextEditingController,
          isEnable: isUserPermitedToTerima,
        ),
      ],
    );
  }
}
