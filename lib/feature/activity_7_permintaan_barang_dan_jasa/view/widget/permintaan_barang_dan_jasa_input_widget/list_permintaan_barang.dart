import 'package:flutter/material.dart';

import '../../../../../core/data/model/alert_request_oli.dart';
import '../../../../../core/data/model/master_barang_dan_jasa.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/util/enum.dart';
import '../../../data/model/history_permintaan_barang_dan_jasa.dart';
import '../d_barang_input.dart';

class ListPermintaanBarang extends StatefulWidget {
  const ListPermintaanBarang({
    Key? key,
    required this.listBarangValue,
    required this.isUserPermitedToSave,
    required this.isBarangSudahDiApprovePortEngginer,
    required this.listMasterBarangOli,
    this.masterBarangDanJasa,
    this.parameterStok,
  }) : super(key: key);

  final ValueNotifier<List<Barang>> listBarangValue;
  final bool isUserPermitedToSave;
  final bool isBarangSudahDiApprovePortEngginer;
  final MasterBarangDanJasa? masterBarangDanJasa;
  final List<AlertRequestOli>? listMasterBarangOli;
  final ParameterStok? parameterStok;
  @override
  State<ListPermintaanBarang> createState() => _ListPermintaanBarangState();
}

class _ListPermintaanBarangState extends State<ListPermintaanBarang> {
  // LIST BARANG
  List<Widget> barangContainer() {
    final List<Widget> list = [];
    for (var i = 0; i < widget.listBarangValue.value.length; i++) {
      list.add(GestureDetector(
        onTap: () async {
          final barang = await showModalBottomSheet(
              isDismissible: false,
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              isScrollControlled: true,
              context: context,
              builder: (context) {
                return Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: BarangInput(
                      isUserPermitedToSave: widget.isUserPermitedToSave,
                      isBarangSudahDiApprovePortEngginer:
                          widget.isBarangSudahDiApprovePortEngginer,
                      barang: widget.listBarangValue.value[i],
                      parameterStok: widget.parameterStok,
                      listMasterBarang:
                          widget.masterBarangDanJasa?.masterBarang ?? [],
                      listMasterBarangOli: widget.listMasterBarangOli ?? [],
                    ));
              });
          if (barang is Barang) {
            widget.listBarangValue.value.removeAt(i);
            widget.listBarangValue.value.insert(i, barang);
            final tmp = [...widget.listBarangValue.value];
            widget.listBarangValue.value = tmp;
          } else if (barang is bool) {
            widget.listBarangValue.value.removeAt(i);
            final tmp = [...widget.listBarangValue.value];
            widget.listBarangValue.value = tmp;
          }
        },
        child: Container(
          padding: const EdgeInsets.all(12),
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: const BoxDecoration(
              color: grey200,
              borderRadius: BorderRadius.all(Radius.circular(14))),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.listBarangValue.value[i].namaBarang ?? empty,
                  style: ptSansTextStyle.copyWith(color: black54, fontSize: 16),
                ),
                widget.listBarangValue.value[i].kode?.isNotEmpty ?? false
                    ? const SizedBox(
                        height: 12,
                      )
                    : const SizedBox(),
                widget.listBarangValue.value[i].kode?.isNotEmpty ?? false
                    ? Text(
                        "Kode    : ${(widget.listBarangValue.value[i].kode ?? empty)}",
                        style: ptSansTextStyle.copyWith(
                            color: black54, fontSize: 14),
                      )
                    : const SizedBox(),
                Text(
                  "Jumlah : ${(widget.listBarangValue.value[i].jumlahApproved ?? widget.listBarangValue.value[i].jumlah ?? empty)} ${(widget.listBarangValue.value[i].satuan ?? empty)}",
                  style: ptSansTextStyle.copyWith(color: black54, fontSize: 14),
                )
              ],
            ),
          ),
        ),
      ));
    }
    if (widget.isUserPermitedToSave) {
      if (list.isEmpty || widget.parameterStok == ParameterStok.lainnya) {
        list.add(Container(
          padding: const EdgeInsets.all(6),
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: const BoxDecoration(
              color: grey200,
              borderRadius: BorderRadius.all(Radius.circular(14))),
          child: Center(
            child: IconButton(
                onPressed: () async {
                  final barang = await showModalBottomSheet(
                      isDismissible: false,
                      enableDrag: false,
                      shape: const RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(20))),
                      isScrollControlled: true,
                      context: context,
                      builder: (context) {
                        return Padding(
                            padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom),
                            child: BarangInput(
                              isUserPermitedToSave: widget.isUserPermitedToSave,
                              isBarangSudahDiApprovePortEngginer: false,
                              parameterStok: widget.parameterStok,
                              listMasterBarang:
                                  widget.masterBarangDanJasa?.masterBarang ??
                                      [],
                              listMasterBarangOli:
                                  widget.listMasterBarangOli ?? [],
                            ));
                      });
                  if (barang is Barang) {
                    final tmp = [...widget.listBarangValue.value, barang];
                    widget.listBarangValue.value = tmp;
                  }
                },
                icon: iconAdd.copyWith(color: black54)),
          ),
        ));
      }
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "List Barang",
          style: ptSansTextStyle.copyWith(color: primary),
        ),
        const SizedBox(
          height: 16,
        ),
        Container(
          decoration: BoxDecoration(
              color: white,
              border: Border.all(color: black26, width: 0.5),
              borderRadius: cardBorderRadius),
          child: Stack(
            children: [
              ValueListenableBuilder(
                  valueListenable: widget.listBarangValue,
                  builder: (context, value, child) {
                    return Column(
                      children: [
                        ...barangContainer(),
                        const SizedBox(
                          height: 12,
                        )
                      ],
                    );
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
