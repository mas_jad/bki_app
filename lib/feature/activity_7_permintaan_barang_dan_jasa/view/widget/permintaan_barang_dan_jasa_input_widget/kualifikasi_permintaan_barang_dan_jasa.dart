import 'package:flutter/material.dart';

import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/widget/textfield/custom_textfield_widget.dart';

class KualifikasiPermintaanBarangDanJasa extends StatelessWidget {
  const KualifikasiPermintaanBarangDanJasa({
    Key? key,
    required this.kualifikasiValue,
  }) : super(key: key);

  final ValueNotifier<String> kualifikasiValue;

  @override
  Widget build(BuildContext context) {
    return Autocomplete<String>(
      initialValue: TextEditingValue(
        text: kualifikasiValue.value,
      ),
      optionsBuilder: (TextEditingValue textEditingValue) {
        return listKualifikasi;
      },
      optionsViewBuilder: (
        BuildContext context,
        AutocompleteOnSelected<String> onSelected,
        Iterable<String> options,
      ) {
        return Align(
          alignment: Alignment.topLeft,
          child: Material(
            child: Container(
              height: 60.0 * options.length,
              decoration: const BoxDecoration(
                color: white,
                boxShadow: [
                  shadowCard,
                ],
              ),
              child: ListView.builder(
                padding: const EdgeInsets.all(10.0),
                itemCount: options.length,
                itemBuilder: (
                  BuildContext context,
                  int index,
                ) {
                  final String option = options.elementAt(index);
                  return GestureDetector(
                    onTap: () {
                      onSelected(option);
                    },
                    child: ListTile(
                      title: Text(
                        option,
                        style: ptSansTextStyle.copyWith(
                          color: black68,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
      fieldViewBuilder: (
        BuildContext context,
        TextEditingController fieldTextEditingController,
        FocusNode fieldFocusNode,
        VoidCallback onFieldSubmitted,
      ) {
        return CustomTextFieldWidget(
          label: "Kualifikasi",
          focusNode: fieldFocusNode,
          textEditingController: fieldTextEditingController,
          textInputType: TextInputType.text,
        );
      },
      onSelected: (String selection) {
        kualifikasiValue.value = selection;
      },
    );
  }
}
