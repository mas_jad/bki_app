import 'package:flutter/material.dart';
import 'package:pms_pcm/core/util/enum.dart';

import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';

class HeaderPermintaanBarangDanJasa extends StatelessWidget {
  const HeaderPermintaanBarangDanJasa({
    Key? key,
    required this.parameterStok,
  }) : super(key: key);

  final ParameterStok? parameterStok;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: const BoxDecoration(
          image:
              DecorationImage(image: AssetImage(itemImage2), fit: BoxFit.cover),
          color: secondary,
          borderRadius: BorderRadius.all(Radius.circular(18))),
      child: Column(
        children: [
          const SizedBox(
            height: 40,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                  onPressed: (() {
                    Navigator.pop(context);
                  }),
                  icon: iconArrow.copyWith(color: white)),
              Flexible(
                child: Text(
                  parameterStok == ParameterStok.air
                      ? "Permintaan Air Tawar"
                      : parameterStok == ParameterStok.bbm
                          ? "Permintaan Bahan Bakar"
                          : parameterStok == ParameterStok.oli
                              ? "Permintaan Oli Mesin"
                              : "Permintaan Barang Dan Jasa",
                  textAlign: TextAlign.center,
                  style: ptSansTextStyle.copyWith(
                      color: white, fontWeight: bold, fontSize: 20),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(8),
                  child: iconArrow.copyWith(color: transparent)),
            ],
          ),
        ],
      ),
    );
  }
}
