import 'package:flutter/material.dart';
import 'package:pms_pcm/core/util/enum.dart';

import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/widget/textfield/custom_textfield_widget.dart';

class JenisPermintaanBarangDanJasa extends StatelessWidget {
  const JenisPermintaanBarangDanJasa({
    Key? key,
    required this.parameterStokValue,
    required this.callback,
  }) : super(key: key);

  final ValueNotifier<ParameterStok> parameterStokValue;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return Autocomplete<ParameterStok>(
      initialValue: TextEditingValue(
        text: parameterStokValue.value.name.toUpperCase(),
      ),
      optionsBuilder: (TextEditingValue textEditingValue) {
        return listParamterStok;
      },
      optionsViewBuilder: (
        BuildContext context,
        AutocompleteOnSelected<ParameterStok> onSelected,
        Iterable<ParameterStok> options,
      ) {
        return Align(
          alignment: Alignment.topLeft,
          child: Material(
            child: Container(
              height: 60.0 * options.length,
              decoration: const BoxDecoration(
                color: white,
                boxShadow: [
                  shadowCard,
                ],
              ),
              child: ListView.builder(
                padding: const EdgeInsets.all(10.0),
                itemCount: options.length,
                itemBuilder: (
                  BuildContext context,
                  int index,
                ) {
                  final ParameterStok option = options.elementAt(index);
                  return GestureDetector(
                    onTap: () {
                      onSelected(option);
                      callback();
                    },
                    child: ListTile(
                      title: Text(
                        option.name.toUpperCase(),
                        style: ptSansTextStyle.copyWith(
                          color: black68,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
      fieldViewBuilder: (
        BuildContext context,
        TextEditingController fieldTextEditingController,
        FocusNode fieldFocusNode,
        VoidCallback onFieldSubmitted,
      ) {
        return CustomTextFieldWidget(
          label: "Jenis Permintaan",
          focusNode: fieldFocusNode,
          textEditingController: fieldTextEditingController,
          textInputType: TextInputType.text,
        );
      },
      onSelected: (ParameterStok selection) {
        parameterStokValue.value = selection;
      },
    );
  }
}
