import 'package:flutter/material.dart';

import '../../../../../core/service/date/date.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/util/locator.dart';
import '../../../data/model/history_permintaan_barang_dan_jasa.dart';

class NomorPermintaanBarangDanJasa extends StatelessWidget {
  const NomorPermintaanBarangDanJasa(
      {required this.historyPermintaanBarangDanJasa,
      required this.isDone,
      super.key});
  final HistoryPermintaanBarangDanJasa? historyPermintaanBarangDanJasa;
  final bool isDone;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 10),
      padding: const EdgeInsets.all(20),
      decoration: const BoxDecoration(
          color: white,
          boxShadow: [shadowCard],
          borderRadius: cardBorderRadius),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    historyPermintaanBarangDanJasa?.noPbj.toString() ?? empty,
                    style: ptSansTextStyle.copyWith(
                        color: secondary, fontWeight: light, fontSize: 16),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                "Tanggal   :  ${locator<MyDate>().getDateWithInputStringIndonesiaFormat(historyPermintaanBarangDanJasa?.date)}",
                style:
                    ptSansTextStyle.copyWith(color: black38, fontWeight: bold),
              ),
              const SizedBox(
                height: 10,
              ),
              if (isDone)
                Text(
                  "Diterima  :  ${locator<MyDate>().getDateWithInputStringIndonesiaFormat(historyPermintaanBarangDanJasa?.approved6Date)}",
                  style: ptSansTextStyle.copyWith(
                      color: black38, fontWeight: bold),
                ),
              if (isDone)
                const SizedBox(
                  height: 10,
                ),
              Row(
                children: [
                  Text(
                    "Status     :  ",
                    style: ptSansTextStyle.copyWith(
                        color: black38, fontWeight: bold),
                  ),
                  Text(
                    historyPermintaanBarangDanJasa?.approve?.getStatus() ??
                        DRAF,
                    style: ptSansTextStyle.copyWith(
                        color: statusColor(historyPermintaanBarangDanJasa
                                ?.approve
                                ?.getStatus() ??
                            DRAF),
                        fontWeight: semiBold,
                        fontSize: 14),
                  ),
                  if (historyPermintaanBarangDanJasa?.approve?.getName() !=
                      null)
                    Text(
                      " by ${historyPermintaanBarangDanJasa?.approve?.getName() ?? empty}",
                      style: ptSansTextStyle.copyWith(
                        color: lightText,
                        fontWeight: reguler,
                        fontSize: 14,
                      ),
                    ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
