import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/util/global_function.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/core/widget/other/error_layout.dart';
import 'package:pms_pcm/core/widget/other/loading_widget.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';
import 'package:pms_pcm/core/widget/textfield/custom_textfield_widget.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/b_permintaan_barang_dan_jasa_item.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/widget/c_permintaan_barang_dan_jasa_input.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/util/locator.dart';
import '../../bloc/permintaan_barang_dan_jasa_bloc.dart';

class HistoryPermintaanBarangDanJasaWidget extends StatefulWidget {
  const HistoryPermintaanBarangDanJasaWidget(
      {required this.departemenId,
      required this.kapalId,
      required this.search,
      required this.dateRange,
      required this.isDeck,
      super.key});

  final int kapalId;
  final int departemenId;
  final ValueNotifier<DateTimeRange?> dateRange;
  final ValueNotifier<String?> search;
  final bool isDeck;
  @override
  State<HistoryPermintaanBarangDanJasaWidget> createState() =>
      _HistoryPermintaanBarangDanJasaWidgetState();
}

class _HistoryPermintaanBarangDanJasaWidgetState
    extends State<HistoryPermintaanBarangDanJasaWidget> {
  final TextEditingController textEditingController = TextEditingController();

  getHistoryFromRange() async {
    final now = locator<MyDate>().getDateToday();
    final result =
        await showMyRangeDatePicker(context, widget.dateRange.value, now);
    if (result != null) {
      if (result != widget.dateRange.value) {
        widget.dateRange.value = result;
        getHistory();
      }
    }
  }

  getHistory() {
    final dateFormatedFrom = widget.dateRange.value?.start != null
        ? locator<MyDate>()
            .getDateWithInputInString(widget.dateRange.value!.start)
        : null;
    final dateFormatedTo = widget.dateRange.value?.end != null
        ? locator<MyDate>()
            .getDateWithInputInString(widget.dateRange.value!.end)
        : null;

    if (mounted) {
      BlocProvider.of<PermintaanBarangDanJasaBloc>(context).add(
          GetAllHistoryPermintaanBarangDanJasaEvent(
              widget.kapalId,
              dateFormatedFrom,
              dateFormatedTo,
              widget.departemenId,
              widget.search.value));
    }
  }

  transformDataToWidget(HistoryPermintaanBarangDanJasaSuccess state) {
    final List<PermintaanBarangDanJasaItem> list = [];
    state.historyPermintaanBarangDanJasaResponse.data?.forEach((e) {
      list.add(PermintaanBarangDanJasaItem(
        date: e.date,
        approve: e.approve ?? Approve(),
        callback: (contextPermintaanBarangDanJasa) {
          Navigator.push(context, MaterialPageRoute(builder: (context2) {
            return BlocProvider.value(
                value: BlocProvider.of<PermintaanBarangDanJasaBloc>(context),
                child: PermintaanBarangDanJasaInput(
                  search: widget.search,
                  dateRange: widget.dateRange,
                  isDateToSaveNotExpired: locator<MyDate>()
                          .getDateWithInputString(e.date)
                          ?.isAfter(locator<MyDate>()
                              .getDateToday()
                              .subtract(const Duration(days: 3))) ??
                      false,
                  kapalId: widget.kapalId,
                  date: locator<MyDate>().getDateWithInputString(e.date),
                  departmenId: widget.departemenId,
                  isDeck: widget.isDeck,
                  historyPermintaanBarangDanJasa: e,
                ));
          }));
        },
        noPbj: e.noPbj ?? empty,
        kualifikasi: e.kualifikasi ?? empty,
      ));
    });
    return list;
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PermintaanBarangDanJasaBloc,
        PermintaanBarangDanJasaState>(
      listener: (context, state) {
        if (state is UpdateStatusPermintaanBarangDanJasaSuccess) {
          MyToast.showSuccess(success, context);
        } else if (state is UpdateStatusPermintaanBarangDanJasaError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: BlocBuilder<PermintaanBarangDanJasaBloc,
              PermintaanBarangDanJasaState>(
            buildWhen: (previous, current) {
              return current is HistoryPermintaanBarangDanJasaState;
            },
            builder: (context, state) {
              if (state is HistoryPermintaanBarangDanJasaSuccess) {
                final List<PermintaanBarangDanJasaItem> mainList =
                    transformDataToWidget(state);
                List<PermintaanBarangDanJasaItem> list = mainList;
                return StatefulBuilder(builder: (context, setStateList) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.vertical(
                                bottom: Radius.circular(12)),
                            boxShadow: [shadowCard]),
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 16,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Expanded(
                                      child: CustomTextFieldWidget(
                                    hint: "Tulis Nomor PBJ",
                                    textEditingController:
                                        textEditingController,
                                    onSubmitted: (value) {
                                      widget.search.value = value;
                                      getHistory();
                                    },
                                  )),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      getHistoryFromRange();
                                    },
                                    child: widget.dateRange.value != null
                                        ? Row(
                                            children: [
                                              Column(
                                                children: [
                                                  Text(
                                                    locator<MyDate>()
                                                        .getDateWithInputIndonesiaFormat(
                                                            widget.dateRange
                                                                .value?.start),
                                                    maxLines: 2,
                                                    style: ptSansTextStyle
                                                        .copyWith(
                                                            color: black54,
                                                            fontWeight: medium,
                                                            fontSize: 12),
                                                  ),
                                                  Text(
                                                    "Until",
                                                    maxLines: 2,
                                                    style: ptSansTextStyle
                                                        .copyWith(
                                                            color: black12,
                                                            fontWeight: medium,
                                                            fontSize: 10),
                                                  ),
                                                  Text(
                                                    locator<MyDate>()
                                                        .getDateWithInputIndonesiaFormat(
                                                            widget.dateRange
                                                                .value?.end),
                                                    maxLines: 2,
                                                    style: ptSansTextStyle
                                                        .copyWith(
                                                            color: black54,
                                                            fontWeight: medium,
                                                            fontSize: 12),
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(
                                                width: 10,
                                              ),
                                              PopWidget(
                                                  onTap: () {
                                                    widget.dateRange.value =
                                                        null;
                                                    getHistory();
                                                  },
                                                  child: iconDelete.copyWith(
                                                      color: secondary))
                                            ],
                                          )
                                        : Text(
                                            "Pilih Range",
                                            maxLines: 2,
                                            style: ptSansTextStyle.copyWith(
                                                color: black54,
                                                fontWeight: semiBold,
                                                fontSize: 16),
                                          ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            SizedBox(
                              height: 50,
                              child: ListView(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                scrollDirection: Axis.horizontal,
                                children: [
                                  ...listStatus.map((e) => Padding(
                                        padding:
                                            const EdgeInsets.only(right: 6),
                                        child: PopWidget(
                                          onTap: () {
                                            setStateList(() {
                                              list = mainList.where((element) {
                                                if (element.approve
                                                            .getStatus() ==
                                                        e ||
                                                    e == ALL) {
                                                  return true;
                                                } else {
                                                  return false;
                                                }
                                              }).toList();
                                            });
                                          },
                                          child: Chip(
                                              backgroundColor: white,
                                              side: BorderSide(
                                                  color: statusColor(e)),
                                              label: Text(
                                                e,
                                                style: ptSansTextStyle.copyWith(
                                                    color: statusColor(e)),
                                              )),
                                        ),
                                      ))
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                          child: list.isNotEmpty
                              ? ListView.builder(
                                  padding: const EdgeInsets.all(20),
                                  itemCount: list.length,
                                  itemBuilder: (BuildContext ctx, index) {
                                    return list[index];
                                  },
                                )
                              : const ErrorLayout(message: dataEmpty)),
                    ],
                  );
                });
              } else if (state is HistoryPermintaanBarangDanJasaError) {
                return ErrorLayout(message: state.message);
              }
              return const Center(child: LoadingWidget());
            },
          ),
        ),
      ),
    );
  }
}
