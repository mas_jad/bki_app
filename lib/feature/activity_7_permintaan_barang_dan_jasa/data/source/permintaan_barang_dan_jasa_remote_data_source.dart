import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/data/model/post_terima_barang.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/service/location/location_service.dart';
import '../../../../core/util/user_role.dart';
import '../model/history_permintaan_barang_dan_jasa.dart';
import '../model/post_permintaan_barang_dan_jasa.dart';

class PermintaanBarangDanJasaRemoteDataSource {
  Future<HistoryPermintaanBarangDanJasaResponse>
      getHistoryPermintaanBarangDanJasa(
          int id, String token, String date, int departemenId) async {
    final endpoint = dotenv.get(dataPBJAllEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {
        "kapal_id": id,
        "date_now": date,
        "departemen_id": departemenId,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPermintaanBarangDanJasaResponse.fromJson(response.data);
  }

  Future<HistoryPermintaanBarangDanJasaResponse>
      getAllHistoryPermintaanBarangDanJasa(
          int id,
          String token,
          String? dateFrom,
          String? dateTo,
          int departemenId,
          String? search) async {
    final endpoint = dotenv.get(dataPBJAllEndpoint);

    final response = await locator<Dio>().post(
      endpoint,
      data: {
        "kapal_id": id,
        "date_from": dateFrom,
        "date_to": dateTo,
        "departemen_id": departemenId,
        "no_pbj": search,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryPermintaanBarangDanJasaResponse.fromJson(response.data);
  }

  Future<HistoryPermintaanBarangDanJasaResponse>
      simpanUpdatePermintaanBarangDanJasa(String token,
          PostPermintaanBarangDanJasa postPermintaanBarangDanJasa) async {
    final endpoint = dotenv.get(simpanUpdateDataPBJEndpoint);
    final position = await locator<LocationService>().getLastPosition();
    final data = postPermintaanBarangDanJasa.toJson(position);

    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPermintaanBarangDanJasaResponse.fromJson(response.data);
  }

  Future<HistoryPermintaanBarangDanJasaResponse>
      updateStatusPermintaanBarangDanJasa(
    int id,
    String token,
    String date,
    int departemenId,
    String noPbj,
    Approve approve,
  ) async {
    final endpoint = dotenv.get(ubahStatusPBJEndpoint);

    final response = await locator<Dio>().post(
      "$endpoint/${locator<UserRole>().getUpdateStatusURL(false, approve)}",
      data: {
        "kapal_id": id,
        "date": date,
        "departemen_id": departemenId,
        "no_pbj": noPbj,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryPermintaanBarangDanJasaResponse.fromJson(response.data);
  }

  Future<HistoryPermintaanBarangDanJasaResponse> terimaBarang(
      PostTerimaBarang postTerimaBarang, String token) async {
    final endpoint = dotenv.get(terimaBarangEndpoint);
    final position = await locator<LocationService>().getLastPosition();
    final response = await locator<Dio>().post(
      endpoint,
      data: await postTerimaBarang.toJson(position),
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryPermintaanBarangDanJasaResponse.fromJson(response.data);
  }

  Future<HistoryPermintaanBarangDanJasaResponse> kirimBarang(
    int id,
    String token,
    String date,
    int departemenId,
    String noPbj,
  ) async {
    final endpoint = dotenv.get(kirimBarangEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {
        "kapal_id": id,
        "date": date,
        "departemen_id": departemenId,
        "no_pbj": noPbj,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryPermintaanBarangDanJasaResponse.fromJson(response.data);
  }
}
