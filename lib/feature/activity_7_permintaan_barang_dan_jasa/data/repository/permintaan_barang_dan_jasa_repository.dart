import '../../../../core/data/model/approve.dart';
import '../../../../core/util/request.dart';
import '../model/history_permintaan_barang_dan_jasa.dart';
import '../model/post_permintaan_barang_dan_jasa.dart';
import '../model/post_terima_barang.dart';
import '../source/permintaan_barang_dan_jasa_remote_data_source.dart';

class PermintaanBarangDanJasaRepository {
  final PermintaanBarangDanJasaRemoteDataSource
      _permintaanBarangDanJasaRemoteDataSource;
  PermintaanBarangDanJasaRepository(
      this._permintaanBarangDanJasaRemoteDataSource);

  Future<HistoryPermintaanBarangDanJasaResponse>
      getHistoryPermintaanBarangDanJasa(
          int id, String token, String date, int departemenId) async {
    return RequestServer<HistoryPermintaanBarangDanJasaResponse>()
        .requestServer(computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .getHistoryPermintaanBarangDanJasa(id, token, date, departemenId);

      return response;
    });
  }

  Future<HistoryPermintaanBarangDanJasaResponse>
      getAllHistoryPermintaanBarangDanJasa(
          int id,
          String token,
          String? dateFrom,
          String? dateTo,
          int departemenId,
          String? search) async {
    return RequestServer<HistoryPermintaanBarangDanJasaResponse>()
        .requestServer(computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .getAllHistoryPermintaanBarangDanJasa(
              id, token, dateFrom, dateTo, departemenId, search);

      return response;
    });
  }

  Future<HistoryPermintaanBarangDanJasaResponse> postPermintaanBarangDanJasa(
      String token,
      PostPermintaanBarangDanJasa postPermintaanBarangDanJasa) async {
    return RequestServer<HistoryPermintaanBarangDanJasaResponse>()
        .requestServer(computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .simpanUpdatePermintaanBarangDanJasa(
              token, postPermintaanBarangDanJasa);
      return response;
    });
  }

  Future<HistoryPermintaanBarangDanJasaResponse>
      updateStatusPermintaanBarangDanJasa(
    int id,
    String token,
    String date,
    int departemenId,
    String noPbj,
    Approve approve,
  ) async {
    return RequestServer<HistoryPermintaanBarangDanJasaResponse>()
        .requestServer(computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .updateStatusPermintaanBarangDanJasa(
              id, token, date, departemenId, noPbj, approve);
      return response;
    });
  }

  Future<HistoryPermintaanBarangDanJasaResponse> kirimBarang(
    int id,
    String token,
    String date,
    int departemenId,
    String noPbj,
  ) async {
    return RequestServer<HistoryPermintaanBarangDanJasaResponse>()
        .requestServer(computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .kirimBarang(id, token, date, departemenId, noPbj);
      return response;
    });
  }

  Future<HistoryPermintaanBarangDanJasaResponse> terimaBarang(
      PostTerimaBarang postTerimaBarang, String token) async {
    return RequestServer<HistoryPermintaanBarangDanJasaResponse>()
        .requestServer(computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .terimaBarang(postTerimaBarang, token);
      return response;
    });
  }
}
