import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/core/util/enum.dart';

class HistoryPermintaanBarangDanJasaResponse {
  String? message;
  int? code;
  List<HistoryPermintaanBarangDanJasa>? data;

  HistoryPermintaanBarangDanJasaResponse({this.message, this.code, this.data});

  HistoryPermintaanBarangDanJasaResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    if (json['data'] != null) {
      data = <HistoryPermintaanBarangDanJasa>[];
      json['data'].forEach((v) {
        data!.add(HistoryPermintaanBarangDanJasa.fromJson(v));
      });
    }
  }
}

class HistoryPermintaanBarangDanJasa {
  int? id;
  String? noPbj;
  String? date;
  String? kualifikasi;
  String? status;
  String? catatan;
  String? approved1;
  String? approved1Date;
  String? approved2;
  String? approved2Date;
  String? approved3;
  String? approved3Date;
  String? approved4;
  String? approved4Date;
  String? approved5;
  String? approved5Date;
  String? approved6;
  String? approved6Date;
  String? kurir;
  String? fotoBuktiTerima;
  String? catatanDariPenerima;
  List<Barang>? barang;
  List<Jasa>? jasa;
  Approve? approve;

  HistoryPermintaanBarangDanJasa(
      {this.id,
      this.noPbj,
      this.date,
      this.kualifikasi,
      this.status,
      this.catatan,
      this.approved1,
      this.approved1Date,
      this.approved2,
      this.approved2Date,
      this.approved3,
      this.approved3Date,
      this.approved4,
      this.approved4Date,
      this.approved5,
      this.approved5Date,
      this.approved6,
      this.approved6Date,
      this.approve,
      this.kurir,
      this.fotoBuktiTerima,
      this.catatanDariPenerima,
      this.barang,
      this.jasa});

  HistoryPermintaanBarangDanJasa.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    noPbj = json['no_pbj'];
    date = json['date'];
    kualifikasi = json['kualifikasi'];
    status = json['status'];
    catatan = json['catatan'];
    approved1 = json['approved_1'];
    approved1Date = json['approved_1_date'];
    approved2 = json['approved_2'];
    approved2Date = json['approved_2_date'];
    approved3 = json['approved_3'];
    approved3Date = json['approved_3_date'];
    approved4 = json['approved_4'];
    approved4Date = json['approved_4_date'];
    approved5 = json['approved_5'];
    approved5Date = json['approved_5_date'];
    approved6 = json['approved_6'];
    approved6Date = json['approved_6_date'];
    kurir = json['kurir'];
    fotoBuktiTerima = json['foto_bukti_terima'];
    catatanDariPenerima = json['catatan_dari_penerima'];
    approve = Approve(
      status: status,
      approved1: approved1,
      approved1Date: approved1Date,
      approved2: approved2,
      approved2Date: approved2Date,
      approved3: approved3,
      approved3Date: approved3Date,
      approved4: approved4,
      approved4Date: approved4Date,
      approved5: approved5,
      approved5Date: approved5Date,
      approved6: approved6,
      approved6Date: approved6Date,
    );
    if (json['Barang'] != null) {
      barang = <Barang>[];
      json['Barang'].forEach((v) {
        barang!.add(Barang.fromJson(v));
      });
    }
    if (json['Jasa'] != null) {
      jasa = <Jasa>[];
      json['Jasa'].forEach((v) {
        jasa!.add(Jasa.fromJson(v));
      });
    }
  }
}

class Barang {
  int? id;
  int? idBarang;
  String? kodefikasi;
  String? kode;
  String? namaBarang;
  String? spesifikasi;
  String? satuan;
  int? jumlah;
  int? jumlahApproved;
  String? tglDigunakan;
  String? keterangan;
  int? isReject;
  int? isUpdateStok;
  String? paramStok;
  String? idKapalItemTangkiBbm;
  String? idKapalItemTangkiAir;
  int? idKapalItemPesawatPenggerak;

  Barang(
      {this.id,
      this.idBarang,
      this.kodefikasi,
      this.kode,
      this.namaBarang,
      this.spesifikasi,
      this.satuan,
      this.jumlah,
      this.jumlahApproved,
      this.tglDigunakan,
      this.keterangan,
      this.isReject,
      this.isUpdateStok,
      this.paramStok,
      this.idKapalItemTangkiBbm,
      this.idKapalItemTangkiAir,
      this.idKapalItemPesawatPenggerak});

  Barang.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idBarang = json['id_barang'];
    kodefikasi = json['kodefikasi'];
    kode = json['kode'];
    namaBarang = json['nama_barang'];
    spesifikasi = json['spesifikasi'];
    satuan = json['satuan'];
    jumlah = json['jumlah'];
    jumlahApproved = json['jumlah_approved'];
    tglDigunakan = json['tgl_digunakan'];
    keterangan = json['keterangan'];
    isReject = json['is_reject'];
    isUpdateStok = json['is_update_stok'];
    paramStok = json['param_stok'];
    idKapalItemTangkiBbm = json['id_kapal_item_tangki_bbm'];
    idKapalItemTangkiAir = json['id_kapal_item_tangki_air'];
    idKapalItemPesawatPenggerak = json['id_kapal_item_pesawat_penggerak'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['id_barang'] = idBarang;
    data['kodefikasi'] = kodefikasi;
    data['kode'] = kode;
    data['nama_barang'] = namaBarang;
    data['spesifikasi'] = spesifikasi;
    data['satuan'] = satuan;
    data['jumlah'] = jumlah;
    data['jumlah_approved'] = jumlahApproved;
    data['tgl_digunakan'] = tglDigunakan;
    data['keterangan'] = keterangan;
    data['is_reject'] = isReject;
    data['is_update_stok'] = isUpdateStok;
    data['param_stok'] =
        paramStok == ParameterStok.lainnya.name ? null : paramStok;
    data['id_kapal_item_tangki_bbm'] = idKapalItemTangkiBbm;
    data['id_kapal_item_tangki_air'] = idKapalItemTangkiAir;
    data['id_kapal_item_pesawat_penggerak'] = idKapalItemPesawatPenggerak;
    return data;
  }
}

class Jasa {
  int? id;
  String? jasa;
  String? penyebab;
  String? keterangan;
  int? isReject;

  Jasa({this.id, this.jasa, this.penyebab, this.keterangan, this.isReject});

  Jasa.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    jasa = json['jasa'];
    penyebab = json['penyebab'];
    keterangan = json['keterangan'];
    isReject = json['is_reject'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['jasa'] = jasa;
    data['penyebab'] = penyebab;
    data['keterangan'] = keterangan?.isEmpty ?? true ? "-" : keterangan;
    data['is_reject'] = isReject;
    return data;
  }
}
