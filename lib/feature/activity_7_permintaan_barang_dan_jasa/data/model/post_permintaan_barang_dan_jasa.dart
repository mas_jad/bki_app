import 'history_permintaan_barang_dan_jasa.dart';

class PostPermintaanBarangDanJasa {
  int? kapalId;
  int? departemenId;
  String? date;
  String? noPbj;
  String? kualifikasi;
  List<Barang>? barang;
  List<Jasa>? jasa;

  PostPermintaanBarangDanJasa(
      {this.kapalId,
      this.departemenId,
      this.date,
      this.noPbj,
      this.kualifikasi,
      this.barang,
      this.jasa});

  PostPermintaanBarangDanJasa.fromJson(Map<String, dynamic> json) {
    kapalId = json['kapal_id'];
    departemenId = json['departemen_id'];
    date = json['date'];
    noPbj = json['no_pbj'];

    kualifikasi = json['kualifikasi'];
    if (json['barang'] != null) {
      barang = <Barang>[];
      json['barang'].forEach((v) {
        barang!.add(Barang.fromJson(v));
      });
    }
    if (json['jasa'] != null) {
      jasa = <Jasa>[];
      json['jasa'].forEach((v) {
        jasa!.add(Jasa.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson(String? position) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kapal_id'] = kapalId;
    data['departemen_id'] = departemenId;
    data['date'] = date;
    data['no_pbj'] = noPbj;
    data["location"] = position;
    data['kualifikasi'] = kualifikasi;

    if (barang != null) {
      data['barang'] = barang!.map((v) => v.toJson()).toList();
    }
    if (jasa != null) {
      data['jasa'] = jasa!.map((v) => v.toJson()).toList();
    }

    return data;
  }
}
