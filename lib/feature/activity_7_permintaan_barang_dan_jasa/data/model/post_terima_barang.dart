import 'package:dio/dio.dart';

class PostTerimaBarang {
  String? noPbj;
  int? kapalId;
  int? departemenId;
  String? date;
  String? namaKurir;
  String? fotoBukti;
  String? catatanPenerima;
  int? isUpdateStok;
  int? idPesawatPenggerak;
  String? paramStok;

  PostTerimaBarang(
      {this.noPbj,
      this.kapalId,
      this.departemenId,
      this.date,
      this.namaKurir,
      this.fotoBukti,
      this.catatanPenerima,
      this.isUpdateStok,
      this.idPesawatPenggerak,
      this.paramStok});

  PostTerimaBarang.fromJson(Map<String, dynamic> json) {
    noPbj = json['no_pbj'];
    kapalId = json['kapal_id'];
    departemenId = json['departemen_id'];
    date = json['date'];
    namaKurir = json['nama_kurir'];
    fotoBukti = json['foto_bukti'];
    catatanPenerima = json['catatan_penerima'];
    isUpdateStok = json['is_update_stok'];
    idPesawatPenggerak = json['id_pesawat_penggerak'];
    paramStok = json['param_stok'];
  }

  Future<FormData> toJson(String? location) async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['no_pbj'] = noPbj;
    data['kapal_id'] = kapalId;
    data['departemen_id'] = departemenId;
    data['date'] = date;
    data['nama_kurir'] = namaKurir;
    data['is_update_stok'] = isUpdateStok;
    data['id_pesawat_penggerak'] = idPesawatPenggerak;
    data['param_stok'] = paramStok;
    data['foto_bukti'] = fotoBukti != null
        ? await MultipartFile.fromFile(
            fotoBukti!,
            filename: fotoBukti!.split('/').last,
          )
        : null;
    data['catatan_penerima'] = catatanPenerima;
    return FormData.fromMap(data);
  }
}
