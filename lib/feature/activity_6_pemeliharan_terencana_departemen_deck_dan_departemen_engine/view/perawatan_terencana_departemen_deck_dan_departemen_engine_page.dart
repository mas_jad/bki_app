import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/view/widget/information_dialog.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/view/widget/list_item_perawatan_widget.dart';

import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/locator.dart';
import '../../../core/util/user_role.dart';
import '../bloc/perawatan_terencana_departemen_deck_bloc.dart';
import '../data/repository/perawatan_terencana_repository.dart';

class PerawatanTerencanaDepartemenDeckDanDepartemenEnginePage
    extends StatefulWidget {
  const PerawatanTerencanaDepartemenDeckDanDepartemenEnginePage(
      {required this.dataKapal, super.key});

  static const route =
      "/activity_perawatan_terencana_departemen_deck_dan_departemen_engine_page";
  final DataKapal dataKapal;

  @override
  State<PerawatanTerencanaDepartemenDeckDanDepartemenEnginePage>
      createState() =>
          _PerawatanTerencanaDepartemenDeckDanDepartemenEnginePageState();
}

class _PerawatanTerencanaDepartemenDeckDanDepartemenEnginePageState
    extends State<PerawatanTerencanaDepartemenDeckDanDepartemenEnginePage>
    with SingleTickerProviderStateMixin {
  int? idDeck;
  int? idEngine;
  late ValueNotifier<bool> deckIsInTime;
  late ValueNotifier<bool> engineIsInTime;
  late TabController _tabController;

  final perawatanDeckBloc = PerawatanTerencanaBloc(
      locator<PerawatanTerencanaRepository>(), locator<UserRepository>());

  final perawatanEngineBloc = PerawatanTerencanaBloc(
      locator<PerawatanTerencanaRepository>(), locator<UserRepository>());

  @override
  void initState() {
    super.initState();

    // GET DATE
    deckIsInTime = ValueNotifier(true);
    engineIsInTime = ValueNotifier(true);

    // ADD EVENT
    perawatanDeckBloc.add(GetHistoryPerawatanTerencanaEvent(
        widget.dataKapal.idKapal ?? -1, true, true));
    perawatanEngineBloc.add(GetHistoryPerawatanTerencanaEvent(
        widget.dataKapal.idKapal ?? -1, true, false));

    final initialIndex = locator<UserRole>().isEngine() ? 1 : 0;
    _tabController =
        TabController(length: 2, vsync: this, initialIndex: initialIndex);
  }

  @override
  void dispose() {
    perawatanDeckBloc.close();
    perawatanEngineBloc.close();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return const InformationDialog();
                      });
                },
                icon: const Icon(
                  Icons.info_outline_rounded,
                  color: white,
                ))
          ],
          backgroundColor: secondary,
          bottom: TabBar(
            indicatorWeight: 2.5,
            controller: _tabController,
            indicatorColor: primaryAccent,
            tabs: [
              Tab(
                child: Text(
                  "Deck",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
              Tab(
                child: Text(
                  "Engine",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
            ],
          ),
          elevation: 0,
          title: Text(
            "Perawatan Terencana",
            style: ptSansTextStyle.copyWith(color: white),
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            BlocProvider.value(
              value: perawatanDeckBloc,
              child: ListItemPerawatanWidget(
                isDeck: true,
                kapalId: widget.dataKapal.idKapal ?? -1,
                isInTime: deckIsInTime,
              ),
            ),
            BlocProvider.value(
              value: perawatanEngineBloc,
              child: ListItemPerawatanWidget(
                isDeck: false,
                kapalId: widget.dataKapal.idKapal ?? -1,
                isInTime: engineIsInTime,
              ),
            )
          ],
        ),
      ),
    );
  }
}
