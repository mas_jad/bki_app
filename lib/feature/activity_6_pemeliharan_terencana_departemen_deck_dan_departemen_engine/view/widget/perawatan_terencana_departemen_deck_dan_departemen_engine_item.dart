import 'package:flutter/material.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/shadow.dart';
import 'package:pms_pcm/core/theme/size.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/core/widget/other/custom_image_network.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';

import '../../../../core/data/model/approve.dart';

class PerawatanTerencanaDepartemenDeckDanDepartemenEngineItem
    extends StatelessWidget {
  const PerawatanTerencanaDepartemenDeckDanDepartemenEngineItem(
      {required this.callback,
      required this.title,
      this.icon,
      required this.image,
      required this.itemImage,
      required this.nextPerawatan,
      required this.approve,
      super.key});
  final Function(BuildContext context) callback;
  final String title;
  final Icon? icon;
  final String? image;
  final String itemImage;
  final String? nextPerawatan;
  final Approve approve;

  @override
  Widget build(BuildContext context) {
    return PopWidget(
        onTap: () {
          callback(context);
        },
        child: Container(
          margin: const EdgeInsets.only(bottom: 16),
          decoration: const BoxDecoration(
            color: white,
            boxShadow: [shadowCard],
            borderRadius: cardBorderRadius,
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Stack(
                    children: [
                      Container(
                        width: 120,
                        height: 120,
                        decoration: const BoxDecoration(
                          color: secondary,
                          borderRadius: cardBorderRadius,
                        ),
                      ),
                      if (image != null)
                        CustomImageNetwork(
                            width: 120,
                            height: 120,
                            borderRadius: cardBorderRadius,
                            imageUrl: image),
                      Container(
                        width: 120,
                        height: 120,
                        padding: const EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(itemImage), fit: BoxFit.cover),
                          borderRadius: cardBorderRadius,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          title,
                          style: ptSansTextStyle.copyWith(
                            color: black54,
                            fontWeight: bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Next : \n${locator<MyDate>().getDateWithInputStringIndonesiaFormat(nextPerawatan) ?? "Belum Ditentukan"}",
                          style: ptSansTextStyle.copyWith(
                              color: black38, fontWeight: medium),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Text(
                              approve.getStatus(),
                              style: ptSansTextStyle.copyWith(
                                  color: statusColor(approve.getStatus()),
                                  fontWeight: semiBold,
                                  fontSize: 14),
                            ),
                            if (approve.getName() != null)
                              Flexible(
                                child: Text(
                                  " by ${approve.getName()}",
                                  style: ptSansTextStyle.copyWith(
                                    color: lightText,
                                    fontWeight: reguler,
                                    fontSize: 14,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
