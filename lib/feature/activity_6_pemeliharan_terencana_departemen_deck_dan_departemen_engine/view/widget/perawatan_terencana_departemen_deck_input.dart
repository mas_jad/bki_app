import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/bloc/perawatan_terencana_departemen_deck_bloc.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/history_perawatan_terencana.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/history_perawatan_terencana_departemen_deck.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/history_perawatan_terencana_departemen_engine.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/post_perawatan_terencana_departemen_deck.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/post_perawatan_terencana_departemen_engine.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/service/date/date.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/util/const.dart';
import '../../../../core/util/locator.dart';
import '../../../../core/util/toast.dart';
import '../../../../core/util/user_role.dart';
import '../../../../core/widget/button/custom_button_widget.dart';
import '../../../../core/widget/dialog/custom_alert_dialog.dart';
import '../../../../core/widget/image/image_container_picker.dart';
import '../../../../core/widget/other/loading_widget.dart';
import '../../../../core/widget/textfield/custom_textfield_widget.dart';

class PerawatanTerencanaInput extends StatefulWidget {
  const PerawatanTerencanaInput(
      {required this.historyPerawatanTerencana,
      required this.deskripsiSptId,
      required this.kapalId,
      required this.isDateToSaveNotExpired,
      required this.isDeck,
      required this.isInTime,
      required this.isCannotApprove,
      super.key});

  final HistoryPerawatanTerencana historyPerawatanTerencana;
  final int kapalId;
  final int deskripsiSptId;
  final bool isDateToSaveNotExpired;
  final bool isDeck;
  final bool isInTime;
  final bool isCannotApprove;

  @override
  State<PerawatanTerencanaInput> createState() =>
      _PerawatanTerencanaInputState();
}

class _PerawatanTerencanaInputState extends State<PerawatanTerencanaInput> {
  final TextEditingController keteranganEditingController =
      TextEditingController();
  final TextEditingController periodeInspeksiEditingController =
      TextEditingController();
  final TextEditingController tanggalAkanDatangController =
      TextEditingController();
  final TextEditingController tanggalTerakhirDiCekController =
      TextEditingController();

  bool isInsulation = false;

  final TextEditingController jamKerjaController = TextEditingController();
  final TextEditingController rController = TextEditingController();
  final TextEditingController sController = TextEditingController();
  final TextEditingController tController = TextEditingController();

  late ValueNotifier<String?> imageValueNotifier;
  late Approve approve;
  bool isUserPermitedToApprove = false;
  bool isUserPermitedToSave = false;

  @override
  void initState() {
    super.initState();

    final history = widget.historyPerawatanTerencana;

    if (history is HistoryPerawatanTerencanaDepartemenDeck) {
    } else if (history is HistoryPerawatanTerencanaDepartemenEngine) {
      isInsulation = history.insulationResistance == "true" ? true : false;

      if (isInsulation) {
        rController.text = history.r?.toString() ?? empty;
        sController.text = history.s?.toString() ?? empty;
        tController.text = history.t?.toString() ?? empty;
      } else {
        jamKerjaController.text = history.jamKerja?.toString() ?? empty;
      }
    }

    approve = widget.historyPerawatanTerencana.approve ?? Approve();
    keteranganEditingController.text =
        widget.historyPerawatanTerencana.keterangan ?? empty;
    tanggalAkanDatangController.text = locator<MyDate>()
            .getDateWithInputStringIndonesiaFormat(
                widget.historyPerawatanTerencana.dateBerikutnya) ??
        "Belum Ditentukan";
    tanggalTerakhirDiCekController.text = locator<MyDate>()
            .getDateWithInputStringIndonesiaFormat(
                widget.historyPerawatanTerencana.date) ??
        "Belum Ditentukan";
    periodeInspeksiEditingController.text =
        "${widget.historyPerawatanTerencana.periodeInspeksi} Hari";
    imageValueNotifier = ValueNotifier(null);

    isUserPermitedToApprove = widget.isCannotApprove
        ? false
        : widget.historyPerawatanTerencana.approve == null
            ? false
            : locator<UserRole>().isUserPermitedToApprove(
                widget.historyPerawatanTerencana.approve!);

    isUserPermitedToSave = locator<UserRole>().isUserPermitedToSave(
        widget.historyPerawatanTerencana.approve ?? Approve(),
        widget.isDateToSaveNotExpired);
  }

  @override
  void dispose() {
    keteranganEditingController.dispose();
    tanggalAkanDatangController.dispose();
    tanggalTerakhirDiCekController.dispose();
    periodeInspeksiEditingController.dispose();
    rController.dispose();
    sController.dispose();
    tController.dispose();
    jamKerjaController.dispose();
    imageValueNotifier.dispose();
    super.dispose();
  }

  postPerawatanTerencana() async {
    if (widget.isDeck) {
      BlocProvider.of<PerawatanTerencanaBloc>(context).add(
          PostPerawatanTerencanaEvent(PostPerawatanTerencanaDepartemenDeck(
              kapalId: widget.kapalId,
              sptId: widget.historyPerawatanTerencana.sptId,
              deskripsiSptId: widget.deskripsiSptId,
              isInTime: widget.isInTime,
              keterangan: keteranganEditingController.text,
              image: imageValueNotifier.value)));
    } else {
      BlocProvider.of<PerawatanTerencanaBloc>(context).add(
          PostPerawatanTerencanaEvent(PostPerawatanTerencanaDepartemenEngine(
              kapalId: widget.kapalId,
              sptId: widget.historyPerawatanTerencana.sptId,
              deskripsiSptId: widget.deskripsiSptId,
              keterangan: keteranganEditingController.text,
              isInTime: widget.isInTime,
              s: sController.text.isNotEmpty
                  ? int.tryParse(sController.text)
                  : null,
              r: rController.text.isNotEmpty
                  ? int.tryParse(rController.text)
                  : null,
              t: tController.text.isNotEmpty
                  ? int.tryParse(tController.text)
                  : null,
              jamKerja: jamKerjaController.text.isNotEmpty
                  ? int.tryParse(jamKerjaController.text)
                  : null,
              image: imageValueNotifier.value)));
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocListener<PerawatanTerencanaBloc, PerawatanTerencanaState>(
      listener: (context, state) {
        if (state is PostPerawatanTerencanaSuccess) {
          MyToast.showSuccess(success, context);
          Navigator.pop(context);
        } else if (state is PostPerawatanTerencanaError) {
          MyToast.showError(state.message, context);
        }

        if (state is UpdateStatusPerawatanTerencanaSuccess) {
          MyToast.showSuccess(success, context);
          Navigator.pop(context);
        } else if (state is UpdateStatusPerawatanTerencanaError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
              width: width,
              decoration: const BoxDecoration(
                  color: white,
                  boxShadow: [shadowCard],
                  borderRadius: cardBorderRadius),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(20),
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(itemImage2), fit: BoxFit.cover),
                        color: secondary,
                        borderRadius: BorderRadius.all(Radius.circular(18))),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                                onPressed: (() {
                                  Navigator.pop(context);
                                }),
                                icon: iconArrow.copyWith(color: white)),
                            Flexible(
                              child: Text(
                                widget.historyPerawatanTerencana.nama ?? empty,
                                style: ptSansTextStyle.copyWith(
                                    color: white,
                                    fontWeight: bold,
                                    fontSize: 22),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(8),
                                child: iconArrow.copyWith(color: transparent)),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20, bottom: 10),
                          child: Center(
                              child: ImageContainerPicker(
                            image: widget.historyPerawatanTerencana.image,
                            imageValueNotifier: imageValueNotifier,
                          )),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              boxShadow: const [shadowCard],
                              color: white,
                              borderRadius: BorderRadius.circular(20)),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "DESKRIPSI :  ",
                                    style: ptSansTextStyle.copyWith(
                                        color: black54,
                                        fontWeight: medium,
                                        fontSize: 14),
                                  ),
                                  Flexible(
                                    child: Text(
                                      widget.historyPerawatanTerencana
                                              .deskripsi ??
                                          empty,
                                      style: ptSansTextStyle.copyWith(
                                          color: primary,
                                          fontWeight: medium,
                                          fontSize: 15),
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "KATEGORI  :  ",
                                    style: ptSansTextStyle.copyWith(
                                        color: black54,
                                        fontWeight: medium,
                                        fontSize: 14),
                                  ),
                                  Flexible(
                                    child: Text(
                                      widget.historyPerawatanTerencana
                                              .kategori ??
                                          empty,
                                      style: ptSansTextStyle.copyWith(
                                          color: secondaryAccent,
                                          fontWeight: medium,
                                          fontSize: 15),
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "STATUS      :  ",
                                    style: ptSansTextStyle.copyWith(
                                        color: black54,
                                        fontWeight: medium,
                                        fontSize: 14),
                                  ),
                                  Text(
                                    approve.getStatus(),
                                    style: ptSansTextStyle.copyWith(
                                        color: statusColor(approve.getStatus()),
                                        fontWeight: semiBold,
                                        fontSize: 14),
                                  ),
                                  if (approve.getName() != null)
                                    Flexible(
                                      child: Text(
                                        " by ${approve.getName()}",
                                        style: ptSansTextStyle.copyWith(
                                          color: lightText,
                                          fontWeight: reguler,
                                          fontSize: 14,
                                        ),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        // ALL
                        CustomTextFieldWidget(
                          label: "Periode Inspeksi",
                          isEnable: false,
                          textEditingController:
                              periodeInspeksiEditingController,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        // ALL
                        CustomTextFieldWidget(
                          label: "Tanggal Terakhir di Cek",
                          isEnable: false,
                          textEditingController: tanggalTerakhirDiCekController,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        // ALL
                        CustomTextFieldWidget(
                          label: "Tanggal Pengecekan Kembali",
                          isEnable: false,
                          textEditingController: tanggalAkanDatangController,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        // ENGINE
                        if (!widget.isDeck && !isInsulation)
                          CustomTextFieldWidget(
                            label: "Jam Kerja",
                            textEditingController: jamKerjaController,
                          ),
                        if (!widget.isDeck && !isInsulation)
                          const SizedBox(
                            height: 16,
                          ),
                        if (!widget.isDeck && isInsulation)
                          Row(
                            children: [
                              // ENGINE && INSULATION
                              Flexible(
                                child: CustomTextFieldWidget(
                                  label: "R",
                                  textEditingController: rController,
                                ),
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                              Flexible(
                                child: CustomTextFieldWidget(
                                  label: "S",
                                  textEditingController: sController,
                                ),
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                              Flexible(
                                child: CustomTextFieldWidget(
                                  label: "T",
                                  textEditingController: tController,
                                ),
                              ),
                            ],
                          ),

                        if (!widget.isDeck && isInsulation)
                          const SizedBox(
                            height: 16,
                          ),
                        // ALL
                        CustomTextFieldWidget(
                          label: "Keadaan",
                          textEditingController: keteranganEditingController,
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        BlocBuilder<PerawatanTerencanaBloc,
                            PerawatanTerencanaState>(
                          builder: (context, state) {
                            if (isUserPermitedToSave) {
                              return CustomButtonWidget(
                                  isActive:
                                      state is PostPerawatanTerencanaLoading
                                          ? false
                                          : true,
                                  callback: () {
                                    postPerawatanTerencana();
                                  },
                                  title: save);
                            } else {
                              return const SizedBox();
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 80,
                  ),
                ],
              )),
        ),
        // FLOATING ACTION BUTTON FOR UPDATE STATUS
        floatingActionButton:
            BlocBuilder<PerawatanTerencanaBloc, PerawatanTerencanaState>(
          builder: (context, state) {
            if (isUserPermitedToApprove) {
              if (state is UpdateStatusPerawatanTerencanaLoading) {
                return FloatingActionButton(
                  elevation: elevationFAB,
                  backgroundColor: secondary,
                  onPressed: () {},
                  child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                itemImage4,
                              ),
                              fit: BoxFit.fill)),
                      child: const Center(
                          child: LoadingWidget(
                        color: white,
                      ))),
                );
              }

              return FloatingActionButton(
                elevation: elevationFAB,
                backgroundColor: primary,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: ((dialogContext) => BlocProvider.value(
                            value: BlocProvider.of<PerawatanTerencanaBloc>(
                                context),
                            child: CustomAlertDialog(
                                description: dataCannotChangeAgain,
                                title: locator<UserRole>().getTitle(approve),
                                voidCallback: () {
                                  BlocProvider.of<
                                          PerawatanTerencanaBloc>(context)
                                      .add(UpdateStatusPerawatanTerencanaEvent(
                                          widget.kapalId,
                                          widget.historyPerawatanTerencana
                                                  .sptId ??
                                              -1,
                                          approve,
                                          widget.isDeck,
                                          widget.isInTime));
                                }),
                          )));
                },
                child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              itemImage4,
                            ),
                            fit: BoxFit.fill)),
                    child: Center(child: locator<UserRole>().getIcon(approve))),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}
