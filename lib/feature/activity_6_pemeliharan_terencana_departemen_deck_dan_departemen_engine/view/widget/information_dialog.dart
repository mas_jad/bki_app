import 'package:flutter/material.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/widget/Other/pop_widget.dart';

class InformationDialog extends StatelessWidget {
  const InformationDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Info",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: black68, fontSize: 22),
                ),
                PopWidget(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: const Icon(
                      Icons.close_rounded,
                      color: black26,
                    ))
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              children: [
                Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                      color: red,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                ),
                const SizedBox(
                  width: 12,
                ),
                Flexible(
                  child: Text(
                    "Sudah Waktunya Perawatan",
                    style:
                        ptSansTextStyle.copyWith(color: black54, fontSize: 15),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                      color: Color(0xFFFFF500),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                ),
                const SizedBox(
                  width: 12,
                ),
                Flexible(
                  child: Text(
                    "Mendekati Waktu Perawatan",
                    style:
                        ptSansTextStyle.copyWith(color: black54, fontSize: 15),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                      color: Color(0xFF33FF00),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                ),
                const SizedBox(
                  width: 12,
                ),
                Flexible(
                  child: Text(
                    "Dalam Proses Perawatan",
                    style:
                        ptSansTextStyle.copyWith(color: black54, fontSize: 15),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                      color: Color(0xFF1F43C3),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                ),
                const SizedBox(
                  width: 12,
                ),
                Flexible(
                  child: Text(
                    "Belum Masuk Waktu Perawatan",
                    style:
                        ptSansTextStyle.copyWith(color: black54, fontSize: 15),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Container(
                  height: 20,
                  width: 20,
                  decoration: const BoxDecoration(
                      color: Color(0xFFB4B2B0),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                ),
                const SizedBox(
                  width: 12,
                ),
                Flexible(
                  child: Text(
                    "Waktu Perawatan Belum Ditentukan",
                    style:
                        ptSansTextStyle.copyWith(color: black54, fontSize: 15),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
