import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/core/widget/other/loading_widget.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/bloc/perawatan_terencana_departemen_deck_bloc.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/view/widget/perawatan_terencana_departemen_deck_dan_departemen_engine_item.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/view/widget/perawatan_terencana_departemen_deck_input.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/util/const.dart';
import '../../../../core/widget/other/error_layout.dart';
import '../../data/model/history_perawatan_terencana.dart';

class ListItemPerawatanWidget extends StatefulWidget {
  const ListItemPerawatanWidget(
      {required this.kapalId,
      required this.isDeck,
      required this.isInTime,
      super.key});

  final int kapalId;
  final bool isDeck;
  final ValueNotifier<bool> isInTime;

  @override
  State<ListItemPerawatanWidget> createState() =>
      _ListItemPerawatanWidgetState();
}

class _ListItemPerawatanWidgetState extends State<ListItemPerawatanWidget> {
  getImageMessage(String message) {
    if (message == "1" || message == "2") {
      return itemImage1l;
    } else if (message == "3") {
      return itemImage1e;
    } else if (message == "4") {
      return itemImage1s;
    } else if (message == "0") {
      return itemImage1n;
    } else {
      return itemImage1q;
    }
  }

  bool isDateToSaveNotExpired(String message) {
    if (message == "1" || message == "2") {
      return true;
    } else if (message == "3") {
      return true;
    } else if (message == "4") {
      return true;
    } else if (message == "0") {
      return false;
    } else {
      return true;
    }
  }

  bool isCannotApprove(String message) {
    if (message == "1" ||
        message == "2" ||
        message == "3" ||
        message == empty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: BlocBuilder<PerawatanTerencanaBloc, PerawatanTerencanaState>(
            builder: (context, state) {
          if (state is HistoryPerawatanTerencanaSuccess) {
            var list = state.historyPerawatanTerencanaResponse?.data ?? [];

            return Column(
              children: [
                SizedBox(
                  height: 60,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      PopWidget(
                        onTap: () {
                          widget.isInTime.value = false;
                          BlocProvider.of<PerawatanTerencanaBloc>(context).add(
                              GetHistoryPerawatanTerencanaEvent(
                                  widget.kapalId, false, widget.isDeck));
                        },
                        child: ValueListenableBuilder(
                            valueListenable: widget.isInTime,
                            builder: (context, value, child) {
                              return Chip(
                                  backgroundColor: white,
                                  side: BorderSide(
                                      color: widget.isInTime.value
                                          ? grey
                                          : primaryAccent),
                                  label: Text(
                                    "Semua Bagian",
                                    style: ptSansTextStyle.copyWith(
                                        color: widget.isInTime.value
                                            ? grey
                                            : primaryAccent),
                                  ));
                            }),
                      ),
                      const SizedBox(
                        width: 6,
                      ),
                      PopWidget(
                        onTap: () {
                          widget.isInTime.value = true;
                          BlocProvider.of<PerawatanTerencanaBloc>(context).add(
                              GetHistoryPerawatanTerencanaEvent(
                                  widget.kapalId, true, widget.isDeck));
                        },
                        child: ValueListenableBuilder(
                            valueListenable: widget.isInTime,
                            builder: (context, value, child) {
                              return Chip(
                                  backgroundColor: white,
                                  side: BorderSide(
                                      color: widget.isInTime.value
                                          ? quaternaryAccent
                                          : grey),
                                  label: Text(
                                    "Bagian yang Sudah Masuk Waktu Perawatan",
                                    style: ptSansTextStyle.copyWith(
                                        color: widget.isInTime.value
                                            ? quaternaryAccent
                                            : grey),
                                  ));
                            }),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: list.isNotEmpty
                      ? ListView.builder(
                          padding: const EdgeInsets.all(20),
                          itemCount: list.length,
                          itemBuilder: (BuildContext ctx, index) {
                            final HistoryPerawatanTerencana item = list[index];

                            return PerawatanTerencanaDepartemenDeckDanDepartemenEngineItem(
                                approve: item.approve ?? Approve(),
                                nextPerawatan: item.dateBerikutnya,
                                itemImage:
                                    getImageMessage(item.message ?? empty),
                                callback: (context) {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context2) {
                                    return BlocProvider.value(
                                      value: BlocProvider.of<
                                          PerawatanTerencanaBloc>(context),
                                      child: PerawatanTerencanaInput(
                                        isDeck: widget.isDeck,
                                        isCannotApprove: isCannotApprove(
                                            item.message ?? empty),
                                        isInTime: widget.isInTime.value,
                                        deskripsiSptId: item.deskripsiId ?? -1,
                                        isDateToSaveNotExpired:
                                            isDateToSaveNotExpired(
                                                item.message ?? empty),
                                        kapalId: widget.kapalId,
                                        historyPerawatanTerencana: item,
                                      ),
                                    );
                                  }));
                                },
                                title: item.nama ?? empty,
                                image: item.image);
                          },
                        )
                      : const ErrorLayout(message: dataEmpty),
                ),
              ],
            );
          } else if (state is HistoryPerawatanTerencanaError) {
            return ErrorLayout(message: state.message);
          } else {
            return const LoadingWidget();
          }
        }));
  }
}
