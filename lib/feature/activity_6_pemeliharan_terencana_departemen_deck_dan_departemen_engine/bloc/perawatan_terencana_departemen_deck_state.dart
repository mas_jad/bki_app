part of 'perawatan_terencana_departemen_deck_bloc.dart';

abstract class PerawatanTerencanaState extends Equatable {
  const PerawatanTerencanaState();

  @override
  List<Object> get props => [];
}

class PerawatanTerencanaInitial extends PerawatanTerencanaState {}

// HISTORY
abstract class HistoryPerawatanTerencanaState extends PerawatanTerencanaState {}

class HistoryPerawatanTerencanaSuccess extends HistoryPerawatanTerencanaState {
  final HistoryPerawatanTerencanaResponse? historyPerawatanTerencanaResponse;
  HistoryPerawatanTerencanaSuccess(this.historyPerawatanTerencanaResponse);
}

class HistoryPerawatanTerencanaError extends HistoryPerawatanTerencanaState {
  final String message;
  HistoryPerawatanTerencanaError(this.message);
}

class HistoryPerawatanTerencanaLoading extends HistoryPerawatanTerencanaState {}

// POST
abstract class PostPerawatanTerencanaState extends PerawatanTerencanaState {}

class PostPerawatanTerencanaSuccess extends PostPerawatanTerencanaState {}

class PostPerawatanTerencanaError extends PostPerawatanTerencanaState {
  final String message;
  PostPerawatanTerencanaError(this.message);
}

class PostPerawatanTerencanaLoading extends PostPerawatanTerencanaState {}

// UPDATE STATUS
abstract class UpdateStatusPerawatanTerencanaState
    extends PerawatanTerencanaState {}

class UpdateStatusPerawatanTerencanaSuccess
    extends UpdateStatusPerawatanTerencanaState {}

class UpdateStatusPerawatanTerencanaError
    extends UpdateStatusPerawatanTerencanaState {
  final String message;
  UpdateStatusPerawatanTerencanaError(this.message);
}

class UpdateStatusPerawatanTerencanaLoading
    extends UpdateStatusPerawatanTerencanaState {}
