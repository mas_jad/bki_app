import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/post_perawatan_terencana.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_perawatan_terencana.dart';
import '../data/repository/perawatan_terencana_repository.dart';

part 'perawatan_terencana_departemen_deck_event.dart';
part 'perawatan_terencana_departemen_deck_state.dart';

class PerawatanTerencanaBloc
    extends Bloc<PerawatanTerencanaEvent, PerawatanTerencanaState> {
  final PerawatanTerencanaRepository _perawatanTerencanaRepository;
  final UserRepository _userRepository;

  PerawatanTerencanaBloc(
      this._perawatanTerencanaRepository, this._userRepository)
      : super(PerawatanTerencanaInitial()) {
    on<PostPerawatanTerencanaEvent>((event, emit) async {
      try {
        emit(PostPerawatanTerencanaLoading());
        final token = await _userRepository.getToken();
        final data = await _perawatanTerencanaRepository.postPerawatanTerencana(
          token,
          event.postPerawatanTerencana,
        );

        emit(PostPerawatanTerencanaSuccess());
        emit(HistoryPerawatanTerencanaSuccess(
          data,
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostPerawatanTerencanaError(e.message));
        } else {
          emit(PostPerawatanTerencanaError(somethingErrorHappen));
        }
      }
    });

    on<GetHistoryPerawatanTerencanaEvent>((event, emit) async {
      try {
        emit(HistoryPerawatanTerencanaLoading());
        final token = await _userRepository.getToken();
        final data =
            await _perawatanTerencanaRepository.getHistoryPerawatanTerencana(
                event.id, token, event.isInTime, event.isDeck);

        emit(HistoryPerawatanTerencanaSuccess(
          data,
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryPerawatanTerencanaError(e.message));
        } else {
          emit(HistoryPerawatanTerencanaError(somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusPerawatanTerencanaEvent>((event, emit) async {
      try {
        emit(UpdateStatusPerawatanTerencanaLoading());
        final token = await _userRepository.getToken();
        final data =
            await _perawatanTerencanaRepository.updateStatusPerawatanTerencana(
                event.id,
                event.sptId,
                token,
                event.approve,
                event.isDeck,
                event.isInTime);

        emit(UpdateStatusPerawatanTerencanaSuccess());
        emit(HistoryPerawatanTerencanaSuccess(
          data,
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(UpdateStatusPerawatanTerencanaError(e.message));
        } else {
          emit(UpdateStatusPerawatanTerencanaError(somethingErrorHappen));
        }
      }
    });
  }
}
