part of 'perawatan_terencana_departemen_deck_bloc.dart';

abstract class PerawatanTerencanaEvent extends Equatable {
  const PerawatanTerencanaEvent();

  @override
  List<Object> get props => [];
}

class PostPerawatanTerencanaEvent extends PerawatanTerencanaEvent {
  final PostPerawatanTerencana postPerawatanTerencana;
  const PostPerawatanTerencanaEvent(this.postPerawatanTerencana);
}

class UpdateStatusPerawatanTerencanaEvent extends PerawatanTerencanaEvent {
  final int id;
  final Approve approve;
  final int sptId;
  final bool isDeck;
  final bool isInTime;
  const UpdateStatusPerawatanTerencanaEvent(
      this.id, this.sptId, this.approve, this.isDeck, this.isInTime);
}

class GetHistoryPerawatanTerencanaEvent extends PerawatanTerencanaEvent {
  final int id;
  final bool isInTime;
  final bool isDeck;
  const GetHistoryPerawatanTerencanaEvent(this.id, this.isInTime, this.isDeck);
}
