import '../../../../core/data/model/approve.dart';

abstract class HistoryPerawatanTerencanaResponse {
  List<HistoryPerawatanTerencana>? data;
  String? message;
  int? code;

  Map<String, dynamic> toJson();
}

abstract class HistoryPerawatanTerencana {
  Map<String, dynamic> toJson();

  String? statusProses;
  String? approved1;
  String? approved2;
  String? approved3;
  String? approved4;
  Approve? approve;
  int? deskripsiId;
  String? deskripsi;
  int? kategoriId;
  String? kategori;
  int? sptId;
  String? nama;
  String? message;
  String? image;
  int? periodeInspeksi;
  String? date;
  String? dateBerikutnya;
  String? keterangan;
}
