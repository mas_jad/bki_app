import 'package:dio/dio.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/post_perawatan_terencana.dart';

class PostPerawatanTerencanaDepartemenDeck extends PostPerawatanTerencana {
  int? kapalId;
  int? sptId;
  int? deskripsiSptId;
  String? keterangan;
  String? image;
  bool? isInTime;

  PostPerawatanTerencanaDepartemenDeck({
    this.kapalId,
    this.sptId,
    this.deskripsiSptId,
    this.keterangan,
    this.image,
    this.isInTime,
  });

  Future<FormData> toJson(String? position) async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kapal_id'] = kapalId;
    data['spt_id'] = sptId;
    data['deskripsi_spt_id'] = deskripsiSptId;
    data['keterangan'] = keterangan;
    data['image'] = image != null
        ? await MultipartFile.fromFile(
            image!,
            filename: image!.split('/').last,
          )
        : null;
    data['location'] = position;
    data['tanggal_tiba_perawatan'] = (isInTime ?? false) ? isInTime : null;
    data.removeWhere((key, value) => value == null);
    return FormData.fromMap(data);
  }
}
