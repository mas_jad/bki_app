import 'package:dio/dio.dart';

import 'post_perawatan_terencana.dart';

class PostPerawatanTerencanaDepartemenEngine extends PostPerawatanTerencana {
  int? kapalId;
  int? deskripsiSptId;
  int? sptId;
  int? jamKerja;
  int? r;
  int? s;
  int? t;
  String? keterangan;
  String? image;
  bool? isInTime;

  PostPerawatanTerencanaDepartemenEngine({
    this.kapalId,
    this.deskripsiSptId,
    this.sptId,
    this.jamKerja,
    this.r,
    this.s,
    this.t,
    this.keterangan,
    this.image,
    this.isInTime,
  });

  Future<FormData> toJson(String? position) async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kapal_id'] = kapalId;
    data['deskripsi_spt_id'] = deskripsiSptId;
    data['spt_id'] = sptId;
    data['jam_kerja'] = jamKerja;
    data['r'] = r;
    data['s'] = s;
    data['t'] = t;
    data['keterangan'] = keterangan;
    data['image'] = image != null
        ? await MultipartFile.fromFile(
            image!,
            filename: image!.split('/').last,
          )
        : null;
    data['location'] = position;
    data['tanggal_tiba_perawatan'] = isInTime;

    return FormData.fromMap(data);
  }
}
