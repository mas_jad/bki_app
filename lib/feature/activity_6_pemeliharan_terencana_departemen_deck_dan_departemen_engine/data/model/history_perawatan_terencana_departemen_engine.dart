import '../../../../core/data/model/approve.dart';
import 'history_perawatan_terencana.dart';

class HistoryPerawatanTerencanaDepartemenEngineResponse
    extends HistoryPerawatanTerencanaResponse {
  HistoryPerawatanTerencanaDepartemenEngineResponse.fromJson(
      Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    if (json['data'] != null) {
      data = <HistoryPerawatanTerencanaDepartemenEngine>[];
      json['data'].forEach((v) {
        data!.add(HistoryPerawatanTerencanaDepartemenEngine.fromJson(v));
      });
    }
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HistoryPerawatanTerencanaDepartemenEngine
    extends HistoryPerawatanTerencana {
  int? jamKerja;
  int? r;
  int? s;
  int? t;
  String? insulationResistance;

  HistoryPerawatanTerencanaDepartemenEngine.fromJson(
      Map<String, dynamic> json) {
    deskripsiId = json['deskripsi_id'];
    deskripsi = json['deskripsi'];
    kategoriId = json['kategori_id'];
    kategori = json['kategori'];
    sptId = json['spt_id'];
    nama = json['nama'];
    periodeInspeksi = json['periode_inspeksi'];
    date = json['date'];
    dateBerikutnya = json['date_berikutnya'];
    keterangan = json['keterangan'];
    image = json['image'];
    jamKerja = json['jam_kerja'];
    r = json['r'];
    s = json['s'];
    t = json['t'];
    insulationResistance = json['insulation_resistance'];
    message = json['message'];
    statusProses = json['status_proses'];
    approved1 = json['approved_1'];
    approved2 = json['approved_2'];
    approved3 = json['approved_3'];
    approved4 = json['approved_4'];
    approve = Approve(
      approved1: approved1,
      approved2: approved2,
      approved3: approved3,
      approved4: approved4,
    );
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['deskripsi_id'] = deskripsiId;
    data['deskripsi'] = deskripsi;
    data['kategori_id'] = kategoriId;
    data['kategori'] = kategori;
    data['spt_id'] = sptId;
    data['nama'] = nama;
    data['periode_inspeksi'] = periodeInspeksi;
    data['date'] = date;
    data['date_berikutnya'] = dateBerikutnya;
    data['keterangan'] = keterangan;
    data['image'] = image;
    data['jam_kerja'] = jamKerja;
    data['r'] = r;
    data['s'] = s;
    data['t'] = t;
    data['insulation_resistance'] = insulationResistance;
    data['message'] = message;
    data['status_proses'] = statusProses;
    data['approved_1'] = approved1;
    data['approved_2'] = approved2;
    data['approved_3'] = approved3;
    data['approved_4'] = approved4;
    return data;
  }
}
