import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/service/location/location_service.dart';
import '../../../../core/util/user_role.dart';
import '../model/history_perawatan_terencana_departemen_deck.dart';
import '../model/post_perawatan_terencana_departemen_deck.dart';

class PerawatanTerencanaDepartemenDeckRemoteDataSource {
  Future<HistoryPerawatanTerencanaDepartemenDeckResponse>
      getHistoryPerawatanTerencanaDepartemenDeck(
          int id, bool isInTime, String token) async {
    final endpoint = dotenv.get(dataPTDPelaporanEndpoint);

    final response = await locator<Dio>().post(
      endpoint,
      data: {
        "kapal_id": id,
        "tanggal_tiba_perawatan": isInTime,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPerawatanTerencanaDepartemenDeckResponse.fromJson(
        response.data);
  }

  Future<HistoryPerawatanTerencanaDepartemenDeckResponse>
      postPerawatanTerencanaDepartemenDeck(
    String token,
    PostPerawatanTerencanaDepartemenDeck perawatanTerencanaDepartemenDeck,
  ) async {
    final endpoint = dotenv.get(simpanDataPTDEndpoint);
    final position = await locator<LocationService>().getLastPosition();
    final data = await perawatanTerencanaDepartemenDeck.toJson(position);
    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPerawatanTerencanaDepartemenDeckResponse.fromJson(
        response.data);
  }

  Future<HistoryPerawatanTerencanaDepartemenDeckResponse>
      updateStatusPerawatanTerencanaDepartemenDeck(int id, String token,
          int sptId, Approve approve, bool isInTime) async {
    final endpoint = dotenv.get(ubahStatusPTDEndpoint);
    final position = await locator<LocationService>().getLastPosition();

    final response = await locator<Dio>().post(
      "$endpoint${locator<UserRole>().getUpdateStatusURL(false, approve)}",
      data: {
        "kapal_id": id,
        "spt_id": sptId,
        "location": position,
        "tanggal_tiba_perawatan": isInTime,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPerawatanTerencanaDepartemenDeckResponse.fromJson(
        response.data);
  }
}
