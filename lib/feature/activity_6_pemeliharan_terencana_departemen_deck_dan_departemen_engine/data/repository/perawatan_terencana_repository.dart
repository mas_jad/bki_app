import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/history_perawatan_terencana.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/post_perawatan_terencana.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/post_perawatan_terencana_departemen_deck.dart';
import 'package:pms_pcm/feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/model/post_perawatan_terencana_departemen_engine.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/util/request.dart';
import '../source/perawatan_terencana_departemen_deck_remote_data_source.dart';
import '../source/perawatan_terencana_departemen_engine_remote_data_source.dart';

class PerawatanTerencanaRepository {
  final PerawatanTerencanaDepartemenDeckRemoteDataSource
      _perawatanTerencanaDepartemenDeckRemoteDataSource;
  final PerawatanTerencanaDepartemenEngineRemoteDataSource
      _perawatanTerencanaDepartemenEngineRemoteDataSource;
  PerawatanTerencanaRepository(
      this._perawatanTerencanaDepartemenDeckRemoteDataSource,
      this._perawatanTerencanaDepartemenEngineRemoteDataSource);

  Future<HistoryPerawatanTerencanaResponse?> getHistoryPerawatanTerencana(
      int id, String token, bool isInTime, bool isDeck) async {
    return RequestServer<HistoryPerawatanTerencanaResponse?>().requestServer(
        computation: () async {
      if (isDeck) {
        final response = await _perawatanTerencanaDepartemenDeckRemoteDataSource
            .getHistoryPerawatanTerencanaDepartemenDeck(id, isInTime, token);
        return response;
      }
      final response = await _perawatanTerencanaDepartemenEngineRemoteDataSource
          .getHistoryPerawatanTerencanaDepartemenEngine(id, isInTime, token);
      return response;
    });
  }

  Future<HistoryPerawatanTerencanaResponse?> postPerawatanTerencana(
    String token,
    PostPerawatanTerencana postPerawatanTerencana,
  ) async {
    return RequestServer<HistoryPerawatanTerencanaResponse?>().requestServer(
        computation: () async {
      if (postPerawatanTerencana is PostPerawatanTerencanaDepartemenDeck) {
        final response = await _perawatanTerencanaDepartemenDeckRemoteDataSource
            .postPerawatanTerencanaDepartemenDeck(
          token,
          postPerawatanTerencana,
        );

        return response;
      }
      if (postPerawatanTerencana is PostPerawatanTerencanaDepartemenEngine) {
        final response =
            await _perawatanTerencanaDepartemenEngineRemoteDataSource
                .postPerawatanTerencanaDepartemenEngine(
          token,
          postPerawatanTerencana,
        );
        return response;
      }
      return null;
    });
  }

  Future<HistoryPerawatanTerencanaResponse?> updateStatusPerawatanTerencana(
      int id,
      int sptId,
      String token,
      Approve approved,
      bool isDeck,
      bool isInTime) async {
    return RequestServer<HistoryPerawatanTerencanaResponse?>().requestServer(
        computation: () async {
      if (isDeck) {
        final response = await _perawatanTerencanaDepartemenDeckRemoteDataSource
            .updateStatusPerawatanTerencanaDepartemenDeck(
                id, token, sptId, approved, isInTime);
        return response;
      }
      final response = await _perawatanTerencanaDepartemenEngineRemoteDataSource
          .updateStatusPerawatanTerencanaDepartemenEngine(
              id, token, sptId, approved, isInTime);
      return response;
    });
  }
}
