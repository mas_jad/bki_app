import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/button/custom_button_widget.dart';
import 'package:pms_pcm/core/widget/image/image_container_picker.dart';
import 'package:pms_pcm/core/widget/textfield/custom_textfield_widget.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/util/locator.dart';
import '../../../../core/util/toast.dart';
import '../../bloc/pemakaian_bahan_bakar_dan_jam_kerja_mesin_bloc.dart';
import '../../data/model/history_pemakaian_bahan_bakar_dan_jam_kerja_mesin.dart';

class PemakaianBahanBakarDanJamKerjaMesinInput extends StatefulWidget {
  const PemakaianBahanBakarDanJamKerjaMesinInput(
      {this.pemakaianBahanBakarDanJamKerjaMesin,
      required this.kapalId,
      required this.date,
      required this.isUserPermitedToSave,
      super.key});
  final int kapalId;
  final DateTime date;
  final bool isUserPermitedToSave;
  final PemakaianBahanBakarDanJamKerjaMesin?
      pemakaianBahanBakarDanJamKerjaMesin;

  @override
  State<PemakaianBahanBakarDanJamKerjaMesinInput> createState() =>
      _PemakaianBahanBakarDanJamKerjaMesinInputState();
}

class _PemakaianBahanBakarDanJamKerjaMesinInputState
    extends State<PemakaianBahanBakarDanJamKerjaMesinInput> {
  final TextEditingController pemakaianBahanBakarEditingController =
      TextEditingController();
  final TextEditingController jamKerjaEditingController =
      TextEditingController();
  late ValueNotifier<String?> imageValueNotifier;

  @override
  void initState() {
    super.initState();
    jamKerjaEditingController.text =
        widget.pemakaianBahanBakarDanJamKerjaMesin?.jamKerja?.toString() ??
            empty;
    pemakaianBahanBakarEditingController.text = widget
            .pemakaianBahanBakarDanJamKerjaMesin?.pemakaianBahanBakar
            ?.toString() ??
        empty;
    empty;
    imageValueNotifier = ValueNotifier(null);
  }

  postPemakaianBahanBakarDanJamKerjaMesin() async {
    BlocProvider.of<PemakaianBahanBakarDanJamKerjaMesinBloc>(context).add(
        PostPemakaianBahanBakarDanJamKerjaMesinEvent(
            PemakaianBahanBakarDanJamKerjaMesin(
      image: imageValueNotifier.value,
      kapalId: widget.kapalId,
      code: widget.pemakaianBahanBakarDanJamKerjaMesin?.code,
      date: locator<MyDate>().getDateWithInputInString(widget.date),
      jamKerja: jamKerjaEditingController.text,
      pemakaianBahanBakar:
          int.tryParse(pemakaianBahanBakarEditingController.text),
    )));
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocListener<PemakaianBahanBakarDanJamKerjaMesinBloc,
        PemakaianBahanBakarDanJamKerjaMesinState>(
      listener: (context, state) {
        if (state is PostPemakaianBahanBakarDanJamKerjaMesinSuccess) {
          Navigator.pop(context);
          MyToast.showSuccess(success, context);
        } else if (state is PostPemakaianBahanBakarDanJamKerjaMesinError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Container(
          width: width,
          decoration: const BoxDecoration(
              color: white,
              boxShadow: [shadowCard],
              borderRadius: cardBorderRadius),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Text(
                          widget.pemakaianBahanBakarDanJamKerjaMesin
                                  ?.nameEnggine
                                  .toString() ??
                              empty,
                          style: ptSansTextStyle.copyWith(
                              color: white, fontWeight: bold, fontSize: 22),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8),
                            child: iconArrow.copyWith(color: transparent)),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      widget.pemakaianBahanBakarDanJamKerjaMesin?.code
                              ?.toUpperCase()
                              .replaceAll("_", " ") ??
                          empty,
                      style: ptSansTextStyle.copyWith(
                          color: white, fontWeight: medium, fontSize: 18),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Center(
                          child: ImageContainerPicker(
                        image:
                            widget.pemakaianBahanBakarDanJamKerjaMesin?.image,
                        imageValueNotifier: imageValueNotifier,
                      )),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    CustomTextFieldWidget(
                      label: "Jam Kerja Mesin",
                      suffix: empty,
                      textEditingController: jamKerjaEditingController,
                      textInputType: widget
                                  .pemakaianBahanBakarDanJamKerjaMesin?.code
                                  ?.substring(0, 2) ==
                              "me"
                          ? TextInputType.text
                          : TextInputType.number,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextFieldWidget(
                      label: "Pemakaian Bahan Bakar",
                      suffix: "Liter",
                      textEditingController:
                          pemakaianBahanBakarEditingController,
                      textInputType: TextInputType.number,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    if (widget.isUserPermitedToSave)
                      BlocBuilder<PemakaianBahanBakarDanJamKerjaMesinBloc,
                          PemakaianBahanBakarDanJamKerjaMesinState>(
                        builder: (context, state) {
                          return CustomButtonWidget(
                              isActive: state
                                      is PostPemakaianBahanBakarDanJamKerjaMesinLoading
                                  ? false
                                  : true,
                              callback: () {
                                postPemakaianBahanBakarDanJamKerjaMesin();
                              },
                              title: save);
                        },
                      )
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
