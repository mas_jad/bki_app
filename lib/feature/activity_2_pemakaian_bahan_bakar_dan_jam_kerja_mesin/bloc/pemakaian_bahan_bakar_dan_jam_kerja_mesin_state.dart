part of 'pemakaian_bahan_bakar_dan_jam_kerja_mesin_bloc.dart';

abstract class PemakaianBahanBakarDanJamKerjaMesinState extends Equatable {
  const PemakaianBahanBakarDanJamKerjaMesinState();

  @override
  List<Object> get props => [];
}

class PemakaianBahanBakarDanJamKerjaMesinInitial
    extends PemakaianBahanBakarDanJamKerjaMesinState {}

// HISTORY
abstract class HistoryPemakaianBahanBakarDanJamKerjaMesinState
    extends PemakaianBahanBakarDanJamKerjaMesinState {}

class HistoryPemakaianBahanBakarDanJamKerjaMesinSuccess
    extends HistoryPemakaianBahanBakarDanJamKerjaMesinState {
  final List<PemakaianBahanBakarDanJamKerjaMesin> map;
  final Approve approve;
  final String? revisedDate;
  HistoryPemakaianBahanBakarDanJamKerjaMesinSuccess(
      this.map, this.revisedDate, this.approve);
}

class HistoryPemakaianBahanBakarDanJamKerjaMesinError
    extends HistoryPemakaianBahanBakarDanJamKerjaMesinState {
  final String message;
  HistoryPemakaianBahanBakarDanJamKerjaMesinError(this.message);
}

class HistoryPemakaianBahanBakarDanJamKerjaMesinLoading
    extends HistoryPemakaianBahanBakarDanJamKerjaMesinState {}

// POST
abstract class PostPemakaianBahanBakarDanJamKerjaMesinState
    extends PemakaianBahanBakarDanJamKerjaMesinState {}

class PostPemakaianBahanBakarDanJamKerjaMesinSuccess
    extends PostPemakaianBahanBakarDanJamKerjaMesinState {}

class PostPemakaianBahanBakarDanJamKerjaMesinError
    extends PostPemakaianBahanBakarDanJamKerjaMesinState {
  final String message;
  PostPemakaianBahanBakarDanJamKerjaMesinError(this.message);
}

class PostPemakaianBahanBakarDanJamKerjaMesinLoading
    extends PostPemakaianBahanBakarDanJamKerjaMesinState {}

// UPDATE STATUS
abstract class UpdateStatusPemakaianBahanBakarDanJamKerjaMesinState
    extends PemakaianBahanBakarDanJamKerjaMesinState {}

class UpdateStatusPemakaianBahanBakarDanJamKerjaMesinSuccess
    extends UpdateStatusPemakaianBahanBakarDanJamKerjaMesinState {}

class UpdateStatusPemakaianBahanBakarDanJamKerjaMesinError
    extends UpdateStatusPemakaianBahanBakarDanJamKerjaMesinState {
  final String message;
  UpdateStatusPemakaianBahanBakarDanJamKerjaMesinError(this.message);
}

class UpdateStatusPemakaianBahanBakarDanJamKerjaMesinLoading
    extends UpdateStatusPemakaianBahanBakarDanJamKerjaMesinState {}
