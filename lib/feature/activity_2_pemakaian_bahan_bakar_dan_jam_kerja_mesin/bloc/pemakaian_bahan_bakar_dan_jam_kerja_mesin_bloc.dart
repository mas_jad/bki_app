import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_pemakaian_bahan_bakar_dan_jam_kerja_mesin.dart';
import '../data/repository/pemakaian_bahan_bakar_dan_jam_kerja_mesin_repository.dart';

part 'pemakaian_bahan_bakar_dan_jam_kerja_mesin_event.dart';
part 'pemakaian_bahan_bakar_dan_jam_kerja_mesin_state.dart';

class PemakaianBahanBakarDanJamKerjaMesinBloc extends Bloc<
    PemakaianBahanBakarDanJamKerjaMesinEvent,
    PemakaianBahanBakarDanJamKerjaMesinState> {
  final PemakaianBahanBakarDanJamKerjaMesinRepository
      _pemakaianBahanBakarDanJamKerjaMesinRepository;
  final UserRepository _userRepository;

  PemakaianBahanBakarDanJamKerjaMesinBloc(
      this._pemakaianBahanBakarDanJamKerjaMesinRepository, this._userRepository)
      : super(PemakaianBahanBakarDanJamKerjaMesinInitial()) {
    on<PostPemakaianBahanBakarDanJamKerjaMesinEvent>((event, emit) async {
      try {
        emit(PostPemakaianBahanBakarDanJamKerjaMesinLoading());
        final token = await _userRepository.getToken();
        final data = await _pemakaianBahanBakarDanJamKerjaMesinRepository
            .postPemakaianBahanBakarDanJamKerjaMesin(
          token,
          event.pemakaianBahanBakarDanJamKerjaMesin,
          event.pemakaianBahanBakarDanJamKerjaMesin.kapalId!,
          event.pemakaianBahanBakarDanJamKerjaMesin.date!,
        );

        emit(PostPemakaianBahanBakarDanJamKerjaMesinSuccess());
        emit(HistoryPemakaianBahanBakarDanJamKerjaMesinSuccess(
          data,
          _pemakaianBahanBakarDanJamKerjaMesinRepository.getRevisedDate(),
          _pemakaianBahanBakarDanJamKerjaMesinRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostPemakaianBahanBakarDanJamKerjaMesinError(e.message));
        } else {
          emit(PostPemakaianBahanBakarDanJamKerjaMesinError(
              somethingErrorHappen));
        }
      }
    });

    on<GetHistoryPemakaianBahanBakarDanJamKerjaMesinEvent>((event, emit) async {
      try {
        emit(HistoryPemakaianBahanBakarDanJamKerjaMesinLoading());
        final token = await _userRepository.getToken();
        final data = await _pemakaianBahanBakarDanJamKerjaMesinRepository
            .getHistoryPemakaianBahanBakarDanJamKerjaMesin(
                event.id, token, event.date);

        emit(HistoryPemakaianBahanBakarDanJamKerjaMesinSuccess(
          data,
          _pemakaianBahanBakarDanJamKerjaMesinRepository.getRevisedDate(),
          _pemakaianBahanBakarDanJamKerjaMesinRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryPemakaianBahanBakarDanJamKerjaMesinError(e.message));
        } else {
          emit(HistoryPemakaianBahanBakarDanJamKerjaMesinError(
              somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusPemakaianBahanBakarDanJamKerjaMesinEvent>(
        (event, emit) async {
      try {
        emit(UpdateStatusPemakaianBahanBakarDanJamKerjaMesinLoading());
        final token = await _userRepository.getToken();
        final data = await _pemakaianBahanBakarDanJamKerjaMesinRepository
            .updateStatusPemakaianBahanBakarDanJamKerjaMesin(
                event.id, token, event.date, event.approve);

        emit(UpdateStatusPemakaianBahanBakarDanJamKerjaMesinSuccess());
        emit(HistoryPemakaianBahanBakarDanJamKerjaMesinSuccess(
          data,
          _pemakaianBahanBakarDanJamKerjaMesinRepository.getRevisedDate(),
          _pemakaianBahanBakarDanJamKerjaMesinRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(UpdateStatusPemakaianBahanBakarDanJamKerjaMesinError(e.message));
        } else {
          emit(UpdateStatusPemakaianBahanBakarDanJamKerjaMesinError(
              somethingErrorHappen));
        }
      }
    });
  }
}
