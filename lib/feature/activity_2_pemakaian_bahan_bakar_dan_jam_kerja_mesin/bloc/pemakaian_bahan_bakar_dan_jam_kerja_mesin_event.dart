part of 'pemakaian_bahan_bakar_dan_jam_kerja_mesin_bloc.dart';

abstract class PemakaianBahanBakarDanJamKerjaMesinEvent extends Equatable {
  const PemakaianBahanBakarDanJamKerjaMesinEvent();

  @override
  List<Object> get props => [];
}

class PostPemakaianBahanBakarDanJamKerjaMesinEvent
    extends PemakaianBahanBakarDanJamKerjaMesinEvent {
  final PemakaianBahanBakarDanJamKerjaMesin pemakaianBahanBakarDanJamKerjaMesin;
  const PostPemakaianBahanBakarDanJamKerjaMesinEvent(
      this.pemakaianBahanBakarDanJamKerjaMesin);
}

class UpdateStatusPemakaianBahanBakarDanJamKerjaMesinEvent
    extends PemakaianBahanBakarDanJamKerjaMesinEvent {
  final int id;
  final String date;
  final Approve approve;
  const UpdateStatusPemakaianBahanBakarDanJamKerjaMesinEvent(
      this.id, this.date, this.approve);
}

class GetHistoryPemakaianBahanBakarDanJamKerjaMesinEvent
    extends PemakaianBahanBakarDanJamKerjaMesinEvent {
  final int id;
  final String date;
  const GetHistoryPemakaianBahanBakarDanJamKerjaMesinEvent(this.id, this.date);
}
