import 'package:pms_pcm/core/util/const.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/util/request.dart';
import '../model/history_pemakaian_bahan_bakar_dan_jam_kerja_mesin.dart';
import '../source/pemakaian_bahan_bakar_dan_jam_kerja_mesin_remote_data_source.dart';

class PemakaianBahanBakarDanJamKerjaMesinRepository {
  final PemakaianBahanBakarDanJamKerjaMesinRemoteDataSource
      _pemakaianBahanBakarDanJamKerjaMesinRemoteDataSource;
  PemakaianBahanBakarDanJamKerjaMesinRepository(
      this._pemakaianBahanBakarDanJamKerjaMesinRemoteDataSource);

  String? revisedDate;
  Approve? approve;

  String? getRevisedDate() {
    return revisedDate;
  }

  Approve getApprove() {
    return approve ??
        Approve(
            approved1: null, approved2: null, approved3: null, approved4: null);
  }

  List<PemakaianBahanBakarDanJamKerjaMesin> _relatingDataToMaster(
      HistoryPemakaianBahanBakarDanJamKerjaMesin pBH,
      int kapalId,
      String date) {
    return [
      PemakaianBahanBakarDanJamKerjaMesin(
          nameEnggine: "Main Engine 1",
          jamKerja: pBH.me1Jkm?.toString(),
          pemakaianBahanBakar: pBH.me1Pbb,
          code: "me_1",
          kapalId: kapalId,
          date: date,
          statusProses: pBH.statusProsesMe1 ?? DRAF,
          image: pBH.me1Image),
      PemakaianBahanBakarDanJamKerjaMesin(
          nameEnggine: "Main Engine 2",
          jamKerja: pBH.me2Jkm?.toString(),
          pemakaianBahanBakar: pBH.me2Pbb,
          code: "me_2",
          kapalId: kapalId,
          date: date,
          statusProses: pBH.statusProsesMe2 ?? DRAF,
          image: pBH.me2Image),
      PemakaianBahanBakarDanJamKerjaMesin(
          nameEnggine: "Auxiliary Engine 1",
          jamKerja: pBH.ae1Jkm?.toString(),
          pemakaianBahanBakar: pBH.ae1Pbb,
          code: "ae_1",
          kapalId: kapalId,
          statusProses: pBH.statusProsesAe1 ?? DRAF,
          date: date,
          image: pBH.ae1Image),
      PemakaianBahanBakarDanJamKerjaMesin(
          nameEnggine: "Auxiliary Engine 2",
          jamKerja: pBH.ae2Jkm?.toString(),
          pemakaianBahanBakar: pBH.ae2Pbb,
          code: "ae_2",
          kapalId: kapalId,
          date: date,
          statusProses: pBH.statusProsesAe2 ?? DRAF,
          image: pBH.ae2Image),
      PemakaianBahanBakarDanJamKerjaMesin(
          nameEnggine: "Auxiliary Engine 3",
          jamKerja: pBH.ae3Jkm?.toString(),
          pemakaianBahanBakar: pBH.ae3Pbb,
          code: "ae_3",
          kapalId: kapalId,
          date: date,
          statusProses: pBH.statusProsesAe3 ?? DRAF,
          image: pBH.ae3Image),
      PemakaianBahanBakarDanJamKerjaMesin(
          nameEnggine: "Auxiliary Engine FF",
          jamKerja: pBH.aeFfJkm?.toString(),
          pemakaianBahanBakar: pBH.aeFfPbb,
          code: "ae_ff",
          kapalId: kapalId,
          date: date,
          statusProses: pBH.statusProsesAeFf ?? DRAF,
          image: pBH.aeFfImage),
      PemakaianBahanBakarDanJamKerjaMesin(
          nameEnggine: "LL",
          jamKerja: null,
          pemakaianBahanBakar: pBH.llPbb,
          code: "ll",
          kapalId: kapalId,
          statusProses: pBH.statusProsesLlPbb ?? DRAF,
          date: date,
          image: pBH.llPbbImage),
    ];
  }

  Future<List<PemakaianBahanBakarDanJamKerjaMesin>>
      getHistoryPemakaianBahanBakarDanJamKerjaMesin(
          int id, String token, String date) async {
    return RequestServer<List<PemakaianBahanBakarDanJamKerjaMesin>>()
        .requestServer(computation: () async {
      final response =
          await _pemakaianBahanBakarDanJamKerjaMesinRemoteDataSource
              .getHistoryPemakaianBahanBakarDanJamKerjaMesin(id, token, date);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingDataToMaster(
          response.data?.first ?? HistoryPemakaianBahanBakarDanJamKerjaMesin(),
          id,
          date);
    });
  }

  Future<List<PemakaianBahanBakarDanJamKerjaMesin>>
      postPemakaianBahanBakarDanJamKerjaMesin(
          String token,
          PemakaianBahanBakarDanJamKerjaMesin
              pemakaianBahanBakarDanJamKerjaMesin,
          int id,
          String date) async {
    return RequestServer<List<PemakaianBahanBakarDanJamKerjaMesin>>()
        .requestServer(computation: () async {
      final response =
          await _pemakaianBahanBakarDanJamKerjaMesinRemoteDataSource
              .postPemakaianBahanBakarDanJamKerjaMesin(
                  token, pemakaianBahanBakarDanJamKerjaMesin);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingDataToMaster(
          response.data?.first ?? HistoryPemakaianBahanBakarDanJamKerjaMesin(),
          id,
          date);
    });
  }

  Future<List<PemakaianBahanBakarDanJamKerjaMesin>>
      updateStatusPemakaianBahanBakarDanJamKerjaMesin(
          int id, String token, String date, Approve approved) async {
    return RequestServer<List<PemakaianBahanBakarDanJamKerjaMesin>>()
        .requestServer(computation: () async {
      final response =
          await _pemakaianBahanBakarDanJamKerjaMesinRemoteDataSource
              .updateStatusPemakaianBahanBakarDanJamKerjaMesin(
                  id, token, date, approved);
      revisedDate = response.tanggalMundur;

      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingDataToMaster(
          response.data?.first ?? HistoryPemakaianBahanBakarDanJamKerjaMesin(),
          id,
          date);
    });
  }
}
