import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/service/location/location_service.dart';
import '../../../../core/util/user_role.dart';
import '../model/history_pemakaian_bahan_bakar_dan_jam_kerja_mesin.dart';

class PemakaianBahanBakarDanJamKerjaMesinRemoteDataSource {
  Future<HistoryPemakaianBahanBakarDanJamKerjaMesinResponse>
      getHistoryPemakaianBahanBakarDanJamKerjaMesin(
          int id, String token, String date) async {
    final endpoint = dotenv.get(dataPBBdanJKMPerTanggalEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {"kapal_id": id, "date": date},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPemakaianBahanBakarDanJamKerjaMesinResponse.fromJson(
        response.data);
  }

  Future<HistoryPemakaianBahanBakarDanJamKerjaMesinResponse>
      postPemakaianBahanBakarDanJamKerjaMesin(
          String token,
          PemakaianBahanBakarDanJamKerjaMesin
              pemakaianBahanBakarDanJamKerjaMesin) async {
    final endpoint = dotenv.get(simpanDataPBBdanJKMEndpoint);
    final position = await locator<LocationService>().getLastPosition();
    final data = await pemakaianBahanBakarDanJamKerjaMesin.toJson(position);
    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPemakaianBahanBakarDanJamKerjaMesinResponse.fromJson(
        response.data);
  }

  Future<HistoryPemakaianBahanBakarDanJamKerjaMesinResponse>
      updateStatusPemakaianBahanBakarDanJamKerjaMesin(
          int id, String token, String date, Approve approve) async {
    final endpoint = dotenv.get(ubahStatusPBBdanJKMEndpoint);
    final response = await locator<Dio>().post(
      "$endpoint${locator<UserRole>().getUpdateStatusURL(true, approve)}",
      data: {"kapal_id": id, "date": date},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPemakaianBahanBakarDanJamKerjaMesinResponse.fromJson(
        response.data);
  }
}
