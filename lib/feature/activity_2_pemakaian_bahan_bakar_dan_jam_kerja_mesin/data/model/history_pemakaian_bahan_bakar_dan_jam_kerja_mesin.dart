import 'package:dio/dio.dart';

class HistoryPemakaianBahanBakarDanJamKerjaMesinResponse {
  String? message;
  int? code;
  List<HistoryPemakaianBahanBakarDanJamKerjaMesin>? data;
  String? date;
  String? tanggalMundur;
  String? approved1;
  String? approved2;
  String? approved3;
  String? approved4;

  HistoryPemakaianBahanBakarDanJamKerjaMesinResponse(
      {this.message,
      this.code,
      this.data,
      this.date,
      this.tanggalMundur,
      this.approved1,
      this.approved2,
      this.approved3,
      this.approved4});

  HistoryPemakaianBahanBakarDanJamKerjaMesinResponse.fromJson(
      Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    if (json['data'] != null) {
      data = <HistoryPemakaianBahanBakarDanJamKerjaMesin>[];
      json['data'].forEach((v) {
        if (v != null) {
          data!.add(HistoryPemakaianBahanBakarDanJamKerjaMesin.fromJson(v));
        }
      });
      if (data?.isEmpty ?? true) {
        data = null;
      }
    }
    approved1 = json['approved_1'];
    approved2 = json['approved_2'];
    approved3 = json['approved_3'];
    approved4 = json['approved_4'];
    date = json['date'];
    tanggalMundur = json['tanggal_mundur'];
  }
}

class HistoryPemakaianBahanBakarDanJamKerjaMesin {
  int? id;
  String? me1Jkm;
  String? me2Jkm;
  String? ae1Jkm;
  String? ae2Jkm;
  String? ae3Jkm;
  String? aeFfJkm;
  int? me1Pbb;
  String? me1Image;
  String? statusProsesMe1;
  int? me2Pbb;
  String? me2Image;
  String? statusProsesMe2;
  int? ae1Pbb;
  String? ae1Image;
  String? statusProsesAe1;
  int? ae2Pbb;
  String? ae2Image;
  String? statusProsesAe2;
  int? ae3Pbb;
  String? ae3Image;
  String? statusProsesAe3;
  int? aeFfPbb;
  String? aeFfImage;
  String? statusProsesAeFf;
  int? llPbb;
  String? llPbbImage;
  String? statusProsesLlPbb;

  HistoryPemakaianBahanBakarDanJamKerjaMesin(
      {this.id,
      this.me1Jkm,
      this.me2Jkm,
      this.ae1Jkm,
      this.ae2Jkm,
      this.ae3Jkm,
      this.aeFfJkm,
      this.me1Pbb,
      this.me1Image,
      this.statusProsesMe1,
      this.me2Pbb,
      this.me2Image,
      this.statusProsesMe2,
      this.ae1Pbb,
      this.ae1Image,
      this.statusProsesAe1,
      this.ae2Pbb,
      this.ae2Image,
      this.statusProsesAe2,
      this.ae3Pbb,
      this.ae3Image,
      this.statusProsesAe3,
      this.aeFfPbb,
      this.aeFfImage,
      this.statusProsesAeFf,
      this.llPbb,
      this.llPbbImage,
      this.statusProsesLlPbb});

  HistoryPemakaianBahanBakarDanJamKerjaMesin.fromJson(
      Map<String, dynamic> json) {
    id = json['id'];
    me1Jkm = json['me_1_jkm']?.toString();
    me2Jkm = json['me_2_jkm']?.toString();
    ae1Jkm = json['ae_1_jkm']?.toString();
    ae2Jkm = json['ae_2_jkm']?.toString();
    ae3Jkm = json['ae_3_jkm']?.toString();
    aeFfJkm = json['ae_ff_jkm']?.toString();
    me1Pbb = json['me_1_pbb'];
    me1Image = json['me_1_image'];
    statusProsesMe1 = json['status_proses_me_1'];
    me2Pbb = json['me_2_pbb'];
    me2Image = json['me_2_image'];
    statusProsesMe2 = json['status_proses_me_2'];
    ae1Pbb = json['ae_1_pbb'];
    ae1Image = json['ae_1_image'];
    statusProsesAe1 = json['status_proses_ae_1'];
    ae2Pbb = json['ae_2_pbb'];
    ae2Image = json['ae_2_image'];
    statusProsesAe2 = json['status_proses_ae_2'];
    ae3Pbb = json['ae_3_pbb'];
    ae3Image = json['ae_3_image'];
    statusProsesAe3 = json['status_proses_ae_3'];
    aeFfPbb = json['ae_ff_pbb'];
    aeFfImage = json['ae_ff_image'];
    statusProsesAeFf = json['status_proses_ae_ff'];
    llPbb = json['ll_pbb'];
    llPbbImage = json['ll_pbb_image'];
    statusProsesLlPbb = json['status_proses_ll_pbb'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['me_1_jkm'] = me1Jkm;
    data['me_2_jkm'] = me2Jkm;
    data['ae_1_jkm'] = ae1Jkm;
    data['ae_2_jkm'] = ae2Jkm;
    data['ae_3_jkm'] = ae3Jkm;
    data['ae_ff_jkm'] = aeFfJkm;
    data['me_1_pbb'] = me1Pbb;
    data['me_1_image'] = me1Image;
    data['status_proses_me_1'] = statusProsesMe1;
    data['me_2_pbb'] = me2Pbb;
    data['me_2_image'] = me2Image;
    data['status_proses_me_2'] = statusProsesMe2;
    data['ae_1_pbb'] = ae1Pbb;
    data['ae_1_image'] = ae1Image;
    data['status_proses_ae_1'] = statusProsesAe1;
    data['ae_2_pbb'] = ae2Pbb;
    data['ae_2_image'] = ae2Image;
    data['status_proses_ae_2'] = statusProsesAe2;
    data['ae_3_pbb'] = ae3Pbb;
    data['ae_3_image'] = ae3Image;
    data['status_proses_ae_3'] = statusProsesAe3;
    data['ae_ff_pbb'] = aeFfPbb;
    data['ae_ff_image'] = aeFfImage;
    data['status_proses_ae_ff'] = statusProsesAeFf;
    data['ll_pbb'] = llPbb;
    data['ll_pbb_image'] = llPbbImage;
    data['status_proses_ll_pbb'] = statusProsesLlPbb;
    return data;
  }
}

class PemakaianBahanBakarDanJamKerjaMesin {
  String? jamKerja;
  int? pemakaianBahanBakar;
  String? image;
  String? nameEnggine;
  String? code;
  String? date;
  String? statusProses;
  int? kapalId;

  PemakaianBahanBakarDanJamKerjaMesin(
      {required this.image,
      required this.jamKerja,
      this.nameEnggine,
      required this.pemakaianBahanBakar,
      this.code,
      required this.date,
      this.statusProses,
      required this.kapalId});

  Future<FormData> toJson(String? location) async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['date'] = date;
    data["location"] = location;
    data['kapal_id'] = kapalId;
    data['${code}_jkm'] = jamKerja;
    data['${code}_pbb'] = pemakaianBahanBakar;
    data['${code}_image'] = image != null
        ? await MultipartFile.fromFile(
            image!,
            filename: image!.split('/').last,
          )
        : null;
    data.removeWhere((key, value) => value == null);
    return FormData.fromMap(data);
  }
}
