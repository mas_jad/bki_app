part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginSuccess extends LoginState {
  final Profile profile;
  final List<DataKapal> list;
  const LoginSuccess(this.profile, this.list);
}

class LoginLoading extends LoginState {}

class LoginError extends LoginState {
  final String message;
  const LoginError(this.message);
}
