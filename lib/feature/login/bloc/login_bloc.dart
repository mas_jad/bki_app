import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/data/repository/user_repository.dart';
import 'package:pms_pcm/core/util/const.dart';

import '../../../core/data/model/error_response.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository _userRepository;

  LoginBloc(this._userRepository) : super(LoginInitial()) {
    on<PostLoginEvent>((event, emit) async {
      try {
        emit(LoginLoading());

        final response =
            await _userRepository.login(event.email, event.password);
        final dataProfile = response.data?.profile;
        final dataKapal = response.data?.dataKapal;
        if (dataProfile != null && dataKapal != null && dataKapal.isNotEmpty) {
          emit(LoginSuccess(dataProfile, dataKapal));
        } else {
          if (dataProfile == null) {
            emit(const LoginError("Data Profile Kamu Kurang Lengkap !"));
          } else {
            emit(const LoginError("Data Kapal Kamu Kurang Lengkap !"));
          }
        }
      } catch (e) {
        if (e is ErrorResponse) {
          emit(LoginError(e.message));
        } else {
          emit(const LoginError(somethingErrorHappen));
        }
      }
    });
  }
}
