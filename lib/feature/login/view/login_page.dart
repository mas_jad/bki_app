import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/size.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/core/widget/button/custom_button_widget.dart';
import 'package:pms_pcm/core/widget/textfield/custom_textfield_widget.dart';
import 'package:pms_pcm/feature/home/view/home_page.dart';
import 'package:pms_pcm/feature/ship_activity/view/ship_activity_page.dart';

import '../../../core/util/const.dart';
import '../bloc/login_bloc.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});
  static const route = "/login_page";

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  String? emailError;
  String? passwordError;
  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginSuccess) {
          if (state.list.length > 1) {
            Navigator.pushReplacementNamed(context, HomePage.route,
                arguments: [state.list, state.profile]);
          } else if (state.list.isNotEmpty) {
            Navigator.pushReplacementNamed(context, ShipActivity.route,
                arguments: [state.list.first, state.profile]);
          } else {
            MyToast.showError("Data Kapal Kamu Kurang Lengkap !", context);
          }
        } else if (state is LoginError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        backgroundColor: white,
        body: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  decoration: const BoxDecoration(
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(20)),
                      image: DecorationImage(
                          image: AssetImage(itemImage5), fit: BoxFit.fill)),
                  padding:
                      const EdgeInsets.symmetric(horizontal: horizontalMargin),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Login",
                        style: ptSansTextStyle.copyWith(
                            color: white, fontWeight: semiBold, fontSize: 40),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Masukkan Username Dan Password",
                        style: ptSansTextStyle.copyWith(
                            color: semiWhite,
                            fontWeight: reguler,
                            fontSize: 16),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: horizontalMargin),
                  child: CustomTextFieldWidget(
                    textEditingController: emailController,
                    prefixIcon: const Icon(Icons.person),
                    hint: "Email Address",
                    error: emailError,
                    textInputType: TextInputType.emailAddress,
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: horizontalMargin),
                  child: CustomTextFieldWidget(
                    textEditingController: passwordController,
                    prefixIcon: const Icon(Icons.lock),
                    hint: "Password",
                    error: passwordError,
                    textInputType: TextInputType.visiblePassword,
                  ),
                ),
                const SizedBox(
                  height: 60,
                ),
                // const CheatButton(),
                Center(
                    child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: horizontalMargin),
                  child: BlocBuilder<LoginBloc, LoginState>(
                    builder: (context, state) {
                      return CustomButtonWidget(
                        callback: () {
                          if (emailController.text == empty) {
                            setState(() {
                              emailError = "Email Wajib Di Isi";
                            });
                            return;
                          }
                          emailError = null;
                          if (passwordController.text == empty) {
                            setState(() {
                              passwordError = "Password Wajib Di Isi";
                            });
                            return;
                          }
                          passwordError = null;
                          BlocProvider.of<LoginBloc>(context).add(
                              PostLoginEvent(
                                  email: emailController.text,
                                  password: passwordController.text));
                        },
                        title: enter,
                        isActive: state is LoginLoading ? false : true,
                      );
                    },
                  ),
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// class CheatButton extends StatelessWidget {
//   const CheatButton({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: [
//         const SizedBox(
//           height: 20,
//         ),
//         Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             IconButton(
//                 onPressed: () {
//                   BlocProvider.of<LoginBloc>(context).add(const PostLoginEvent(
//                       email: "munif@mail.com", password: "123456789"));
//                 },
//                 icon: const Icon(Icons.one_k)),
//             IconButton(
//                 onPressed: () {
//                   BlocProvider.of<LoginBloc>(context).add(const PostLoginEvent(
//                       email: "gunawan@mail.com", password: "123456789"));
//                 },
//                 icon: const Icon(Icons.two_k)),
//             IconButton(
//                 onPressed: () {
//                   BlocProvider.of<LoginBloc>(context).add(const PostLoginEvent(
//                       email: "sudarto@mail.com", password: "123456789"));
//                 },
//                 icon: const Icon(Icons.three_k)),
//             IconButton(
//                 onPressed: () {
//                   BlocProvider.of<LoginBloc>(context).add(const PostLoginEvent(
//                       email: "admin@gmail.com", password: "1234567"));
//                 },
//                 icon: const Icon(Icons.four_k))
//           ],
//         ),
//       ],
//     );
//   }
// }
