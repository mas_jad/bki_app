import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/repository/user_repository.dart';
import 'package:pms_pcm/core/util/const.dart';

part 'ship_activity_event.dart';
part 'ship_activity_state.dart';

class ShipActivityBloc extends Bloc<ShipActivityEvent, ShipActivityState> {
  final UserRepository _userRepository;
  ShipActivityBloc(this._userRepository) : super(ShipActivityInitial()) {
    on<LogoutEvent>((event, emit) {
      try {
        emit(LogoutLoading());
        _userRepository.logout();
        emit(const LogoutSuccess(success));
      } catch (e) {
        emit(const LogoutError(somethingErrorHappen));
      }
    });
  }
}
