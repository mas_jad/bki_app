part of 'ship_activity_bloc.dart';

abstract class ShipActivityState extends Equatable {
  const ShipActivityState();

  @override
  List<Object> get props => [];
}

class ShipActivityInitial extends ShipActivityState {}

class LogoutSuccess extends ShipActivityState {
  final String message;
  const LogoutSuccess(this.message);
}

class LogoutError extends ShipActivityState {
  final String message;
  const LogoutError(this.message);
}

class LogoutLoading extends ShipActivityState {}
