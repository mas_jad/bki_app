part of 'alert_bahan_bakar_dan_air_tawar_bloc.dart';

abstract class AlertBahanBakarDanAirTawarEvent extends Equatable {
  const AlertBahanBakarDanAirTawarEvent();

  @override
  List<Object> get props => [];
}

class GetAlertBahanBakarDanAirTawarEvent
    extends AlertBahanBakarDanAirTawarEvent {
  final int kapalId;
  const GetAlertBahanBakarDanAirTawarEvent(this.kapalId);
}
