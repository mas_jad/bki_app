part of 'ship_activity_bloc.dart';

abstract class ShipActivityEvent extends Equatable {
  const ShipActivityEvent();

  @override
  List<Object> get props => [];
}

class LogoutEvent extends ShipActivityEvent {}
