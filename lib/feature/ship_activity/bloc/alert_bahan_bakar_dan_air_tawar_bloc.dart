import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/feature/ship_activity/data/model/alert_air_tawar.dart';
import 'package:pms_pcm/feature/ship_activity/data/model/alert_bahan_bakar.dart';
import 'package:pms_pcm/feature/ship_activity/data/repository/alert_bahan_bakar_dan_air_tawar_repository.dart';

import '../../../core/data/repository/user_repository.dart';

part 'alert_bahan_bakar_dan_air_tawar_event.dart';
part 'alert_bahan_bakar_dan_air_tawar_state.dart';

class AlertBahanBakarDanAirTawarBloc extends Bloc<
    AlertBahanBakarDanAirTawarEvent, AlertBahanBakarDanAirTawarState> {
  final AlertBahanBakarDanAirTawarRepository
      _alertBahanBakarDanAirTawarRepository;
  final UserRepository _userRepository;
  AlertBahanBakarDanAirTawarBloc(
      this._alertBahanBakarDanAirTawarRepository, this._userRepository)
      : super(AlertBahanBakarDanAirTawarInitial()) {
    on<GetAlertBahanBakarDanAirTawarEvent>((event, emit) async {
      try {
        emit(AlertBahanBakarDanAirTawarLoading());
        final token = await _userRepository.getToken();
        final alertBahanBakar = _alertBahanBakarDanAirTawarRepository
            .getAlertBahanBakar(event.kapalId, token);
        final alertAirTawar = _alertBahanBakarDanAirTawarRepository
            .getAlertAirTawar(event.kapalId, token);
        emit(AlertBahanBakarDanAirTawarSuccess(
            (await alertBahanBakar)!, (await alertAirTawar)!));
      } catch (e) {
        emit(AlertBahanBakarDanAirTawarFailed());
      }
    });
  }
}
