part of 'alert_bahan_bakar_dan_air_tawar_bloc.dart';

abstract class AlertBahanBakarDanAirTawarState extends Equatable {
  const AlertBahanBakarDanAirTawarState();

  @override
  List<Object> get props => [];
}

class AlertBahanBakarDanAirTawarInitial
    extends AlertBahanBakarDanAirTawarState {}

class AlertBahanBakarDanAirTawarSuccess
    extends AlertBahanBakarDanAirTawarState {
  final AlertBahanBakar alertBahanBakar;
  final AlertAirTawar alertAirTawar;

  const AlertBahanBakarDanAirTawarSuccess(
      this.alertBahanBakar, this.alertAirTawar);
}

class AlertBahanBakarDanAirTawarFailed extends AlertBahanBakarDanAirTawarState {
}

class AlertBahanBakarDanAirTawarLoading
    extends AlertBahanBakarDanAirTawarState {}
