import 'package:flutter/material.dart';
import 'package:pms_pcm/core/util/const.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/widget/other/pop_widget.dart';

class ActivityItem extends StatelessWidget {
  const ActivityItem(
      {required this.callback,
      required this.title,
      required this.image,
      required this.icon,
      super.key});
  final String title;
  final VoidCallback callback;
  final String image;
  final Widget icon;
  @override
  Widget build(BuildContext context) {
    const height = 120.0;
    return PopWidget(
      onTap: callback,
      child: Container(
        margin: const EdgeInsets.only(bottom: 18),
        width: MediaQuery.of(context).size.width,
        height: height,
        decoration: const BoxDecoration(
            color: white,
            boxShadow: [shadowCard],
            borderRadius: cardBorderRadius),
        child: Row(
          children: [
            Stack(
              children: [
                Container(
                  height: height,
                  width: height,
                  decoration: BoxDecoration(
                      borderRadius: cardBorderRadius,
                      image: DecorationImage(
                          image: AssetImage(image), fit: BoxFit.cover)),
                ),
                Container(
                  height: height,
                  width: height,
                  decoration: const BoxDecoration(
                      borderRadius: cardBorderRadius,
                      image: DecorationImage(
                          image: AssetImage(itemImage3), fit: BoxFit.cover)),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child:
                      Padding(padding: const EdgeInsets.all(20), child: icon),
                ),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  title,
                  style: ptSansTextStyle.copyWith(
                      color: lightText, fontSize: 14, fontWeight: medium),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
