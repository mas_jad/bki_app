import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:pms_pcm/core/bloc/master_barang_dan_jasa_bloc.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/util/loading.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/view/sounding_bahan_bakar_page.dart';
import 'package:pms_pcm/feature/activity_4_pemakaian_air_tawar/view/pemakaian_air_tawar_page.dart';
import 'package:pms_pcm/feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/view/perawatan_harian_departemen_deck_dan_departemen_engine_page.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/permintaan_barang_dan_jasa_page.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/kerusakan_dan_perbaikan_kapal_page.dart';
import 'package:pms_pcm/feature/login/view/login_page.dart';
import 'package:pms_pcm/feature/ship_activity/bloc/ship_activity_bloc.dart';
import 'package:pms_pcm/feature/ship_activity/view/widget/activity_item.dart';

import '../../../core/bloc/master_barang_oli_bloc.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/service/date/date.dart';
import '../../../core/theme/theme.dart';
import '../../../core/util/const.dart';
import '../../../core/util/enum.dart';
import '../../../core/util/locator.dart';
import '../../../core/util/user_role.dart';
import '../../../core/widget/other/custom_image_network.dart';
import '../../activity_2_pemakaian_bahan_bakar_dan_jam_kerja_mesin/view/pemakaian_bahan_bakar_dan_jam_kerja_mesin_page.dart';
import '../../activity_3_pemakaian_oli_mesin/view/pemakaian_oli_mesin_page.dart';
import '../../activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/view/perawatan_terencana_departemen_deck_dan_departemen_engine_page.dart';
import '../../activity_7_permintaan_barang_dan_jasa/bloc/permintaan_barang_dan_jasa_bloc.dart';
import '../../activity_7_permintaan_barang_dan_jasa/data/repository/permintaan_barang_dan_jasa_repository.dart';
import '../../activity_7_permintaan_barang_dan_jasa/view/widget/c_permintaan_barang_dan_jasa_input.dart';
import '../bloc/alert_bahan_bakar_dan_air_tawar_bloc.dart';

class ShipActivity extends StatefulWidget {
  const ShipActivity(
      {required this.dataKapal, required this.profile, super.key});
  static const route = "/ship_activity_page";
  final DataKapal dataKapal;
  final Profile profile;

  @override
  State<ShipActivity> createState() => _ShipActivityState();
}

class _ShipActivityState extends State<ShipActivity> {
  int? idDepartemenDeck;
  int? idDepartemenEngine;
  @override
  void initState() {
    super.initState();
    if (widget.dataKapal.idKapal != null) {
      BlocProvider.of<MasterBarangDanJasaBloc>(context)
          .add(GetMasterBarangDanJasaEvent(widget.dataKapal.idKapal!));
      BlocProvider.of<MasterBarangOliBloc>(context)
          .add(GetMasterBarangOliEvent(widget.dataKapal.idKapal!));
      BlocProvider.of<AlertBahanBakarDanAirTawarBloc>(context)
          .add(GetAlertBahanBakarDanAirTawarEvent(widget.dataKapal.idKapal!));
    }
    // DEPARTEMEN
    widget.dataKapal.departemen?.forEach((element) {
      if (element.nama == ENGINE || element.nama == ENGINE2) {
        idDepartemenEngine = element.id;
      }
      if (element.nama == DECK) {
        idDepartemenDeck = element.id;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocListener<ShipActivityBloc, ShipActivityState>(
      listener: (context, state) {
        if (state is LogoutSuccess) {
          LoadingOverlay.dismiss();
          MyToast.showSuccess(state.message, context);
          Navigator.pushNamedAndRemoveUntil(
              context, LoginPage.route, (route) => false);
        } else if (state is LogoutError) {
          LoadingOverlay.dismiss();
          MyToast.showError(state.message, context);
        } else {
          LoadingOverlay.show(context);
        }
      },
      child: Scaffold(
        // DRAWER
        endDrawer: Drawer(
          shape: const RoundedRectangleBorder(
            borderRadius: cardBorderRadius,
          ),
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                decoration: const BoxDecoration(
                  color: secondary,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.dataKapal.namaKapal ?? empty,
                      style: ptSansTextStyle.copyWith(
                        color: white,
                        fontSize: 16,
                        fontWeight: medium,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        CustomImageNetwork(
                          imageUrl: widget.dataKapal.image,
                          height: 80,
                          borderRadius: const BorderRadius.all(
                              Radius.circular(defaultRadius)),
                          width: 80,
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "IMO : ${widget.dataKapal.imoNumber ?? empty}",
                              style: ptSansTextStyle.copyWith(
                                color: primaryAccent,
                                fontSize: 12,
                              ),
                            ),
                            const SizedBox(
                              height: 6,
                            ),
                            Text(
                              "GT   : ${widget.dataKapal.gt ?? empty}",
                              style: ptSansTextStyle.copyWith(
                                color: primaryAccent,
                                fontSize: 12,
                              ),
                            ),
                            const SizedBox(
                              height: 6,
                            ),
                            Text(
                              "LOA : ${widget.dataKapal.loa ?? empty}",
                              style: ptSansTextStyle.copyWith(
                                color: primaryAccent,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
              ListTile(
                leading: const Icon(
                  Icons.info,
                  color: secondaryAccent,
                ),
                title: Text(
                  'App Info',
                  style: ptSansTextStyle.copyWith(
                    color: black54,
                    fontWeight: medium,
                    fontSize: 16,
                  ),
                ),
                onTap: () async {
                  PackageInfo packageInfo = await PackageInfo.fromPlatform();
                  String version = packageInfo.version;
                  showDialog(
                      context: context,
                      builder: ((context) => Dialog(
                            elevation: 6,
                            child: Container(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Image.asset(
                                    imageLogo2,
                                    height: 50,
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "V$version",
                                    style: ptSansTextStyle.copyWith(
                                        fontWeight: semiBold, color: lightText),
                                  ),
                                ],
                              ),
                            ),
                          )));
                },
              ),
              ListTile(
                leading: const Icon(
                  Icons.logout,
                  color: secondaryAccent,
                ),
                subtitle: Text(
                    '${widget.profile.namaDepan ?? empty} ${widget.profile.namaBelakang ?? empty} ACCOUNT',
                    style: ptSansTextStyle.copyWith(
                      color: black38,
                      fontWeight: light,
                      fontSize: 9,
                    )),
                title: Text(
                  'Logout',
                  style: ptSansTextStyle.copyWith(
                    color: black54,
                    fontWeight: medium,
                    fontSize: 16,
                  ),
                ),
                onTap: () {
                  BlocProvider.of<ShipActivityBloc>(context).add(LogoutEvent());
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          iconTheme: const IconThemeData(color: white, size: 19),
          backgroundColor: secondary,
          title: Image.asset(
            imageLogo2,
            height: 70,
          ),
        ),
        backgroundColor: semiWhite,
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: [
                    CustomImageNetwork(
                      imageUrl: widget.dataKapal.image,
                      height: 190,
                      width: width,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(itemImage6),
                              fit: BoxFit.cover)),
                      height: 190,
                      width: width,
                      padding: const EdgeInsets.all(
                        horizontalMargin2,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              CustomImageNetwork(
                                imageUrl: widget.dataKapal.image,
                                height: 120,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(defaultRadius)),
                                width: 120,
                              ),
                              const SizedBox(
                                width: horizontalMargin2,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(widget.dataKapal.namaKapal.toString(),
                                      style: ptSansTextStyle.copyWith(
                                          color: white,
                                          fontSize: 18,
                                          fontWeight: bold)),
                                  const SizedBox(
                                    height: 6,
                                  ),
                                  BlocBuilder<AlertBahanBakarDanAirTawarBloc,
                                      AlertBahanBakarDanAirTawarState>(
                                    builder: (context, state) {
                                      if (state
                                          is AlertBahanBakarDanAirTawarSuccess) {
                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const SizedBox(
                                              height: 12,
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                if (widget.dataKapal.idKapal !=
                                                    null) {
                                                  Navigator.push(context,
                                                      MaterialPageRoute(
                                                          builder: (context2) {
                                                    return BlocProvider(
                                                        create: (BuildContext
                                                            context) {
                                                          return PermintaanBarangDanJasaBloc(
                                                              locator<
                                                                  PermintaanBarangDanJasaRepository>(),
                                                              locator<
                                                                  UserRepository>());
                                                        },
                                                        child:
                                                            PermintaanBarangDanJasaInput(
                                                          kapalId: widget
                                                                  .dataKapal
                                                                  .idKapal ??
                                                              -1,
                                                          isDateToSaveNotExpired:
                                                              true,
                                                          parameterStok:
                                                              ParameterStok.bbm,
                                                          date: locator<
                                                                  MyDate>()
                                                              .getDateToday(),
                                                          departmenId: locator<
                                                                      UserRole>()
                                                                  .isEngine()
                                                              ? idDepartemenEngine ??
                                                                  -1
                                                              : idDepartemenDeck ??
                                                                  -1,
                                                          isDeck: !locator<
                                                                  UserRole>()
                                                              .isEngine(),
                                                        ));
                                                  }));
                                                }
                                              },
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "Bahan Bakar : ",
                                                    style: ptSansTextStyle
                                                        .copyWith(
                                                            color:
                                                                primaryAccent),
                                                  ),
                                                  Text(
                                                    "${state.alertBahanBakar.totalSaatIni}",
                                                    style: ptSansTextStyle.copyWith(
                                                        color: state.alertBahanBakar
                                                                    .isRequestBbm ==
                                                                1
                                                            ? red
                                                            : green,
                                                        fontSize: 14,
                                                        fontWeight: semiBold),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 6,
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                if (widget.dataKapal.idKapal !=
                                                    null) {
                                                  Navigator.push(context,
                                                      MaterialPageRoute(
                                                          builder: (context2) {
                                                    return BlocProvider(
                                                        create: (BuildContext
                                                            context) {
                                                          return PermintaanBarangDanJasaBloc(
                                                              locator<
                                                                  PermintaanBarangDanJasaRepository>(),
                                                              locator<
                                                                  UserRepository>());
                                                        },
                                                        child:
                                                            PermintaanBarangDanJasaInput(
                                                          kapalId: widget
                                                                  .dataKapal
                                                                  .idKapal ??
                                                              -1,
                                                          isDateToSaveNotExpired:
                                                              true,
                                                          parameterStok:
                                                              ParameterStok.air,
                                                          date: locator<
                                                                  MyDate>()
                                                              .getDateToday(),
                                                          departmenId: locator<
                                                                      UserRole>()
                                                                  .isEngine()
                                                              ? idDepartemenEngine ??
                                                                  -1
                                                              : idDepartemenDeck ??
                                                                  -1,
                                                          isDeck: !locator<
                                                                  UserRole>()
                                                              .isEngine(),
                                                        ));
                                                  }));
                                                }
                                              },
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "Air Tawar      : ",
                                                    style: ptSansTextStyle
                                                        .copyWith(
                                                            color:
                                                                primaryAccent),
                                                  ),
                                                  Text(
                                                    "${state.alertAirTawar.totalSaatIni}",
                                                    style: ptSansTextStyle.copyWith(
                                                        color: state.alertAirTawar
                                                                    .isRequestAirTawar ==
                                                                1
                                                            ? red
                                                            : green,
                                                        fontSize: 14,
                                                        fontWeight: semiBold),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        );
                                      }
                                      return const SizedBox();
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  color: semiWhite,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: horizontalMargin2),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Activity",
                                  style: ptSansTextStyle.copyWith(
                                      fontWeight: bold,
                                      color: secondary,
                                      fontSize: 24),
                                ),
                                Text(
                                  widget.profile.roleName ?? empty,
                                  style: ptSansTextStyle.copyWith(
                                      color: black38,
                                      fontSize: 14,
                                      fontWeight: light),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: [
                                Text(
                                  "by  ",
                                  style: ptSansTextStyle.copyWith(
                                      fontWeight: medium,
                                      color: primary,
                                      fontSize: 16),
                                ),
                                Text(
                                  "${widget.profile.namaDepan ?? empty} ${widget.profile.namaBelakang ?? empty} ",
                                  style: ptSansTextStyle.copyWith(
                                      fontWeight: medium,
                                      color: lightText,
                                      fontSize: 16),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            const Divider(
                              thickness: 1,
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            ActivityItem(
                              callback: () {
                                Navigator.pushNamed(
                                    context, SoundingBahanBakarPage.route,
                                    arguments: widget.dataKapal.idKapal ?? 0);
                              },
                              title: "Sounding Bahan Bakar",
                              image: activityImage1,
                              icon: const FaIcon(
                                FontAwesomeIcons.oilWell,
                                color: semiWhite,
                              ),
                            ),
                            ActivityItem(
                              callback: () {
                                Navigator.pushNamed(
                                    context,
                                    PemakaianBahanBakarDanJamKerjaMesinPage
                                        .route,
                                    arguments: [
                                      widget.dataKapal.idKapal ?? 0,
                                      locator<UserRole>().isEngine()
                                          ? idDepartemenEngine ?? -1
                                          : idDepartemenDeck ?? -1
                                    ]);
                              },
                              title:
                                  "Pemakaian Bahan Bakar dan Jam Kerja Mesin",
                              image: activityImage2,
                              icon: const FaIcon(
                                FontAwesomeIcons.clock,
                                color: semiWhite,
                              ),
                            ),
                            ActivityItem(
                              callback: () {
                                Navigator.pushNamed(
                                    context, PemakaianOliMesinPage.route,
                                    arguments: [
                                      widget.dataKapal.idKapal ?? 0,
                                      locator<UserRole>().isEngine()
                                          ? idDepartemenEngine ?? -1
                                          : idDepartemenDeck ?? -1
                                    ]);
                              },
                              title: "Pemakaian Oli Mesin",
                              image: activityImage3,
                              icon: const FaIcon(
                                FontAwesomeIcons.soap,
                                color: semiWhite,
                              ),
                            ),
                            ActivityItem(
                              callback: () {
                                Navigator.pushNamed(
                                    context, PemakaianAirTawarPage.route,
                                    arguments: [
                                      widget.dataKapal.idKapal ?? 0,
                                      locator<UserRole>().isEngine()
                                          ? idDepartemenEngine ?? -1
                                          : idDepartemenDeck ?? -1
                                    ]);
                              },
                              title: "Pemakaian Air Tawar",
                              image: activityImage4,
                              icon: const FaIcon(
                                FontAwesomeIcons.bottleWater,
                                color: semiWhite,
                              ),
                            ),
                            ActivityItem(
                              callback: () {
                                Navigator.pushNamed(
                                  context,
                                  PerawatanHarianDepartemenDeckDanDepartemenEnginePage
                                      .route,
                                  arguments: widget.dataKapal,
                                );
                              },
                              title:
                                  "Perawatan Harian Dept. Deck dan Dept. Engine",
                              image: activityImage5,
                              icon: const FaIcon(
                                FontAwesomeIcons.calendarDay,
                                color: semiWhite,
                              ),
                            ),
                            ActivityItem(
                              callback: () {
                                Navigator.pushNamed(
                                  context,
                                  PerawatanTerencanaDepartemenDeckDanDepartemenEnginePage
                                      .route,
                                  arguments: widget.dataKapal,
                                );
                              },
                              title:
                                  "Pemeliharan Terencana Dept. Deck dan Dept. Engine",
                              image: activityImage6,
                              icon: const FaIcon(
                                FontAwesomeIcons.calendarCheck,
                                color: semiWhite,
                              ),
                            ),
                            ActivityItem(
                              callback: () {
                                Navigator.pushNamed(
                                  context,
                                  PermintaanBarangDanJasaPage.route,
                                  arguments: widget.dataKapal,
                                );
                              },
                              title:
                                  "Permintaan Barang dan Jasa & Tanda Terima Barang",
                              image: activityImage7,
                              icon: const FaIcon(
                                FontAwesomeIcons.boxesStacked,
                                color: semiWhite,
                              ),
                            ),
                            ActivityItem(
                              callback: () {
                                Navigator.pushNamed(
                                  context,
                                  KerusakanDanPerbaikanKapalPage.route,
                                  arguments: widget.dataKapal,
                                );
                              },
                              title: "Kerusakan Dan Perbaikan Kapal",
                              image: activityImage8,
                              icon: const FaIcon(
                                FontAwesomeIcons.ship,
                                color: semiWhite,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
