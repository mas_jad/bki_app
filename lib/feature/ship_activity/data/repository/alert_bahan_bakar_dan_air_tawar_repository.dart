import 'package:pms_pcm/core/util/request.dart';
import 'package:pms_pcm/feature/ship_activity/data/source/alert_bahan_bakar_dan_air_tawar_remote_data_source.dart';

import '../model/alert_air_tawar.dart';
import '../model/alert_bahan_bakar.dart';

class AlertBahanBakarDanAirTawarRepository {
  final AlertBahanBakarDanAirTawarRemoteDataSouce
      _alertBahanBakarDanAirTawarRemoteDataSouce;

  AlertBahanBakarDanAirTawarRepository(
      this._alertBahanBakarDanAirTawarRemoteDataSouce);
  Future<AlertBahanBakar?> getAlertBahanBakar(int kapalId, String token) async {
    return RequestServer<AlertBahanBakar?>().requestServer(
        computation: () async {
      final data = await _alertBahanBakarDanAirTawarRemoteDataSouce
          .getAlertBahanBakar(kapalId, token);
      return data;
    });
  }

  Future<AlertAirTawar?> getAlertAirTawar(int kapalId, String token) async {
    return RequestServer<AlertAirTawar?>().requestServer(computation: () async {
      final data = await _alertBahanBakarDanAirTawarRemoteDataSouce
          .getAlertAirTawar(kapalId, token);
      return data;
    });
  }
}
