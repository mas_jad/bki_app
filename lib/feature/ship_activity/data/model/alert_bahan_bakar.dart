class AlertBahanBakar {
  int? code;
  String? batasMinimum;
  String? totalSaatIni;
  int? isRequestBbm;
  String? message;

  AlertBahanBakar(
      {this.code,
      this.batasMinimum,
      this.totalSaatIni,
      this.isRequestBbm,
      this.message});

  AlertBahanBakar.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    batasMinimum = json['batas_minimum'];
    totalSaatIni = json['total_saat_ini'];
    isRequestBbm = json['is_request_bbm'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['batas_minimum'] = batasMinimum;
    data['total_saat_ini'] = totalSaatIni;
    data['is_request_bbm'] = isRequestBbm;
    data['message'] = message;
    return data;
  }
}
