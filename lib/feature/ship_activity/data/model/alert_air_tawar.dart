class AlertAirTawar {
  int? code;
  String? batasMinimum;
  String? totalSaatIni;
  int? isRequestAirTawar;
  String? message;

  AlertAirTawar(
      {this.code,
      this.batasMinimum,
      this.totalSaatIni,
      this.isRequestAirTawar,
      this.message});

  AlertAirTawar.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    batasMinimum = json['batas_minimum'];
    totalSaatIni = json['total_saat_ini'];
    isRequestAirTawar = json['is_request_air_tawar'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['batas_minimum'] = batasMinimum;
    data['total_saat_ini'] = totalSaatIni;
    data['is_request_air_tawar'] = isRequestAirTawar;
    data['message'] = message;
    return data;
  }
}
