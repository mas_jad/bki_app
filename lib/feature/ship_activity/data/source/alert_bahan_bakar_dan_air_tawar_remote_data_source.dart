import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/feature/ship_activity/data/model/alert_air_tawar.dart';
import 'package:pms_pcm/feature/ship_activity/data/model/alert_bahan_bakar.dart';

class AlertBahanBakarDanAirTawarRemoteDataSouce {
  Future<AlertBahanBakar> getAlertBahanBakar(int kapalId, String token) async {
    final endpoint = dotenv.get(alertBahanBakarEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {"kapal_id": kapalId.toString()},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return AlertBahanBakar.fromJson(response.data);
  }

  Future<AlertAirTawar> getAlertAirTawar(int kapalId, String token) async {
    final endpoint = dotenv.get(alertAirTawarEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {"kapal_id": kapalId.toString()},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return AlertAirTawar.fromJson(response.data);
  }
}
