import 'package:pms_pcm/core/data/model/approve.dart';

import 'laporan_kerusakan_dan_perbaikan.dart';

class HistoryLaporanPerbaikanKapalResponse {
  String? message;
  int? code;
  int? total;
  List<HistoryLaporanPerbaikanKapal>? data;

  HistoryLaporanPerbaikanKapalResponse(
      {this.message, this.code, this.total, this.data});

  HistoryLaporanPerbaikanKapalResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    total = json['total'];
    if (json['data'] != null) {
      data = <HistoryLaporanPerbaikanKapal>[];
      json['data'].forEach((v) {
        data!.add(HistoryLaporanPerbaikanKapal.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    data['total'] = total;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HistoryLaporanPerbaikanKapal {
  int? id;
  String? noLpkk;
  RefrenceFromLkk? refrenceFromLkk;
  String? date;
  String? catatan;
  String? status;
  String? approved1;
  String? approved1Date;
  String? approved2;
  String? approved2Date;
  String? approved3;
  String? approved3Date;
  String? approved4;
  String? approved4Date;
  int? totalRevisi;
  List<LaporanPerbaikanKapal>? lpkk;
  Approve? approve;

  HistoryLaporanPerbaikanKapal(
      {this.id,
      this.noLpkk,
      this.refrenceFromLkk,
      this.date,
      this.catatan,
      this.status,
      this.approved1,
      this.approved1Date,
      this.approved2,
      this.approved2Date,
      this.approved3,
      this.approved3Date,
      this.approved4,
      this.approved4Date,
      this.totalRevisi,
      this.approve,
      this.lpkk});

  HistoryLaporanPerbaikanKapal.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    noLpkk = json['no_lpkk'];
    refrenceFromLkk = json['refrence_from_lkk'] != null
        ? RefrenceFromLkk.fromJson(json['refrence_from_lkk'])
        : null;
    date = json['date'];
    catatan = json['catatan'];
    status = json['status'];
    approved1 = json['approved_1'];
    approved1Date = json['approved_1_date'];
    approved2 = json['approved_2'];
    approved2Date = json['approved_2_date'];
    approved3 = json['approved_3'];
    approved3Date = json['approved_3_date'];
    approved4 = json['approved_4'];
    approved4Date = json['approved_4_date'];
    totalRevisi = json['total_revisi'];
    if (json['lpkk'] != null) {
      lpkk = <LaporanPerbaikanKapal>[];
      json['lpkk'].forEach((v) {
        lpkk!.add(LaporanPerbaikanKapal.fromJson(v));
      });
    }
    approve = Approve(
      status: status,
      approved1: approved1,
      approved1Date: approved1Date,
      approved2: approved2,
      approved2Date: approved2Date,
      approved3: approved3,
      approved3Date: approved3Date,
      approved4: approved4,
      approved4Date: approved4Date,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['no_lpkk'] = noLpkk;
    if (refrenceFromLkk != null) {
      data['refrence_from_lkk'] = refrenceFromLkk!.toJson();
    }
    data['date'] = date;
    data['catatan'] = catatan;
    data['status'] = status;
    data['approved_1'] = approved1;
    data['approved_1_date'] = approved1Date;
    data['approved_2'] = approved2;
    data['approved_2_date'] = approved2Date;
    data['approved_3'] = approved3;
    data['approved_3_date'] = approved3Date;
    data['approved_4'] = approved4;
    data['approved_4_date'] = approved4Date;
    data['total_revisi'] = totalRevisi;
    if (lpkk != null) {
      data['lpkk'] = lpkk!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RefrenceFromLkk {
  String? refNoLkk;
  String? dariKapal;
  String? departemen;
  String? kualifikasi;
  String? date;

  RefrenceFromLkk(
      {this.refNoLkk,
      this.dariKapal,
      this.departemen,
      this.kualifikasi,
      this.date});

  RefrenceFromLkk.fromJson(Map<String, dynamic> json) {
    refNoLkk = json['ref_no_lkk'];
    dariKapal = json['dari_kapal'];
    departemen = json['departemen'];
    kualifikasi = json['kualifikasi'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ref_no_lkk'] = refNoLkk;
    data['dari_kapal'] = dariKapal;
    data['departemen'] = departemen;
    data['kualifikasi'] = kualifikasi;
    data['date'] = date;
    return data;
  }
}

class PenggunaanMaterial {
  int? idMasterBarang;
  int? idMasterBarangDiKapal;
  String? kodefikasiBarang;
  String? namaBarang;
  int? qty;
  String? satuan;

  PenggunaanMaterial(
      {this.idMasterBarang,
      this.idMasterBarangDiKapal,
      this.kodefikasiBarang,
      this.namaBarang,
      this.qty,
      this.satuan});

  PenggunaanMaterial.fromJson(Map<String, dynamic> json) {
    idMasterBarang = json['barang_id'];
    idMasterBarangDiKapal = json['barang_part_kapal_id'];
    kodefikasiBarang = json['kodefikasi_barang'];
    namaBarang = json['nama_barang'];
    qty = json['qty'];
    satuan = json['satuan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id_master_barang'] = idMasterBarang;
    data['id_master_barang_di_kapal'] = idMasterBarangDiKapal;
    data['kodefikasi_barang'] = kodefikasiBarang;
    data['nama_barang'] = namaBarang;
    data['qty'] = qty;
    data['satuan'] = satuan;
    return data;
  }
}
