import 'package:dio/dio.dart';

import 'laporan_kerusakan_dan_perbaikan.dart';

class PostPerbaikanKapal {
  String? noLkk;
  String? date;
  String? catatan;
  String? noLpkk;
  List<LaporanPerbaikanKapal>? lpkk;

  PostPerbaikanKapal(
      {this.noLkk, this.date, this.catatan, this.noLpkk, this.lpkk});

  PostPerbaikanKapal.fromJson(Map<String, dynamic> json) {
    noLkk = json['no_lkk'];
    date = json['date'];
    catatan = json['catatan'];
    noLpkk = json['no_lpkk'];
    if (json['lpkk'] != null) {
      lpkk = <LaporanPerbaikanKapal>[];
      json['lpkk'].forEach((v) {
        lpkk!.add(LaporanPerbaikanKapal.fromJson(v));
      });
    }
  }

  Future<FormData> toJson() async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['no_lkk'] = noLkk;
    data['date'] = date;
    data['catatan'] = catatan;
    data['no_lpkk'] = noLpkk;
    if (lpkk != null) {
      final List listLpkkFormData = [];
      for (int i = 0; i < lpkk!.length; i++) {
        final temporaryLpkk = await lpkk![i].toJson();
        listLpkkFormData.add(temporaryLpkk);
      }
      data['lpkk'] = listLpkkFormData;
    }

    return FormData.fromMap(data);
  }
}
