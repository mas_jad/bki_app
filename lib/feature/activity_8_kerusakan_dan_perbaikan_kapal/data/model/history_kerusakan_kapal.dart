import '../../../../core/data/model/approve.dart';
import 'laporan_kerusakan_dan_perbaikan.dart';

class HistoryLaporanKerusakanKapalResponse {
  String? message;
  int? code;
  List<HistoryLaporanKerusakanKapal>? data;

  HistoryLaporanKerusakanKapalResponse({this.message, this.code, this.data});

  HistoryLaporanKerusakanKapalResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    if (json['data'] != null) {
      data = <HistoryLaporanKerusakanKapal>[];
      json['data'].forEach((v) {
        data!.add(HistoryLaporanKerusakanKapal.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HistoryLaporanKerusakanKapal {
  int? id;
  String? noLkk;
  String? dariKapal;
  String? departemen;
  String? kualifikasi;
  String? date;
  String? catatan;
  String? status;
  String? approved1;
  String? approved1Date;
  String? approved2;
  String? approved2Date;
  String? approved3;
  String? approved3Date;
  String? approved4;
  String? approved4Date;
  String? solvedBy;
  String? solvedDate;
  int? totalRevisi;
  List<LaporanKerusakanKapal>? lkk;
  Approve? approve;

  HistoryLaporanKerusakanKapal(
      {this.id,
      this.noLkk,
      this.dariKapal,
      this.departemen,
      this.kualifikasi,
      this.date,
      this.catatan,
      this.status,
      this.approved1,
      this.approved1Date,
      this.approved2,
      this.approved2Date,
      this.approved3,
      this.approve,
      this.solvedBy,
      this.solvedDate,
      this.approved3Date,
      this.approved4,
      this.approved4Date,
      this.totalRevisi,
      this.lkk});

  HistoryLaporanKerusakanKapal.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    noLkk = json['no_lkk'];
    dariKapal = json['dari_kapal'];
    departemen = json['departemen'];
    kualifikasi = json['kualifikasi'];
    date = json['date'];
    catatan = json['catatan'];
    status = json['status'];
    approved1 = json['approved_1'];
    approved1Date = json['approved_1_date'];
    approved2 = json['approved_2'];
    approved2Date = json['approved_2_date'];
    approved3 = json['approved_3'];
    approved3Date = json['approved_3_date'];
    approved4 = json['approved_4'];
    approved4Date = json['approved_4_date'];
    totalRevisi = json['total_revisi'];
    solvedBy = json['solved_by'];
    solvedDate = json['solved_date'];
    if (json['lkk'] != null) {
      lkk = <LaporanKerusakanKapal>[];
      json['lkk'].forEach((v) {
        lkk!.add(LaporanKerusakanKapal.fromJson(v));
      });
    }
    approve = Approve(
      status: status,
      approved1: approved1,
      approved1Date: approved1Date,
      approved2: approved2,
      approved2Date: approved2Date,
      approved3: approved3,
      approved3Date: approved3Date,
      approved4: approved4,
      approved4Date: approved4Date,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['no_lkk'] = noLkk;
    data['dari_kapal'] = dariKapal;
    data['departemen'] = departemen;
    data['kualifikasi'] = kualifikasi;
    data['date'] = date;
    data['catatan'] = catatan;
    data['status'] = status;
    data['approved_1'] = approved1;
    data['approved_1_date'] = approved1Date;
    data['approved_2'] = approved2;
    data['approved_2_date'] = approved2Date;
    data['approved_3'] = approved3;
    data['approved_3_date'] = approved3Date;
    data['approved_4'] = approved4;
    data['approved_4_date'] = approved4Date;
    data['total_revisi'] = totalRevisi;
    data['solved_by'] = solvedBy;
    data['solved_date'] = solvedDate;
    if (lkk != null) {
      data['lkk'] = lkk!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
