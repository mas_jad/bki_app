import 'package:dio/dio.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import '../../../../core/util/const.dart';
import 'history_perbaikan_kapal.dart';

abstract class LaporanKerusakanDanPerbaikanKapal {
  int? id;
  String? activityLkkOrBagian;
  String? uraian;
  String? keterangan;
  String? networkImage;
  String? localImage;
  MultipartFile? sentImage;

  LaporanKerusakanDanPerbaikanKapal(
      {this.activityLkkOrBagian,
      this.keterangan,
      this.localImage,
      this.id,
      this.networkImage,
      this.uraian});

  Future<dynamic> getImage() async {
    if (localImage != null) {
      sentImage = await MultipartFile.fromFile(
        localImage!,
        filename: localImage!.split('/').last,
      );
    } else if (networkImage != null || networkImage != empty) {
      final FileInfo? imageFromNetwork =
          await DefaultCacheManager().getFileFromCache(networkImage ?? empty);
      if (imageFromNetwork?.file != null) {
        sentImage = await MultipartFile.fromFile(
          imageFromNetwork!.file.path,
          filename: imageFromNetwork.file.path.split('/').last,
        );
      }
    }

    return sentImage ?? empty;
  }
}

class LaporanKerusakanKapal extends LaporanKerusakanDanPerbaikanKapal {
  String? usahaPenanggulangan;
  LaporanKerusakanKapal(
      {this.usahaPenanggulangan,
      super.activityLkkOrBagian,
      super.keterangan,
      super.localImage,
      super.id,
      super.networkImage,
      super.uraian});

  LaporanKerusakanKapal.fromJson(Map<String, dynamic> json) {
    activityLkkOrBagian = json['bagian'];
    uraian = json['uraian_kerusakan'];
    usahaPenanggulangan = json['usaha_penanggulangan'];
    keterangan = json['keterangan'];
    networkImage = json['foto'];
    id = json['id'];
  }

  Future<Map<String, dynamic>> toJson() async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bagian'] = activityLkkOrBagian;
    data['uraian_kerusakan'] = uraian;
    data['usaha_penanggulangan'] = usahaPenanggulangan;
    data['keterangan'] = keterangan;
    data['foto'] = await getImage();
    return data;
  }

  LaporanPerbaikanKapal toLaporanPerbaikanKapal() {
    return LaporanPerbaikanKapal(
        penggunaanMaterial: [],
        activityLkkOrBagian: activityLkkOrBagian,
        id: id,
        keterangan: empty,
        localImage: null,
        networkImage: null,
        uraian: emptyStrip);
  }
}

class LaporanPerbaikanKapal extends LaporanKerusakanDanPerbaikanKapal {
  List<PenggunaanMaterial>? penggunaanMaterial;

  LaporanPerbaikanKapal({
    this.penggunaanMaterial,
    super.activityLkkOrBagian,
    super.keterangan,
    super.localImage,
    super.id,
    super.networkImage,
    super.uraian,
  });

  LaporanPerbaikanKapal.fromJson(Map<String, dynamic> json) {
    id = json['id_activity_lkk'];
    activityLkkOrBagian = json['activity_lkk'];
    uraian = json['uraian_perbaikan'];
    keterangan = json['keterangan'];
    networkImage = json['foto'];
    if (json['penggunaan_material'] != null) {
      penggunaanMaterial = <PenggunaanMaterial>[];
      json['penggunaan_material'].forEach((v) {
        penggunaanMaterial!.add(PenggunaanMaterial.fromJson(v));
      });
    }
  }

  Future<Map<String, dynamic>> toJson() async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id_activity_lkk'] = id;
    data['uraian_perbaikan'] = uraian;
    if (penggunaanMaterial != null) {
      data['penggunaan_material'] =
          penggunaanMaterial!.map((v) => v.toJson()).toList();
    }
    data['keterangan'] = keterangan ?? empty;
    data['foto'] = await getImage();
    return data;
  }
}
