import 'package:dio/dio.dart';

import 'laporan_kerusakan_dan_perbaikan.dart';

class PostKerusakanKapal {
  int? kapalId;
  int? departemenId;
  String? date;
  String? kualifikasi;
  String? catatan;
  String? noLkk;
  List<LaporanKerusakanKapal>? lkk;

  PostKerusakanKapal(
      {this.kapalId,
      this.departemenId,
      this.date,
      this.kualifikasi,
      this.catatan,
      this.noLkk,
      this.lkk});

  PostKerusakanKapal.fromJson(Map<String, dynamic> json) {
    kapalId = json['kapal_id'];
    departemenId = json['departemen_id'];
    date = json['date'];
    kualifikasi = json['kualifikasi'];
    catatan = json['catatan'];
    noLkk = json['no_lkk'];
    if (json['lkk'] != null) {
      lkk = <LaporanKerusakanKapal>[];
      json['lkk'].forEach((v) {
        lkk!.add(LaporanKerusakanKapal.fromJson(v));
      });
    }
  }

  Future<FormData> toJson() async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kapal_id'] = kapalId;
    data['departemen_id'] = departemenId;
    data['date'] = date;
    data['kualifikasi'] = kualifikasi;
    data['catatan'] = catatan;
    data['no_lkk'] = noLkk;
    if (lkk != null) {
      final List listLkkFormData = [];
      for (int i = 0; i < lkk!.length; i++) {
        final temporaryLkk = await lkk![i].toJson();
        listLkkFormData.add(temporaryLkk);
      }
      data['lkk'] = listLkkFormData;
    }

    return FormData.fromMap(data);
  }
}
