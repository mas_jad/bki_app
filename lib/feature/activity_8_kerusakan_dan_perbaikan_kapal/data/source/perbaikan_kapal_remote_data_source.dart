import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/util/user_role.dart';
import '../model/history_kerusakan_kapal.dart';
import '../model/history_perbaikan_kapal.dart';
import '../model/post_perbaikan_kapal.dart';

class PerbaikanKapalRemoteDataSource {
  Future<HistoryLaporanPerbaikanKapalResponse> getHistoryPerbaikanKapal(
      int id, String token, String date) async {
    final endpoint = dotenv.get(dataLPKKAllEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {
        "kapal_id": id,
        "date_now": date,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryLaporanPerbaikanKapalResponse.fromJson(response.data);
  }

  Future<HistoryLaporanKerusakanKapalResponse> getTaskKapal(
      int id, String token) async {
    final endpoint = dotenv.get(dataLPKKTASKAllEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {
        "kapal_id": id,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryLaporanKerusakanKapalResponse.fromJson(response.data);
  }

  Future<HistoryLaporanPerbaikanKapalResponse> simpanUpdatePerbaikanKapal(
      String token, PostPerbaikanKapal postPerbaikanKapal) async {
    final endpoint = dotenv.get(simpanUpdateDataLPKKEndpoint);
    // final position = await locator<LocationService>().getLastPosition();
    final data = await postPerbaikanKapal.toJson();

    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryLaporanPerbaikanKapalResponse.fromJson(response.data);
  }

  Future<HistoryLaporanPerbaikanKapalResponse> updateStatusPerbaikanKapal(
    int id,
    String token,
    String date,
    String noLkk,
    String noLpkk,
    Approve approve,
  ) async {
    final endpoint = dotenv.get(ubahStatusLPKKEndpoint);

    final response = await locator<Dio>().post(
      "$endpoint/${locator<UserRole>().getUpdateStatusURL(false, approve)}",
      data: {
        "kapal_id": id,
        "date": date,
        "no_lkk": noLkk,
        "no_lpkk": noLpkk,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryLaporanPerbaikanKapalResponse.fromJson(response.data);
  }
}
