import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/util/user_role.dart';
import '../model/history_kerusakan_kapal.dart';
import '../model/post_kerusakan_kapal.dart';

class KerusakanKapalRemoteDataSource {
  Future<HistoryLaporanKerusakanKapalResponse> getHistoryKerusakanKapal(
      int id, String token, String date) async {
    final endpoint = dotenv.get(dataLKKAllEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {
        "kapal_id": id,
        "date_now": date,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryLaporanKerusakanKapalResponse.fromJson(response.data);
  }

  Future<HistoryLaporanKerusakanKapalResponse> simpanUpdateKerusakanKapal(
      String token, PostKerusakanKapal postKerusakanKapal) async {
    final endpoint = dotenv.get(simpanUpdateDataLKKEndpoint);
    // final position = await locator<LocationService>().getLastPosition();
    final data = await postKerusakanKapal.toJson();

    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryLaporanKerusakanKapalResponse.fromJson(response.data);
  }

  Future<HistoryLaporanKerusakanKapalResponse> updateStatusKerusakanKapal(
    int id,
    String token,
    String date,
    String noLkk,
    Approve approve,
  ) async {
    final endpoint = dotenv.get(ubahStatusLKKEndpoint);

    final response = await locator<Dio>().post(
      "$endpoint/${locator<UserRole>().getUpdateStatusURL(false, approve)}",
      data: {
        "kapal_id": id,
        "date": date,
        "no_lkk": noLkk,
        "departemen_id": empty,
      },
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryLaporanKerusakanKapalResponse.fromJson(response.data);
  }
}
