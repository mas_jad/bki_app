import '../../../../core/data/model/approve.dart';
import '../../../../core/util/request.dart';
import '../model/history_kerusakan_kapal.dart';
import '../model/post_kerusakan_kapal.dart';
import '../source/kerusakan_kapal_remote_data_source.dart';

class KerusakanKapalRepository {
  final KerusakanKapalRemoteDataSource _permintaanBarangDanJasaRemoteDataSource;
  KerusakanKapalRepository(this._permintaanBarangDanJasaRemoteDataSource);

  Future<HistoryLaporanKerusakanKapalResponse> getHistoryKerusakanKapal(
      int id, String token, String date) async {
    return RequestServer<HistoryLaporanKerusakanKapalResponse>().requestServer(
        computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .getHistoryKerusakanKapal(id, token, date);

      return response;
    });
  }

  Future<HistoryLaporanKerusakanKapalResponse> postKerusakanKapal(
      String token, PostKerusakanKapal postKerusakanKapal) async {
    return RequestServer<HistoryLaporanKerusakanKapalResponse>().requestServer(
        computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .simpanUpdateKerusakanKapal(token, postKerusakanKapal);
      return response;
    });
  }

  Future<HistoryLaporanKerusakanKapalResponse> updateStatusKerusakanKapal(
    int id,
    String token,
    String date,
    String noLkk,
    Approve approve,
  ) async {
    return RequestServer<HistoryLaporanKerusakanKapalResponse>().requestServer(
        computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .updateStatusKerusakanKapal(id, token, date, noLkk, approve);
      return response;
    });
  }
}
