import '../../../../core/data/model/approve.dart';
import '../../../../core/util/request.dart';
import '../model/history_kerusakan_kapal.dart';
import '../model/history_perbaikan_kapal.dart';
import '../model/post_perbaikan_kapal.dart';
import '../source/perbaikan_kapal_remote_data_source.dart';

class PerbaikanKapalRepository {
  final PerbaikanKapalRemoteDataSource _permintaanBarangDanJasaRemoteDataSource;
  PerbaikanKapalRepository(this._permintaanBarangDanJasaRemoteDataSource);

  Future<HistoryLaporanPerbaikanKapalResponse> getHistoryPerbaikanKapal(
      int id, String token, String date) async {
    return RequestServer<HistoryLaporanPerbaikanKapalResponse>().requestServer(
        computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .getHistoryPerbaikanKapal(id, token, date);

      return response;
    });
  }

  Future<HistoryLaporanKerusakanKapalResponse> getTaskKapal(
      int id, String token) async {
    return RequestServer<HistoryLaporanKerusakanKapalResponse>().requestServer(
        computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .getTaskKapal(id, token);

      return response;
    });
  }

  Future<HistoryLaporanPerbaikanKapalResponse> postPerbaikanKapal(
      String token, PostPerbaikanKapal postPerbaikanKapal) async {
    return RequestServer<HistoryLaporanPerbaikanKapalResponse>().requestServer(
        computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .simpanUpdatePerbaikanKapal(token, postPerbaikanKapal);
      return response;
    });
  }

  Future<HistoryLaporanPerbaikanKapalResponse> updateStatusPerbaikanKapal(
    int id,
    String token,
    String date,
    String noLkk,
    String noLpkk,
    Approve approve,
  ) async {
    return RequestServer<HistoryLaporanPerbaikanKapalResponse>().requestServer(
        computation: () async {
      final response = await _permintaanBarangDanJasaRemoteDataSource
          .updateStatusPerbaikanKapal(id, token, date, noLkk, noLpkk, approve);
      return response;
    });
  }
}
