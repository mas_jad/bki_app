import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/widget/kerusakan/kerusakan_kapal_widget.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/widget/perbaikan/perbaikan_kapal_widget.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/widget/task/task_kapal_widget.dart';

import '../../../core/data/model/data_kapal.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/service/date/date.dart';
import '../../../core/theme/theme.dart';
import '../../../core/util/locator.dart';
import '../bloc/kerusakan_kapal_bloc.dart';
import '../bloc/perbaikan_kapal_bloc.dart';
import '../bloc/task_kapal_bloc.dart';
import '../data/repository/kerusakan_kapal_repository.dart';
import '../data/repository/perbaikan_kapal_repository.dart';

class KerusakanDanPerbaikanKapalPage extends StatefulWidget {
  const KerusakanDanPerbaikanKapalPage({required this.dataKapal, super.key});
  static const route = "/activity_kerusakan_dan_perbaikan_kapal_page";
  final DataKapal dataKapal;

  @override
  State<KerusakanDanPerbaikanKapalPage> createState() =>
      _KerusakanDanPerbaikanKapalPageState();
}

class _KerusakanDanPerbaikanKapalPageState
    extends State<KerusakanDanPerbaikanKapalPage> {
  late ValueNotifier<DateTime> kerusakanDate;
  late ValueNotifier<DateTime> perbaikanDate;
  late ValueNotifier<DateTime> taskDate;

  final kerusakanBloc = KerusakanKapalBloc(
      locator<KerusakanKapalRepository>(), locator<UserRepository>());

  final perbaikanBloc = PerbaikanKapalBloc(
      locator<PerbaikanKapalRepository>(), locator<UserRepository>());

  final taskBloc = TaskKapalBloc(
      locator<PerbaikanKapalRepository>(), locator<UserRepository>());

  @override
  void initState() {
    super.initState();

    // GET DATE
    kerusakanDate = ValueNotifier(locator<MyDate>().getDateToday());
    perbaikanDate = ValueNotifier(locator<MyDate>().getDateToday());
    taskDate = ValueNotifier(locator<MyDate>().getDateToday());

    // ADD EVENT
    kerusakanBloc.add(GetHistoryKerusakanKapalEvent(
        widget.dataKapal.idKapal ?? -1,
        locator<MyDate>().getDateTodayInString()));
    perbaikanBloc.add(GetHistoryPerbaikanKapalEvent(
        widget.dataKapal.idKapal ?? -1,
        locator<MyDate>().getDateTodayInString()));
    taskBloc.add(GetHistoryTaskKapalEvent(
      widget.dataKapal.idKapal ?? -1,
    ));
  }

  @override
  void dispose() {
    kerusakanBloc.close();
    perbaikanBloc.close();
    taskBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: secondary,
          bottom: TabBar(
            indicatorWeight: 2.5,
            indicatorColor: primaryAccent,
            tabs: [
              Tab(
                child: Text(
                  "Kerusakan",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
              Tab(
                child: Text(
                  "Task",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
              Tab(
                child: Text(
                  "Perbaikan",
                  style: ptSansTextStyle.copyWith(
                      fontWeight: semiBold, color: white),
                ),
              ),
            ],
          ),
          elevation: 0,
          title: Text(
            "Kerusakan Dan Perbaikan Kapal",
            style: ptSansTextStyle.copyWith(color: white),
          ),
        ),
        body: MultiBlocProvider(
          providers: [
            BlocProvider.value(
              value: kerusakanBloc,
            ),
            BlocProvider.value(
              value: taskBloc,
            ),
            BlocProvider.value(
              value: perbaikanBloc,
            ),
          ],
          child: TabBarView(
            children: [
              KerusakanKapalWidget(
                dataKapal: widget.dataKapal,
                date: kerusakanDate,
              ),
              TaskKapalWidget(
                dataKapal: widget.dataKapal,
                date: perbaikanDate,
              ),
              PerbaikanKapalWidget(
                dataKapal: widget.dataKapal,
                date: perbaikanDate,
              )
            ],
          ),
        ),
      ),
    );
  }
}
