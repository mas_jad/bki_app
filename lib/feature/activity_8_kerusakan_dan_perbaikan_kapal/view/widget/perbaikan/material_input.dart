import 'package:flutter/material.dart';
import 'package:pms_pcm/core/data/model/master_barang_dan_jasa.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/data/model/history_perbaikan_kapal.dart';

import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/widget/button/custom_button_widget.dart';
import '../../../../../core/widget/textfield/custom_textfield_widget.dart';

class MaterialInput extends StatefulWidget {
  const MaterialInput(
      {this.material,
      required this.listMasterBarang,
      required this.isFromTask,
      required this.isUserPermitedToSave,
      super.key});
  final PenggunaanMaterial? material;
  final List<MasterBarang> listMasterBarang;
  final bool isUserPermitedToSave;
  final bool isFromTask;
  @override
  State<MaterialInput> createState() => _MaterialInputState();
}

class _MaterialInputState extends State<MaterialInput> {
  final TextEditingController namaBarangEditingController =
      TextEditingController();
  final TextEditingController jumlahEditingController = TextEditingController();
  final TextEditingController satuanEditingController = TextEditingController();
  late ValueNotifier<String?> kodefikasiBarang;
  late ValueNotifier<int?> idMasterBarang;
  late ValueNotifier<int?> idMasterBarangDiKapal;

  @override
  void initState() {
    super.initState();
    jumlahEditingController.text = widget.material?.qty?.toString() ?? empty;
    namaBarangEditingController.text =
        widget.material?.namaBarang?.toString() ?? empty;
    satuanEditingController.text = widget.material?.satuan ?? empty;
    kodefikasiBarang = ValueNotifier(widget.material?.kodefikasiBarang);
    idMasterBarang = ValueNotifier(widget.material?.idMasterBarang);
    idMasterBarangDiKapal =
        ValueNotifier(widget.material?.idMasterBarangDiKapal);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Container(
        width: width,
        decoration: const BoxDecoration(
            color: white,
            boxShadow: [shadowCard],
            borderRadius: cardBorderRadius),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Text(
                          "Pilih Material",
                          style: ptSansTextStyle.copyWith(
                              color: white, fontWeight: bold, fontSize: 22),
                        ),
                        Visibility(
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                          visible: widget.material != null ? true : false,
                          child: IconButton(
                              onPressed: (() {
                                Navigator.pop(context, true);
                              }),
                              icon: iconDelete.copyWith(color: white)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Autocomplete<MasterBarang>(
                      initialValue: TextEditingValue(
                        text: widget.material?.namaBarang ?? empty,
                      ),
                      optionsBuilder: (TextEditingValue textEditingValue) {
                        if (textEditingValue.text == empty) {
                          return widget.listMasterBarang;
                        }

                        return widget.listMasterBarang
                            .where((MasterBarang option) {
                          return (option.namaBarang?.toLowerCase() ?? empty)
                              .contains(
                            textEditingValue.text.toLowerCase(),
                          );
                        });
                      },
                      optionsViewBuilder: (
                        BuildContext context,
                        AutocompleteOnSelected<MasterBarang> onSelected,
                        Iterable<MasterBarang> options,
                      ) {
                        return Align(
                          alignment: Alignment.topLeft,
                          child: Material(
                            child: Container(
                              height: 60.0 * options.length,
                              decoration: const BoxDecoration(
                                color: white,
                                boxShadow: [
                                  shadowCard,
                                ],
                              ),
                              child: ListView.builder(
                                padding: const EdgeInsets.all(10.0),
                                itemCount: options.length,
                                itemBuilder: (
                                  BuildContext context,
                                  int index,
                                ) {
                                  final MasterBarang option =
                                      options.elementAt(index);
                                  return GestureDetector(
                                    onTap: () {
                                      onSelected(option);
                                    },
                                    child: ListTile(
                                      title: Text(
                                        option.namaBarang ?? empty,
                                        style: ptSansTextStyle.copyWith(
                                          color: black68,
                                          fontSize: 14,
                                        ),
                                      ),
                                      subtitle: Text(
                                        option.kodePart ?? empty,
                                        style: ptSansTextStyle.copyWith(
                                          color: black54,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                      fieldViewBuilder: (
                        BuildContext context,
                        TextEditingController fieldTextEditingController,
                        FocusNode fieldFocusNode,
                        VoidCallback onFieldSubmitted,
                      ) {
                        return CustomTextFieldWidget(
                          label: "Nama Barang",
                          focusNode: fieldFocusNode,
                          textEditingController: fieldTextEditingController,
                          textInputType: TextInputType.text,
                        );
                      },
                      onSelected: (MasterBarang selection) {
                        namaBarangEditingController.text =
                            selection.namaBarang ?? empty;
                        satuanEditingController.text =
                            selection.satuan ?? empty;
                        kodefikasiBarang.value = selection.kodefikasi;
                        idMasterBarang.value = selection.barangId;
                        idMasterBarangDiKapal.value = selection.barangIdDiKapal;
                      },
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: CustomTextFieldWidget(
                            label: "Jumlah Barang",
                            textEditingController: jumlahEditingController,
                            textInputType: TextInputType.number,
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          flex: 1,
                          child: CustomTextFieldWidget(
                            label: "Satuan",
                            isEnable: false,
                            textEditingController: satuanEditingController,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    if (widget.isFromTask || widget.isUserPermitedToSave)
                      CustomButtonWidget(
                          callback: () {
                            if (kodefikasiBarang.value != null) {
                              if (jumlahEditingController.text.isEmpty) {
                                MyToast.showError(
                                    "Jumlah belum ditentukan", context);
                              } else {
                                Navigator.pop(
                                    context,
                                    PenggunaanMaterial(
                                        namaBarang:
                                            namaBarangEditingController.text,
                                        satuan: satuanEditingController.text,
                                        idMasterBarang: idMasterBarang.value,
                                        idMasterBarangDiKapal:
                                            idMasterBarangDiKapal.value,
                                        kodefikasiBarang:
                                            kodefikasiBarang.value,
                                        qty: int.tryParse(
                                                jumlahEditingController.text) ??
                                            0));
                              }
                            } else {
                              MyToast.showError(
                                  "Barang Belum Dipilih", context);
                            }
                          },
                          title: save)
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
