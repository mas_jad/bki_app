import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/departemen.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/data/model/history_kerusakan_kapal.dart';

import '../../../../../core/data/model/approve.dart';
import '../../../../../core/data/model/data_kapal.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/util/locator.dart';
import '../../../../../core/util/user_role.dart';
import '../../../../../core/widget/button/custom_button_widget.dart';
import '../../../../../core/widget/dialog/custom_alert_dialog.dart';
import '../../../../../core/widget/image/image_container_picker.dart';
import '../../../../../core/widget/other/loading_widget.dart';
import '../../../../../core/widget/textfield/custom_textfield_widget.dart';
import '../../../bloc/perbaikan_kapal_bloc.dart';
import '../../../data/model/history_perbaikan_kapal.dart';
import '../../../data/model/laporan_kerusakan_dan_perbaikan.dart';
import '../../../data/model/post_perbaikan_kapal.dart';
import 'lpkk_input.dart';

class PerbaikanKapalInput extends StatefulWidget {
  const PerbaikanKapalInput(
      {required this.dataKapal,
      this.historyLaporanPerbaikanKapal,
      this.historyLaporanKerusakanKapal,
      this.isDateToSaveNotExpired = false,
      this.isFromTask = false,
      super.key});
  final DataKapal dataKapal;
  final HistoryLaporanPerbaikanKapal? historyLaporanPerbaikanKapal;
  final HistoryLaporanKerusakanKapal? historyLaporanKerusakanKapal;
  final bool isDateToSaveNotExpired;
  final bool isFromTask;
  @override
  State<PerbaikanKapalInput> createState() => _PerbaikanKapalInputState();
}

class _PerbaikanKapalInputState extends State<PerbaikanKapalInput> {
  final TextEditingController catatanTextEditingController =
      TextEditingController();
  List<Departemen> listDepartemen = [];
  final ValueNotifier<List<LaporanKerusakanDanPerbaikanKapal>>
      listLkkAndLpkkValue = ValueNotifier([]);

  bool isUserPermitedToApprove = false;
  bool isUserPermitedToSave = false;

  @override
  void initState() {
    super.initState();
    // PERMISSION
    isUserPermitedToApprove = widget.historyLaporanPerbaikanKapal == null
        ? false
        : locator<UserRole>().isUserPermitedToApprove(
            widget.historyLaporanPerbaikanKapal?.approve ?? Approve());
    isUserPermitedToSave = locator<UserRole>().isUserPermitedToSave(
        widget.historyLaporanPerbaikanKapal?.approve ?? Approve(),
        widget.isDateToSaveNotExpired);

    // LKK
    listLkkAndLpkkValue.value = [
      ...widget.historyLaporanPerbaikanKapal?.lpkk ?? [],
      ...widget.historyLaporanKerusakanKapal?.lkk ?? []
    ];

    // CATATAN
    catatanTextEditingController.text =
        widget.historyLaporanPerbaikanKapal?.catatan ?? empty;
  }

  void savePerbaikanKapal() {
    final List<LaporanPerbaikanKapal> laporanPerbaiakanKapalList = [];
    for (var element in listLkkAndLpkkValue.value) {
      if (element is LaporanPerbaikanKapal) {
        laporanPerbaiakanKapalList.add(element);
      } else if (element is LaporanKerusakanKapal) {
        laporanPerbaiakanKapalList.add(element.toLaporanPerbaikanKapal());
      }
    }
    BlocProvider.of<PerbaikanKapalBloc>(context).add(PostPerbaikanKapalEvent(
        PostPerbaikanKapal(
            date: widget.historyLaporanPerbaikanKapal?.date ??
                locator<MyDate>().getDateTodayInString(),
            catatan: catatanTextEditingController.text,
            noLpkk: widget.historyLaporanPerbaikanKapal?.noLpkk,
            noLkk: widget
                    .historyLaporanPerbaikanKapal?.refrenceFromLkk?.refNoLkk ??
                widget.historyLaporanKerusakanKapal?.noLkk,
            lpkk: laporanPerbaiakanKapalList)));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PerbaikanKapalBloc, PerbaikanKapalState>(
      listener: (context, state) {
        if (state is PostPerbaikanKapalSuccess) {
          MyToast.showSuccess(success, context);
          Navigator.pop(context);
        } else if (state is PostPerbaikanKapalError) {
          MyToast.showError(state.message, context);
        }
        if (state is UpdateStatusPerbaikanKapalSuccess) {
          MyToast.showSuccess(success, context);

          Navigator.pop(context);
        } else if (state is UpdateStatusPerbaikanKapalError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              // HEADER
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Flexible(
                          child: Text(
                            "Perbaikan Kapal",
                            textAlign: TextAlign.center,
                            style: ptSansTextStyle.copyWith(
                                color: white, fontWeight: bold, fontSize: 20),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8),
                            child: iconArrow.copyWith(color: transparent)),
                      ],
                    ),
                  ],
                ),
              ),
              if (widget.historyLaporanKerusakanKapal != null)
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(
                      top: 20, right: 20, left: 20, bottom: 10),
                  padding: const EdgeInsets.all(20),
                  decoration: const BoxDecoration(
                      color: white,
                      boxShadow: [shadowCard],
                      borderRadius: cardBorderRadius),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                widget.historyLaporanKerusakanKapal?.noLkk
                                        .toString() ??
                                    empty,
                                style: ptSansTextStyle.copyWith(
                                    color: secondary,
                                    fontWeight: light,
                                    fontSize: 16),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Text(
                                "Rusak   :  ",
                                style: ptSansTextStyle.copyWith(
                                    color: black38, fontWeight: bold),
                              ),
                              Text(
                                "${locator<MyDate>().getDateWithInputStringIndonesiaFormat(widget.historyLaporanKerusakanKapal?.date)}",
                                style: ptSansTextStyle.copyWith(
                                    color: red, fontWeight: medium),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              if (widget.historyLaporanPerbaikanKapal != null)
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(
                      top: 20, right: 20, left: 20, bottom: 10),
                  padding: const EdgeInsets.all(20),
                  decoration: const BoxDecoration(
                      color: white,
                      boxShadow: [shadowCard],
                      borderRadius: cardBorderRadius),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                widget.historyLaporanPerbaikanKapal?.noLpkk
                                        .toString() ??
                                    empty,
                                style: ptSansTextStyle.copyWith(
                                    color: primary,
                                    fontWeight: light,
                                    fontSize: 16),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          Row(
                            children: [
                              Text(
                                "REF : ",
                                style: ptSansTextStyle.copyWith(
                                    color: red,
                                    fontWeight: light,
                                    fontSize: 12),
                              ),
                              Text(
                                widget.historyLaporanPerbaikanKapal
                                        ?.refrenceFromLkk?.refNoLkk ??
                                    empty,
                                style: ptSansTextStyle.copyWith(
                                    color: secondaryAccent,
                                    fontWeight: light,
                                    fontSize: 12),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Text(
                                "Rusak         :  ",
                                style: ptSansTextStyle.copyWith(
                                    color: black38, fontWeight: bold),
                              ),
                              Text(
                                "${locator<MyDate>().getDateWithInputStringIndonesiaFormat(widget.historyLaporanPerbaikanKapal?.refrenceFromLkk?.date)}",
                                style: ptSansTextStyle.copyWith(
                                    color: red, fontWeight: medium),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          Row(
                            children: [
                              Text(
                                "Diperbaiki  :  ",
                                style: ptSansTextStyle.copyWith(
                                    color: black38, fontWeight: bold),
                              ),
                              Text(
                                "${locator<MyDate>().getDateWithInputStringIndonesiaFormat(widget.historyLaporanPerbaikanKapal?.date)}",
                                style: ptSansTextStyle.copyWith(
                                    color: primary, fontWeight: medium),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          Row(
                            children: [
                              Text(
                                "Status         :  ",
                                style: ptSansTextStyle.copyWith(
                                    color: black38, fontWeight: bold),
                              ),
                              Text(
                                widget.historyLaporanPerbaikanKapal?.approve
                                        ?.getStatus() ??
                                    DRAF,
                                style: ptSansTextStyle.copyWith(
                                    color: statusColor(widget
                                            .historyLaporanPerbaikanKapal
                                            ?.approve
                                            ?.getStatus() ??
                                        DRAF),
                                    fontWeight: semiBold,
                                    fontSize: 14),
                              ),
                              if (widget.historyLaporanPerbaikanKapal?.approve
                                      ?.getName() !=
                                  null)
                                Flexible(
                                  child: Text(
                                    " by ${widget.historyLaporanPerbaikanKapal?.approve?.getName() ?? empty}",
                                    maxLines: 1,
                                    style: ptSansTextStyle.copyWith(
                                        color: lightText,
                                        fontWeight: reguler,
                                        fontSize: 14,
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // LIST KERUSAKAN
                    Text(
                      "List Perbaikan",
                      style: ptSansTextStyle.copyWith(color: primary),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: white,
                          border: Border.all(color: black26, width: 0.5),
                          borderRadius: cardBorderRadius),
                      child: ValueListenableBuilder(
                          valueListenable: listLkkAndLpkkValue,
                          builder: (context, value, child) {
                            return Column(
                              children: [
                                ...lpkkContainer(),
                                const SizedBox(
                                  height: 12,
                                )
                              ],
                            );
                          }),
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    // CATATAN
                    CustomTextFieldWidget(
                      label: "Catatan",
                      maxlines: 6,
                      textEditingController: catatanTextEditingController,
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    // BUTTON
                    if (isUserPermitedToSave || widget.isFromTask)
                      BlocBuilder<PerbaikanKapalBloc, PerbaikanKapalState>(
                        builder: (context, state) {
                          return CustomButtonWidget(
                              isActive: state is PostPerbaikanKapalLoading
                                  ? false
                                  : true,
                              callback: () {
                                savePerbaikanKapal();
                              },
                              title: save);
                        },
                      ),
                    const SizedBox(
                      height: 60,
                    ),
                  ],
                ),
              )
            ],
          ),
        ), // FLOATING ACTION BUTTON
        floatingActionButton:
            BlocBuilder<PerbaikanKapalBloc, PerbaikanKapalState>(
          builder: (context, state) {
            if (isUserPermitedToApprove) {
              if (state is UpdateStatusPerbaikanKapalLoading) {
                return FloatingActionButton(
                  elevation: 12,
                  backgroundColor: secondary,
                  onPressed: () {},
                  child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                itemImage4,
                              ),
                              fit: BoxFit.fill)),
                      child: const Center(
                          child: LoadingWidget(
                        color: white,
                      ))),
                );
              }

              return FloatingActionButton(
                elevation: elevationFAB,
                backgroundColor: primary,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: ((dialogContext) => BlocProvider.value(
                            value: BlocProvider.of<PerbaikanKapalBloc>(context),
                            child: CustomAlertDialog(
                                description: dataCannotChangeAgain,
                                title: locator<UserRole>().getTitle(widget
                                        .historyLaporanPerbaikanKapal
                                        ?.approve ??
                                    Approve()),
                                voidCallback: () {
                                  BlocProvider.of<PerbaikanKapalBloc>(context)
                                      .add(UpdateStatusPerbaikanKapalEvent(
                                          widget.dataKapal.idKapal ?? -1,
                                          locator<MyDate>()
                                              .getDateTodayInString(),
                                          widget.historyLaporanPerbaikanKapal
                                                  ?.refrenceFromLkk?.refNoLkk ??
                                              empty,
                                          widget.historyLaporanPerbaikanKapal
                                                  ?.noLpkk ??
                                              empty,
                                          widget.historyLaporanPerbaikanKapal
                                                  ?.approve ??
                                              Approve()));
                                }),
                          )));
                },
                child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              itemImage4,
                            ),
                            fit: BoxFit.fill)),
                    child: Center(
                        child: locator<UserRole>().getIcon(
                            widget.historyLaporanPerbaikanKapal?.approve ??
                                Approve()))),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }

  // LIST LPKK
  List<Widget> lpkkContainer() {
    final List<Widget> list = [];
    for (var i = 0; i < listLkkAndLpkkValue.value.length; i++) {
      list.add(GestureDetector(
        onTap: () async {
          final lpkk = await showModalBottomSheet(
              isDismissible: false,
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              isScrollControlled: true,
              context: context,
              builder: (context) {
                return Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: LpkkInput(
                      isFromTask: widget.isFromTask,
                      isUserPermitedToSave: isUserPermitedToSave,
                      laporanKerusakanDanPerbaikanKapal:
                          listLkkAndLpkkValue.value[i],
                    ));
              });
          if (lpkk is LaporanKerusakanDanPerbaikanKapal) {
            listLkkAndLpkkValue.value.removeAt(i);
            listLkkAndLpkkValue.value.insert(i, lpkk);
            final tmp = [...listLkkAndLpkkValue.value];
            listLkkAndLpkkValue.value = tmp;
          } else if (lpkk is bool) {
            listLkkAndLpkkValue.value.removeAt(i);
            final tmp = [...listLkkAndLpkkValue.value];
            listLkkAndLpkkValue.value = tmp;
          }
        },
        child: Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: const BoxDecoration(
              color: grey200,
              borderRadius: BorderRadius.all(Radius.circular(14))),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 12, top: 10, bottom: 10),
                  child: Center(
                      child: ImageContainerPicker(
                    image: listLkkAndLpkkValue.value[i].networkImage,
                    ratioToWidth: 5,
                    isEnable: false,
                    imageValueNotifier:
                        ValueNotifier(listLkkAndLpkkValue.value[i].localImage),
                  )),
                ),
                Expanded(
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 12, top: 10, bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          listLkkAndLpkkValue.value[i].activityLkkOrBagian ??
                              empty,
                          style: ptSansTextStyle.copyWith(
                              color: black54,
                              fontSize: 16,
                              fontWeight: semiBold,
                              overflow: TextOverflow.ellipsis),
                          maxLines: 1,
                        ),
                        listLkkAndLpkkValue.value[i].uraian?.isNotEmpty ?? false
                            ? const SizedBox(
                                height: 6,
                              )
                            : const SizedBox(),
                        listLkkAndLpkkValue.value[i].uraian?.isNotEmpty ?? false
                            ? Text(
                                (listLkkAndLpkkValue.value[i].uraian ?? empty),
                                style: ptSansTextStyle.copyWith(
                                    color: black54,
                                    fontSize: 14,
                                    overflow: TextOverflow.ellipsis),
                                maxLines: 2,
                              )
                            : const SizedBox(),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  width: 6,
                ),
                Container(
                  width: 10,
                  decoration: BoxDecoration(
                      color:
                          listLkkAndLpkkValue.value[i] is LaporanKerusakanKapal
                              ? red
                              : green,
                      borderRadius: const BorderRadius.horizontal(
                          right: Radius.circular(14))),
                )
              ],
            ),
          ),
        ),
      ));
    }

    return list;
  }
}
