import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/data/model/history_perbaikan_kapal.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/widget/perbaikan/material_input.dart';

import '../../../../../core/bloc/master_barang_dan_jasa_bloc.dart';
import '../../../../../core/data/model/master_barang_dan_jasa.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/widget/button/custom_button_widget.dart';
import '../../../../../core/widget/image/image_container_picker.dart';
import '../../../../../core/widget/textfield/custom_textfield_widget.dart';
import '../../../data/model/laporan_kerusakan_dan_perbaikan.dart';

class LpkkInput extends StatefulWidget {
  const LpkkInput(
      {this.laporanKerusakanDanPerbaikanKapal,
      required this.isUserPermitedToSave,
      required this.isFromTask,
      super.key});
  final LaporanKerusakanDanPerbaikanKapal? laporanKerusakanDanPerbaikanKapal;
  final bool isUserPermitedToSave;
  final bool isFromTask;
  @override
  State<LpkkInput> createState() => _SoundingBahanBakarInputState();
}

class _SoundingBahanBakarInputState extends State<LpkkInput> {
  final TextEditingController bagianEditingController = TextEditingController();
  final TextEditingController uraianPerbaikanEditingController =
      TextEditingController();

  final TextEditingController keteranganEditingController =
      TextEditingController();
  late ValueNotifier<String?> imageValueNotifier;

  late ValueNotifier<List<PenggunaanMaterial>> listMaterialValue;

  // MASTER
  MasterBarangDanJasa? masterBarangDanJasa;

  @override
  void initState() {
    super.initState();
    final state = BlocProvider.of<MasterBarangDanJasaBloc>(context).state;
    masterBarangDanJasa =
        state is MasterBarangDanJasaSuccess ? state.masterBarangDanJasa : null;

    uraianPerbaikanEditingController.text = widget
            .laporanKerusakanDanPerbaikanKapal is LaporanPerbaikanKapal
        ? widget.laporanKerusakanDanPerbaikanKapal?.uraian?.toString() ?? empty
        : empty;
    bagianEditingController.text =
        widget.laporanKerusakanDanPerbaikanKapal?.activityLkkOrBagian ?? empty;

    keteranganEditingController.text = widget.laporanKerusakanDanPerbaikanKapal
            is LaporanPerbaikanKapal
        ? widget.laporanKerusakanDanPerbaikanKapal?.keterangan?.toString() ??
            empty
        : empty;

    imageValueNotifier =
        ValueNotifier(widget.laporanKerusakanDanPerbaikanKapal?.localImage);

    // INITIATE VALUE
    listMaterialValue = ValueNotifier<List<PenggunaanMaterial>>([]);
    if (widget.laporanKerusakanDanPerbaikanKapal is LaporanPerbaikanKapal) {
      final historyPerbaikan =
          widget.laporanKerusakanDanPerbaikanKapal as LaporanPerbaikanKapal;
      final List<PenggunaanMaterial> renewMaterialBarang = [
        ...historyPerbaikan.penggunaanMaterial ?? []
      ];

      listMaterialValue.value.addAll(renewMaterialBarang);
    }
  }

  saveLpkk() async {
    if (uraianPerbaikanEditingController.text.isEmpty) {
      MyToast.showError("Uraian Perbaikan Belum Diinputkan", context);
      return;
    }

    Navigator.pop(
        context,
        LaporanPerbaikanKapal(
            activityLkkOrBagian: bagianEditingController.text,
            keterangan: keteranganEditingController.text,
            uraian: uraianPerbaikanEditingController.text,
            id: widget.laporanKerusakanDanPerbaikanKapal?.id,
            penggunaanMaterial: listMaterialValue.value,
            localImage: imageValueNotifier.value,
            networkImage: widget.laporanKerusakanDanPerbaikanKapal
                    is LaporanPerbaikanKapal
                ? widget.laporanKerusakanDanPerbaikanKapal?.networkImage
                : empty));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(itemImage2), fit: BoxFit.cover),
              color: secondary,
              borderRadius: BorderRadius.all(Radius.circular(18))),
          child: Column(
            children: [
              const SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      onPressed: (() {
                        Navigator.pop(context);
                      }),
                      icon: iconArrow.copyWith(color: white)),
                  Text(
                    "Perbaikan Kapal",
                    style: ptSansTextStyle.copyWith(
                        color: white, fontWeight: bold, fontSize: 22),
                  ),
                  Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: false,
                    child: IconButton(
                        onPressed: (() {}),
                        icon: iconDelete.copyWith(color: white)),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 10),
                child: Center(
                    child: ImageContainerPicker(
                  image: widget.laporanKerusakanDanPerbaikanKapal
                          is LaporanPerbaikanKapal
                      ? widget.laporanKerusakanDanPerbaikanKapal?.networkImage
                      : null,
                  imageValueNotifier: imageValueNotifier,
                )),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomTextFieldWidget(
                label: "Bagian & Posisi",
                textEditingController: bagianEditingController,
                isEnable: false,
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextFieldWidget(
                label: "Uraian Perbaikan",
                maxlines: 3,
                textEditingController: uraianPerbaikanEditingController,
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                "List Material",
                style: ptSansTextStyle.copyWith(color: primary),
              ),
              const SizedBox(
                height: 16,
              ),
              Container(
                decoration: BoxDecoration(
                    color: white,
                    border: Border.all(color: black26, width: 0.5),
                    borderRadius: cardBorderRadius),
                child: Stack(
                  children: [
                    ValueListenableBuilder(
                        valueListenable: listMaterialValue,
                        builder: (context, value, child) {
                          return Column(
                            children: [
                              ...barangContainer(),
                              const SizedBox(
                                height: 12,
                              )
                            ],
                          );
                        }),
                  ],
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextFieldWidget(
                label: "Keterangan",
                maxlines: 3,
                textEditingController: keteranganEditingController,
              ),
              const SizedBox(
                height: 30,
              ),
              if (widget.isUserPermitedToSave || widget.isFromTask)
                CustomButtonWidget(
                    callback: () {
                      saveLpkk();
                    },
                    title: save)
            ],
          ),
        ),
      ],
    ));
  }

  // LIST BARANG
  List<Widget> barangContainer() {
    final List<Widget> list = [];
    for (var i = 0; i < listMaterialValue.value.length; i++) {
      list.add(GestureDetector(
        onTap: () async {
          final material = await showModalBottomSheet(
              isDismissible: false,
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              isScrollControlled: true,
              context: context,
              builder: (context) {
                return Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: MaterialInput(
                      isFromTask: widget.isFromTask,
                      isUserPermitedToSave: widget.isUserPermitedToSave,
                      material: listMaterialValue.value[i],
                      listMasterBarang: masterBarangDanJasa?.masterBarang ?? [],
                    ));
              });
          if (material is PenggunaanMaterial) {
            listMaterialValue.value.removeAt(i);
            listMaterialValue.value.insert(i, material);
            final tmp = [...listMaterialValue.value];
            listMaterialValue.value = tmp;
          } else if (material is bool) {
            listMaterialValue.value.removeAt(i);
            final tmp = [...listMaterialValue.value];
            listMaterialValue.value = tmp;
          }
        },
        child: Container(
          padding: const EdgeInsets.all(12),
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: const BoxDecoration(
              color: grey200,
              borderRadius: BorderRadius.all(Radius.circular(14))),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  listMaterialValue.value[i].namaBarang ?? empty,
                  style: ptSansTextStyle.copyWith(color: black54, fontSize: 16),
                ),
                Text(
                  "Jumlah : ${(listMaterialValue.value[i].qty ?? empty)} ${(listMaterialValue.value[i].satuan ?? empty)}",
                  style: ptSansTextStyle.copyWith(color: black54, fontSize: 14),
                )
              ],
            ),
          ),
        ),
      ));
    }
    if (widget.isFromTask || widget.isUserPermitedToSave) {
      list.add(Container(
        padding: const EdgeInsets.all(6),
        margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
        decoration: const BoxDecoration(
            color: grey200,
            borderRadius: BorderRadius.all(Radius.circular(14))),
        child: Center(
          child: IconButton(
              onPressed: () async {
                final barang = await showModalBottomSheet(
                    isDismissible: false,
                    enableDrag: false,
                    shape: const RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(20))),
                    isScrollControlled: true,
                    context: context,
                    builder: (context) {
                      return Padding(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: MaterialInput(
                            isFromTask: widget.isFromTask,
                            isUserPermitedToSave: widget.isUserPermitedToSave,
                            listMasterBarang:
                                masterBarangDanJasa?.masterBarang ?? [],
                          ));
                    });
                if (barang is PenggunaanMaterial) {
                  final tmp = [...listMaterialValue.value, barang];
                  listMaterialValue.value = tmp;
                }
              },
              icon: iconAdd.copyWith(color: black54)),
        ),
      ));
    }
    return list;
  }
}
