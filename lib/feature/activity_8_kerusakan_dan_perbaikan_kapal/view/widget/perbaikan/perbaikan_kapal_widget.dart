import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/widget/perbaikan/perbaikan_kapal_input.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/widget/perbaikan/perbaikan_kapal_item.dart';

import '../../../../../core/data/model/approve.dart';
import '../../../../../core/service/date/date.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/util/global_function.dart';
import '../../../../../core/util/locator.dart';
import '../../../../../core/widget/other/error_layout.dart';
import '../../../../../core/widget/other/loading_widget.dart';
import '../../../bloc/perbaikan_kapal_bloc.dart';

class PerbaikanKapalWidget extends StatefulWidget {
  const PerbaikanKapalWidget(
      {super.key, required this.dataKapal, required this.date});
  final DataKapal dataKapal;
  final ValueNotifier<DateTime> date;
  @override
  State<PerbaikanKapalWidget> createState() => _PerbaikanKapalWidgetState();
}

class _PerbaikanKapalWidgetState extends State<PerbaikanKapalWidget> {
  bool isDateToSaveNotExpired = false;

  getHistory() async {
    final now = locator<MyDate>().getDateToday();
    final result = await showMyDatePicker(context, widget.date.value, now);
    if (result != null) {
      if (result != widget.date.value) {
        widget.date.value = result;
        refreshDate();
        final dateFormated =
            locator<MyDate>().getDateWithInputInString(widget.date.value);
        if (mounted) {
          BlocProvider.of<PerbaikanKapalBloc>(context).add(
              GetHistoryPerbaikanKapalEvent(
                  widget.dataKapal.idKapal ?? -1, dateFormated));
        }
      }
    }
  }

  @override
  initState() {
    super.initState();
    refreshDate();
  }

  refreshDate() {
    // GET REVISED DATE
    final revisedDate =
        locator<MyDate>().getDateToday().subtract(const Duration(days: 3));
    if (widget.date.value.isBefore(revisedDate)) {
      isDateToSaveNotExpired = false;
    } else {
      isDateToSaveNotExpired = true;
    }
  }

  transformDataToWidget(HistoryPerbaikanKapalSuccess state) {
    final List<PerbaikanKapalItem> list = [];
    state.historyPerbaikanKapalResponse.data?.forEach((e) {
      list.add(PerbaikanKapalItem(
        date: e.date,
        approve: e.approve ?? Approve(),
        callback: (contextPerbaikanKapal) {
          Navigator.push(context, MaterialPageRoute(builder: (context2) {
            return BlocProvider.value(
                value: BlocProvider.of<PerbaikanKapalBloc>(context),
                child: PerbaikanKapalInput(
                  dataKapal: widget.dataKapal,
                  isDateToSaveNotExpired: isDateToSaveNotExpired,
                  historyLaporanPerbaikanKapal: e,
                ));
          }));
        },
        historyLaporanPerbaikanKapal: e,
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<PerbaikanKapalBloc, PerbaikanKapalState>(
        buildWhen: ((previous, current) =>
            current is HistoryPerbaikanKapalState),
        builder: (context, state) {
          if (state is HistoryPerbaikanKapalSuccess) {
            final List list = transformDataToWidget(state);
            return Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  decoration: const BoxDecoration(
                      color: white,
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(12)),
                      boxShadow: [shadowCard]),
                  padding: const EdgeInsets.all(20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          getHistory();
                        },
                        child: Text(
                          locator<MyDate>().getDateWithInputIndonesiaFormat(
                              widget.date.value),
                          maxLines: 1,
                          style: ptSansTextStyle.copyWith(
                              color: black54,
                              fontWeight: semiBold,
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                    child: list.isNotEmpty
                        ? ListView.builder(
                            padding: const EdgeInsets.all(20),
                            itemCount: list.length,
                            itemBuilder: (BuildContext ctx, index) {
                              return list[index];
                            },
                          )
                        : const ErrorLayout(message: dataEmpty)),
              ],
            );
          } else if (state is HistoryPerbaikanKapalError) {
            return ErrorLayout(message: state.message);
          }
          return const Center(child: LoadingWidget());
        },
      ),
    );
  }
}
