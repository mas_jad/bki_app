import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/bloc/perbaikan_kapal_bloc.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/widget/task/task_kapal_item.dart';

import '../../../../../core/data/model/data_kapal.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/widget/other/error_layout.dart';
import '../../../../../core/widget/other/loading_widget.dart';
import '../../../bloc/task_kapal_bloc.dart';
import '../perbaikan/perbaikan_kapal_input.dart';

class TaskKapalWidget extends StatefulWidget {
  const TaskKapalWidget(
      {super.key, required this.dataKapal, required this.date});
  final DataKapal dataKapal;
  final ValueNotifier<DateTime> date;
  @override
  State<TaskKapalWidget> createState() => _TaskKapalWidgetState();
}

class _TaskKapalWidgetState extends State<TaskKapalWidget> {
  transformDataToWidget(HistoryTaskKapalSuccess state) {
    final List list = [];
    state.historyLaporanKerusakanKapalResponse.data?.forEach((e) {
      list.add(TaskKapalItem(
        date: e.date,
        callback: (contextTaskKapal) {
          Navigator.push(context, MaterialPageRoute(builder: (context2) {
            return BlocProvider.value(
                value: BlocProvider.of<PerbaikanKapalBloc>(context),
                child: PerbaikanKapalInput(
                  dataKapal: widget.dataKapal,
                  isFromTask: true,
                  historyLaporanKerusakanKapal: e,
                ));
          }));
        },
        historyLaporanKerusakanKapal: e,
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PerbaikanKapalBloc, PerbaikanKapalState>(
      listener: (context, state) {
        if (state is PostPerbaikanKapalSuccess) {
          BlocProvider.of<PerbaikanKapalBloc>(context).add(
              GetHistoryPerbaikanKapalEvent(
                  widget.dataKapal.idKapal ?? -1,
                  locator<MyDate>()
                      .getDateWithInputInString(widget.date.value)));
          BlocProvider.of<TaskKapalBloc>(context).add(GetHistoryTaskKapalEvent(
            widget.dataKapal.idKapal ?? -1,
          ));
        }
      },
      child: Scaffold(
        body: BlocBuilder<TaskKapalBloc, TaskKapalState>(
          buildWhen: ((previous, current) => current is HistoryTaskKapalState),
          builder: (context, state) {
            if (state is HistoryTaskKapalSuccess) {
              final List list = transformDataToWidget(state);
              return Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                      child: list.isNotEmpty
                          ? ListView.builder(
                              padding: const EdgeInsets.all(20),
                              itemCount: list.length,
                              itemBuilder: (BuildContext ctx, index) {
                                return list[index];
                              },
                            )
                          : const ErrorLayout(message: dataEmpty)),
                ],
              );
            } else if (state is HistoryTaskKapalError) {
              return ErrorLayout(message: state.message);
            }
            return const Center(child: LoadingWidget());
          },
        ),
      ),
    );
  }
}
