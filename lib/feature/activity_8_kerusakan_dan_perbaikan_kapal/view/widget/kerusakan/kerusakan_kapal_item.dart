import 'package:flutter/material.dart';
import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/core/util/const.dart';

import '../../../../../core/service/date/date.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/locator.dart';
import '../../../../../core/widget/Other/pop_widget.dart';
import '../../../data/model/history_kerusakan_kapal.dart';

class KerusakanKapalItem extends StatelessWidget {
  const KerusakanKapalItem(
      {required this.historyLaporanKerusakanKapal,
      required this.approve,
      required this.callback,
      required this.date,
      super.key});
  final HistoryLaporanKerusakanKapal historyLaporanKerusakanKapal;
  final String? date;
  final Approve approve;
  final Function(BuildContext context) callback;
  @override
  Widget build(BuildContext context) {
    return PopWidget(
        onTap: () {
          callback(context);
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(bottom: 20),
          decoration: const BoxDecoration(
              color: white,
              boxShadow: [shadowCard],
              borderRadius: cardBorderRadius),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: secondaryAccent,
                    image: const DecorationImage(
                        image: AssetImage(itemImage7e), fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(10)),
                padding: const EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                          color: semiWhite,
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        historyLaporanKerusakanKapal.noLkk ?? empty,
                        style: ptSansTextStyle.copyWith(
                            color: secondary, fontWeight: light, fontSize: 16),
                      ),
                    ),
                    Text(
                      historyLaporanKerusakanKapal.departemen ?? empty,
                      style: ptSansTextStyle.copyWith(
                          color: white, fontWeight: bold, fontSize: 12),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Row(
                        children: [
                          Text(
                            approve.getStatus(),
                            style: ptSansTextStyle.copyWith(
                                color: statusColor(approve.getStatus()),
                                fontWeight: semiBold,
                                fontSize: 14),
                          ),
                          if (approve.getName() != null)
                            Flexible(
                              child: Text(
                                " by ${approve.getName()}",
                                style: ptSansTextStyle.copyWith(
                                    color: lightText,
                                    fontWeight: reguler,
                                    fontSize: 14,
                                    overflow: TextOverflow.ellipsis),
                                maxLines: 1,
                              ),
                            ),
                        ],
                      ),
                    ),
                    historyLaporanKerusakanKapal.solvedBy != null
                        ? Container(
                            padding: const EdgeInsets.all(6),
                            decoration: BoxDecoration(
                                color: primary,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              "SOLVED",
                              style: ptSansTextStyle.copyWith(
                                color: white,
                                fontWeight: light,
                                fontSize: 10,
                              ),
                            ),
                          )
                        : const SizedBox(),
                  ],
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20, right: 20, left: 20),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    locator<MyDate>()
                            .getDateWithInputStringIndonesiaFormat(date) ??
                        empty,
                    style: ptSansTextStyle.copyWith(
                        color: black38, fontWeight: light, fontSize: 14),
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
