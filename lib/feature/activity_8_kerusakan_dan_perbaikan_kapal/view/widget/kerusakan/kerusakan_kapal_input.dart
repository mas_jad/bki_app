import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/departemen.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/bloc/kerusakan_kapal_bloc.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/data/model/history_kerusakan_kapal.dart';

import '../../../../../core/data/model/approve.dart';
import '../../../../../core/data/model/data_kapal.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/util/locator.dart';
import '../../../../../core/util/user_role.dart';
import '../../../../../core/widget/button/custom_button_widget.dart';
import '../../../../../core/widget/dialog/custom_alert_dialog.dart';
import '../../../../../core/widget/image/image_container_picker.dart';
import '../../../../../core/widget/other/loading_widget.dart';
import '../../../../../core/widget/textfield/custom_textfield_widget.dart';
import '../../../data/model/laporan_kerusakan_dan_perbaikan.dart';
import '../../../data/model/post_kerusakan_kapal.dart';
import 'lkk_input.dart';

class KerusakanKapalInput extends StatefulWidget {
  const KerusakanKapalInput(
      {required this.dataKapal,
      this.historyLaporanKerusakanKapal,
      required this.date,
      this.isDateToSaveNotExpired = false,
      super.key});
  final DataKapal dataKapal;
  final HistoryLaporanKerusakanKapal? historyLaporanKerusakanKapal;
  final String date;
  final bool isDateToSaveNotExpired;
  @override
  State<KerusakanKapalInput> createState() => _KerusakanKapalInputState();
}

class _KerusakanKapalInputState extends State<KerusakanKapalInput> {
  final ValueNotifier<Departemen?> departemen = ValueNotifier(null);
  final ValueNotifier<String> kualifikasi = ValueNotifier(empty);
  final TextEditingController catatanTextEditingController =
      TextEditingController();
  List<Departemen> listDepartemen = [];
  final ValueNotifier<List<LaporanKerusakanKapal>> listLkkValue =
      ValueNotifier([]);

  bool isUserPermitedToApprove = false;
  bool isUserPermitedToSave = false;

  @override
  void initState() {
    super.initState();
    // PERMISSION
    isUserPermitedToApprove = widget.historyLaporanKerusakanKapal == null
        ? false
        : locator<UserRole>().isUserPermitedToApprove(
            widget.historyLaporanKerusakanKapal?.approve ?? Approve());
    isUserPermitedToSave = locator<UserRole>().isUserPermitedToSave(
        widget.historyLaporanKerusakanKapal?.approve ?? Approve(),
        widget.isDateToSaveNotExpired);

    // DEPARTEMEN
    listDepartemen = widget.dataKapal.departemen ?? [];
    final tmpDepartemen = widget.historyLaporanKerusakanKapal?.departemen;
    departemen.value = tmpDepartemen != null
        ? listDepartemen.firstWhere((e) => e.nama == tmpDepartemen)
        : listDepartemen.first;
    // KUALIFIKASI
    final tmpKualifikasi = widget.historyLaporanKerusakanKapal?.kualifikasi;
    kualifikasi.value = tmpKualifikasi ?? listKualifikasi.first;

    // LKK
    listLkkValue.value = [...widget.historyLaporanKerusakanKapal?.lkk ?? []];

    // CATATAN
    catatanTextEditingController.text =
        widget.historyLaporanKerusakanKapal?.catatan ?? empty;
  }

  void saveKerusakanKapal() {
    BlocProvider.of<KerusakanKapalBloc>(context).add(PostKerusakanKapalEvent(
        PostKerusakanKapal(
            kapalId: widget.dataKapal.idKapal,
            departemenId: departemen.value?.id,
            date: widget.date,
            kualifikasi: kualifikasi.value,
            catatan: catatanTextEditingController.text,
            noLkk: widget.historyLaporanKerusakanKapal?.noLkk,
            lkk: listLkkValue.value)));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<KerusakanKapalBloc, KerusakanKapalState>(
      listener: (context, state) {
        if (state is PostKerusakanKapalSuccess) {
          MyToast.showSuccess(success, context);
          Navigator.pop(context);
        } else if (state is PostKerusakanKapalError) {
          MyToast.showError(state.message, context);
        }
        if (state is UpdateStatusKerusakanKapalSuccess) {
          MyToast.showSuccess(success, context);

          Navigator.pop(context);
        } else if (state is UpdateStatusKerusakanKapalError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              // HEADER
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Flexible(
                          child: Text(
                            "Kerusakan Kapal",
                            textAlign: TextAlign.center,
                            style: ptSansTextStyle.copyWith(
                                color: white, fontWeight: bold, fontSize: 20),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8),
                            child: iconArrow.copyWith(color: transparent)),
                      ],
                    ),
                  ],
                ),
              ),
              if (widget.historyLaporanKerusakanKapal != null)
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(
                      top: 20, right: 20, left: 20, bottom: 10),
                  padding: const EdgeInsets.all(20),
                  decoration: const BoxDecoration(
                      color: white,
                      boxShadow: [shadowCard],
                      borderRadius: cardBorderRadius),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                widget.historyLaporanKerusakanKapal?.noLkk
                                        .toString() ??
                                    empty,
                                style: ptSansTextStyle.copyWith(
                                    color: secondary,
                                    fontWeight: light,
                                    fontSize: 16),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Text(
                                "Rusak   :  ",
                                style: ptSansTextStyle.copyWith(
                                    color: black38, fontWeight: bold),
                              ),
                              Text(
                                "${locator<MyDate>().getDateWithInputStringIndonesiaFormat(widget.historyLaporanKerusakanKapal?.date)}",
                                style: ptSansTextStyle.copyWith(
                                    color: red, fontWeight: medium),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Text(
                                "Status   :  ",
                                style: ptSansTextStyle.copyWith(
                                    color: black38, fontWeight: bold),
                              ),
                              Text(
                                widget.historyLaporanKerusakanKapal?.approve
                                        ?.getStatus() ??
                                    DRAF,
                                style: ptSansTextStyle.copyWith(
                                    color: statusColor(widget
                                            .historyLaporanKerusakanKapal
                                            ?.approve
                                            ?.getStatus() ??
                                        DRAF),
                                    fontWeight: semiBold,
                                    fontSize: 14),
                              ),
                              if (widget.historyLaporanKerusakanKapal?.approve
                                      ?.getName() !=
                                  null)
                                Flexible(
                                  child: Text(
                                    " by ${widget.historyLaporanKerusakanKapal?.approve?.getName() ?? empty}",
                                    style: ptSansTextStyle.copyWith(
                                        color: lightText,
                                        fontWeight: reguler,
                                        fontSize: 14,
                                        overflow: TextOverflow.ellipsis),
                                    maxLines: 1,
                                  ),
                                ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // DECK OR ENGINE
                    Autocomplete<Departemen>(
                      initialValue: TextEditingValue(
                        text: departemen.value?.nama ?? empty,
                      ),
                      optionsBuilder: (TextEditingValue textEditingValue) {
                        return listDepartemen;
                      },
                      optionsViewBuilder: (
                        BuildContext context,
                        AutocompleteOnSelected<Departemen> onSelected,
                        Iterable<Departemen> options,
                      ) {
                        return Align(
                          alignment: Alignment.topLeft,
                          child: Material(
                            child: Container(
                              height: 60.0 * options.length,
                              decoration: const BoxDecoration(
                                color: white,
                                boxShadow: [
                                  shadowCard,
                                ],
                              ),
                              child: ListView.builder(
                                padding: const EdgeInsets.all(10.0),
                                itemCount: options.length,
                                itemBuilder: (
                                  BuildContext context,
                                  int index,
                                ) {
                                  final option = options.elementAt(index);
                                  return GestureDetector(
                                    onTap: () {
                                      onSelected(option);
                                    },
                                    child: ListTile(
                                      title: Text(
                                        option.nama ?? empty,
                                        style: ptSansTextStyle.copyWith(
                                          color: black68,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                      fieldViewBuilder: (
                        BuildContext context,
                        TextEditingController fieldTextEditingController,
                        FocusNode fieldFocusNode,
                        VoidCallback onFieldSubmitted,
                      ) {
                        return CustomTextFieldWidget(
                          label: "Departemen",
                          focusNode: fieldFocusNode,
                          isEnable: widget.historyLaporanKerusakanKapal == null,
                          textEditingController: fieldTextEditingController,
                          textInputType: TextInputType.text,
                        );
                      },
                      onSelected: (Departemen selection) {
                        departemen.value = selection;
                      },
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    // KUALIFIKASI
                    Autocomplete<String>(
                      initialValue: TextEditingValue(
                        text: kualifikasi.value,
                      ),
                      optionsBuilder: (TextEditingValue textEditingValue) {
                        return listKualifikasi;
                      },
                      optionsViewBuilder: (
                        BuildContext context,
                        AutocompleteOnSelected<String> onSelected,
                        Iterable<String> options,
                      ) {
                        return Align(
                          alignment: Alignment.topLeft,
                          child: Material(
                            child: Container(
                              height:60.0 * options.length,
                              decoration: const BoxDecoration(
                                color: white,
                                boxShadow: [
                                  shadowCard,
                                ],
                              ),
                              child: ListView.builder(
                                padding: const EdgeInsets.all(10.0),
                                itemCount: options.length,
                                itemBuilder: (
                                  BuildContext context,
                                  int index,
                                ) {
                                  final String option =
                                      options.elementAt(index);
                                  return GestureDetector(
                                    onTap: () {
                                      onSelected(option);
                                    },
                                    child: ListTile(
                                      title: Text(
                                        option,
                                        style: ptSansTextStyle.copyWith(
                                          color: black68,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                      fieldViewBuilder: (
                        BuildContext context,
                        TextEditingController fieldTextEditingController,
                        FocusNode fieldFocusNode,
                        VoidCallback onFieldSubmitted,
                      ) {
                        return CustomTextFieldWidget(
                          label: "Kualifikasi",
                          focusNode: fieldFocusNode,
                          textEditingController: fieldTextEditingController,
                          textInputType: TextInputType.text,
                        );
                      },
                      onSelected: (String selection) {
                        kualifikasi.value = selection;
                      },
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    // LIST KERUSAKAN
                    Text(
                      "List Kerusakan",
                      style: ptSansTextStyle.copyWith(color: primary),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: white,
                          border: Border.all(color: black26, width: 0.5),
                          borderRadius: cardBorderRadius),
                      child: ValueListenableBuilder(
                          valueListenable: listLkkValue,
                          builder: (context, value, child) {
                            return Column(
                              children: [
                                ...lkkContainer(),
                                const SizedBox(
                                  height: 12,
                                )
                              ],
                            );
                          }),
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    // CATATAN
                    CustomTextFieldWidget(
                      label: "Catatan",
                      maxlines: 6,
                      textEditingController: catatanTextEditingController,
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    // BUTTON
                    if (isUserPermitedToSave)
                      BlocBuilder<KerusakanKapalBloc, KerusakanKapalState>(
                        builder: (context, state) {
                          return CustomButtonWidget(
                              isActive: state is PostKerusakanKapalLoading
                                  ? false
                                  : true,
                              callback: () {
                                saveKerusakanKapal();
                              },
                              title: save);
                        },
                      ),
                  ],
                ),
              )
            ],
          ),
        ), // FLOATING ACTION BUTTON
        floatingActionButton:
            BlocBuilder<KerusakanKapalBloc, KerusakanKapalState>(
          builder: (context, state) {
            if (isUserPermitedToApprove) {
              if (state is UpdateStatusKerusakanKapalLoading) {
                return FloatingActionButton(
                  elevation: 12,
                  backgroundColor: secondary,
                  onPressed: () {},
                  child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                itemImage4,
                              ),
                              fit: BoxFit.fill)),
                      child: const Center(
                          child: LoadingWidget(
                        color: white,
                      ))),
                );
              }

              return FloatingActionButton(
                elevation: elevationFAB,
                backgroundColor: primary,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: ((dialogContext) => BlocProvider.value(
                            value: BlocProvider.of<KerusakanKapalBloc>(context),
                            child: CustomAlertDialog(
                                description: dataCannotChangeAgain,
                                title: locator<UserRole>().getTitle(widget
                                        .historyLaporanKerusakanKapal
                                        ?.approve ??
                                    Approve()),
                                voidCallback: () {
                                  BlocProvider.of<KerusakanKapalBloc>(context)
                                      .add(UpdateStatusKerusakanKapalEvent(
                                          widget.dataKapal.idKapal ?? -1,
                                          locator<MyDate>()
                                              .getDateTodayInString(),
                                          widget.historyLaporanKerusakanKapal
                                                  ?.noLkk ??
                                              empty,
                                          widget.historyLaporanKerusakanKapal
                                                  ?.approve ??
                                              Approve()));
                                }),
                          )));
                },
                child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              itemImage4,
                            ),
                            fit: BoxFit.fill)),
                    child: Center(
                        child: locator<UserRole>().getIcon(
                            widget.historyLaporanKerusakanKapal?.approve ??
                                Approve()))),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }

  // LIST LKK
  List<Widget> lkkContainer() {
    final List<Widget> list = [];
    for (var i = 0; i < listLkkValue.value.length; i++) {
      list.add(GestureDetector(
        onTap: () async {
          final lkk = await showModalBottomSheet(
              isDismissible: false,
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              isScrollControlled: true,
              context: context,
              builder: (context) {
                return Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: LkkInput(
                      isUserPermitedToSave: isUserPermitedToSave,
                      lkk: listLkkValue.value[i],
                    ));
              });
          if (lkk is LaporanKerusakanKapal) {
            listLkkValue.value.removeAt(i);
            listLkkValue.value.insert(i, lkk);
            final tmp = [...listLkkValue.value];
            listLkkValue.value = tmp;
          } else if (lkk is bool) {
            listLkkValue.value.removeAt(i);
            final tmp = [...listLkkValue.value];
            listLkkValue.value = tmp;
          }
        },
        child: Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          decoration: const BoxDecoration(
              color: grey200,
              borderRadius: BorderRadius.all(Radius.circular(14))),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 12, top: 10, bottom: 10),
                  child: Center(
                      child: ImageContainerPicker(
                    image: listLkkValue.value[i].networkImage,
                    ratioToWidth: 5,
                    isEnable: false,
                    imageValueNotifier:
                        ValueNotifier(listLkkValue.value[i].localImage),
                  )),
                ),
                Expanded(
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 12, top: 10, bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          listLkkValue.value[i].activityLkkOrBagian ?? empty,
                          style: ptSansTextStyle.copyWith(
                              color: black54,
                              fontSize: 16,
                              fontWeight: semiBold,
                              overflow: TextOverflow.ellipsis),
                          maxLines: 1,
                        ),
                        listLkkValue.value[i].uraian?.isNotEmpty ?? false
                            ? const SizedBox(
                                height: 6,
                              )
                            : const SizedBox(),
                        listLkkValue.value[i].uraian?.isNotEmpty ?? false
                            ? Text(
                                (listLkkValue.value[i].uraian ?? empty),
                                style: ptSansTextStyle.copyWith(
                                    color: black54,
                                    fontSize: 14,
                                    overflow: TextOverflow.ellipsis),
                                maxLines: 2,
                              )
                            : const SizedBox(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ));
    }
    if (isUserPermitedToSave) {
      list.add(Container(
        padding: const EdgeInsets.all(6),
        margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
        decoration: const BoxDecoration(
            color: grey200,
            borderRadius: BorderRadius.all(Radius.circular(14))),
        child: Center(
          child: IconButton(
              onPressed: () async {
                final lkk = await showModalBottomSheet(
                    isDismissible: false,
                    enableDrag: false,
                    shape: const RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(20))),
                    isScrollControlled: true,
                    context: context,
                    builder: (context) {
                      return Padding(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: LkkInput(
                            isUserPermitedToSave: isUserPermitedToSave,
                          ));
                    });
                if (lkk is LaporanKerusakanKapal) {
                  final tmp = [...listLkkValue.value, lkk];
                  listLkkValue.value = tmp;
                }
              },
              icon: iconAdd.copyWith(color: black54)),
        ),
      ));
    }
    return list;
  }
}
