import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/bloc/task_kapal_bloc.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/widget/kerusakan/kerusakan_kapal_item.dart';

import '../../../../../core/data/model/approve.dart';
import '../../../../../core/service/date/date.dart';
import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/util/global_function.dart';
import '../../../../../core/util/locator.dart';
import '../../../../../core/util/user_role.dart';
import '../../../../../core/widget/other/error_layout.dart';
import '../../../../../core/widget/other/loading_widget.dart';
import '../../../bloc/kerusakan_kapal_bloc.dart';
import 'kerusakan_kapal_input.dart';

class KerusakanKapalWidget extends StatefulWidget {
  const KerusakanKapalWidget(
      {super.key, required this.dataKapal, required this.date});
  final DataKapal dataKapal;
  final ValueNotifier<DateTime> date;
  @override
  State<KerusakanKapalWidget> createState() => _KerusakanKapalWidgetState();
}

class _KerusakanKapalWidgetState extends State<KerusakanKapalWidget> {
  bool isDateToSaveNotExpired = false;

  getHistory() async {
    final now = locator<MyDate>().getDateToday();
    final result = await showMyDatePicker(context, widget.date.value, now);
    if (result != null) {
      if (result != widget.date.value) {
        widget.date.value = result;
        refreshDate();
        final dateFormated =
            locator<MyDate>().getDateWithInputInString(widget.date.value);
        if (mounted) {
          BlocProvider.of<KerusakanKapalBloc>(context).add(
              GetHistoryKerusakanKapalEvent(
                  widget.dataKapal.idKapal ?? -1, dateFormated));
        }
      }
    }
  }

  @override
  initState() {
    super.initState();
    refreshDate();
  }

  refreshDate() {
    // GET REVISED DATE
    final revisedDate =
        locator<MyDate>().getDateToday().subtract(const Duration(days: 3));
    if (widget.date.value.isBefore(revisedDate)) {
      isDateToSaveNotExpired = false;
    } else {
      isDateToSaveNotExpired = true;
    }
  }

  transformDataToWidget(HistoryKerusakanKapalSuccess state) {
    final List<KerusakanKapalItem> list = [];
    state.historyKerusakanKapalResponse.data?.forEach((e) {
      list.add(KerusakanKapalItem(
        date: e.date,
        approve: e.approve ?? Approve(),
        callback: (contextKerusakanKapal) {
          Navigator.push(context, MaterialPageRoute(builder: (context2) {
            return BlocProvider.value(
                value: BlocProvider.of<KerusakanKapalBloc>(context),
                child: KerusakanKapalInput(
                  date: locator<MyDate>()
                      .getDateWithInputInString(widget.date.value),
                  dataKapal: widget.dataKapal,
                  isDateToSaveNotExpired: isDateToSaveNotExpired,
                  historyLaporanKerusakanKapal: e,
                ));
          }));
        },
        historyLaporanKerusakanKapal: e,
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<KerusakanKapalBloc, KerusakanKapalState>(
      listener: (context, state) {
        if (state is UpdateStatusKerusakanKapalSuccess) {
          BlocProvider.of<TaskKapalBloc>(context)
              .add(GetHistoryTaskKapalEvent(widget.dataKapal.idKapal ?? -1));
        }
      },
      child: Scaffold(
        body: BlocBuilder<KerusakanKapalBloc, KerusakanKapalState>(
          buildWhen: ((previous, current) =>
              current is HistoryKerusakanKapalState),
          builder: (context, state) {
            if (state is HistoryKerusakanKapalSuccess) {
              final List list = transformDataToWidget(state);
              return Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        color: white,
                        borderRadius:
                            BorderRadius.vertical(bottom: Radius.circular(12)),
                        boxShadow: [shadowCard]),
                    padding: const EdgeInsets.all(20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: () {
                            getHistory();
                          },
                          child: Text(
                            locator<MyDate>().getDateWithInputIndonesiaFormat(
                                widget.date.value),
                            maxLines: 1,
                            style: ptSansTextStyle.copyWith(
                                color: black54,
                                fontWeight: semiBold,
                                fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                      child: list.isNotEmpty
                          ? ListView.builder(
                              padding: const EdgeInsets.all(20),
                              itemCount: list.length,
                              itemBuilder: (BuildContext ctx, index) {
                                return list[index];
                              },
                            )
                          : const ErrorLayout(message: dataEmpty)),
                ],
              );
            } else if (state is HistoryKerusakanKapalError) {
              return ErrorLayout(message: state.message);
            }
            return const Center(child: LoadingWidget());
          },
        ),
        floatingActionButton:
            BlocBuilder<KerusakanKapalBloc, KerusakanKapalState>(
          buildWhen: ((previous, current) =>
              current is HistoryKerusakanKapalState),
          builder: (context, state) {
            if (isDateToSaveNotExpired &&
                locator<UserRole>().isUserPermitedToCreate()) {
              return FloatingActionButton(
                elevation: elevationFAB,
                backgroundColor: primary,
                onPressed: () {
                  if (state is HistoryKerusakanKapalSuccess) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context2) {
                      return BlocProvider.value(
                          value: BlocProvider.of<KerusakanKapalBloc>(context),
                          child: KerusakanKapalInput(
                            isDateToSaveNotExpired: isDateToSaveNotExpired,
                            date: locator<MyDate>()
                                .getDateWithInputInString(widget.date.value),
                            dataKapal: widget.dataKapal,
                          ));
                    }));
                  }
                },
                child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              itemImage4,
                            ),
                            fit: BoxFit.fill)),
                    child: const Center(child: iconAdd)),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}
