import 'package:flutter/material.dart';
import 'package:pms_pcm/core/util/toast.dart';

import '../../../../../core/theme/theme.dart';
import '../../../../../core/util/const.dart';
import '../../../../../core/widget/button/custom_button_widget.dart';
import '../../../../../core/widget/image/image_container_picker.dart';
import '../../../../../core/widget/textfield/custom_textfield_widget.dart';
import '../../../data/model/laporan_kerusakan_dan_perbaikan.dart';

class LkkInput extends StatefulWidget {
  const LkkInput({this.lkk, required this.isUserPermitedToSave, super.key});
  final LaporanKerusakanKapal? lkk;
  final bool isUserPermitedToSave;
  @override
  State<LkkInput> createState() => _SoundingBahanBakarInputState();
}

class _SoundingBahanBakarInputState extends State<LkkInput> {
  final TextEditingController bagianEditingController = TextEditingController();
  final TextEditingController uraianKerusakanEditingController =
      TextEditingController();
  final TextEditingController usahaPenanggulanganEditingController =
      TextEditingController();
  final TextEditingController keteranganEditingController =
      TextEditingController();
  late ValueNotifier<String?> imageValueNotifier;

  @override
  void initState() {
    super.initState();
    uraianKerusakanEditingController.text =
        widget.lkk?.uraian?.toString() ?? empty;
    bagianEditingController.text =
        widget.lkk?.activityLkkOrBagian?.toString() ?? empty;
    usahaPenanggulanganEditingController.text =
        widget.lkk?.usahaPenanggulangan?.toString() ?? empty;
    keteranganEditingController.text =
        widget.lkk?.keterangan?.toString() ?? empty;

    imageValueNotifier = ValueNotifier(widget.lkk?.localImage);
  }

  saveLkk() async {
    if (bagianEditingController.text.isEmpty) {
      MyToast.showError("Bagian Belum Diinputkan", context);
      return;
    }
    if (uraianKerusakanEditingController.text.isEmpty) {
      MyToast.showError("Uraian Kerusakan Belum Diinputkan", context);
      return;
    }
    Navigator.pop(
        context,
        LaporanKerusakanKapal(
            uraian: uraianKerusakanEditingController.text,
            usahaPenanggulangan: usahaPenanggulanganEditingController.text,
            keterangan: keteranganEditingController.text,
            activityLkkOrBagian: bagianEditingController.text,
            localImage: imageValueNotifier.value,
            networkImage: widget.lkk?.networkImage));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(itemImage2), fit: BoxFit.cover),
              color: secondary,
              borderRadius: BorderRadius.all(Radius.circular(18))),
          child: Column(
            children: [
              const SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      onPressed: (() {
                        Navigator.pop(context);
                      }),
                      icon: iconArrow.copyWith(color: white)),
                  Text(
                    "Kerusakan Kapal",
                    style: ptSansTextStyle.copyWith(
                        color: white, fontWeight: bold, fontSize: 22),
                  ),
                  Visibility(
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: widget.lkk != null ? true : false,
                    child: IconButton(
                        onPressed: (() {
                          Navigator.pop(context, true);
                        }),
                        icon: iconDelete.copyWith(color: white)),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 10),
                child: Center(
                    child: ImageContainerPicker(
                  image: widget.lkk?.networkImage,
                  imageValueNotifier: imageValueNotifier,
                )),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              CustomTextFieldWidget(
                label: "Bagian & Posisi",
                textEditingController: bagianEditingController,
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextFieldWidget(
                label: "Uraian Kerusakan",
                maxlines: 3,
                textEditingController: uraianKerusakanEditingController,
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextFieldWidget(
                label: "Usaha Penanggulangan",
                maxlines: 3,
                textEditingController: usahaPenanggulanganEditingController,
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextFieldWidget(
                label: "Keterangan",
                maxlines: 3,
                textEditingController: keteranganEditingController,
              ),
              const SizedBox(
                height: 30,
              ),
              if (widget.isUserPermitedToSave)
                CustomButtonWidget(
                    callback: () {
                      saveLkk();
                    },
                    title: save)
            ],
          ),
        ),
      ],
    ));
  }
}
