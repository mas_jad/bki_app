part of 'task_kapal_bloc.dart';

abstract class TaskKapalState extends Equatable {
  const TaskKapalState();

  @override
  List<Object> get props => [];
}

class TaskKapalInitial extends TaskKapalState {}

// HISTORY
abstract class HistoryTaskKapalState extends TaskKapalState {}

class HistoryTaskKapalSuccess extends HistoryTaskKapalState {
  final HistoryLaporanKerusakanKapalResponse
      historyLaporanKerusakanKapalResponse;
  HistoryTaskKapalSuccess(this.historyLaporanKerusakanKapalResponse);
}

class HistoryTaskKapalError extends HistoryTaskKapalState {
  final String message;
  HistoryTaskKapalError(this.message);
}

class HistoryTaskKapalLoading extends HistoryTaskKapalState {}
