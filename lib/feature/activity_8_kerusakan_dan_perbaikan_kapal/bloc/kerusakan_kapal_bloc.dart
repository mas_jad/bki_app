import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_kerusakan_kapal.dart';
import '../data/model/post_kerusakan_kapal.dart';
import '../data/repository/kerusakan_kapal_repository.dart';

part 'kerusakan_kapal_event.dart';
part 'kerusakan_kapal_state.dart';

class KerusakanKapalBloc
    extends Bloc<KerusakanKapalEvent, KerusakanKapalState> {
  final KerusakanKapalRepository _kerusakanKapalRepository;
  final UserRepository _userRepository;

  KerusakanKapalBloc(this._kerusakanKapalRepository, this._userRepository)
      : super(KerusakanKapalInitial()) {
    on<PostKerusakanKapalEvent>((event, emit) async {
      try {
        emit(PostKerusakanKapalLoading());
        final token = await _userRepository.getToken();
        final data = await _kerusakanKapalRepository.postKerusakanKapal(
            token, event.postKerusakanKapal);
        emit(PostKerusakanKapalSuccess());
        emit(HistoryKerusakanKapalSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostKerusakanKapalError(e.message));
        } else {
          emit(PostKerusakanKapalError(somethingErrorHappen));
        }
      }
    });

    on<GetHistoryKerusakanKapalEvent>((event, emit) async {
      try {
        emit(HistoryKerusakanKapalLoading());
        final token = await _userRepository.getToken();
        final data = await _kerusakanKapalRepository.getHistoryKerusakanKapal(
            event.id, token, event.date);
        emit(HistoryKerusakanKapalSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryKerusakanKapalError(e.message));
        } else {
          emit(HistoryKerusakanKapalError(somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusKerusakanKapalEvent>((event, emit) async {
      try {
        emit(UpdateStatusKerusakanKapalLoading());
        final token = await _userRepository.getToken();
        final data = await _kerusakanKapalRepository.updateStatusKerusakanKapal(
            event.id, token, event.date, event.noLkk, event.approve);

        emit(UpdateStatusKerusakanKapalSuccess());
        emit(HistoryKerusakanKapalSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(UpdateStatusKerusakanKapalError(e.message));
        } else {
          emit(UpdateStatusKerusakanKapalError(somethingErrorHappen));
        }
      }
    });
  }
}
