part of 'perbaikan_kapal_bloc.dart';

abstract class PerbaikanKapalState extends Equatable {
  const PerbaikanKapalState();

  @override
  List<Object> get props => [];
}

class PerbaikanKapalInitial extends PerbaikanKapalState {}

// HISTORY
abstract class HistoryPerbaikanKapalState extends PerbaikanKapalState {}

class HistoryPerbaikanKapalSuccess extends HistoryPerbaikanKapalState {
  final HistoryLaporanPerbaikanKapalResponse historyPerbaikanKapalResponse;
  HistoryPerbaikanKapalSuccess(this.historyPerbaikanKapalResponse);
}

class HistoryPerbaikanKapalError extends HistoryPerbaikanKapalState {
  final String message;
  HistoryPerbaikanKapalError(this.message);
}

class HistoryPerbaikanKapalLoading extends HistoryPerbaikanKapalState {}

// POST
abstract class PostPerbaikanKapalState extends PerbaikanKapalState {}

class PostPerbaikanKapalSuccess extends PostPerbaikanKapalState {}

class PostPerbaikanKapalError extends PostPerbaikanKapalState {
  final String message;
  PostPerbaikanKapalError(this.message);
}

class PostPerbaikanKapalLoading extends PostPerbaikanKapalState {}

// UPDATE STATUS
abstract class UpdateStatusPerbaikanKapalState extends PerbaikanKapalState {}

class UpdateStatusPerbaikanKapalSuccess
    extends UpdateStatusPerbaikanKapalState {}

class UpdateStatusPerbaikanKapalError extends UpdateStatusPerbaikanKapalState {
  final String message;
  UpdateStatusPerbaikanKapalError(this.message);
}

class UpdateStatusPerbaikanKapalLoading
    extends UpdateStatusPerbaikanKapalState {}
