import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_perbaikan_kapal.dart';
import '../data/model/post_perbaikan_kapal.dart';
import '../data/repository/perbaikan_kapal_repository.dart';

part 'perbaikan_kapal_event.dart';
part 'perbaikan_kapal_state.dart';

class PerbaikanKapalBloc
    extends Bloc<PerbaikanKapalEvent, PerbaikanKapalState> {
  final PerbaikanKapalRepository _perbaikanKapalRepository;
  final UserRepository _userRepository;

  PerbaikanKapalBloc(this._perbaikanKapalRepository, this._userRepository)
      : super(PerbaikanKapalInitial()) {
    on<PostPerbaikanKapalEvent>((event, emit) async {
      try {
        emit(PostPerbaikanKapalLoading());
        final token = await _userRepository.getToken();
        final data = await _perbaikanKapalRepository.postPerbaikanKapal(
            token, event.postPerbaikanKapal);
        emit(PostPerbaikanKapalSuccess());
        emit(HistoryPerbaikanKapalSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostPerbaikanKapalError(e.message));
        } else {
          emit(PostPerbaikanKapalError(somethingErrorHappen));
        }
      }
    });

    on<GetHistoryPerbaikanKapalEvent>((event, emit) async {
      try {
        emit(HistoryPerbaikanKapalLoading());
        final token = await _userRepository.getToken();
        final data = await _perbaikanKapalRepository.getHistoryPerbaikanKapal(
            event.id, token, event.date);
        emit(HistoryPerbaikanKapalSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryPerbaikanKapalError(e.message));
        } else {
          emit(HistoryPerbaikanKapalError(somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusPerbaikanKapalEvent>((event, emit) async {
      try {
        emit(UpdateStatusPerbaikanKapalLoading());
        final token = await _userRepository.getToken();
        final data = await _perbaikanKapalRepository.updateStatusPerbaikanKapal(
            event.id,
            token,
            event.date,
            event.noLkk,
            event.noLpkk,
            event.approve);

        emit(UpdateStatusPerbaikanKapalSuccess());
        emit(HistoryPerbaikanKapalSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(UpdateStatusPerbaikanKapalError(e.message));
        } else {
          emit(UpdateStatusPerbaikanKapalError(somethingErrorHappen));
        }
      }
    });
  }
}
