part of 'kerusakan_kapal_bloc.dart';

abstract class KerusakanKapalEvent extends Equatable {
  const KerusakanKapalEvent();

  @override
  List<Object> get props => [];
}

class PostKerusakanKapalEvent extends KerusakanKapalEvent {
  final PostKerusakanKapal postKerusakanKapal;
  const PostKerusakanKapalEvent(this.postKerusakanKapal);
}

class UpdateStatusKerusakanKapalEvent extends KerusakanKapalEvent {
  final int id;
  final String date;
  final String noLkk;
  final Approve approve;
  const UpdateStatusKerusakanKapalEvent(
      this.id, this.date, this.noLkk, this.approve);
}

class GetHistoryKerusakanKapalEvent extends KerusakanKapalEvent {
  final int id;
  final String date;
  const GetHistoryKerusakanKapalEvent(this.id, this.date);
}
