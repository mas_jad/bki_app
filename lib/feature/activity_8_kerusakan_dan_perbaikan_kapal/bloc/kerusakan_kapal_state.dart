part of 'kerusakan_kapal_bloc.dart';

abstract class KerusakanKapalState extends Equatable {
  const KerusakanKapalState();

  @override
  List<Object> get props => [];
}

class KerusakanKapalInitial extends KerusakanKapalState {}

// HISTORY
abstract class HistoryKerusakanKapalState extends KerusakanKapalState {}

class HistoryKerusakanKapalSuccess extends HistoryKerusakanKapalState {
  final HistoryLaporanKerusakanKapalResponse historyKerusakanKapalResponse;
  HistoryKerusakanKapalSuccess(this.historyKerusakanKapalResponse);
}

class HistoryKerusakanKapalError extends HistoryKerusakanKapalState {
  final String message;
  HistoryKerusakanKapalError(this.message);
}

class HistoryKerusakanKapalLoading extends HistoryKerusakanKapalState {}

// POST
abstract class PostKerusakanKapalState extends KerusakanKapalState {}

class PostKerusakanKapalSuccess extends PostKerusakanKapalState {}

class PostKerusakanKapalError extends PostKerusakanKapalState {
  final String message;
  PostKerusakanKapalError(this.message);
}

class PostKerusakanKapalLoading extends PostKerusakanKapalState {}

// UPDATE STATUS
abstract class UpdateStatusKerusakanKapalState extends KerusakanKapalState {}

class UpdateStatusKerusakanKapalSuccess
    extends UpdateStatusKerusakanKapalState {}

class UpdateStatusKerusakanKapalError extends UpdateStatusKerusakanKapalState {
  final String message;
  UpdateStatusKerusakanKapalError(this.message);
}

class UpdateStatusKerusakanKapalLoading
    extends UpdateStatusKerusakanKapalState {}
