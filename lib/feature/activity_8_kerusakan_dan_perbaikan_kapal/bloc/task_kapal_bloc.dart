import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_kerusakan_kapal.dart';
import '../data/repository/perbaikan_kapal_repository.dart';

part 'task_kapal_event.dart';
part 'task_kapal_state.dart';

class TaskKapalBloc extends Bloc<TaskKapalEvent, TaskKapalState> {
  final PerbaikanKapalRepository _perbaikanKapalRepository;
  final UserRepository _userRepository;
  TaskKapalBloc(this._perbaikanKapalRepository, this._userRepository)
      : super(TaskKapalInitial()) {
    on<GetHistoryTaskKapalEvent>((event, emit) async {
      try {
        emit(HistoryTaskKapalLoading());
        final token = await _userRepository.getToken();
        final data =
            await _perbaikanKapalRepository.getTaskKapal(event.id, token);
        emit(HistoryTaskKapalSuccess(data));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryTaskKapalError(e.message));
        } else {
          emit(HistoryTaskKapalError(somethingErrorHappen));
        }
      }
    });
  }
}
