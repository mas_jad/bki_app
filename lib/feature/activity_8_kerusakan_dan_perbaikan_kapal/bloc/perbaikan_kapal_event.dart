part of 'perbaikan_kapal_bloc.dart';

abstract class PerbaikanKapalEvent extends Equatable {
  const PerbaikanKapalEvent();

  @override
  List<Object> get props => [];
}

class PostPerbaikanKapalEvent extends PerbaikanKapalEvent {
  final PostPerbaikanKapal postPerbaikanKapal;
  const PostPerbaikanKapalEvent(this.postPerbaikanKapal);
}

class UpdateStatusPerbaikanKapalEvent extends PerbaikanKapalEvent {
  final int id;
  final String date;
  final String noLkk;
  final String noLpkk;
  final Approve approve;
  const UpdateStatusPerbaikanKapalEvent(
      this.id, this.date, this.noLkk, this.noLpkk, this.approve);
}

class GetHistoryPerbaikanKapalEvent extends PerbaikanKapalEvent {
  final int id;
  final String date;
  const GetHistoryPerbaikanKapalEvent(this.id, this.date);
}
