part of 'task_kapal_bloc.dart';

abstract class TaskKapalEvent extends Equatable {
  const TaskKapalEvent();

  @override
  List<Object> get props => [];
}

class GetHistoryTaskKapalEvent extends TaskKapalEvent {
  final int id;
  const GetHistoryTaskKapalEvent(this.id);
}
