import 'package:flutter/material.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/feature/home/view/widget/list_kapal_container.dart';
import 'package:pms_pcm/feature/home/view/widget/profile_container.dart';

class HomePage extends StatefulWidget {
  const HomePage(
      {required this.listDataKapal, required this.profile, super.key});
  static const route = "/home_page";
  final List<DataKapal> listDataKapal;
  final Profile profile;
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: semiWhite,
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Stack(
            children: [
              ListKapalContainer(
                listDataKapal: widget.listDataKapal,
                profile: widget.profile,
              ),
              ProfileContainer(
                profile: widget.profile,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
