import 'package:flutter/cupertino.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/shadow.dart';
import 'package:pms_pcm/core/theme/size.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';

class ProfileContainer extends StatelessWidget {
  const ProfileContainer({required this.profile, super.key});
  final Profile profile;
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Container(
      height: 170,
      width: width,
      decoration: const BoxDecoration(
          color: secondary,
          boxShadow: [shadowCard],
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(20))),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: horizontalMargin),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 60,
            ),
            Text(
              "Halo ${profile.namaDepan}",
              style: ptSansTextStyle.copyWith(
                  color: white, fontWeight: bold, fontSize: 30),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              "Pilih Kapal yang ingin di Monitoring",
              style: ptSansTextStyle.copyWith(
                  color: primaryAccent, fontWeight: medium, fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}
