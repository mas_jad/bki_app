import 'package:flutter/material.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/theme/theme.dart';
import 'package:pms_pcm/feature/home/view/widget/kapal_item.dart';

import '../../../../core/data/model/data_kapal.dart';

class ListKapalContainer extends StatelessWidget {
  const ListKapalContainer(
      {required this.listDataKapal, required this.profile, super.key});
  final List<DataKapal> listDataKapal;
  final Profile profile;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
            padding: const EdgeInsets.only(
                left: horizontalMargin2,
                right: horizontalMargin2,
                top: 170 + verticalMargin,
                bottom: verticalMargin),
            itemCount: listDataKapal.length,
            itemBuilder: (context, index) {
              return KapalItem(
                kapal: listDataKapal[index],
                profile: profile,
              );
            }));
  }
}
