import 'package:flutter/material.dart';
import 'package:pms_pcm/core/data/model/data_kapal.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/shadow.dart';
import 'package:pms_pcm/core/theme/size.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/other/custom_image_network.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';
import 'package:pms_pcm/feature/ship_activity/view/ship_activity_page.dart';

class KapalItem extends StatelessWidget {
  const KapalItem({required this.kapal, required this.profile, super.key});
  final DataKapal kapal;
  final Profile profile;
  @override
  Widget build(BuildContext context) {
    const height = 120.0;
    return PopWidget(
      onTap: () {
        Navigator.pushNamed(context, ShipActivity.route,
            arguments: [kapal, profile]);
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 18),
        width: MediaQuery.of(context).size.width,
        height: height,
        decoration: const BoxDecoration(
            color: white,
            boxShadow: [shadowCard],
            borderRadius: cardBorderRadius),
        child: Row(
          children: [
            CustomImageNetwork(
              imageUrl: kapal.image,
              height: height,
              borderRadius: cardBorderRadius,
              width: height,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      kapal.namaKapal ?? empty,
                      style: ptSansTextStyle.copyWith(
                          color: secondary, fontSize: 16, fontWeight: medium),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      kapal.kategori ?? empty,
                      style: ptSansTextStyle.copyWith(
                          color: primary, fontSize: 14, fontWeight: reguler),
                    ),
                    const Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "IMO : ${kapal.imoNumber ?? empty}",
                          style: ptSansTextStyle.copyWith(
                              color: lightText,
                              fontSize: 14,
                              fontWeight: light),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
