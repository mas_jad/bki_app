import '../../../../core/data/model/approve.dart';
import '../../../../core/util/request.dart';
import '../model/history_pemakaian_air_tawar.dart';
import '../model/post_pemakaian_air_tawar.dart';
import '../source/pemakaian_air_tawar_remote_data_source.dart';

class PemakaianAirTawarRepository {
  final PemakaianAirTawarRemoteDataSource _pemakaianAirTawarRemoteDataSource;
  PemakaianAirTawarRepository(this._pemakaianAirTawarRemoteDataSource);

  String? revisedDate;

  Approve? approve;

  String? getRevisedDate() {
    return revisedDate;
  }

  Approve getApprove() {
    return approve ?? Approve();
  }

  Map<MasterTangkiAir, HistoryPemakaianAirTawar?> _relatingTheDataToMaster(
      HistoryPemakaianAirTawarResponse response) {
    final Map<MasterTangkiAir, HistoryPemakaianAirTawar?> map = {};
    final listHistoryBahanBakar = response.data ?? [];
    final listTankiKapal = response.masterTangkiAir ?? [];
    // Relating Tanki To Data History
    for (var element in listTankiKapal) {
      var isThere = false;
      for (var element2 in listHistoryBahanBakar) {
        if (element.posisi == element2.posisiTangki) {
          map.addAll({element: element2});
          isThere = true;
          break;
        }
      }
      if (!isThere) {
        map.addAll({element: null});
      }
    }

    return map;
  }

  Future<Map<MasterTangkiAir, HistoryPemakaianAirTawar?>>
      getHistoryPemakaianAirTawar(int id, String token, String date) async {
    return RequestServer<Map<MasterTangkiAir, HistoryPemakaianAirTawar?>>()
        .requestServer(computation: () async {
      final response = await _pemakaianAirTawarRemoteDataSource
          .getHistoryPemakaianAirTawar(id, token, date);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingTheDataToMaster(response);
    });
  }

  Future<Map<MasterTangkiAir, HistoryPemakaianAirTawar?>> postPemakaianAirTawar(
      String token, PostPemakaianAirTawar pemakaianAirTawar) async {
    return RequestServer<Map<MasterTangkiAir, HistoryPemakaianAirTawar?>>()
        .requestServer(computation: () async {
      final response = await _pemakaianAirTawarRemoteDataSource
          .postPemakaianAirTawar(token, pemakaianAirTawar);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingTheDataToMaster(response);
    });
  }

  Future<Map<MasterTangkiAir, HistoryPemakaianAirTawar?>>
      updateStatusPemakaianAirTawar(
          int id, String token, String date, Approve approved) async {
    return RequestServer<Map<MasterTangkiAir, HistoryPemakaianAirTawar?>>()
        .requestServer(computation: () async {
      final response = await _pemakaianAirTawarRemoteDataSource
          .updateStatusPemakaianAirTawar(id, token, date, approved);
      revisedDate = response.tanggalMundur;

      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingTheDataToMaster(response);
    });
  }
}
