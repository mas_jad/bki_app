import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/service/location/location_service.dart';
import '../../../../core/util/user_role.dart';
import '../model/history_pemakaian_air_tawar.dart';
import '../model/post_pemakaian_air_tawar.dart';

class PemakaianAirTawarRemoteDataSource {
  Future<HistoryPemakaianAirTawarResponse> getHistoryPemakaianAirTawar(
      int id, String token, String date) async {
    final endpoint = dotenv.get(dataPATPerTanggalEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {"kapal_id": id, "date": date},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return HistoryPemakaianAirTawarResponse.fromJson(response.data);
  }

  Future<HistoryPemakaianAirTawarResponse> postPemakaianAirTawar(
      String token, PostPemakaianAirTawar pemakaianAirTawar) async {
    final endpoint = dotenv.get(simpanDataPATEndpoint);
    final position = await locator<LocationService>().getLastPosition();
    final data = await pemakaianAirTawar.toJson(position);
    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPemakaianAirTawarResponse.fromJson(response.data);
  }

  Future<HistoryPemakaianAirTawarResponse> updateStatusPemakaianAirTawar(
      int id, String token, String date, Approve approve) async {
    final endpoint = dotenv.get(ubahStatusPATEndpoint);
    final response = await locator<Dio>().post(
      "$endpoint${locator<UserRole>().getUpdateStatusURL(true, approve)}",
      data: {"kapal_id": id, "date": date},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPemakaianAirTawarResponse.fromJson(response.data);
  }
}
