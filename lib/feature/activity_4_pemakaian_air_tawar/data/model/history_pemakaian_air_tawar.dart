class HistoryPemakaianAirTawarResponse {
  String? message;
  int? code;
  List<HistoryPemakaianAirTawar>? data;
  String? date;
  List<MasterTangkiAir>? masterTangkiAir;
  String? tanggalMundur;
  String? approved1;
  String? approved2;
  String? approved3;
  String? approved4;

  HistoryPemakaianAirTawarResponse(
      {this.message,
      this.code,
      this.data,
      this.date,
      this.masterTangkiAir,
      this.tanggalMundur,
      this.approved1,
      this.approved2,
      this.approved3,
      this.approved4});

  HistoryPemakaianAirTawarResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    if (json['data'] != null) {
      data = <HistoryPemakaianAirTawar>[];
      json['data'].forEach((v) {
        data!.add(HistoryPemakaianAirTawar.fromJson(v));
      });
    }
    date = json['date'];
    if (json['master_tangki_air'] != null) {
      masterTangkiAir = <MasterTangkiAir>[];
      json['master_tangki_air'].forEach((v) {
        masterTangkiAir!.add(MasterTangkiAir.fromJson(v));
      });
    }
    tanggalMundur = json['tanggal_mundur'];
    approved1 = json['approved_1'];
    approved2 = json['approved_2'];
    approved3 = json['approved_3'];
    approved4 = json['approved_4'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['date'] = date;
    if (masterTangkiAir != null) {
      data['master_tangki_air'] =
          masterTangkiAir!.map((v) => v.toJson()).toList();
    }
    data['tanggal_mundur'] = tanggalMundur;
    return data;
  }
}

class HistoryPemakaianAirTawar {
  int? id;
  String? posisiTangki;
  String? frameStart;
  String? frameTo;
  double? soundingDepth;
  double? soundingCap;
  int? trimKapal;
  String? image;
  String? statusProses;

  HistoryPemakaianAirTawar(
      {this.id,
      this.posisiTangki,
      this.frameStart,
      this.frameTo,
      this.soundingDepth,
      this.soundingCap,
      this.trimKapal,
      this.image,
      this.statusProses});

  HistoryPemakaianAirTawar.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    posisiTangki = json['posisi_tangki'];
    frameStart = json['frame_start']?.toString();
    frameTo = json['frame_to']?.toString();
    soundingDepth = double.tryParse(json['sounding_depth']?.toString() ?? "0");
    soundingCap = double.tryParse(json['sounding_cap']?.toString() ?? "0");
    trimKapal = json['trim_kapal'];
    image = json['image'];
    statusProses = json['status_proses'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['posisi_tangki'] = posisiTangki;
    data['frame_start'] = frameStart;
    data['frame_to'] = frameTo;
    data['sounding_depth'] = soundingDepth;
    data['sounding_cap'] = soundingCap;
    data['trim_kapal'] = trimKapal;
    data['image'] = image;
    data['status_proses'] = statusProses;
    return data;
  }
}

class MasterTangkiAir {
  int? id;
  String? posisi;
  String? frameStart;
  String? frameTo;

  MasterTangkiAir({this.id, this.posisi, this.frameStart, this.frameTo});

  MasterTangkiAir.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    posisi = json['posisi'];
    frameStart = json['frame_start']?.toString();
    frameTo = json['frame_to']?.toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['posisi'] = posisi;
    data['frame_start'] = frameStart;
    data['frame_to'] = frameTo;
    return data;
  }
}
