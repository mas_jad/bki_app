import 'package:dio/dio.dart';

class PostPemakaianAirTawar {
  int? kapalId;
  String? date;
  int? tangkiAirId;
  double? soundingDepth;
  double? soundingCap;
  int? trimKapal;
  String? image;

  PostPemakaianAirTawar(
      {this.kapalId,
      this.date,
      this.tangkiAirId,
      this.soundingDepth,
      this.soundingCap,
      this.trimKapal,
      this.image});

  Future<FormData> toJson(String? location) async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kapal_id'] = kapalId;
    data['date'] = date;
    data['tangki_air_id'] = tangkiAirId;
    data['sounding_depth'] = soundingDepth?.toDouble();
    data['sounding_cap'] = soundingCap?.toDouble();
    data['trim_kapal'] = trimKapal;
    data['location'] = location;
    data['image'] = image != null
        ? await MultipartFile.fromFile(
            image!,
            filename: image!.split('/').last,
          )
        : null;
    return FormData.fromMap(data);
  }
}
