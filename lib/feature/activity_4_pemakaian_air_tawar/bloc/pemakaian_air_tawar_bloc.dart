import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_pemakaian_air_tawar.dart';
import '../data/model/post_pemakaian_air_tawar.dart';
import '../data/repository/pemakaian_air_tawar_repository.dart';

part 'pemakaian_air_tawar_event.dart';
part 'pemakaian_air_tawar_state.dart';

class PemakaianAirTawarBloc
    extends Bloc<PemakaianAirTawarEvent, PemakaianAirTawarState> {
  final PemakaianAirTawarRepository _pemakaianAirTawarRepository;
  final UserRepository _userRepository;

  PemakaianAirTawarBloc(this._pemakaianAirTawarRepository, this._userRepository)
      : super(PemakaianAirTawarInitial()) {
    on<PostPemakaianAirTawarEvent>((event, emit) async {
      try {
        emit(PostPemakaianAirTawarLoading());
        final token = await _userRepository.getToken();
        final data = await _pemakaianAirTawarRepository.postPemakaianAirTawar(
            token, event.postPemakaianAirTawar);

        emit(PostPemakaianAirTawarSuccess());
        emit(HistoryPemakaianAirTawarSuccess(
          data,
          _pemakaianAirTawarRepository.getRevisedDate(),
          _pemakaianAirTawarRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostPemakaianAirTawarError(e.message));
        } else {
          emit(PostPemakaianAirTawarError(somethingErrorHappen));
        }
      }
    });

    on<GetHistoryPemakaianAirTawarEvent>((event, emit) async {
      try {
        emit(HistoryPemakaianAirTawarLoading());
        final token = await _userRepository.getToken();
        final data = await _pemakaianAirTawarRepository
            .getHistoryPemakaianAirTawar(event.id, token, event.date);

        emit(HistoryPemakaianAirTawarSuccess(
          data,
          _pemakaianAirTawarRepository.getRevisedDate(),
          _pemakaianAirTawarRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryPemakaianAirTawarError(e.message));
        } else {
          emit(HistoryPemakaianAirTawarError(somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusPemakaianAirTawarEvent>((event, emit) async {
      try {
        emit(UpdateStatusPemakaianAirTawarLoading());
        final token = await _userRepository.getToken();
        final data =
            await _pemakaianAirTawarRepository.updateStatusPemakaianAirTawar(
                event.id, token, event.date, event.approve);

        emit(UpdateStatusPemakaianAirTawarSuccess());

        emit(HistoryPemakaianAirTawarSuccess(
          data,
          _pemakaianAirTawarRepository.getRevisedDate(),
          _pemakaianAirTawarRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(UpdateStatusPemakaianAirTawarError(e.message));
        } else {
          emit(UpdateStatusPemakaianAirTawarError(somethingErrorHappen));
        }
      }
    });
  }
}
