part of 'pemakaian_air_tawar_bloc.dart';

abstract class PemakaianAirTawarState extends Equatable {
  const PemakaianAirTawarState();

  @override
  List<Object> get props => [];
}

class PemakaianAirTawarInitial extends PemakaianAirTawarState {}

// HISTORY
abstract class HistoryPemakaianAirTawarState extends PemakaianAirTawarState {}

class HistoryPemakaianAirTawarSuccess extends HistoryPemakaianAirTawarState {
  final Map<MasterTangkiAir, HistoryPemakaianAirTawar?> map;
  final String? revisedDate;
  final Approve approve;
  HistoryPemakaianAirTawarSuccess(this.map, this.revisedDate, this.approve);
}

class HistoryPemakaianAirTawarError extends HistoryPemakaianAirTawarState {
  final String message;
  HistoryPemakaianAirTawarError(this.message);
}

class HistoryPemakaianAirTawarLoading extends HistoryPemakaianAirTawarState {}

// POST
abstract class PostPemakaianAirTawarState extends PemakaianAirTawarState {}

class PostPemakaianAirTawarSuccess extends PostPemakaianAirTawarState {}

class PostPemakaianAirTawarError extends PostPemakaianAirTawarState {
  final String message;
  PostPemakaianAirTawarError(this.message);
}

class PostPemakaianAirTawarLoading extends PostPemakaianAirTawarState {}

// UPDATE STATUS
abstract class UpdateStatusPemakaianAirTawarState
    extends PemakaianAirTawarState {}

class UpdateStatusPemakaianAirTawarSuccess
    extends UpdateStatusPemakaianAirTawarState {}

class UpdateStatusPemakaianAirTawarError
    extends UpdateStatusPemakaianAirTawarState {
  final String message;
  UpdateStatusPemakaianAirTawarError(this.message);
}

class UpdateStatusPemakaianAirTawarLoading
    extends UpdateStatusPemakaianAirTawarState {}
