part of 'pemakaian_air_tawar_bloc.dart';

abstract class PemakaianAirTawarEvent extends Equatable {
  const PemakaianAirTawarEvent();

  @override
  List<Object> get props => [];
}

class PostPemakaianAirTawarEvent extends PemakaianAirTawarEvent {
  final PostPemakaianAirTawar postPemakaianAirTawar;
  const PostPemakaianAirTawarEvent(this.postPemakaianAirTawar);
}

class UpdateStatusPemakaianAirTawarEvent extends PemakaianAirTawarEvent {
  final int id;
  final String date;
  final Approve approve;
  const UpdateStatusPemakaianAirTawarEvent(this.id, this.date, this.approve);
}

class GetHistoryPemakaianAirTawarEvent extends PemakaianAirTawarEvent {
  final int id;
  final String date;
  const GetHistoryPemakaianAirTawarEvent(this.id, this.date);
}
