import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/button/custom_button_widget.dart';
import 'package:pms_pcm/core/widget/image/image_container_picker.dart';
import 'package:pms_pcm/core/widget/textfield/custom_textfield_widget.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/util/locator.dart';
import '../../../../core/util/toast.dart';
import '../../bloc/pemakaian_air_tawar_bloc.dart';
import '../../data/model/history_pemakaian_air_tawar.dart';
import '../../data/model/post_pemakaian_air_tawar.dart';

class PemakaianAirTawarInput extends StatefulWidget {
  const PemakaianAirTawarInput(
      {this.historyPemakaianAirTawar,
      required this.kapalId,
      required this.tankiKapal,
      required this.date,
      required this.isUserPermitedToSave,
      super.key});
  final int kapalId;
  final DateTime date;
  final bool isUserPermitedToSave;
  final MasterTangkiAir tankiKapal;
  final HistoryPemakaianAirTawar? historyPemakaianAirTawar;

  @override
  State<PemakaianAirTawarInput> createState() => _PemakaianAirTawarInputState();
}

class _PemakaianAirTawarInputState extends State<PemakaianAirTawarInput> {
  final TextEditingController soundingDeptEditingController =
      TextEditingController();
  final TextEditingController soundingCapEditingController =
      TextEditingController();
  final TextEditingController trimKapalEditingController =
      TextEditingController();
  late ValueNotifier<String?> imageValueNotifier;

  @override
  void initState() {
    super.initState();
    soundingCapEditingController.text =
        widget.historyPemakaianAirTawar?.soundingCap?.toString() ?? empty;
    soundingDeptEditingController.text =
        widget.historyPemakaianAirTawar?.soundingDepth?.toString() ?? empty;
    trimKapalEditingController.text =
        widget.historyPemakaianAirTawar?.trimKapal?.toString() ?? empty;
    imageValueNotifier = ValueNotifier(null);
  }

  postPemakaianAirTawar() async {
    BlocProvider.of<PemakaianAirTawarBloc>(context).add(
        PostPemakaianAirTawarEvent(PostPemakaianAirTawar(
            image: imageValueNotifier.value,
            kapalId: widget.kapalId,
            date: locator<MyDate>().getDateWithInputInString(widget.date),
            tangkiAirId: widget.tankiKapal.id,
            soundingCap: double.tryParse(soundingCapEditingController.text),
            soundingDepth: double.tryParse(soundingDeptEditingController.text),
            trimKapal: int.tryParse(trimKapalEditingController.text))));
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocListener<PemakaianAirTawarBloc, PemakaianAirTawarState>(
      listener: (context, state) {
        if (state is PostPemakaianAirTawarSuccess) {
          Navigator.pop(context);
          MyToast.showSuccess(success, context);
        } else if (state is PostPemakaianAirTawarError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Container(
          width: width,
          decoration: const BoxDecoration(
              color: white,
              boxShadow: [shadowCard],
              borderRadius: cardBorderRadius),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Text(
                          widget.tankiKapal.posisi.toString(),
                          style: ptSansTextStyle.copyWith(
                              color: white, fontWeight: bold, fontSize: 22),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8),
                            child: iconArrow.copyWith(color: transparent)),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Frame ${widget.tankiKapal.frameStart} - ${widget.tankiKapal.frameTo}",
                      style: ptSansTextStyle.copyWith(
                          color: white, fontWeight: medium, fontSize: 18),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Center(
                          child: ImageContainerPicker(
                        image: widget.historyPemakaianAirTawar?.image,
                        imageValueNotifier: imageValueNotifier,
                      )),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    CustomTextFieldWidget(
                      label: "Sounding Depth",
                      suffix: "CM",
                      textEditingController: soundingDeptEditingController,
                      textInputType: TextInputType.number,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextFieldWidget(
                      label: "Sounding Cap",
                      suffix: "Ton/Liter",
                      textEditingController: soundingCapEditingController,
                      textInputType: TextInputType.number,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextFieldWidget(
                      label: "Trim Kapal",
                      textEditingController: trimKapalEditingController,
                      textInputType: TextInputType.number,
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    if (widget.isUserPermitedToSave)
                      BlocBuilder<PemakaianAirTawarBloc,
                          PemakaianAirTawarState>(
                        builder: (context, state) {
                          return CustomButtonWidget(
                              isActive: state is PostPemakaianAirTawarLoading
                                  ? false
                                  : true,
                              callback: () {
                                postPemakaianAirTawar();
                              },
                              title: save);
                        },
                      )
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
