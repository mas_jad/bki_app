import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/util/enum.dart';
import 'package:pms_pcm/core/util/global_function.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/core/widget/dialog/custom_alert_dialog.dart';
import 'package:pms_pcm/core/widget/other/error_layout.dart';
import 'package:pms_pcm/core/widget/other/loading_widget.dart';
import 'package:pms_pcm/feature/activity_4_pemakaian_air_tawar/view/widget/pemakaian_air_tawar_input.dart';
import 'package:pms_pcm/feature/activity_4_pemakaian_air_tawar/view/widget/pemakaian_air_tawar_item.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/theme/theme.dart';
import '../../../core/util/locator.dart';
import '../../../core/util/user_role.dart';
import '../../activity_7_permintaan_barang_dan_jasa/bloc/permintaan_barang_dan_jasa_bloc.dart';
import '../../activity_7_permintaan_barang_dan_jasa/data/repository/permintaan_barang_dan_jasa_repository.dart';
import '../../activity_7_permintaan_barang_dan_jasa/view/widget/c_permintaan_barang_dan_jasa_input.dart';
import '../bloc/pemakaian_air_tawar_bloc.dart';

class PemakaianAirTawarPage extends StatefulWidget {
  const PemakaianAirTawarPage(
      {required this.kapalId, required this.departemenId, super.key});
  static const route = "/activity_pemakaian_air_tawar_page";
  final int kapalId;
  final int departemenId;
  @override
  State<PemakaianAirTawarPage> createState() => _PemakaianAirTawarPageState();
}

class _PemakaianAirTawarPageState extends State<PemakaianAirTawarPage> {
  late DateTime date;
  DateTime? revisedDate;
  Approve approveStatus = Approve(
      approved1: null, approved2: null, approved3: null, approved4: null);
  bool isDateToSaveNotExpired = false;
  bool isUserPermitedToApprove = false;
  bool isUserPermitedToSave = false;

  @override
  void initState() {
    super.initState();
    date = locator<MyDate>().getDateToday();
    BlocProvider.of<PemakaianAirTawarBloc>(context).add(
        GetHistoryPemakaianAirTawarEvent(
            widget.kapalId, locator<MyDate>().getDateTodayInString()));
  }

  getHistory() async {
    final now = locator<MyDate>().getDateToday();
    final result = await showMyDatePicker(context, date, now);
    if (result != null) {
      if (result != date) {
        date = result;
        final dateFormated = locator<MyDate>().getDateWithInputInString(date);
        if (mounted) {
          BlocProvider.of<PemakaianAirTawarBloc>(context).add(
              GetHistoryPemakaianAirTawarEvent(widget.kapalId, dateFormated));
        }
      }
    }
  }

  transformDataToWidget(HistoryPemakaianAirTawarSuccess state) {
    // GET REVISED DATE
    revisedDate = locator<MyDate>().getDateWithInputString(state.revisedDate);

    // GET TODAY
    final List<Widget> list = [];
    isDateToSaveNotExpired =
        !date.isBefore(revisedDate ?? locator<MyDate>().getDateToday());

    // GET STATUS DATA
    approveStatus = state.approve;
    isUserPermitedToApprove =
        locator<UserRole>().isUserPermitedToApprove(state.approve);
    isUserPermitedToSave = locator<UserRole>()
        .isUserPermitedToSave(state.approve, isDateToSaveNotExpired);

    state.map.forEach((key, value) {
      list.add(PemakaianAirTawarItem(
        historyPemakaianAirTawar: value,
        callback: (contextPemakaianAirTawar) {
          showModalBottomSheet(
              isDismissible: false,
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              isScrollControlled: true,
              context: context,
              builder: (modalContext) {
                return Padding(
                  padding: EdgeInsets.only(
                    bottom: MediaQuery.of(modalContext).viewInsets.bottom,
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        BlocProvider.value(
                          value:
                              BlocProvider.of<PemakaianAirTawarBloc>(context),
                          child: PemakaianAirTawarInput(
                            date: date,
                            tankiKapal: key,
                            historyPemakaianAirTawar: value,
                            kapalId: widget.kapalId,
                            isUserPermitedToSave: isUserPermitedToSave,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              });
        },
        title: key.posisi ?? empty,
        subtitle: "Frame ${key.frameStart}-${key.frameTo}",
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PemakaianAirTawarBloc, PemakaianAirTawarState>(
      listener: (context, state) {
        if (state is UpdateStatusPemakaianAirTawarSuccess) {
          MyToast.showSuccess(success, context);
        } else if (state is UpdateStatusPemakaianAirTawarError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context2) {
                    return BlocProvider(
                        create: (BuildContext context) {
                          return PermintaanBarangDanJasaBloc(
                              locator<PermintaanBarangDanJasaRepository>(),
                              locator<UserRepository>());
                        },
                        child: PermintaanBarangDanJasaInput(
                          kapalId: widget.kapalId,
                          isDateToSaveNotExpired: true,
                          parameterStok: ParameterStok.air,
                          date: locator<MyDate>().getDateToday(),
                          departmenId: widget.departemenId,
                          isDeck: !locator<UserRole>().isEngine(),
                        ));
                  }));
                },
                icon: const Icon(
                  Icons.request_page,
                  color: white,
                ))
          ],
          elevation: 0,
          backgroundColor: secondary,
          title: Text(
            "Pemakaian Air Tawar",
            style: ptSansTextStyle.copyWith(color: white),
          ),
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: BlocBuilder<PemakaianAirTawarBloc, PemakaianAirTawarState>(
            buildWhen: (previous, current) {
              return current is HistoryPemakaianAirTawarState;
            },
            builder: (context, state) {
              if (state is HistoryPemakaianAirTawarSuccess) {
                final list = transformDataToWidget(state);
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.vertical(
                              bottom: Radius.circular(12)),
                          boxShadow: [shadowCard]),
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(approveStatus.getStatus(),
                                    style: ptSansTextStyle.copyWith(
                                      color: statusColor(
                                          approveStatus.getStatus()),
                                      fontWeight: semiBold,
                                      fontSize: 16,
                                    )),
                                approveStatus.getName() != null
                                    ? Text(
                                        "by ${approveStatus.getName() ?? empty}",
                                        style: ptSansTextStyle.copyWith(
                                          color: lightText,
                                          fontWeight: reguler,
                                          fontSize: 12,
                                        ))
                                    : const SizedBox(),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          GestureDetector(
                            onTap: () {
                              getHistory();
                            },
                            child: Text(
                              locator<MyDate>()
                                  .getDateWithInputIndonesiaFormat(date),
                              maxLines: 1,
                              style: ptSansTextStyle.copyWith(
                                  color: black54,
                                  fontWeight: semiBold,
                                  fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: GridView.builder(
                      padding: const EdgeInsets.all(20),
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 200,
                        childAspectRatio: 1,
                        crossAxisSpacing: 20,
                        mainAxisSpacing: 20,
                      ),
                      itemCount: list.length,
                      itemBuilder: (BuildContext ctx, index) {
                        return list[index];
                      },
                    )),
                  ],
                );
              } else if (state is HistoryPemakaianAirTawarError) {
                return ErrorLayout(message: state.message);
              }
              return const Center(child: LoadingWidget());
            },
          ),
        ),
        floatingActionButton:
            BlocBuilder<PemakaianAirTawarBloc, PemakaianAirTawarState>(
          builder: (context, state) {
            if (isUserPermitedToApprove) {
              if (state is UpdateStatusPemakaianAirTawarLoading) {
                return FloatingActionButton(
                  elevation: 12,
                  backgroundColor: secondary,
                  onPressed: () {},
                  child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                itemImage4,
                              ),
                              fit: BoxFit.fill)),
                      child: const Center(
                          child: LoadingWidget(
                        color: white,
                      ))),
                );
              }

              return FloatingActionButton(
                elevation: elevationFAB,
                backgroundColor: primary,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: ((dialogContext) => BlocProvider.value(
                            value:
                                BlocProvider.of<PemakaianAirTawarBloc>(context),
                            child: CustomAlertDialog(
                                description: dataCannotChangeAgain,
                                title:
                                    locator<UserRole>().getTitle(approveStatus),
                                voidCallback: () {
                                  BlocProvider.of<PemakaianAirTawarBloc>(
                                          context)
                                      .add(UpdateStatusPemakaianAirTawarEvent(
                                          widget.kapalId,
                                          locator<MyDate>()
                                              .getDateWithInputInString(date),
                                          approveStatus));
                                }),
                          )));
                },
                child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              itemImage4,
                            ),
                            fit: BoxFit.fill)),
                    child: Center(
                        child: locator<UserRole>().getIcon(approveStatus))),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}
