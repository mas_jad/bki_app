import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';

import '../../../../core/data/model/approve.dart';
import '../../../../core/service/location/location_service.dart';
import '../../../../core/util/user_role.dart';
import '../model/history_pemakaian_oli_mesin.dart';
import '../model/post_pemakaian_oli_mesin.dart';

class PemakaianOliMesinRemoteDataSource {
  Future<HistoryPemakaianOliMesinResponse> getHistoryPemakaianOliMesin(
      int id, String token, String date) async {
    final endpoint = dotenv.get(dataPOMPerTanggalEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {"kapal_id": id, "date": date},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPemakaianOliMesinResponse.fromJson(response.data);
  }

  Future<HistoryPemakaianOliMesinResponse> postPemakaianOliMesin(
      String token, PostPemakaianOliMesin pemakaianOliMesin) async {
    final endpoint = dotenv.get(simpanDataPOMEndpoint);
    final position = await locator<LocationService>().getLastPosition();
    final data = await pemakaianOliMesin.toJson(position);
    final response = await locator<Dio>().post(
      endpoint,
      data: data,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPemakaianOliMesinResponse.fromJson(response.data);
  }

  Future<HistoryPemakaianOliMesinResponse> updateStatusPemakaianOliMesin(
      int id, String token, String date, Approve approve) async {
    final endpoint = dotenv.get(ubahStatusPOMEndpoint);
    final response = await locator<Dio>().post(
      "$endpoint${locator<UserRole>().getUpdateStatusURL(true, approve)}",
      data: {"kapal_id": id, "date": date},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return HistoryPemakaianOliMesinResponse.fromJson(response.data);
  }
}
