import '../../../../core/data/model/approve.dart';
import '../../../../core/util/request.dart';
import '../model/history_pemakaian_oli_mesin.dart';
import '../model/post_pemakaian_oli_mesin.dart';
import '../source/pemakaian_oli_mesin_remote_data_source.dart';

class PemakaianOliMesinRepository {
  final PemakaianOliMesinRemoteDataSource _pemakaianOliMesinRemoteDataSource;
  PemakaianOliMesinRepository(this._pemakaianOliMesinRemoteDataSource);

  String? revisedDate;

  Approve? approve;

  String? getRevisedDate() {
    return revisedDate;
  }

  Approve getApprove() {
    return approve ??
        Approve(
            approved1: null, approved2: null, approved3: null, approved4: null);
  }

  Map<MasterPp, HistoryPemakaianOliMesin?> _relatingTheDataToMaster(
      HistoryPemakaianOliMesinResponse response) {
    final Map<MasterPp, HistoryPemakaianOliMesin?> map = {};
    final listHistoryBahanBakar = response.data ?? [];
    final listPesawatPenggerak = response.masterPp ?? [];
    // Relating Tanki To Data History
    for (var element in listPesawatPenggerak) {
      var isThere = false;
      for (var element2 in listHistoryBahanBakar) {
        if (element.pesawatPenggerakId == element2.pesawatPenggerakId) {
          map.addAll({element: element2});
          isThere = true;
          break;
        }
      }
      if (!isThere) {
        map.addAll({element: null});
      }
    }
    return map;
  }

  Future<Map<MasterPp, HistoryPemakaianOliMesin?>> getHistoryPemakaianOliMesin(
      int id, String token, String date) async {
    return RequestServer<Map<MasterPp, HistoryPemakaianOliMesin?>>()
        .requestServer(computation: () async {
      final response = await _pemakaianOliMesinRemoteDataSource
          .getHistoryPemakaianOliMesin(id, token, date);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingTheDataToMaster(response);
    });
  }

  Future<Map<MasterPp, HistoryPemakaianOliMesin?>> postPemakaianOliMesin(
      String token, PostPemakaianOliMesin pemakaianOliMesin) async {
    return RequestServer<Map<MasterPp, HistoryPemakaianOliMesin?>>()
        .requestServer(computation: () async {
      final response = await _pemakaianOliMesinRemoteDataSource
          .postPemakaianOliMesin(token, pemakaianOliMesin);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingTheDataToMaster(response);
    });
  }

  Future<Map<MasterPp, HistoryPemakaianOliMesin?>>
      updateStatusPemakaianOliMesin(
          int id, String token, String date, Approve approved) async {
    return RequestServer<Map<MasterPp, HistoryPemakaianOliMesin?>>()
        .requestServer(computation: () async {
      final response = await _pemakaianOliMesinRemoteDataSource
          .updateStatusPemakaianOliMesin(id, token, date, approved);
      revisedDate = response.tanggalMundur;
      approve = Approve(
          approved1: response.approved1,
          approved2: response.approved2,
          approved3: response.approved3,
          approved4: response.approved4);
      return _relatingTheDataToMaster(response);
    });
  }
}
