import 'package:dio/dio.dart';

class PostPemakaianOliMesin {
  int? kapalId;
  String? date;
  int? pesawatPenggerakId;
  int? pemakaianHariIni;
  int? penambahanHariIni;
  String? image;

  PostPemakaianOliMesin(
      {this.kapalId,
      this.date,
      this.pesawatPenggerakId,
      this.pemakaianHariIni,
      this.penambahanHariIni,
      this.image});

  Future<FormData> toJson(String? location) async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kapal_id'] = kapalId;
    data['date'] = date;
    data['pesawat_penggerak_id'] = pesawatPenggerakId;
    data['pemakaian_hari_ini'] = pemakaianHariIni;
    data['penambahan_hari_ini'] = penambahanHariIni;
    data['location'] = location;
    data['image'] = image != null
        ? await MultipartFile.fromFile(
            image!,
            filename: image!.split('/').last,
          )
        : null;
    return FormData.fromMap(data);
  }
}
