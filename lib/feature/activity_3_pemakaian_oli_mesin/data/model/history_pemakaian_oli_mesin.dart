class HistoryPemakaianOliMesinResponse {
  String? message;
  int? code;
  List<HistoryPemakaianOliMesin>? data;
  String? date;
  List<MasterPp>? masterPp;
  String? tanggalMundur;
  String? approved1;
  String? approved2;
  String? approved3;
  String? approved4;

  HistoryPemakaianOliMesinResponse(
      {this.message,
      this.code,
      this.data,
      this.date,
      this.masterPp,
      this.tanggalMundur,
      this.approved1,
      this.approved2,
      this.approved3,
      this.approved4});

  HistoryPemakaianOliMesinResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    if (json['data'] != null) {
      data = <HistoryPemakaianOliMesin>[];
      json['data'].forEach((v) {
        data!.add(HistoryPemakaianOliMesin.fromJson(v));
      });
    }
    date = json['date'];
    if (json['master_pp'] != null) {
      masterPp = <MasterPp>[];
      json['master_pp'].forEach((v) {
        masterPp!.add(MasterPp.fromJson(v));
      });
    }
    tanggalMundur = json['tanggal_mundur'];
    approved1 = json['approved_1'];
    approved2 = json['approved_2'];
    approved3 = json['approved_3'];
    approved4 = json['approved_4'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['date'] = date;
    if (masterPp != null) {
      data['master_pp'] = masterPp!.map((v) => v.toJson()).toList();
    }
    data['tanggal_mundur'] = tanggalMundur;
    return data;
  }
}

class HistoryPemakaianOliMesin {
  int? id;
  int? pesawatPenggerakId;
  String? pesawatPenggerak;
  String? jenisOli;
  int? sisaKemarin;
  int? pemakaianHariIni;
  int? penambahanHariIni;
  int? sisaHariIni;
  String? image;
  String? statusProses;

  HistoryPemakaianOliMesin(
      {this.id,
      this.pesawatPenggerakId,
      this.pesawatPenggerak,
      this.jenisOli,
      this.sisaKemarin,
      this.pemakaianHariIni,
      this.penambahanHariIni,
      this.sisaHariIni,
      this.image,
      this.statusProses});

  HistoryPemakaianOliMesin.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pesawatPenggerakId = json['pesawat_penggerak_id'];
    pesawatPenggerak = json['pesawat_penggerak'];
    jenisOli = json['jenis_oli'];
    sisaKemarin = json['sisa_kemarin'];
    pemakaianHariIni = json['pemakaian_hari_ini'];
    penambahanHariIni = json['penambahan_hari_ini'];
    sisaHariIni = json['sisa_hari_ini'];
    image = json['image'];
    statusProses = json['status_proses'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['pesawat_penggerak_id'] = pesawatPenggerakId;
    data['pesawat_penggerak'] = pesawatPenggerak;
    data['jenis_oli'] = jenisOli;
    data['sisa_kemarin'] = sisaKemarin;
    data['pemakaian_hari_ini'] = pemakaianHariIni;
    data['penambahan_hari_ini'] = penambahanHariIni;
    data['sisa_hari_ini'] = sisaHariIni;
    data['image'] = image;
    data['status_proses'] = statusProses;
    return data;
  }
}

class MasterPp {
  int? pesawatPenggerakId;
  String? namaPp;
  String? jenisOli;
  int? sisaKemarin;

  MasterPp(
      {this.pesawatPenggerakId, this.namaPp, this.jenisOli, this.sisaKemarin});

  MasterPp.fromJson(Map<String, dynamic> json) {
    pesawatPenggerakId = json['pesawat_penggerak_id'];
    namaPp = json['nama_pp'];
    jenisOli = json['jenis_oli'];
    sisaKemarin = json['sisa_kemarin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['pesawat_penggerak_id'] = pesawatPenggerakId;
    data['nama_pp'] = namaPp;
    data['jenis_oli'] = jenisOli;
    return data;
  }
}
