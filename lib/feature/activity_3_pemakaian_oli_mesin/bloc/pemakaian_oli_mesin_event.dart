part of 'pemakaian_oli_mesin_bloc.dart';

abstract class PemakaianOliMesinEvent extends Equatable {
  const PemakaianOliMesinEvent();

  @override
  List<Object> get props => [];
}

class PostPemakaianOliMesinEvent extends PemakaianOliMesinEvent {
  final PostPemakaianOliMesin postPemakaianOliMesin;
  const PostPemakaianOliMesinEvent(this.postPemakaianOliMesin);
}

class UpdateStatusPemakaianOliMesinEvent extends PemakaianOliMesinEvent {
  final int id;
  final String date;
  final Approve approve;
  const UpdateStatusPemakaianOliMesinEvent(this.id, this.date, this.approve);
}

class GetHistoryPemakaianOliMesinEvent extends PemakaianOliMesinEvent {
  final int id;
  final String date;
  const GetHistoryPemakaianOliMesinEvent(this.id, this.date);
}
