part of 'pemakaian_oli_mesin_bloc.dart';

abstract class PemakaianOliMesinState extends Equatable {
  const PemakaianOliMesinState();

  @override
  List<Object> get props => [];
}

class PemakaianOliMesinInitial extends PemakaianOliMesinState {}

// HISTORY
abstract class HistoryPemakaianOliMesinState extends PemakaianOliMesinState {}

class HistoryPemakaianOliMesinSuccess extends HistoryPemakaianOliMesinState {
  final Map<MasterPp, HistoryPemakaianOliMesin?> map;
  final String? revisedDate;
  final Approve approve;
  HistoryPemakaianOliMesinSuccess(this.map, this.revisedDate, this.approve);
}

class HistoryPemakaianOliMesinError extends HistoryPemakaianOliMesinState {
  final String message;
  HistoryPemakaianOliMesinError(this.message);
}

class HistoryPemakaianOliMesinLoading extends HistoryPemakaianOliMesinState {}

// POST
abstract class PostPemakaianOliMesinState extends PemakaianOliMesinState {}

class PostPemakaianOliMesinSuccess extends PostPemakaianOliMesinState {}

class PostPemakaianOliMesinError extends PostPemakaianOliMesinState {
  final String message;
  PostPemakaianOliMesinError(this.message);
}

class PostPemakaianOliMesinLoading extends PostPemakaianOliMesinState {}

// UPDATE STATUS
abstract class UpdateStatusPemakaianOliMesinState
    extends PemakaianOliMesinState {}

class UpdateStatusPemakaianOliMesinSuccess
    extends UpdateStatusPemakaianOliMesinState {}

class UpdateStatusPemakaianOliMesinError
    extends UpdateStatusPemakaianOliMesinState {
  final String message;
  UpdateStatusPemakaianOliMesinError(this.message);
}

class UpdateStatusPemakaianOliMesinLoading
    extends UpdateStatusPemakaianOliMesinState {}
