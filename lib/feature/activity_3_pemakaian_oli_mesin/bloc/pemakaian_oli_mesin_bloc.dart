import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/data/model/error_response.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/util/const.dart';
import '../data/model/history_pemakaian_oli_mesin.dart';
import '../data/model/post_pemakaian_oli_mesin.dart';
import '../data/repository/pemakaian_oli_mesin_repository.dart';

part 'pemakaian_oli_mesin_event.dart';
part 'pemakaian_oli_mesin_state.dart';

class PemakaianOliMesinBloc
    extends Bloc<PemakaianOliMesinEvent, PemakaianOliMesinState> {
  final PemakaianOliMesinRepository _pemakaianOliMesinRepository;
  final UserRepository _userRepository;

  PemakaianOliMesinBloc(this._pemakaianOliMesinRepository, this._userRepository)
      : super(PemakaianOliMesinInitial()) {
    on<PostPemakaianOliMesinEvent>((event, emit) async {
      try {
        emit(PostPemakaianOliMesinLoading());
        final token = await _userRepository.getToken();
        final data = await _pemakaianOliMesinRepository.postPemakaianOliMesin(
            token, event.postPemakaianOliMesin);

        emit(PostPemakaianOliMesinSuccess());
        emit(HistoryPemakaianOliMesinSuccess(
          data,
          _pemakaianOliMesinRepository.getRevisedDate(),
          _pemakaianOliMesinRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(PostPemakaianOliMesinError(e.message));
        } else {
          emit(PostPemakaianOliMesinError(somethingErrorHappen));
        }
      }
    });

    on<GetHistoryPemakaianOliMesinEvent>((event, emit) async {
      try {
        emit(HistoryPemakaianOliMesinLoading());
        final token = await _userRepository.getToken();
        final data = await _pemakaianOliMesinRepository
            .getHistoryPemakaianOliMesin(event.id, token, event.date);

        emit(HistoryPemakaianOliMesinSuccess(
          data,
          _pemakaianOliMesinRepository.getRevisedDate(),
          _pemakaianOliMesinRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(HistoryPemakaianOliMesinError(e.message));
        } else {
          emit(HistoryPemakaianOliMesinError(somethingErrorHappen));
        }
      }
    });

    on<UpdateStatusPemakaianOliMesinEvent>((event, emit) async {
      try {
        emit(UpdateStatusPemakaianOliMesinLoading());
        final token = await _userRepository.getToken();
        final data =
            await _pemakaianOliMesinRepository.updateStatusPemakaianOliMesin(
                event.id, token, event.date, event.approve);

        emit(UpdateStatusPemakaianOliMesinSuccess());
        emit(HistoryPemakaianOliMesinSuccess(
          data,
          _pemakaianOliMesinRepository.getRevisedDate(),
          _pemakaianOliMesinRepository.getApprove(),
        ));
      } catch (e) {
        if (e is ErrorResponse) {
          emit(UpdateStatusPemakaianOliMesinError(e.message));
        } else {
          emit(UpdateStatusPemakaianOliMesinError(somethingErrorHappen));
        }
      }
    });
  }
}
