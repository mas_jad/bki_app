import 'package:flutter/material.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/size.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/other/custom_image_network.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';

import '../../data/model/history_pemakaian_oli_mesin.dart';

class PemakaianOliMesinItem extends StatelessWidget {
  const PemakaianOliMesinItem(
      {required this.callback,
      required this.title,
      this.icon,
      this.subtitle,
      required this.historyPemakaianOliMesin,
      super.key});
  final Function(BuildContext context) callback;
  final String title;
  final String? subtitle;
  final Icon? icon;
  final HistoryPemakaianOliMesin? historyPemakaianOliMesin;

  @override
  Widget build(BuildContext context) {
    final isFilled = historyPemakaianOliMesin?.pemakaianHariIni != null;
    return PopWidget(
        onTap: () {
          callback(context);
        },
        child: Stack(
          children: [
            Container(
              width: sizeGridWidth,
              height: sizeGridHeight,
              decoration: const BoxDecoration(
                color: secondary,
                borderRadius: cardBorderRadius,
              ),
            ),
            if (historyPemakaianOliMesin?.image != null)
              CustomImageNetwork(
                  width: sizeGridWidth,
                  height: sizeGridHeight,
                  borderRadius: cardBorderRadius,
                  imageUrl: historyPemakaianOliMesin?.image),
            Container(
              width: sizeGridWidth,
              height: sizeGridHeight,
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(isFilled ? itemImage1f : itemImage1),
                    fit: BoxFit.cover),
                borderRadius: cardBorderRadius,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    title,
                    style: ptSansTextStyle.copyWith(
                        color: white, fontWeight: bold),
                  ),
                  if (subtitle != null)
                    Column(
                      children: [
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          subtitle ?? empty,
                          style: ptSansTextStyle.copyWith(
                              color: white, fontSize: 12),
                        ),
                      ],
                    ),
                ],
              ),
            ),
          ],
        ));
  }
}
