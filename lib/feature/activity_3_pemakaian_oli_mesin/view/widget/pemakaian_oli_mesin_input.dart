import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/button/custom_button_widget.dart';
import 'package:pms_pcm/core/widget/image/image_container_picker.dart';
import 'package:pms_pcm/core/widget/textfield/custom_textfield_widget.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/util/locator.dart';
import '../../../../core/util/toast.dart';
import '../../bloc/pemakaian_oli_mesin_bloc.dart';
import '../../data/model/history_pemakaian_oli_mesin.dart';
import '../../data/model/post_pemakaian_oli_mesin.dart';

class PemakaianOliMesinInput extends StatefulWidget {
  const PemakaianOliMesinInput(
      {this.historyPemakaianOliMesin,
      required this.kapalId,
      required this.pesawatPenggerak,
      required this.date,
      required this.isUserPermitedtoSave,
      super.key});
  final int kapalId;
  final DateTime date;
  final bool isUserPermitedtoSave;
  final MasterPp pesawatPenggerak;
  final HistoryPemakaianOliMesin? historyPemakaianOliMesin;

  @override
  State<PemakaianOliMesinInput> createState() => _PemakaianOliMesinInputState();
}

class _PemakaianOliMesinInputState extends State<PemakaianOliMesinInput> {
  // final TextEditingController penambahanHariIniEditingController =
  //     TextEditingController();
  final TextEditingController pemakaianHariIniEditingController =
      TextEditingController();
  final TextEditingController sisaKemarinEditingController =
      TextEditingController();
  late ValueNotifier<String?> imageValueNotifier;

  @override
  void initState() {
    super.initState();
    pemakaianHariIniEditingController.text =
        widget.historyPemakaianOliMesin?.pemakaianHariIni?.toString() ?? empty;
    // penambahanHariIniEditingController.text =
    //     widget.historyPemakaianOliMesin?.penambahanHariIni?.toString() ?? empty;
    sisaKemarinEditingController.text =
        widget.pesawatPenggerak.sisaKemarin?.toString() ?? empty;

    imageValueNotifier = ValueNotifier(null);
  }

  postPemakaianOliMesin() async {
    BlocProvider.of<PemakaianOliMesinBloc>(context)
        .add(PostPemakaianOliMesinEvent(PostPemakaianOliMesin(
      image: imageValueNotifier.value,
      kapalId: widget.kapalId,
      date: locator<MyDate>().getDateWithInputInString(widget.date),
      pesawatPenggerakId: widget.pesawatPenggerak.pesawatPenggerakId,
      // penambahanHariIni: int.tryParse(penambahanHariIniEditingController.text),
      pemakaianHariIni: int.tryParse(pemakaianHariIniEditingController.text),
    )));
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocListener<PemakaianOliMesinBloc, PemakaianOliMesinState>(
      listener: (context, state) {
        if (state is PostPemakaianOliMesinSuccess) {
          Navigator.pop(context);
          MyToast.showSuccess(success, context);
        } else if (state is PostPemakaianOliMesinError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Container(
          width: width,
          decoration: const BoxDecoration(
              color: white,
              boxShadow: [shadowCard],
              borderRadius: cardBorderRadius),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(itemImage2), fit: BoxFit.cover),
                    color: secondary,
                    borderRadius: BorderRadius.all(Radius.circular(18))),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: (() {
                              Navigator.pop(context);
                            }),
                            icon: iconArrow.copyWith(color: white)),
                        Text(
                          widget.pesawatPenggerak.namaPp.toString(),
                          style: ptSansTextStyle.copyWith(
                              color: white, fontWeight: bold, fontSize: 22),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8),
                            child: iconArrow.copyWith(color: transparent)),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      widget.pesawatPenggerak.jenisOli ?? empty,
                      style: ptSansTextStyle.copyWith(
                          color: white, fontWeight: medium, fontSize: 18),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Center(
                          child: ImageContainerPicker(
                        image: widget.historyPemakaianOliMesin?.image,
                        imageValueNotifier: imageValueNotifier,
                      )),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    if (widget.date
                        .isAtSameMomentAs(locator<MyDate>().getDateToday()))
                      CustomTextFieldWidget(
                        label: "Sisa Oli",
                        suffix: "Liter",
                        isEnable: false,
                        textEditingController: sisaKemarinEditingController,
                        textInputType: TextInputType.number,
                      ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextFieldWidget(
                      label: "Pemakaian Hari Ini",
                      suffix: "Liter",
                      textEditingController: pemakaianHariIniEditingController,
                      textInputType: TextInputType.number,
                    ),
                    // const SizedBox(
                    //   height: 16,
                    // ),
                    // CustomTextFieldWidget(
                    //   label: "Penambahan Hari Ini",
                    //   suffix: "Liter",
                    //   textEditingController: penambahanHariIniEditingController,
                    //   textInputType: TextInputType.number,
                    // ),
                    const SizedBox(
                      height: 30,
                    ),
                    if (widget.isUserPermitedtoSave)
                      BlocBuilder<PemakaianOliMesinBloc,
                          PemakaianOliMesinState>(
                        builder: (context, state) {
                          return CustomButtonWidget(
                              isActive: state is PostPemakaianOliMesinLoading
                                  ? false
                                  : true,
                              callback: () {
                                postPemakaianOliMesin();
                              },
                              title: save);
                        },
                      )
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
