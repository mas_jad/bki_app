import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';

import '../../../core/bloc/master_barang_oli_bloc.dart';
import '../../../core/data/repository/user_repository.dart';
import '../../../core/service/date/date.dart';
import '../../../core/theme/theme.dart';
import '../../../core/util/enum.dart';
import '../../../core/util/locator.dart';
import '../../../core/util/user_role.dart';
import '../../../core/widget/other/error_layout.dart';
import '../../../core/widget/other/loading_widget.dart';
import '../../activity_7_permintaan_barang_dan_jasa/bloc/permintaan_barang_dan_jasa_bloc.dart';
import '../../activity_7_permintaan_barang_dan_jasa/data/repository/permintaan_barang_dan_jasa_repository.dart';
import '../../activity_7_permintaan_barang_dan_jasa/view/widget/c_permintaan_barang_dan_jasa_input.dart';

class AlertOliPage extends StatefulWidget {
  const AlertOliPage(
      {required this.kapalId, required this.departemenId, super.key});
  static const route = "/alert_oli_page";
  final int kapalId;
  final int departemenId;
  @override
  State<AlertOliPage> createState() => _AlertOliPageState();
}

class _AlertOliPageState extends State<AlertOliPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: secondary,
        title: Text(
          "Request Oli Mesin",
          style: ptSansTextStyle.copyWith(color: white),
        ),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: BlocBuilder<MasterBarangOliBloc, MasterBarangOliState>(
          builder: (context, state) {
            if (state is MasterBarangOliSuccess) {
              return ListView(padding: const EdgeInsets.all(16), children: [
                ...state.listMasterOli.map((e) => PopWidget(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context2) {
                          return BlocProvider(
                              create: (BuildContext context) {
                                return PermintaanBarangDanJasaBloc(
                                    locator<
                                        PermintaanBarangDanJasaRepository>(),
                                    locator<UserRepository>());
                              },
                              child: PermintaanBarangDanJasaInput(
                                kapalId: widget.kapalId,
                                parameterStok: ParameterStok.oli,
                                isDateToSaveNotExpired: true,
                                date: locator<MyDate>().getDateToday(),
                                departmenId: widget.departemenId,
                                isDeck: !locator<UserRole>().isEngine(),
                              ));
                        }));
                      },
                      child: Container(
                        padding: const EdgeInsets.all(20),
                        margin: const EdgeInsets.only(bottom: 16),
                        decoration: const BoxDecoration(
                            color: white,
                            boxShadow: [shadowCard],
                            borderRadius: cardBorderRadius),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  e.namaJenisOliYgDigunakan ?? empty,
                                  style:
                                      ptSansTextStyle.copyWith(color: primary),
                                ),
                                Text(
                                  e.namaPesawatPenggerak ?? empty,
                                  style:
                                      ptSansTextStyle.copyWith(color: black54),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 6,
                            ),
                            Text(
                              e.merekOliYgDigunakan ?? empty,
                              style: ptSansTextStyle.copyWith(
                                  fontSize: 12, color: quaternaryAccent),
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  e.message ?? empty,
                                  style: ptSansTextStyle.copyWith(
                                      color: lightText, fontWeight: light),
                                ),
                                Text(
                                  e.totalSaatIni ?? empty,
                                  style: ptSansTextStyle.copyWith(
                                      color: e.isRequestOli == 1 ? red : green),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ))
              ]);
            } else if (state is MasterBarangOliError) {
              return ErrorLayout(message: state.message);
            }
            return const Center(child: LoadingWidget());
          },
        ),
      ),
    );
  }
}
