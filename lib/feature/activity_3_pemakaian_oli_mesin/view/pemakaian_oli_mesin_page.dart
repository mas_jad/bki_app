import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/util/global_function.dart';
import 'package:pms_pcm/core/util/toast.dart';
import 'package:pms_pcm/core/widget/dialog/custom_alert_dialog.dart';
import 'package:pms_pcm/core/widget/other/error_layout.dart';
import 'package:pms_pcm/core/widget/other/loading_widget.dart';
import 'package:pms_pcm/feature/activity_3_pemakaian_oli_mesin/view/alert_oli_page.dart';
import 'package:pms_pcm/feature/activity_3_pemakaian_oli_mesin/view/widget/pemakaian_oli_mesin_input.dart';
import 'package:pms_pcm/feature/activity_3_pemakaian_oli_mesin/view/widget/pemakaian_oli_mesin_item.dart';

import '../../../core/data/model/approve.dart';
import '../../../core/theme/theme.dart';
import '../../../core/util/locator.dart';
import '../../../core/util/user_role.dart';
import '../bloc/pemakaian_oli_mesin_bloc.dart';

class PemakaianOliMesinPage extends StatefulWidget {
  const PemakaianOliMesinPage(
      {required this.kapalId, required this.departemenId, super.key});
  static const route = "/activity_pemakaian_oli_mesin_page";
  final int kapalId;
  final int departemenId;
  @override
  State<PemakaianOliMesinPage> createState() => _PemakaianOliMesinPageState();
}

class _PemakaianOliMesinPageState extends State<PemakaianOliMesinPage> {
  late DateTime date;
  DateTime? revisedDate;
  Approve approveStatus = Approve(
      approved1: null, approved2: null, approved3: null, approved4: null);
  bool isDateToSaveNotExpired = false;
  bool isUserPermitedToApprove = false;
  bool isUserPermitedToSave = false;

  @override
  void initState() {
    super.initState();
    date = locator<MyDate>().getDateToday();
    BlocProvider.of<PemakaianOliMesinBloc>(context).add(
        GetHistoryPemakaianOliMesinEvent(
            widget.kapalId, locator<MyDate>().getDateTodayInString()));
  }

  getHistory() async {
    final now = locator<MyDate>().getDateToday();
    final result = await showMyDatePicker(context, date, now);
    if (result != null) {
      if (result != date) {
        date = result;
        final dateFormated = locator<MyDate>().getDateWithInputInString(date);
        if (mounted) {
          BlocProvider.of<PemakaianOliMesinBloc>(context).add(
              GetHistoryPemakaianOliMesinEvent(widget.kapalId, dateFormated));
        }
      }
    }
  }

  transformDataToWidget(HistoryPemakaianOliMesinSuccess state) {
    // GET REVISED DATE
    revisedDate = locator<MyDate>().getDateWithInputString(state.revisedDate);

    // GET TODAY
    final List<Widget> list = [];
    isDateToSaveNotExpired =
        !date.isBefore(revisedDate ?? locator<MyDate>().getDateToday());

    // GET STATUS DATA
    approveStatus = state.approve;
    isUserPermitedToApprove =
        locator<UserRole>().isUserPermitedToApprove(state.approve);
    isUserPermitedToSave = locator<UserRole>()
        .isUserPermitedToSave(state.approve, isDateToSaveNotExpired);

    state.map.forEach((key, value) {
      list.add(PemakaianOliMesinItem(
        historyPemakaianOliMesin: value,
        callback: (contextPemakaianOliMesin) {
          showModalBottomSheet(
              isDismissible: false,
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(20))),
              isScrollControlled: true,
              context: context,
              builder: (modalContext) {
                return Padding(
                  padding: EdgeInsets.only(
                    bottom: MediaQuery.of(modalContext).viewInsets.bottom,
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        BlocProvider.value(
                          value:
                              BlocProvider.of<PemakaianOliMesinBloc>(context),
                          child: PemakaianOliMesinInput(
                            date: date,
                            pesawatPenggerak: key,
                            historyPemakaianOliMesin: value,
                            kapalId: widget.kapalId,
                            isUserPermitedtoSave: isUserPermitedToSave,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              });
        },
        title: key.namaPp ?? empty,
        subtitle: key.jenisOli ?? empty,
      ));
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PemakaianOliMesinBloc, PemakaianOliMesinState>(
      listener: (context, state) {
        if (state is UpdateStatusPemakaianOliMesinSuccess) {
          MyToast.showSuccess(success, context);
        } else if (state is UpdateStatusPemakaianOliMesinError) {
          MyToast.showError(state.message, context);
        }
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, AlertOliPage.route,
                      arguments: [widget.kapalId, widget.departemenId]);
                },
                icon: const Icon(
                  Icons.request_page,
                  color: white,
                ))
          ],
          elevation: 0,
          backgroundColor: secondary,
          title: Text(
            "Pemakaian Oli Mesin",
            style: ptSansTextStyle.copyWith(color: white),
          ),
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: BlocBuilder<PemakaianOliMesinBloc, PemakaianOliMesinState>(
            buildWhen: (previous, current) {
              return current is HistoryPemakaianOliMesinState;
            },
            builder: (context, state) {
              if (state is HistoryPemakaianOliMesinSuccess) {
                final list = transformDataToWidget(state);
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.vertical(
                              bottom: Radius.circular(12)),
                          boxShadow: [shadowCard]),
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(approveStatus.getStatus(),
                                    style: ptSansTextStyle.copyWith(
                                      color: statusColor(
                                          approveStatus.getStatus()),
                                      fontWeight: semiBold,
                                      fontSize: 16,
                                    )),
                                approveStatus.getName() != null
                                    ? Text(
                                        "by ${approveStatus.getName() ?? empty}",
                                        style: ptSansTextStyle.copyWith(
                                          color: lightText,
                                          fontWeight: reguler,
                                          fontSize: 12,
                                        ))
                                    : const SizedBox(),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          GestureDetector(
                            onTap: () {
                              getHistory();
                            },
                            child: Text(
                              locator<MyDate>()
                                  .getDateWithInputIndonesiaFormat(date),
                              maxLines: 1,
                              style: ptSansTextStyle.copyWith(
                                  color: black54,
                                  fontWeight: semiBold,
                                  fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: GridView.builder(
                      padding: const EdgeInsets.all(20),
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 200,
                        childAspectRatio: 1,
                        crossAxisSpacing: 20,
                        mainAxisSpacing: 20,
                      ),
                      itemCount: list.length,
                      itemBuilder: (BuildContext ctx, index) {
                        return list[index];
                      },
                    )),
                  ],
                );
              } else if (state is HistoryPemakaianOliMesinError) {
                return ErrorLayout(message: state.message);
              }
              return const Center(child: LoadingWidget());
            },
          ),
        ),
        floatingActionButton:
            BlocBuilder<PemakaianOliMesinBloc, PemakaianOliMesinState>(
          builder: (context, state) {
            if (isUserPermitedToApprove) {
              if (state is UpdateStatusPemakaianOliMesinLoading) {
                return FloatingActionButton(
                  elevation: elevationFAB,
                  backgroundColor: secondary,
                  onPressed: () {},
                  child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                itemImage4,
                              ),
                              fit: BoxFit.fill)),
                      child: const Center(
                          child: LoadingWidget(
                        color: white,
                      ))),
                );
              }

              return FloatingActionButton(
                elevation: 12,
                backgroundColor: primary,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: ((dialogContext) => BlocProvider.value(
                            value:
                                BlocProvider.of<PemakaianOliMesinBloc>(context),
                            child: CustomAlertDialog(
                                description: dataCannotChangeAgain,
                                title:
                                    locator<UserRole>().getTitle(approveStatus),
                                voidCallback: () {
                                  BlocProvider.of<PemakaianOliMesinBloc>(
                                          context)
                                      .add(UpdateStatusPemakaianOliMesinEvent(
                                          widget.kapalId,
                                          locator<MyDate>()
                                              .getDateWithInputInString(date),
                                          approveStatus));
                                }),
                          )));
                },
                child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              itemImage4,
                            ),
                            fit: BoxFit.fill)),
                    child: Center(
                        child: locator<UserRole>().getIcon(approveStatus))),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}
