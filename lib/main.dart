import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:pms_pcm/core/data/repository/master_barang_dan_jasa_repository.dart';
import 'package:pms_pcm/core/data/repository/user_repository.dart';
import 'package:pms_pcm/core/service/image/camera.dart';

import 'core/bloc/master_barang_dan_jasa_bloc.dart';
import 'core/bloc/master_barang_oli_bloc.dart';
import 'core/data/repository/master_barang_oli_repository.dart';
import 'core/theme/theme.dart';
import 'core/util/const.dart';
import 'core/util/environment.dart';
import 'core/util/locator.dart';
import 'core/util/route.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  initEnvirontment();
  await initServiceLocator();
  await initCamera();
  await initializeDateFormatting('id_ID', null);
  runApp(const Main());
}

class Main extends StatelessWidget {
  const Main({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ///Set color status bar
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light.copyWith(statusBarColor: transparent),
    );

    /// To set orientation always portrait
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    final router = AppRouter();

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => MasterBarangDanJasaBloc(
              locator<MasterBarangDanJasaRepository>(),
              locator<UserRepository>()),
        ),
        BlocProvider(
          create: (context) => MasterBarangOliBloc(
            locator<MasterBarangOliRepository>(),
            locator<UserRepository>(),
          ),
        ),
      ],
      child: MaterialApp(
        title: appName,
        onGenerateRoute: router.routes,
        navigatorKey: locator<NavigationService>().navigatorKey,
      ),
    );
  }
}
