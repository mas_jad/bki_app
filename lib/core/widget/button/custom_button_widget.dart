import 'package:flutter/material.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/other/loading_widget.dart';

import '../../theme/theme.dart';

class CustomButtonWidget extends StatelessWidget {
  const CustomButtonWidget(
      {required this.callback,
      this.title,
      this.isActive = true,
      this.color,
      this.child,
      super.key});
  final String? title;
  final VoidCallback callback;
  final Color? color;
  final bool isActive;
  final Widget? child;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 60,
      child: isActive
          ? ElevatedButton(
              style: ButtonStyle(
                  elevation: MaterialStateProperty.all<double>(1),
                  backgroundColor: MaterialStateProperty.all<Color>(
                      color ?? secondaryAccent),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      const RoundedRectangleBorder(
                    borderRadius: cardBorderRadius,
                  ))),
              onPressed: () {
                if (isActive) {
                  callback();
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  child ?? const SizedBox(),
                  title != null
                      ? Text(
                          title ?? empty,
                          style: ptSansTextStyle.copyWith(
                              color: white, fontWeight: semiBold, fontSize: 16),
                        )
                      : const SizedBox(),
                ],
              ))
          : const LoadingWidget(),
    );
  }
}

class CustomButtonWidgetBorderSide extends StatelessWidget {
  const CustomButtonWidgetBorderSide(
      {required this.callback,
      this.title,
      this.child,
      this.isActive = true,
      this.color,
      super.key});
  final String? title;
  final VoidCallback callback;
  final Color? color;
  final Widget? child;
  final bool isActive;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 60,
      child: isActive
          ? ElevatedButton(
              style: ButtonStyle(
                  elevation: MaterialStateProperty.all<double>(1),
                  backgroundColor: MaterialStateProperty.all<Color>(white),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: cardBorderRadius,
                          side: BorderSide(color: color ?? secondaryAccent)))),
              onPressed: () {
                if (isActive) {
                  callback();
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  child ?? const SizedBox(),
                  title != null
                      ? Text(
                          title ?? empty,
                          style: ptSansTextStyle.copyWith(
                              color: color ?? secondaryAccent,
                              fontWeight: semiBold,
                              fontSize: 16),
                        )
                      : const SizedBox(),
                ],
              ))
          : const LoadingWidget(),
    );
  }
}
