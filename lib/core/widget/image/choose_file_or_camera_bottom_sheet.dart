import 'package:flutter/material.dart';

import '../../theme/theme.dart';

class ChooseFileOrCameraBottomSheet extends StatelessWidget {
  const ChooseFileOrCameraBottomSheet({required this.isToCamera, Key? key})
      : super(key: key);
  final ValueNotifier<bool?> isToCamera;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      padding: const EdgeInsets.all(20),
      decoration: const BoxDecoration(
        color: semiWhite,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsets.all(20),
              decoration: const BoxDecoration(
                color: white,
                boxShadow: [shadowCard],
                borderRadius: BorderRadius.all(Radius.circular(12)),
              ),
              child: Material(
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                color: transparent,
                child: InkWell(
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                  onTap: () {
                    isToCamera.value = false;
                    Navigator.pop(context);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      iconFile.copyWith(size: 40, color: secondaryAccent),
                      const SizedBox(
                        height: 16,
                      ),
                      Text(
                        'File',
                        style: ptSansTextStyle.copyWith(
                          fontSize: 14,
                          color: black54,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.all(20),
              decoration: const BoxDecoration(
                color: white,
                boxShadow: [shadowCard],
                borderRadius: BorderRadius.all(Radius.circular(12)),
              ),
              child: Material(
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                color: transparent,
                child: InkWell(
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                  onTap: () {
                    isToCamera.value = true;
                    Navigator.pop(context);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      iconCamera2.copyWith(size: 42, color: secondaryAccent),
                      const SizedBox(
                        height: 16,
                      ),
                      Text(
                        'Camera',
                        style: ptSansTextStyle.copyWith(
                          fontSize: 14,
                          color: black54,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
