import 'dart:io';

import 'package:flutter/material.dart';

import '../../service/image/image_service.dart';
import '../../theme/theme.dart';
import '../../util/const.dart';
import '../../util/locator.dart';
import '../other/custom_image_network.dart';

class ImageContainerPicker extends StatelessWidget {
  const ImageContainerPicker(
      {required this.imageValueNotifier,
      this.ratioToWidth = 2,
      required this.image,
      this.isEnable = true,
      super.key});
  final ValueNotifier<String?> imageValueNotifier;
  final String? image;
  final double ratioToWidth;
  final bool isEnable;
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return ValueListenableBuilder(
        valueListenable: imageValueNotifier,
        builder: (context, value, child) {
          return (imageValueNotifier.value != null)
              ? SizedBox(
                  height: width / ratioToWidth,
                  width: width / ratioToWidth,
                  child: Stack(
                    children: [
                      ClipRRect(
                          borderRadius: cardBorderRadius,
                          child: Image.file(
                            File(imageValueNotifier.value!),
                            height: width / ratioToWidth,
                            width: width / ratioToWidth,
                            fit: BoxFit.cover,
                          )),
                      if (isEnable)
                        Align(
                          alignment: Alignment.bottomRight,
                          child: GestureDetector(
                            onTap: () async {
                              final image = await locator<ImageService>()
                                  .getPictures(context);
                              if (image != null) {
                                imageValueNotifier.value = image;
                              }
                            },
                            child: Container(
                              margin: const EdgeInsets.all(10),
                              padding: const EdgeInsets.all(10),
                              decoration: const BoxDecoration(
                                  color: primary,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12)),
                                  boxShadow: [shadowCard]),
                              child: iconPhoto.copyWith(color: white, size: 16),
                            ),
                          ),
                        ),
                    ],
                  ),
                )
              : (image != null && (image?.isNotEmpty ?? false))
                  ? SizedBox(
                      height: width / ratioToWidth,
                      width: width / ratioToWidth,
                      child: Stack(
                        children: [
                          CustomImageNetwork(
                              height: width / ratioToWidth,
                              width: width / ratioToWidth,
                              borderRadius: cardBorderRadius,
                              imageUrl: image),
                          if (isEnable)
                            Align(
                              alignment: Alignment.bottomRight,
                              child: GestureDetector(
                                onTap: () async {
                                  final image = await locator<ImageService>()
                                      .getPictures(context);
                                  if (image != null) {
                                    imageValueNotifier.value = image;
                                  }
                                },
                                child: Container(
                                  margin: const EdgeInsets.all(10),
                                  padding: const EdgeInsets.all(10),
                                  decoration: const BoxDecoration(
                                      color: primary,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(12)),
                                      boxShadow: [shadowCard]),
                                  child: iconPhoto.copyWith(
                                      color: white, size: 16),
                                ),
                              ),
                            ),
                        ],
                      ),
                    )
                  : isEnable
                      ? GestureDetector(
                          onTap: () async {
                            if (isEnable) {
                              final image = await locator<ImageService>()
                                  .getPictures(context);
                              if (image != null) {
                                imageValueNotifier.value = image;
                              }
                            }
                          },
                          child: Container(
                            height: width / ratioToWidth,
                            width: width / ratioToWidth,
                            decoration: const BoxDecoration(
                              color: grey,
                              borderRadius: cardBorderRadius,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                iconPicture.copyWith(
                                    size: width / (ratioToWidth * 3),
                                    color: black54),
                                const SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  inputImage,
                                  style: ptSansTextStyle.copyWith(
                                      color: black54,
                                      fontWeight: light,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                        )
                      : const SizedBox();
        });
  }
}
