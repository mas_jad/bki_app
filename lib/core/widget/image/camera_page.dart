import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

import '../../service/image/camera.dart';
import '../../theme/theme.dart';
import '../../util/loading.dart';
import '../Other/pop_widget.dart';

class CameraPage extends StatefulWidget {
  const CameraPage({required this.valueNotifier, Key? key}) : super(key: key);
  final ValueNotifier<String?> valueNotifier;
  @override
  State<CameraPage> createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  late CameraController controller;

  @override
  void initState() {
    super.initState();
    controller = CameraController(
      cameras[0],
      ResolutionPreset.max,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );

    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      // ignore: no-empty-block
      setState(() {});
    }).catchError((Object e) {
      if (e is CameraException) {
        switch (e.code) {
          case 'CameraAccessDenied':
            debugPrint('User denied camera access.');
            break;
          default:
            debugPrint('Handle other errors.');
            break;
        }
        Navigator.pop(context);
      }
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void takePictures(XFile file) {
    widget.valueNotifier.value = file.path;
    LoadingOverlay.dismiss();
    Navigator.pop(context);
  }

  bool isAlreadyTakePictures = false;

  @override
  Widget build(BuildContext context) {
    if (!controller.value.isInitialized) {
      return Container();
    }
    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: secondary,
        iconTheme: const IconThemeData(color: white),
        title: Text(
          'Camera',
          style: ptSansTextStyle.copyWith(color: white),
        ),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height - 60,
        width: MediaQuery.of(context).size.width,
        child: CameraPreview(
          controller,
          child: Column(
            children: [
              const Spacer(),
              StatefulBuilder(
                builder: (context, setState) {
                  if (isAlreadyTakePictures) {
                    return const SizedBox();
                  }
                  return PopWidget(
                    // ignore: prefer-extracting-callbacks
                    onTap: () async {
                      if (!isAlreadyTakePictures) {
                        setState(
                          () {
                            isAlreadyTakePictures = true;
                          },
                        );
                        LoadingOverlay.show(context);
                        controller.setFlashMode(FlashMode.off);
                        final file = await controller.takePicture();
                        takePictures(file);
                      }
                    },
                    child: Container(
                      height: 80,
                      width: 80,
                      decoration: const BoxDecoration(
                        color: primary,
                        shape: BoxShape.circle,
                      ),
                      child: iconCamera2.copyWith(
                        size: 40,
                        color: white,
                      ),
                    ),
                  );
                },
              ),
              const SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
