import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/icon.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';

class CustomTextFieldWidget extends StatefulWidget {
  const CustomTextFieldWidget(
      {this.textEditingController,
      this.textInputType,
      this.prefixIcon,
      this.error,
      this.hint,
      this.onSubmitted,
      this.enableInteractiveSelection,
      this.label,
      this.textAlign,
      this.isEnable,
      this.style,
      this.suffix,
      this.maxlines,
      this.suffixWidget,
      this.focusNode,
      this.onlyNumber = false,
      this.onTap,
      super.key});
  final TextEditingController? textEditingController;
  final TextInputType? textInputType;
  final Icon? prefixIcon;
  final String? suffix;
  final Widget? suffixWidget;
  final String? hint;
  final String? label;
  final bool? isEnable;
  final String? error;
  final Function(String value)? onSubmitted;
  final TextAlign? textAlign;
  final Function? onTap;
  final FocusNode? focusNode;
  final bool? enableInteractiveSelection;
  final TextStyle? style;
  final int? maxlines;
  final bool onlyNumber;

  @override
  State<CustomTextFieldWidget> createState() => _CustomTextFieldWidgetState();
}

class _CustomTextFieldWidgetState extends State<CustomTextFieldWidget> {
  bool isObscure = false;
  @override
  void initState() {
    super.initState();
    if (widget.textInputType == TextInputType.visiblePassword) {
      isObscure = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.label != null)
          Column(
            children: [
              Text(
                widget.label ?? empty,
                style: ptSansTextStyle.copyWith(
                    fontWeight: light, color: primary, fontSize: 14),
              ),
              const SizedBox(
                height: 12,
              ),
            ],
          ),
        TextField(
            onTap: () {
              if (widget.onTap != null) {
                widget.onTap!();
                FocusScope.of(context).requestFocus(FocusNode());
              }
            },
            focusNode: widget.focusNode,
            enableInteractiveSelection: widget.enableInteractiveSelection,
            controller: widget.textEditingController,
            enabled: widget.isEnable ?? true,
            maxLines: widget.maxlines ?? 1,
            onSubmitted: widget.onSubmitted,
            keyboardType: widget.textInputType,
            inputFormatters: widget.onlyNumber
                ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
                : null,
            textAlign: widget.textAlign ?? TextAlign.start,
            style: widget.style ??
                ptSansTextStyle.copyWith(
                    color: black68, fontSize: 18, fontWeight: medium),
            obscureText: isObscure,
            enableSuggestions:
                widget.textInputType != TextInputType.visiblePassword,
            autocorrect: widget.textInputType != TextInputType.visiblePassword,
            decoration: InputDecoration(
                suffixText: widget.suffix,
                contentPadding: const EdgeInsets.all(20.0),
                errorText: widget.error,
                isCollapsed: true,
                prefixIcon: widget.prefixIcon,
                suffix: widget.suffixWidget ??
                    (widget.textInputType == TextInputType.visiblePassword
                        ? Transform.translate(
                            offset: const Offset(0, 4),
                            child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isObscure = !isObscure;
                                  });
                                },
                                child: isObscure
                                    ? iconVisibility.copyWith(color: black26)
                                    : iconVisibility2.copyWith(color: black26)),
                          )
                        : null),
                hintText: widget.hint,
                hintStyle: ptSansTextStyle.copyWith(
                    color: black26, fontSize: 18, fontWeight: medium),
                border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  borderSide: BorderSide(color: secondary, width: 5.0),
                ))),
      ],
    );
  }
}
