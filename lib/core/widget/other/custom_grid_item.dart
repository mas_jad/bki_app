import 'package:flutter/material.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/shadow.dart';
import 'package:pms_pcm/core/theme/size.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';
import 'package:pms_pcm/core/widget/other/pop_widget.dart';

class CustomGridItem extends StatelessWidget {
  const CustomGridItem(
      {required this.callback,
      required this.title,
      this.icon,
      this.subtitle,
      super.key});
  final Function(BuildContext context) callback;
  final String title;
  final String? subtitle;
  final Icon? icon;

  @override
  Widget build(BuildContext context) {
    return PopWidget(
      onTap: () {
        callback(context);
      },
      child: Container(
        padding: const EdgeInsets.all(20),
        height: 200,
        decoration: const BoxDecoration(
            boxShadow: [shadowCard],
            image: DecorationImage(
                image: AssetImage(itemImage1), fit: BoxFit.cover),
            borderRadius: cardBorderRadius,
            color: white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              title,
              style: ptSansTextStyle.copyWith(color: white, fontWeight: bold),
            ),
            if (subtitle != null)
              Column(
                children: [
                  const SizedBox(
                    height: 6,
                  ),
                  Text(
                    subtitle ?? empty,
                    style: ptSansTextStyle.copyWith(color: white, fontSize: 12),
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
