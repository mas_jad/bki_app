import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../theme/theme.dart';

class ShimmerWidget extends StatelessWidget {
  const ShimmerWidget({
    this.height = 0,
    this.width = 0,
    this.borderRadius,
    this.margin,
    Key? key,
  }) : super(key: key);
  final double height, width;
  final BorderRadius? borderRadius;
  final EdgeInsets? margin;
  @override
  Widget build(BuildContext context) {
    return Shimmer(
      gradient: shimmerGradient,
      child: Container(
        height: height,
        width: width,
        margin: margin,
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          color: Colors.blue,
        ),
      ),
    );
  }
}

// ignore: prefer-single-widget-per-file
class DetailShimmer extends StatelessWidget {
  const DetailShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer(
      gradient: shimmerGradient,
      child: Container(
        color: Colors.blue,
        height: 500,
        width: MediaQuery.of(context).size.width,
      ),
    );
  }
}

// ignore: prefer-single-widget-per-file
class ShimmerListItemWidget extends StatelessWidget {
  const ShimmerListItemWidget({
    this.height = 0,
    this.width = 0,
    this.borderRadius,
    Key? key,
  }) : super(key: key);
  final double height, width;
  final BorderRadius? borderRadius;
  @override
  Widget build(BuildContext context) {
    return Shimmer(
      gradient: shimmerGradient,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          color: Colors.blue,
        ),
      ),
    );
  }
}
