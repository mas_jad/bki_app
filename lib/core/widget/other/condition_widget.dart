// import 'package:flutter/material.dart';

// import 'pop_widget.dart';

// class ConditionWidget extends StatelessWidget {
//   const ConditionWidget({
//     this.margin,
//     this.size = 220,
//     this.errorType = UIErrorType.error,
//     this.callback,
//     Key? key,
//   }) : super(key: key);
//   final EdgeInsets? margin;
//   final double size;
//   final UIErrorType errorType;
//   final VoidCallback? callback;
//   @override
//   Widget build(BuildContext context) {
//     var asset = empty;
//     var title = empty;
//     switch (errorType) {
//       case UIErrorType.searchEmpty:
//         asset = 'assets/image/undraw/search_not_found.png';
//         title = Label.itemYouAreLookingForNotFound.get();
//         break;
//       case UIErrorType.empty:
//         asset = 'assets/image/undraw/no_data.png';
//         title = Label.nodatayet.get();
//         break;
//       case UIErrorType.internetError:
//         asset = 'assets/image/undraw/internet_error.png';
//         title = Label.checkYourConnection.get();
//         break;

//       case UIErrorType.serverError:
//         asset = 'assets/image/undraw/server_error.png';
//         title = Label.serverError.get();
//         break;
//       case UIErrorType.error:
//         asset = 'assets/image/undraw/error.png';
//         title = Label.thereIsAnError.get();
//         break;
//       case UIErrorType.inDevelopment:
//         asset = 'assets/image/undraw/in_development.png';
//         title = Label.thisPageUnderDevelopment.get();
//         break;
//       case UIErrorType.notLogin:
//         asset = 'assets/image/undraw/login.png';
//         title = Label.comeonLoginInordertobeabletoopenthisfeature.get();
//         break;
//     }
//     return Center(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Padding(
//             padding: margin ?? const EdgeInsets.all(0),
//             child: Column(
//               children: [
//                 Container(
//                   height: size,
//                   width: size,
//                   decoration: BoxDecoration(
//                     image: DecorationImage(image: AssetImage(asset)),
//                   ),
//                 ),
//                 const SizedBox(
//                   height: 20,
//                 ),
//                 Text(
//                   title,
//                   style: sofiaTextStyle.copyWith(
//                     fontSize: 15,
//                     color: black54,
//                     fontWeight: light,
//                   ),
//                   textAlign: TextAlign.center,
//                 ),
//                 if (errorType == UIErrorType.internetError || errorType == UIErrorType.error)
//                   const SizedBox(
//                     height: 20,
//                   ),
//                 if (errorType == UIErrorType.internetError || errorType == UIErrorType.error)
//                   PopWidget2(
//                     borderRadius: 100,
//                     onTap: restart,
//                     child: Container(
//                       height: 50,
//                       width: 50,
//                       decoration: const BoxDecoration(
//                         color: accent1,
//                         borderRadius: BorderRadius.all(Radius.circular(100)),
//                       ),
//                       child: iconRefresh.copyWith(color: white, size: 25),
//                     ),
//                   ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   void restart() async {
//     if (callback != null) {
//       final internet = await locator<NetworkInfoImpl>().isConnected;
//       if (internet) {
//         callback!();
//       }
//     }
//   }
// }

// // ignore: prefer-single-widget-per-file
// class CompanyEmptyWidget extends StatelessWidget {
//   const CompanyEmptyWidget({this.margin, Key? key}) : super(key: key);
//   final EdgeInsets? margin;
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Padding(
//         padding: margin ??
//             EdgeInsets.only(top: MediaQuery.of(context).size.height / 10),
//         child: Column(
//           children: [
//             Container(
//               height: 280,
//               width: 280,
//               decoration: const BoxDecoration(
//                 image: DecorationImage(
//                   image: AssetImage(
//                     'assets/image/undraw/container_ship.png',
//                   ),
//                 ),
//               ),
//             ),
//             const SizedBox(
//               height: 4,
//             ),
//             Text(
//               Label.addYourCompanyInThisPort.get(),
//               style: sofiaTextStyle.copyWith(
//                 fontSize: 15,
//                 color: black54,
//                 fontWeight: light,
//               ),
//               textAlign: TextAlign.center,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// // ignore: prefer-single-widget-per-file
// class FacilityEmptyWidget extends StatelessWidget {
//   const FacilityEmptyWidget({this.margin, Key? key}) : super(key: key);
//   final EdgeInsets? margin;
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Padding(
//         padding: const EdgeInsets.only(top: 40, bottom: 60),
//         child: Column(
//           children: [
//             Container(
//               height: 160,
//               width: 160,
//               decoration: const BoxDecoration(
//                 image: DecorationImage(
//                   image: AssetImage('assets/image/undraw/building.png'),
//                 ),
//               ),
//             ),
//             const SizedBox(
//               height: 24,
//             ),
//             Text(
//               Label.sorrythisfacilitydatadoesnexistyet.get(),
//               style: sofiaTextStyle.copyWith(
//                 fontSize: 15,
//                 color: black54,
//                 fontWeight: light,
//               ),
//               textAlign: TextAlign.center,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
