import 'package:flutter/material.dart';

class GradientWidget extends StatelessWidget {
  const GradientWidget(
    this.child, {
    Key? key,
    required this.gradient,
    this.isActive = true,
  }) : super(key: key);

  final Widget child;
  final Gradient gradient;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return isActive
        ? ShaderMask(
            blendMode: BlendMode.srcIn,
            shaderCallback: (bounds) => gradient.createShader(
              Rect.fromLTWH(0, 0, bounds.width, bounds.height),
            ),
            child: child,
          )
        : child;
  }
}
