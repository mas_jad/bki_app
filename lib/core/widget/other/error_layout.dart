import 'package:flutter/material.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';

class ErrorLayout extends StatelessWidget {
  const ErrorLayout({required this.message, super.key});
  final String message;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/image/error.png",
            height: 220,
            width: 220,
          ),
          const SizedBox(
            height: 30,
          ),
          SizedBox(
            width: 220,
            child: Text(
              message,
              style: ptSansTextStyle.copyWith(
                  color: lightText, fontWeight: reguler, fontSize: 16),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
