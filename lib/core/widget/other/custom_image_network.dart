import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../theme/theme.dart';
import '../../util/const.dart';

class CustomImageNetwork extends StatelessWidget {
  const CustomImageNetwork({
    required this.imageUrl,
    this.height = 0,
    this.width = 0,
    this.child,
    this.borderRadius,
    this.color,
    this.errorImage = const AssetImage('assets/image/undraw/image_error.png'),
    this.boxFit,
    this.fadeInDuration,
    this.fadeOutDuration,
    this.shadow,
    Key? key,
  }) : super(key: key);
  final String? imageUrl;
  final Color? color;
  final double width, height;
  final BorderRadius? borderRadius;
  final Widget? child;
  final AssetImage? errorImage;
  final BoxFit? boxFit;
  final Duration? fadeInDuration;
  final Duration? fadeOutDuration;
  final BoxShadow? shadow;

  @override
  Widget build(BuildContext context) {
    if (imageUrl == empty) {
      return const SizedBox();
    }
    // return const SizedBox();
    return CachedNetworkImage(
      height: height,
      width: width,
      fadeInDuration: fadeInDuration ?? const Duration(milliseconds: 250),
      fadeOutDuration: fadeOutDuration ?? const Duration(milliseconds: 250),
      imageUrl: imageUrl ?? empty,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          boxShadow: shadow != null ? [shadow!] : [],
          borderRadius: borderRadius,
          image: DecorationImage(
            image: imageProvider,
            fit: boxFit ?? BoxFit.cover,
            colorFilter: color == null
                ? null
                : ColorFilter.mode(color ?? black12, BlendMode.darken),
          ),
        ),
        child: child,
      ),
      placeholder: (context, url) => Container(
        decoration: BoxDecoration(color: grey200, borderRadius: borderRadius),
        height: height,
        width: width,
      ),
      // ShimmerWidget(
      //   height: height,
      //   width: width,
      //   borderRadius: borderRadius,
      // ),
      errorWidget: (context, url, error) =>
          // errorImage == null
          //     ?
          Container(
        decoration: BoxDecoration(color: grey200, borderRadius: borderRadius),
        height: height,
        width: width,
      ),

      // Container(
      //     decoration:
      //         BoxDecoration(color: black12, borderRadius: borderRadius),
      //     child: Center(child: iconImageError.copyWith(color: accent1)),
      //   )
      // : Container(
      //     height: height,
      //     width: width,
      //     decoration: BoxDecoration(
      //       borderRadius: borderRadius,
      //       image: DecorationImage(
      //         image: errorImage!,
      //         fit: BoxFit.contain,
      //       ),
      //     ),
      //   ),
    );
  }
}
