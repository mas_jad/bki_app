import 'package:flutter/material.dart';

class PopWidget extends StatefulWidget {
  const PopWidget({
    required this.onTap,
    required this.child,
    this.scale = 0.02,
    this.duration,
    Key? key,
  }) : super(key: key);
  final VoidCallback onTap;
  final Widget child;
  final Duration? duration;
  final double scale;

  @override
  State<PopWidget> createState() => _PopWidgetState3();
}

class _PopWidgetState3 extends State<PopWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: widget.duration ?? const Duration(milliseconds: 100),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (value) {
        _controller.forward();
      },
      onPanStart: (value) {
        _controller.forward();
      },
      onPanCancel: () {
        _controller.reverse();
      },
      onPanEnd: (value) {
        _controller.reverse();
      },
      onLongPressEnd: (details) {
        _controller.reverse();
      },
      onTap: () async {
        await _controller.forward();
        await _controller.reverse();
        widget.onTap();
      },
      child: AnimatedBuilder(
        animation: _controller,
        builder: (context, state) => Transform.scale(
          scale: 1 - _controller.value * (widget.scale),
          child: widget.child,
        ),
      ),
    );
  }
}
