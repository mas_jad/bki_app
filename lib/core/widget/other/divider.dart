import 'package:flutter/material.dart';

import '../../theme/theme.dart';

class CustomDivider extends StatelessWidget {
  const CustomDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Divider(
      thickness: 12,
      color: veryLightGrey,
    );
  }
}
