import 'package:flutter/material.dart';
import 'package:pms_pcm/core/widget/button/custom_button_widget.dart';

import '../../theme/theme.dart';

class CustomAlertDialog extends StatelessWidget {
  const CustomAlertDialog(
      {super.key,
      required this.description,
      required this.title,
      required this.voidCallback});
  final String title;
  final String description;
  final VoidCallback voidCallback;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actionsPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      shape: const RoundedRectangleBorder(borderRadius: cardBorderRadius),
      title: Text(title, style: ptSansTextStyle.copyWith(color: secondary)),
      content: Text(
        description,
        style: ptSansTextStyle.copyWith(color: black54),
      ),
      actions: <Widget>[
        SizedBox(
          height: 46,
          child: Row(
            children: [
              const Expanded(
                child: SizedBox(),
              ),
              Expanded(
                child: CustomButtonWidgetBorderSide(
                    callback: () {
                      Navigator.pop(context, 'Cancel');
                    },
                    title: 'No'),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: CustomButtonWidget(
                    callback: () {
                      voidCallback();
                      Navigator.pop(context, 'OK');
                    },
                    title: 'Yes'),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
