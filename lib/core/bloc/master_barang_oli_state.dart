part of 'master_barang_oli_bloc.dart';

abstract class MasterBarangOliState extends Equatable {
  const MasterBarangOliState();

  @override
  List<Object> get props => [];
}

class MasterBarangOliInitial extends MasterBarangOliState {}

class MasterBarangOliSuccess extends MasterBarangOliState {
  final List<AlertRequestOli> listMasterOli;
  const MasterBarangOliSuccess(this.listMasterOli);
}

class MasterBarangOliError extends MasterBarangOliState {
  final String message;
  const MasterBarangOliError(this.message);
}

class MasterBarangOliLoading extends MasterBarangOliState {}
