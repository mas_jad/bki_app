part of 'master_barang_dan_jasa_bloc.dart';

abstract class MasterBarangDanJasaEvent extends Equatable {
  const MasterBarangDanJasaEvent();

  @override
  List<Object> get props => [];
}

class GetMasterBarangDanJasaEvent extends MasterBarangDanJasaEvent {
  final int kapalId;
  const GetMasterBarangDanJasaEvent(this.kapalId);
}
