import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/master_barang_dan_jasa.dart';
import 'package:pms_pcm/core/data/repository/master_barang_dan_jasa_repository.dart';
import 'package:pms_pcm/core/data/repository/user_repository.dart';

import '../../../core/util/const.dart';

part 'master_barang_dan_jasa_event.dart';
part 'master_barang_dan_jasa_state.dart';

class MasterBarangDanJasaBloc
    extends Bloc<MasterBarangDanJasaEvent, MasterBarangDanJasaState> {
  final MasterBarangDanJasaRepository _masterBarangDanJasaRepository;
  final UserRepository _userRepository;
  MasterBarangDanJasaBloc(
      this._masterBarangDanJasaRepository, this._userRepository)
      : super(MasterBarangDanJasaInitial()) {
    on<GetMasterBarangDanJasaEvent>((event, emit) async {
      try {
        emit(MasterBarangDanJasaLoading());
        final token = await _userRepository.getToken();
        final data =
            await _masterBarangDanJasaRepository.getMasterBarangDanJasa(
          event.kapalId,
          token,
        );
        emit(MasterBarangDanJasaSuccess(data));
      } catch (e) {
        await Future.delayed(const Duration(seconds: 3));
        add(GetMasterBarangDanJasaEvent(event.kapalId));
        emit(const MasterBarangDanJasaError(somethingErrorHappen));
      }
    });
  }
}
