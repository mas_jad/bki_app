part of 'master_barang_dan_jasa_bloc.dart';

abstract class MasterBarangDanJasaState extends Equatable {
  const MasterBarangDanJasaState();

  @override
  List<Object> get props => [];
}

class MasterBarangDanJasaInitial extends MasterBarangDanJasaState {}

class MasterBarangDanJasaSuccess extends MasterBarangDanJasaState {
  final MasterBarangDanJasa masterBarangDanJasa;
  const MasterBarangDanJasaSuccess(this.masterBarangDanJasa);
}

class MasterBarangDanJasaError extends MasterBarangDanJasaState {
  final String message;
  const MasterBarangDanJasaError(this.message);
}

class MasterBarangDanJasaLoading extends MasterBarangDanJasaState {}
