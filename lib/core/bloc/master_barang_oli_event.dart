part of 'master_barang_oli_bloc.dart';

abstract class MasterBarangOliEvent extends Equatable {
  const MasterBarangOliEvent();

  @override
  List<Object> get props => [];
}

class GetMasterBarangOliEvent extends MasterBarangOliEvent {
  final int kapalId;
  const GetMasterBarangOliEvent(this.kapalId);
}
