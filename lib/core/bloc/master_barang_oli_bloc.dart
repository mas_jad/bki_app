import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/util/const.dart';

import '../data/model/alert_request_oli.dart';
import '../data/repository/master_barang_oli_repository.dart';
import '../data/repository/user_repository.dart';

part 'master_barang_oli_event.dart';
part 'master_barang_oli_state.dart';

class MasterBarangOliBloc
    extends Bloc<MasterBarangOliEvent, MasterBarangOliState> {
  final MasterBarangOliRepository _masterBarangOliRepository;
  final UserRepository _userRepository;
  MasterBarangOliBloc(this._masterBarangOliRepository, this._userRepository)
      : super(MasterBarangOliInitial()) {
    on<GetMasterBarangOliEvent>((event, emit) async {
      try {
        emit(MasterBarangOliLoading());
        final token = await _userRepository.getToken();
        final data = await _masterBarangOliRepository.getMasterBarangOli(
          event.kapalId,
          token,
        );
        emit(MasterBarangOliSuccess(data));
      } catch (e) {
        await Future.delayed(const Duration(seconds: 3));
        add(GetMasterBarangOliEvent(event.kapalId));
        emit(const MasterBarangOliError(somethingErrorHappen));
      }
    });
  }
}
