import 'package:flutter/material.dart';
import 'package:pms_pcm/core/util/const.dart';

// BLACK
const Color nativeGrey = Colors.grey;
const Color shadowGrey = Color(0x24656565);
const Color shadowGrey2 = Color.fromARGB(35, 153, 153, 153);
const Color black12 = Colors.black12;
const Color black26 = Colors.black26;
const Color black38 = Colors.black38;
const Color black54 = Colors.black54;
const Color black68 = Color.fromARGB(173, 0, 0, 0);
const Color black87 = Colors.black87;
const Color background = Color(0xFF0e1015);
const Color black = Color(0xFF000000);

// WHITE
const Color transparent = Color(0x00FFFFFF);
const Color veryLightGrey = Color.fromARGB(255, 244, 244, 244);
const Color lightGrey = Color.fromARGB(255, 240, 239, 239);
const Color semiWhite = Color(0xFFFAFAFA);
const Color grey = Color.fromARGB(255, 209, 209, 209);
const Color grey200 = Color.fromARGB(255, 238, 238, 238);
const Color white = Color(0xFFFFFFFF);
const Color whiteTransparent = Color(0x66FFFFFF);

// CUSTOM COLOR
const Color green = Color.fromARGB(255, 35, 163, 131);
const Color green2 = Colors.green;
const Color red = Color.fromARGB(255, 194, 36, 36);
const Color primary = Color(0xFF0F73AF);
const Color primaryAccent = Color(0xFF8EB3E7);
const Color secondary = Color(0xFF101D2D);
const Color secondaryAccent = Color(0xFF172433);
const Color secondaryTransparent = Color(0xEE101D2D);
const Color tertiary = Color(0xFF1D4ED8);
const Color heavyText = Color(0xFF030B16);
const Color lightText = Color(0xFF827C9D);
const Color quaternary = Color(0xFFF16829);
const Color quaternaryAccent = Color(0xFFF68B20);

Color statusColor(String status) {
  switch (status) {
    case APPROVED:
      return green;
    case DRAF:
      return lightText;
    case SEND:
      return primary;
    case ONTHEWAY:
      return quaternary;
    case ACCEPTED:
      return tertiary;
    default:
      return black;
  }
}
