import 'package:flutter/material.dart';

import 'color.dart';

const shadowCard =
    BoxShadow(color: shadowGrey2, blurRadius: 2, offset: Offset(0, 2));
const shadowCard2 = BoxShadow(
    color: shadowGrey2, blurRadius: 4, spreadRadius: -4, offset: Offset(0, 2));
const reverseShadowCard =
    BoxShadow(color: shadowGrey2, blurRadius: 2, offset: Offset(0, -2));
