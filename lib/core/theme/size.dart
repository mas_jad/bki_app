import 'package:flutter/material.dart';

const double defaultRadius = 20;
const double horizontalMargin = 40;
const double horizontalMargin2 = 20;
const double verticalMargin = 30;
const double sizeGridHeight = 200;
const double sizeGridWidth = 200;
const double heightButton = 90;
const double elevationFAB = 2;
const cardBorderRadius = BorderRadius.all(Radius.circular(20));
