import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

extension ModularIcon on Icon {
  Icon copyWith({double? size, Color? color, Key? key}) {
    return Icon(
      icon,
      size: size ?? this.size,
      color: color ?? this.color,
      key: key,
    );
  }
}

extension ModularFaIcon on FaIcon {
  FaIcon copyWith({double? size, Color? color}) {
    return FaIcon(
      icon,
      size: size ?? this.size,
      color: color ?? this.color,
    );
  }
}

const iconSearch = Icon(Icons.search);
const iconCamera2 = Icon(Icons.camera_alt);
const iconPicture = Icon(Icons.photo_library);
const iconArrow = Icon(Icons.arrow_back_rounded);
const iconDelete = Icon(Icons.delete);
const iconVisibility = Icon(Icons.visibility_rounded);
const iconVisibility2 = Icon(Icons.visibility_off_rounded);
const iconPhoto = Icon(Icons.edit);
const iconFile = Icon(Icons.file_open);
const iconMaterial = Icon(Icons.search_rounded);

const iconDate = Icon(Icons.date_range);

const iconAdd = Icon(Icons.add_rounded);
const iconSend = Icon(Icons.upload_rounded);
const iconApproval = Icon(Icons.approval_rounded);
