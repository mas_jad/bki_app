import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/bloc/sounding_bahan_bakar_bloc.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/view/sounding_bahan_bakar_page.dart';
import 'package:pms_pcm/feature/activity_3_pemakaian_oli_mesin/view/alert_oli_page.dart';
import 'package:pms_pcm/feature/activity_4_pemakaian_air_tawar/view/pemakaian_air_tawar_page.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/view/permintaan_barang_dan_jasa_page.dart';
import 'package:pms_pcm/feature/activity_8_kerusakan_dan_perbaikan_kapal/view/kerusakan_dan_perbaikan_kapal_page.dart';
import 'package:pms_pcm/feature/home/view/home_page.dart';
import 'package:pms_pcm/feature/ship_activity/bloc/alert_bahan_bakar_dan_air_tawar_bloc.dart';
import 'package:pms_pcm/feature/ship_activity/view/ship_activity_page.dart';
import 'package:pms_pcm/feature/splash/bloc/splash_bloc.dart';

import '../../feature/activity_1_sounding_bahan_bakar/data/repository/sounding_bahan_bakar_repository.dart';
import '../../feature/activity_2_pemakaian_bahan_bakar_dan_jam_kerja_mesin/bloc/pemakaian_bahan_bakar_dan_jam_kerja_mesin_bloc.dart';
import '../../feature/activity_2_pemakaian_bahan_bakar_dan_jam_kerja_mesin/data/repository/pemakaian_bahan_bakar_dan_jam_kerja_mesin_repository.dart';
import '../../feature/activity_2_pemakaian_bahan_bakar_dan_jam_kerja_mesin/view/pemakaian_bahan_bakar_dan_jam_kerja_mesin_page.dart';
import '../../feature/activity_3_pemakaian_oli_mesin/bloc/pemakaian_oli_mesin_bloc.dart';
import '../../feature/activity_3_pemakaian_oli_mesin/data/repository/pemakaian_oli_mesin_repository.dart';
import '../../feature/activity_3_pemakaian_oli_mesin/view/pemakaian_oli_mesin_page.dart';
import '../../feature/activity_4_pemakaian_air_tawar/bloc/pemakaian_air_tawar_bloc.dart';
import '../../feature/activity_4_pemakaian_air_tawar/data/repository/pemakaian_air_tawar_repository.dart';
import '../../feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/view/perawatan_harian_departemen_deck_dan_departemen_engine_page.dart';
import '../../feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/bloc/perawatan_terencana_departemen_deck_bloc.dart';
import '../../feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/repository/perawatan_terencana_repository.dart';
import '../../feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/view/perawatan_terencana_departemen_deck_dan_departemen_engine_page.dart';
import '../../feature/activity_7_permintaan_barang_dan_jasa/view/history_permintaan_barang_dan_jasa_page.dart';
import '../../feature/login/bloc/login_bloc.dart';
import '../../feature/login/view/login_page.dart';
import '../../feature/ship_activity/bloc/ship_activity_bloc.dart';
import '../../feature/ship_activity/data/repository/alert_bahan_bakar_dan_air_tawar_repository.dart';
import '../../feature/splash/view/splash_page.dart';
import '../data/model/data_kapal.dart';
import '../data/repository/user_repository.dart';
import '../service/location/location_service.dart';
import 'loading.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<void> navigateBack() async {
    LoadingOverlay.dismiss();
    navigatorKey.currentState?.pop();
  }

  Future<dynamic> navigateTo(String routeName) async {
    LoadingOverlay.dismiss();
    return navigatorKey.currentState?.pushNamed(routeName);
  }
}

class AppRouter {
  Route navigateToSplashPage() {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return BlocProvider(
          create: (context) => SplashBloc(locator<UserRepository>()),
          child: const SplashPage(),
        );
      },
    );
  }

  Route navigateToLoginPage() {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return BlocProvider(
          create: (context) => LoginBloc(locator<UserRepository>()),
          child: const LoginPage(),
        );
      },
    );
  }

  Route navigateToHomePage(List<DataKapal> list, Profile profile) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return HomePage(
          listDataKapal: list,
          profile: profile,
        );
      },
    );
  }

  Route navigateToShipActivityPage(DataKapal dataKapal, Profile profile) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        locator<LocationService>().getPosition();
        return MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ShipActivityBloc(locator<UserRepository>()),
            ),
            BlocProvider(
              create: (context) => AlertBahanBakarDanAirTawarBloc(
                  locator<AlertBahanBakarDanAirTawarRepository>(),
                  locator<UserRepository>()),
            ),
          ],
          child: ShipActivity(
            dataKapal: dataKapal,
            profile: profile,
          ),
        );
      },
    );
  }

  Route navigateToSoundingBahanBakarPage(int kapalId) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(
                create: (context) => SoundingBahanBakarBloc(
                    locator<SoundingBahanBakarRepository>(),
                    locator<UserRepository>())),
          ],
          child: SoundingBahanBakarPage(
            kapalId: kapalId,
          ),
        );
      },
    );
  }

  Route navigateToPemakaianBahanBakarDanJamKerjaMesinPage(
      int kapalId, int departemenId) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(
                create: (context) => PemakaianBahanBakarDanJamKerjaMesinBloc(
                    locator<PemakaianBahanBakarDanJamKerjaMesinRepository>(),
                    locator<UserRepository>())),
          ],
          child: PemakaianBahanBakarDanJamKerjaMesinPage(
            kapalId: kapalId,
            departemenId: departemenId,
          ),
        );
      },
    );
  }

  Route navigateToPemakaianOliMesinPage(int kapalId, int departemenId) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return MultiBlocProvider(
            providers: [
              BlocProvider(
                  create: (context) => PemakaianOliMesinBloc(
                      locator<PemakaianOliMesinRepository>(),
                      locator<UserRepository>())),
            ],
            child: PemakaianOliMesinPage(
              kapalId: kapalId,
              departemenId: departemenId,
            ));
      },
    );
  }

  Route navigateToPemakaianAirTawarPage(int kapalId, int departemenId) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return MultiBlocProvider(
            providers: [
              BlocProvider(
                  create: (context) => PemakaianAirTawarBloc(
                      locator<PemakaianAirTawarRepository>(),
                      locator<UserRepository>())),
            ],
            child: PemakaianAirTawarPage(
              kapalId: kapalId,
              departemenId: departemenId,
            ));
      },
    );
  }

  Route navigateToPerawatanHarianDepartemenDeckDanDepartemenEnginePage(
      DataKapal dataKapal) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return PerawatanHarianDepartemenDeckDanDepartemenEnginePage(
          dataKapal: dataKapal,
        );
      },
    );
  }

  Route navigateToPerawatanTerencanaDepartemenDeckDanDepartemenEnginePage(
      DataKapal dataKapal) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(
                create: (context) => PerawatanTerencanaBloc(
                    locator<PerawatanTerencanaRepository>(),
                    locator<UserRepository>())),
          ],
          child: PerawatanTerencanaDepartemenDeckDanDepartemenEnginePage(
            dataKapal: dataKapal,
          ),
        );
      },
    );
  }

  Route navigateToPermintaanBarangDanJasaPage(DataKapal dataKapal) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return PermintaanBarangDanJasaPage(
          dataKapal: dataKapal,
        );
      },
    );
  }

  Route navigateToHistoryPermintaanBarangDanJasaPage(DataKapal dataKapal) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return HistoryPermintaanBarangDanJasaPage(
          dataKapal: dataKapal,
        );
      },
    );
  }

  Route navigateToKerusakanDanPerbaikanKapalPage(DataKapal dataKapal) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return KerusakanDanPerbaikanKapalPage(
          dataKapal: dataKapal,
        );
      },
    );
  }

  Route navigateToAlertOliPage(int kapalId, int departemenId) {
    return MaterialPageRoute(
      builder: (BuildContext context) {
        return AlertOliPage(
          kapalId: kapalId,
          departemenId: departemenId,
        );
      },
    );
  }

  Route? routes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return navigateToSplashPage();
      case LoginPage.route:
        return navigateToLoginPage();
      case HomePage.route:
        final list = settings.arguments as List;
        final listKapal = list[0] as List<DataKapal>;
        final profile = list[1] as Profile;
        return navigateToHomePage(listKapal, profile);
      case ShipActivity.route:
        final list = settings.arguments as List;
        final kapal = list[0] as DataKapal;
        final profile = list[1] as Profile;
        return navigateToShipActivityPage(kapal, profile);
      case SoundingBahanBakarPage.route:
        return navigateToSoundingBahanBakarPage(settings.arguments as int);
      case PemakaianBahanBakarDanJamKerjaMesinPage.route:
        final list = settings.arguments as List;
        final kapalId = list[0] as int;
        final departemenId = list[1] as int;
        return navigateToPemakaianBahanBakarDanJamKerjaMesinPage(
            kapalId, departemenId);
      case PemakaianOliMesinPage.route:
        final list = settings.arguments as List;
        final kapalId = list[0] as int;
        final departemenId = list[1] as int;
        return navigateToPemakaianOliMesinPage(kapalId, departemenId);
      case PemakaianAirTawarPage.route:
        final list = settings.arguments as List;
        final kapalId = list[0] as int;
        final departemenId = list[1] as int;
        return navigateToPemakaianAirTawarPage(kapalId, departemenId);
      case PerawatanHarianDepartemenDeckDanDepartemenEnginePage.route:
        return navigateToPerawatanHarianDepartemenDeckDanDepartemenEnginePage(
            settings.arguments as DataKapal);
      case PerawatanTerencanaDepartemenDeckDanDepartemenEnginePage.route:
        return navigateToPerawatanTerencanaDepartemenDeckDanDepartemenEnginePage(
            settings.arguments as DataKapal);
      case PermintaanBarangDanJasaPage.route:
        return navigateToPermintaanBarangDanJasaPage(
            settings.arguments as DataKapal);
      case HistoryPermintaanBarangDanJasaPage.route:
        return navigateToHistoryPermintaanBarangDanJasaPage(
            settings.arguments as DataKapal);
      case KerusakanDanPerbaikanKapalPage.route:
        return navigateToKerusakanDanPerbaikanKapalPage(
            settings.arguments as DataKapal);
      case AlertOliPage.route:
        final list = settings.arguments as List;
        final kapalId = list[0] as int;
        final departemenId = list[1] as int;
        return navigateToAlertOliPage(kapalId, departemenId);
      default:
        return null;
    }
  }
}
