import 'package:flutter/cupertino.dart';
import 'package:pms_pcm/core/data/model/approve.dart';
import 'package:pms_pcm/core/theme/icon.dart';
import 'package:pms_pcm/core/util/const.dart';

class UserRole {
  String? _code;
  String? _name;

  void setCodeAndName(String? code, String? name) {
    _code = code;
    _name = name;
  }

  bool isEngine() {
    if (_code == "MS1" || _code == "MS2" || _code == "ED") {
      return true;
    } else {
      return false;
    }
  }

  String? getName() {
    return _name;
  }

  bool isUserPermitedToApprove(Approve approve) {
    // SEBAGAI KKM
    if (_code == "MS1" ||
        _code == "MS2" ||
        _code == "ED" ||
        _code == "MU1" ||
        _code == "MU2") {
      if (approve.approved1 == null &&
          approve.approved2 == null &&
          approve.approved3 == null &&
          approve.approved4 == null) {
        return true;
      } else {
        return false;
      }
      // SEBAGAI NAHKODA
    } else if (_code == "NA" || _code == "MU1") {
      if (approve.approved1 != null &&
          approve.approved2 == null &&
          approve.approved3 == null &&
          approve.approved4 == null) {
        return true;
      } else {
        return false;
      }
      // SEBAGAI PORT ENGGINER
    } else if (_code == "PE") {
      if (approve.approved1 != null &&
          approve.approved2 != null &&
          approve.approved3 == null &&
          approve.approved4 == null) {
        return true;
      } else {
        return false;
      }
      // SEBAGAI ADMIN
    } else if (_code == "SA") {
      if (approve.approved1 != null &&
          approve.approved2 != null &&
          approve.approved3 != null &&
          approve.approved4 == null) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isUserPermitedToKirim(Approve approve) {
    // SEBAGAI ADMIN
    if (_code == "SA") {
      if (approve.approved1 != null &&
          approve.approved2 != null &&
          approve.approved3 != null &&
          approve.approved4 != null &&
          approve.approved5 == null) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isUserPermitedToTerima(Approve approve) {
    // SEBAGAI ADMIN
    if (_code == "NA" || _code == "MU1") {
      if (approve.approved1 != null &&
          approve.approved2 != null &&
          approve.approved3 != null &&
          approve.approved4 != null &&
          approve.approved5 != null &&
          approve.approved6 == null) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isUserDoneBarang(Approve approve) {
    if (approve.approved1 != null &&
        approve.approved2 != null &&
        approve.approved3 != null &&
        approve.approved4 != null &&
        approve.approved5 != null &&
        approve.approved6 != null) {
      return true;
    } else {
      return false;
    }
  }

  bool isSudahDiApprovePortEngginer(Approve approve) {
    if (approve.approved1 != null &&
        approve.approved2 != null &&
        approve.approved3 != null) {
      return true;
    } else {
      return false;
    }
  }

  Icon getIcon(Approve approve) {
    if (approve.approved1 == null) {
      return iconSend;
    } else {
      return iconApproval;
    }
  }

  String getTitle(Approve approve) {
    if (approve.approved1 != null) {
      return approveData;
    } else {
      return sendData;
    }
  }

  bool isUserPermitedToSave(Approve approve, bool isDateToSaveNotExpired) {
    // SEBAGAI KKM
    if (_code == "MS1" ||
        _code == "MS2" ||
        _code == "ED" ||
        _code == "MU1" ||
        _code == "MU2") {
      if (approve.approved1 == null &&
          approve.approved2 == null &&
          approve.approved3 == null &&
          approve.approved4 == null) {
        if (isDateToSaveNotExpired) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
      // SEBAGAI NAHKODA
    } else if (_code == "NA" || _code == "MU1") {
      if (approve.approved1 != null &&
          approve.approved2 == null &&
          approve.approved3 == null &&
          approve.approved4 == null) {
        return true;
      } else {
        return false;
      }
      // SEBAGAI PORT ENGGINER
    } else if (_code == "PE") {
      return false;
      // SEBAGAI ADMIN
    } else if (_code == "SA") {
      if (approve.approved1 != null &&
          approve.approved2 != null &&
          approve.approved3 != null &&
          approve.approved4 == null) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  String getUpdateStatusURL(bool isModul1Until4, Approve approve) {
    if (approve.approved1 == null) {
      return isModul1Until4 ? "update_status" : "sendReport";
    } else if (approve.approved2 == null) {
      return "approved_1_na";
    } else if (approve.approved3 == null) {
      return "approved_2_pe";
    } else if (approve.approved4 == null) {
      return "approved_3_sa";
    } else if (approve.approved5 == null) {
      return "approved_4_sa";
    } else if (approve.approved6 == null) {
      return "approved_5_na";
    } else {
      return "-----";
    }
  }

  bool isUserPermitedToCreate() {
    // SEBAGAI KKM
    if (_code == "MS1" ||
        _code == "MS2" ||
        _code == "ED" ||
        _code == "MU1" ||
        _code == "MU2") {
      return true;
      // SEBAGAI ADMIN
    } else {
      return false;
    }
  }
}
