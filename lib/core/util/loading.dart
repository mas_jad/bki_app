import 'package:flutter/material.dart';

import '../theme/theme.dart';
import '../widget/other/loading_widget.dart';

class LoadingOverlay {
  static void show(BuildContext context) async {
    dismiss();
    LoadingOverlay._createView(context);
  }

  static OverlayEntry? _overlayEntry;
  static bool isVisible = false;

  static void _createView(BuildContext context) async {
    var overlayState = Overlay.of(context);

    _overlayEntry = OverlayEntry(
      builder: (BuildContext context) => const LoadingWidget(),
    );
    isVisible = true;
    overlayState!.insert(_overlayEntry!);
  }

  static dismiss() async {
    if (!isVisible) {
      return;
    }
    isVisible = false;
    _overlayEntry!.remove();
  }
}

class LoadingFullOverlay {
  static void show(BuildContext context) async {
    dismiss();
    LoadingFullOverlay._createView(context);
  }

  static OverlayEntry? _overlayEntry;
  static bool isVisible = false;

  static void _createView(BuildContext context) async {
    var overlayState = Overlay.of(context);

    _overlayEntry = OverlayEntry(
      builder: (BuildContext context) => GestureDetector(
        child: Container(
          color: black26,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: const LoadingWidget(),
        ),
      ),
    );
    isVisible = true;
    overlayState!.insert(_overlayEntry!);
  }

  static dismiss() async {
    if (!isVisible) {
      return;
    }
    isVisible = false;
    _overlayEntry!.remove();
  }
}
