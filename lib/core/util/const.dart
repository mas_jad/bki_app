// ignore_for_file: constant_identifier_names

import 'package:pms_pcm/core/util/enum.dart';

const success = 'sukses';
const empty = '';
const emptyStrip = '-';
const emptyStripHTML = '<p>-</p>';
const appName = 'MasCip';
const oneMillion = 1000000;
const widthTab = 599;
const connectionError = "Connection Error";
const somethingErrorHappen = "Something Error Happen";
const tokenKey = 'TOKEN_KEY';
const youHaveSuccessfullySentData = "You have successfully sent data";
const dataCannotChangeAgain =
    "Data tidak dapat diganti lagi, Apakah Anda yakin?";
const dataEmpty = "Data Masih Kosong";
const sendData = "Kirim Data";
const approveData = "Approve Data";
const send = "Send";
const enter = "Masuk";

const imageLogo = 'assets/image/logo.png';
const imageLogo2 = 'assets/image/mascip_logo.png';

const activityImage1 = 'assets/image/activity_1.png';
const activityImage2 = 'assets/image/activity_2.png';
const activityImage3 = 'assets/image/activity_3.png';
const activityImage4 = 'assets/image/activity_4.png';
const activityImage5 = 'assets/image/activity_5.png';
const activityImage6 = 'assets/image/activity_6.png';
const activityImage7 = 'assets/image/activity_7.png';
const activityImage8 = 'assets/image/activity_8.png';
const itemImage1 = 'assets/image/item_1.png';
const itemImage1f = 'assets/image/item_1_2.png';
const itemImage1e = 'assets/image/item_1_e.png';
const itemImage1l = 'assets/image/item_1_l.png';
const itemImage1s = 'assets/image/item_1_s.png';
const itemImage1n = 'assets/image/item_1_n.png';
const itemImage1q = 'assets/image/item_1_q.png';
const itemImage2 = 'assets/image/item_2.png';
const itemImage3 = 'assets/image/item_3.png';
const itemImage4 = 'assets/image/item_4.png';
const itemImage5 = 'assets/image/item_5.png';
const itemImage6 = 'assets/image/item_6.png';
const itemImage7e = 'assets/image/item_7.png';
const itemImage7s = 'assets/image/item_7_1.png';
const itemImage7t = 'assets/image/item_7_2.png';
const itemImage8 = 'assets/image/item_8.png';
const itemImage9 = 'assets/image/item_9.png';
const itemImage10 = 'assets/image/item_10.png';

const save = "Simpan";
const inputImage = "Masukkan Gambar";

const ALL = "All";
const DRAF = "DRAF";
const SEND = "SEND";
const APPROVED = "APPROVED";
const ONTHEWAY = "ON THE WAY";
const ACCEPTED = "ACCEPTED";

const listStatus = [ALL, DRAF, SEND, APPROVED, ONTHEWAY, ACCEPTED];

const SEGERA = "SEGERA";
const BIASA = "BIASA";
const listKualifikasi = [BIASA, SEGERA];
const listParamterStok = [
  ParameterStok.lainnya,
  ParameterStok.air,
  ParameterStok.bbm,
  ParameterStok.oli
];

const DECK = "DECK";
const ENGINE2 = "ENGINE DEPT";
const ENGINE = "ENGINE";

const consumable = "consumable";
const jenisBarangAir = "air";
const jenisBarangOliMesin = "oli mesin";
const jenisBarangBahanBakar = "bahan bakar mesin";
