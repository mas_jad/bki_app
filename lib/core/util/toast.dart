import 'package:flutter/material.dart';
import 'package:pms_pcm/core/theme/color.dart';
import 'package:pms_pcm/core/theme/textstyle.dart';
import 'package:pms_pcm/core/util/const.dart';

import '../library/smart_snack_bar/animate_from.dart';
import '../library/smart_snack_bar/smart_snackbars.dart';

class MyToast {
  static void showSuccess(
    String msg,
    BuildContext context, {
    IconData? icon,
    Duration duration = const Duration(milliseconds: 2500),
  }) async {
    FocusManager.instance.primaryFocus?.unfocus();
    SmartSnackBars.showTemplatedSnackbar(
      context: context,
      backgroundColor: green2,
      animateFrom: AnimateFrom.fromBottom,
      duration: const Duration(seconds: 2),
      animationCurve: Curves.easeOutQuad,
      leading: Container(
        margin: const EdgeInsets.only(right: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: white.withOpacity(0.2),
        ),
        child: const Icon(
          Icons.check,
          color: white,
        ),
      ),
      titleWidget: Text(
        "Good Job",
        style: ptSansTextStyle.copyWith(
          color: white,
          fontWeight: bold,
          fontSize: 20,
        ),
      ),
      subTitleWidget: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          youHaveSuccessfullySentData,
          style: ptSansTextStyle.copyWith(
              color: white, fontSize: 15, fontWeight: medium),
        ),
      ),
      trailing: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {},
        child: const Icon(
          Icons.close,
          color: white,
        ),
      ),
    );
  }

  static void showError(
    String msg,
    BuildContext context, {
    IconData? icon,
    Duration duration = const Duration(milliseconds: 3000),
  }) async {
    FocusManager.instance.primaryFocus?.unfocus();
    SmartSnackBars.showTemplatedSnackbar(
      context: context,
      duration: const Duration(seconds: 3, milliseconds: 500),
      animationCurve: Curves.easeOutQuad,
      backgroundColor: red,
      animateFrom: AnimateFrom.fromBottom,
      leading: Container(
        margin: const EdgeInsets.only(right: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: white.withOpacity(0.2),
        ),
        child: const Icon(
          Icons.error,
          color: white,
        ),
      ),
      titleWidget: Text(
        "Failed",
        style: ptSansTextStyle.copyWith(
          color: white,
          fontWeight: bold,
          fontSize: 20,
        ),
      ),
      subTitleWidget: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          msg,
          style: ptSansTextStyle.copyWith(
            color: white,
            fontSize: 15,
            fontWeight: medium,
          ),
        ),
      ),
      trailing: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {},
        child: const Icon(
          Icons.close,
          color: white,
        ),
      ),
    );
  }
}
