import 'package:flutter/material.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/core/util/route.dart';

import '../../feature/login/view/login_page.dart';
import '../theme/theme.dart';

toLoginBack() {
  locator<NavigationService>()
      .navigatorKey
      .currentState
      ?.pushNamedAndRemoveUntil(LoginPage.route, (route) => false);
}

Future<DateTime?> showMyDatePicker(
  BuildContext context,
  DateTime initialDate,
  DateTime lastDate,
) async {
  return await showDatePicker(
    context: context,
    initialDate: initialDate,
    firstDate: DateTime(2000),
    lastDate: lastDate,
    // lastDate: DateTime(3000),
    builder: (context, child) {
      return Theme(
        data: Theme.of(context).copyWith(
          colorScheme: const ColorScheme.light(
            primary: secondary,
            onPrimary: white,
            onSurface: primary,
          ),
          textButtonTheme: TextButtonThemeData(
            style: TextButton.styleFrom(
              foregroundColor: primary,
            ),
          ),
        ),
        child: child!,
      );
    },
  );
}

Future<DateTimeRange?> showMyRangeDatePicker(
  BuildContext context,
  DateTimeRange? dateRange,
  DateTime now,
) async {
  return await showDateRangePicker(
    context: context,
    initialDateRange: dateRange,
    firstDate: DateTime(2000),
    lastDate: now,
    // lastDate: DateTime(3000),
    builder: (context, child) {
      return Theme(
        data: Theme.of(context).copyWith(
          colorScheme: const ColorScheme.light(
            primary: secondary,
            onPrimary: white,
            onSurface: primary,
          ),
          textButtonTheme: TextButtonThemeData(
            style: TextButton.styleFrom(
              foregroundColor: primary,
            ),
          ),
        ),
        child: child!,
      );
    },
  );
}
