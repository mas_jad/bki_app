import 'package:flutter/material.dart';

import '../theme/theme.dart';

void showSnackBar(
  BuildContext context,
  String message,
  Color color,
  Color fontColor,
  Duration duration,
) {
  final snackBar = SnackBar(
    backgroundColor: color,
    duration: duration,
    content: Text(
      message,
      style: ptSansTextStyle.copyWith(color: fontColor, fontSize: 14),
    ),
  );
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

void showSnackBarError(
  BuildContext context,
  String message,
) {
  showSnackBar(
    context,
    message,
    red,
    white,
    const Duration(milliseconds: 1000),
  );
}
