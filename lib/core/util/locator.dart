import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:pms_pcm/core/data/repository/user_repository.dart';
import 'package:pms_pcm/core/data/source/master_barang_dan_jasa_remote_data_source.dart';
import 'package:pms_pcm/core/data/source/user_local_data_source.dart';
import 'package:pms_pcm/core/data/source/user_remote_data_source.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/service/image/image_service.dart';
import 'package:pms_pcm/core/util/user_role.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/data/repository/sounding_bahan_bakar_repository.dart';
import 'package:pms_pcm/feature/activity_1_sounding_bahan_bakar/data/source/sounding_bahan_bakar_remote_data_source.dart';
import 'package:pms_pcm/feature/activity_7_permintaan_barang_dan_jasa/data/source/permintaan_barang_dan_jasa_remote_data_source.dart';
import 'package:pms_pcm/feature/ship_activity/data/repository/alert_bahan_bakar_dan_air_tawar_repository.dart';
import 'package:pms_pcm/feature/ship_activity/data/source/alert_bahan_bakar_dan_air_tawar_remote_data_source.dart';

import '../../feature/activity_2_pemakaian_bahan_bakar_dan_jam_kerja_mesin/data/repository/pemakaian_bahan_bakar_dan_jam_kerja_mesin_repository.dart';
import '../../feature/activity_2_pemakaian_bahan_bakar_dan_jam_kerja_mesin/data/source/pemakaian_bahan_bakar_dan_jam_kerja_mesin_remote_data_source.dart';
import '../../feature/activity_3_pemakaian_oli_mesin/data/repository/pemakaian_oli_mesin_repository.dart';
import '../../feature/activity_3_pemakaian_oli_mesin/data/source/pemakaian_oli_mesin_remote_data_source.dart';
import '../../feature/activity_4_pemakaian_air_tawar/data/repository/pemakaian_air_tawar_repository.dart';
import '../../feature/activity_4_pemakaian_air_tawar/data/source/pemakaian_air_tawar_remote_data_source.dart';
import '../../feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/data/repository/perawatan_harian_departemen_deck_dan_departemen_engine_repository.dart';
import '../../feature/activity_5_perawatan_harian_departemen_deck_dan_departemen_engine/data/source/perawatan_harian_departemen_deck_dan_departemen_engine_remote_data_source.dart';
import '../../feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/repository/perawatan_terencana_repository.dart';
import '../../feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/source/perawatan_terencana_departemen_deck_remote_data_source.dart';
import '../../feature/activity_6_pemeliharan_terencana_departemen_deck_dan_departemen_engine/data/source/perawatan_terencana_departemen_engine_remote_data_source.dart';
import '../../feature/activity_7_permintaan_barang_dan_jasa/data/repository/permintaan_barang_dan_jasa_repository.dart';
import '../../feature/activity_8_kerusakan_dan_perbaikan_kapal/data/repository/kerusakan_kapal_repository.dart';
import '../../feature/activity_8_kerusakan_dan_perbaikan_kapal/data/repository/perbaikan_kapal_repository.dart';
import '../../feature/activity_8_kerusakan_dan_perbaikan_kapal/data/source/kerusakan_kapal_remote_data_source.dart';
import '../../feature/activity_8_kerusakan_dan_perbaikan_kapal/data/source/perbaikan_kapal_remote_data_source.dart';
import '../data/repository/master_barang_dan_jasa_repository.dart';
import '../data/repository/master_barang_oli_repository.dart';
import '../data/source/master_barang_oli_remote_data_source.dart';
import '../service/location/location_service.dart';
import 'network_info.dart';
import 'route.dart';

final locator = GetIt.instance;

Future<void> initServiceLocator() async {
  locator.registerSingleton(NetworkInfoImpl(InternetConnectionChecker()));
  locator.registerSingleton(Dio());
  // locator.registerSingleton(Dio()..interceptors.add(ChuckerDioInterceptor()));
  locator.registerSingleton(const FlutterSecureStorage());
  locator.registerSingleton(
      UserRepository(UserRemoteDataSource(), UserLocalDataSource()));
  locator.registerSingleton(
      SoundingBahanBakarRepository(SoundingBahanBakarRemoteDataSource()));
  locator.registerSingleton(PemakaianBahanBakarDanJamKerjaMesinRepository(
      PemakaianBahanBakarDanJamKerjaMesinRemoteDataSource()));
  locator.registerSingleton(
      PemakaianOliMesinRepository(PemakaianOliMesinRemoteDataSource()));
  locator.registerSingleton(
      PemakaianAirTawarRepository(PemakaianAirTawarRemoteDataSource()));
  locator.registerSingleton(
      PerawatanHarianDepartemenDeckDanDepartemenEngineRepository(
          PerawatanHarianDepartemenDeckDanDepartemenEngineRemoteDataSource()));
  locator.registerSingleton(PerawatanTerencanaRepository(
      PerawatanTerencanaDepartemenDeckRemoteDataSource(),
      PerawatanTerencanaDepartemenEngineRemoteDataSource()));
  locator.registerSingleton(PermintaanBarangDanJasaRepository(
      PermintaanBarangDanJasaRemoteDataSource()));
  locator.registerSingleton(
      KerusakanKapalRepository(KerusakanKapalRemoteDataSource()));
  locator.registerSingleton(
      PerbaikanKapalRepository(PerbaikanKapalRemoteDataSource()));
  locator.registerSingleton(
      MasterBarangDanJasaRepository(MasterBarangDanJasaRemoteDataSource()));

  locator.registerSingleton(AlertBahanBakarDanAirTawarRepository(
      AlertBahanBakarDanAirTawarRemoteDataSouce()));

  locator.registerSingleton(
      MasterBarangOliRepository(MasterBarangOliRemoteDataSource()));

  locator.registerSingleton(MyDate());
  locator.registerSingleton(ImageService());
  locator.registerSingleton(LocationService());
  locator.registerSingleton(UserRole());

  locator.registerSingleton(
    NavigationService(),
  );
}
