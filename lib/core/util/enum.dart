import 'package:pms_pcm/core/util/const.dart';

enum ParameterStok {
  air,
  oli,
  bbm,
  lainnya;

  @override
  String toString() => name.toUpperCase();
}

ParameterStok convertStringToParamterStok(String? value) {
  if (value?.toLowerCase() == ParameterStok.air.name) {
    return ParameterStok.air;
  } else if (value?.toLowerCase() == ParameterStok.oli.name) {
    return ParameterStok.oli;
  } else if (value?.toLowerCase() == ParameterStok.bbm.name) {
    return ParameterStok.bbm;
  } else {
    return ParameterStok.lainnya;
  }
}

String getJenisParameter(ParameterStok? parameterStok) {
  switch (parameterStok) {
    case ParameterStok.air:
      return jenisBarangAir;
    case ParameterStok.bbm:
      return jenisBarangBahanBakar;
    case ParameterStok.oli:
      return jenisBarangOliMesin;
    default:
      return empty;
  }
}
