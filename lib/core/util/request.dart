import 'dart:async';

import 'package:dio/dio.dart';
import 'package:pms_pcm/core/data/model/error_response.dart';
import 'package:pms_pcm/core/util/const.dart';

import 'global_function.dart';
import 'locator.dart';
import 'network_info.dart';

class RequestServer<T> {
  Future<T> requestServer({
    required FutureOr<T> Function() computation,
  }) async {
    try {
      return await computation();
    } catch (e) {
      if (e is DioError) {
        final errorMessage = e.response?.data['message'];
        final int? code = e.response?.data['code'];
        if (code == 401) {
          toLoginBack();
        }
        return errorMessage != null
            ? Future.error(ErrorResponse(
                message: errorMessage,
                statusCode: code ?? e.response?.statusCode,
              ))
            : Future.error(ErrorResponse(
                message: somethingErrorHappen,
                statusCode: code ?? e.response?.statusCode,
              ));
      }

      if (await locator<NetworkInfoImpl>().isConnected) {
        return Future.error(ErrorResponse(
          message: e.toString(),
          statusCode: null,
        ));
      } else {
        return Future.error(const ErrorResponse(
          message: 'Check Your Connection',
          statusCode: null,
        ));
      }
    }
  }
}

class RequestLocal<T> {
  Future<T> requestLocal({
    required FutureOr<T> Function() computation,
  }) async {
    try {
      return await computation();
    } catch (e) {
      return Future.error('Failed');
    }
  }
}
