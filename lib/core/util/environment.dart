import 'package:flutter_dotenv/flutter_dotenv.dart';

initEnvirontment() async {
  await dotenv.load(fileName: 'api_endpoint.env');
}

const developURL = 'DEVELOP_URL';
const productionURL = 'PRODUCTION_URL';

const baseURL = developURL;

// AUTH
const loginEndpoint = "LOGIN_ENDPOINT";
const profileEndpoint = "PROFILE_ENDPOINT";

// MASTER DATA
const daftarKapalEndpoint = "DAFTAR_KAPAL_ENDPOINT";
const daftarBarangDanJasaEndpoint = "DAFTAR_BARANG_DAN_JASA_ENDPOINT";
const alertBahanBakarEndpoint = "ALERT_BAHAN_BAKAR_ENDPOINT";
const alertAirTawarEndpoint = "ALERT_AIR_TAWAR_ENDPOINT";

// MODUL
// 1 SOUNDING BAHAN BAKAR
const dataSBBPerTanggalEndpoint = "DATA_SBB_PER_TANGGAL_ENDPOINT";
const simpanDataSBBEndpoint = "SIMPAN_DATA_SBB_ENDPOINT";
const ubahStatusSBBEndpoint = "UBAH_STATUS_SBB_ENDPOINT";

// 2 PEMAKAIAN BAHAN BAKAR DAN JAM KERJA MESIN
const dataPBBdanJKMPerTanggalEndpoint = "DATA_PBB_DAN_JKM_PER_TANGGAL_ENDPOINT";
const simpanDataPBBdanJKMEndpoint = "SIMPAN_DATA_PBB_DAN_JKM_ENDPOINT";
const ubahStatusPBBdanJKMEndpoint = "UBAH_STATUS_PBB_DAN_JKM_ENDPOINT";

// 3 PEMAKAIAN OLI MESIN
const dataPOMPerTanggalEndpoint = "DATA_POM_PER_TANGGAL_ENDPOINT";
const simpanDataPOMEndpoint = "SIMPAN_DATA_POM_ENDPOINT";
const ubahStatusPOMEndpoint = "UBAH_STATUS_POM_ENDPOINT";
const alertRequestOLIEndpoint = "ALERT_REQUEST_OLI_ENDPOINT";

// 4 PEMAKAIAN AIR TAWAR
const dataPATPerTanggalEndpoint = "DATA_PAT_PER_TANGGAL_ENDPOINT";
const simpanDataPATEndpoint = "SIMPAN_DATA_PAT_ENDPOINT";
const ubahStatusPATEndpoint = "UBAH_STATUS_PAT_ENDPOINT";

// 5  PERAWATAN HARIAN
const dataPHDPerTanggalEndpoint = "DATA_PHD_PER_TANGGAL_ENDPOINT";
const simpanDataPHDEndpoint = "SIMPAN_DATA_PHD_ENDPOINT";
const updateDataPHDEndpoint = "UPDATE_DATA_PHD_ENDPOINT";
const ubahStatusPHDEndpoint = "UBAH_STATUS_PHD_ENDPOINT";

// 6 PERAWATAN TERENCANA
const dataPTDEndpoint = "DATA_PTD_ENDPOINT";
const dataPTDPelaporanEndpoint = "DATA_PTD_PELAPORAN_ENDPOINT";
const simpanDataPTDEndpoint = "SIMPAN_DATA_PTD_ENDPOINT";
const ubahStatusPTDEndpoint = "UBAH_STATUS_PTD_ENDPOINT";

const dataPTEEndpoint = "DATA_PTE_ENDPOINT";
const dataPTEPelaporanEndpoint = "DATA_PTE_PELAPORAN_ENDPOINT";
const simpanDataPTEEndpoint = "SIMPAN_DATA_PTE_ENDPOINT";
const ubahStatusPTEEndpoint = "UBAH_STATUS_PTE_ENDPOINT";

// 7 PERMINTAAN BARANG DAN JASA
const dataPBJAllEndpoint = "DATA_PBJ_ALL_ENDPOINT";
const simpanUpdateDataPBJEndpoint = "SIMPAN_UPDATE_DATA_PBJ_ENDPOINT";
const ubahStatusPBJEndpoint = "UBAH_STATUS_PBJ_ENDPOINT";
const kirimBarangEndpoint = "KIRIM_BARANG_ENDPOINT";
const terimaBarangEndpoint = "BARANG_DITERIMA_ENDPOINT";

// 8.1 KERUSAKAN KAPAL
const dataLKKAllEndpoint = "DATA_LKK_ALL_ENDPOINT";
const simpanUpdateDataLKKEndpoint = "SIMPAN_UPDATE_DATA_LKK_ENDPOINT";
const ubahStatusLKKEndpoint = "UBAH_STATUS_LKK_ENDPOINT";

// 8.2 PERBAIKAN KAPAL
const dataLPKKAllEndpoint = "DATA_LPKK_ALL_ENDPOINT";
const dataLPKKTASKAllEndpoint = "DATA_TASK_ALL_ENDPOINT";
const simpanUpdateDataLPKKEndpoint = "SIMPAN_UPDATE_DATA_LPKK_ENDPOINT";
const ubahStatusLPKKEndpoint = "UBAH_STATUS_LPKK_ENDPOINT";
