import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:pms_pcm/core/util/const.dart';

class MyDate {
  DateTime? _dateToday;

  setDateToday(String? date) {
    try {
      if (date != null) {
        final year = int.parse(date.substring(0, 4));
        final month = int.parse(date.substring(5, 7));
        final day = int.parse(date.substring(8, 10));
        _dateToday = DateTime(year, month, day);
      } else {
        _dateToday = DateTime.now();
      }
    } catch (e) {
      debugPrint(e.toString());
      _dateToday = DateTime.now();
    }
  }

  DateTime? getDateWithInputString(String? date) {
    try {
      if (date != null) {
        final year = int.parse(date.substring(0, 4));
        final month = int.parse(date.substring(5, 7));
        final day = int.parse(date.substring(8, 10));
        return DateTime(year, month, day);
      } else {
        return null;
      }
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  DateTime getDateToday() {
    return _dateToday ?? DateTime.now();
  }

  String getDateTodayInString() {
    return getDateToday().toString().substring(0, 10);
  }

  String getDateWithInputInString(DateTime date) {
    return date.toString().substring(0, 10);
  }

  String getDateTodayIndonesiaFormat() {
    return DateFormat("EEEE, d MMMM yyyy", "id_ID").format(getDateToday());
  }

  String getDateWithInputIndonesiaFormat(DateTime? dateTime) {
    if (dateTime == null) {
      return emptyStrip;
    }
    return DateFormat("EEEE, d MMMM yyyy", "id_ID").format(dateTime);
  }

  String? getDateWithInputStringIndonesiaFormat(String? dateTime) {
    final date = getDateWithInputString(dateTime);
    if (date == null) {
      return null;
    }
    return DateFormat("EEEE, d MMMM yyyy", "id_ID").format(date);
  }
}
