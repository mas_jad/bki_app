import 'dart:io';
import 'dart:math';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:pms_pcm/core/widget/image/choose_file_or_camera_bottom_sheet.dart';

import '../../theme/theme.dart';
import '../../widget/image/camera_page.dart';

class ImageService {
  Future<String?> getPictures(BuildContext context,
      [bool mounted = true]) async {
    try {
      final ValueNotifier<String?> imagePath = ValueNotifier(null);
      final ValueNotifier<bool?> isToCamera = ValueNotifier(null);

      await showModalBottomSheet(
          shape: const RoundedRectangleBorder(borderRadius: cardBorderRadius),
          context: context,
          builder: ((context) => ChooseFileOrCameraBottomSheet(
                isToCamera: isToCamera,
              )));

      if (isToCamera.value == null) {
        return null;
      }

      if (isToCamera.value!) {
        if (!mounted) return null;
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: ((context) => CameraPage(
                  valueNotifier: imagePath,
                )),
          ),
        );
      } else {
        FilePickerResult? image = await FilePicker.platform.pickFiles(
          type: FileType.custom,
          allowedExtensions: ['jpg', 'png', 'jpeg', 'gif'],
        );
        imagePath.value = image?.files.single.path;
      }

      if (imagePath.value != null) {
        String compressedImagePath = await _compressImage(imagePath.value!);
        String croppedImagePath = await _cropImage(compressedImagePath);

        return croppedImagePath;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  _deleteOldImage(String? oldImagePath) async {
    try {
      if (oldImagePath != null) {
        FileSystemEntity image = File(oldImagePath);
        await image.delete();
      }
    } catch (e) {
      // debugPrint("Delete Image Failed -----> $e");
    }
  }

  Future<String> _cropImage(String imagePath) async {
    try {
      CroppedFile? croppedFile = await ImageCropper().cropImage(
        sourcePath: imagePath,
        compressQuality: 60,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
        ],
        uiSettings: [
          AndroidUiSettings(
            toolbarTitle: 'Edit Photo',
            toolbarColor: secondary,
            toolbarWidgetColor: white,
            cropFrameColor: primary,
            activeControlsWidgetColor: primaryAccent,
            initAspectRatio: CropAspectRatioPreset.square,
            lockAspectRatio: false,
          ),
          IOSUiSettings(
            title: 'Edit Photo',
          ),
        ],
      );

      if (croppedFile != null) {
        _deleteOldImage(imagePath);
      }
      return croppedFile != null ? croppedFile.path : imagePath;
    } catch (e) {
      return imagePath;
    }
  }

  Future<String> _compressImage(String path) async {
    try {
      ImageProperties properties =
          await FlutterNativeImage.getImageProperties(path);
      final height = properties.height ?? 0;
      final width = properties.width ?? 0;
      File result = await FlutterNativeImage.compressImage(path,
          targetWidth: 1300, targetHeight: (height * 1300 / width).round());
      // print(
      //     "- DRAGONITE Size After Compress ${await _getFileSize(result.lengthSync(), 2)}");
      return result.path;
    } catch (e) {
      return path;
    }
  }

// ignore: unused_element
  _getFileSize(int bytes, int decimals) async {
    if (bytes <= 0) return '0 B';
    const suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = (log(bytes) / log(1024)).floor();
    return '${(bytes / pow(1024, i)).toStringAsFixed(decimals)} ${suffixes[i]}';
  }
}
