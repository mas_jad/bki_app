import 'package:camera/camera.dart';

late List<CameraDescription> cameras;

initCamera() async {
  try {
    cameras = await availableCameras();
  } catch (e) {
    // DO NOTHING
  }
}
