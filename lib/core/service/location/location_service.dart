import 'package:location/location.dart';

class LocationService {
  LocationData? locationData;
  Future<String?> getLastPosition() async {
    if (locationData == null) {
      return null;
    }
    return "${locationData?.latitude},${locationData?.longitude}";
  }

  Future<String?> getPosition() async {
    try {
      Location location = Location();

      bool serviceEnabled;
      PermissionStatus permissionGranted;

      serviceEnabled = await location.serviceEnabled();
      if (!serviceEnabled) {
        serviceEnabled = await location.requestService();
        if (!serviceEnabled) {
          return null;
        }
      }

      permissionGranted = await location.hasPermission();
      if (permissionGranted == PermissionStatus.denied) {
        permissionGranted = await location.requestPermission();
        if (permissionGranted != PermissionStatus.granted) {
          return null;
        }
      }

      locationData = await location.getLocation();
      return "${locationData?.latitude},${locationData?.longitude}";
    } catch (e) {
      return null;
    }
  }
}
