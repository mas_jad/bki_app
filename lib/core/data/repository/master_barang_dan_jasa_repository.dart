import 'package:pms_pcm/core/data/model/master_barang_dan_jasa.dart';
import 'package:pms_pcm/core/data/source/master_barang_dan_jasa_remote_data_source.dart';

import '../../util/request.dart';

class MasterBarangDanJasaRepository {
  final MasterBarangDanJasaRemoteDataSource
      _masterBarangDanJasaRemoteDataSource;
  MasterBarangDanJasaRepository(this._masterBarangDanJasaRemoteDataSource);
  Future<MasterBarangDanJasa> getMasterBarangDanJasa(
      int kapalId, String token) async {
    return RequestServer<MasterBarangDanJasa>().requestServer(
        computation: () async {
      final response = await _masterBarangDanJasaRemoteDataSource
          .getMasterBarangDanJasa(kapalId, token);

      return response.data!;
    });
  }
}
