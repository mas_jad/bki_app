import '../../util/request.dart';
import '../model/alert_request_oli.dart';
import '../source/master_barang_oli_remote_data_source.dart';

class MasterBarangOliRepository {
  final MasterBarangOliRemoteDataSource _masterBarangOliRemoteDataSource;
  MasterBarangOliRepository(this._masterBarangOliRemoteDataSource);
  Future<List<AlertRequestOli>> getMasterBarangOli(int id, String token) async {
    return RequestServer<List<AlertRequestOli>>().requestServer(
        computation: () async {
      final response =
          await _masterBarangOliRemoteDataSource.getMasterBarangOli(id, token);
      if ((response.data?.isEmpty ?? true)) {
        throw Exception("Data Empty");
      }

      return response.data!;
    });
  }
}
