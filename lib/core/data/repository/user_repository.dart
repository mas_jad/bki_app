import 'package:pms_pcm/core/data/model/login.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/data/source/user_local_data_source.dart';
import 'package:pms_pcm/core/service/date/date.dart';
import 'package:pms_pcm/core/util/global_function.dart';
import 'package:pms_pcm/core/util/locator.dart';
import 'package:pms_pcm/core/util/request.dart';
import 'package:pms_pcm/core/util/user_role.dart';

import '../source/user_remote_data_source.dart';

class UserRepository {
  final UserRemoteDataSource userRemoteDataSource;
  final UserLocalDataSource userLocalDataSource;
  UserRepository(this.userRemoteDataSource, this.userLocalDataSource);

  Future<LoginResponse> login(String email, String password) async {
    return RequestServer<LoginResponse>().requestServer(computation: () async {
      final response = await userRemoteDataSource.login(email, password);
      locator<MyDate>().setDateToday(response.date);
      final token = response.data!.token!;
      await userLocalDataSource.saveToken(token);
      locator<UserRole>().setCodeAndName(
          response.data?.profile?.roleCode, response.data?.profile?.roleName);
      return response;
    });
  }

  Future<void> logout() async {
    return RequestLocal<void>().requestLocal(computation: () async {
      await userLocalDataSource.deleteToken();
    });
  }

  Future<ProfileResponse> initialLogin() async {
    return RequestServer<ProfileResponse>().requestServer(
        computation: () async {
      final token = await userLocalDataSource.getToken();
      final responseProfile = await userRemoteDataSource.getProfile(token!);
      locator<UserRole>().setCodeAndName(
          responseProfile.data?.roleCode, responseProfile.data?.roleName);
      locator<MyDate>().setDateToday(responseProfile.date);
      return responseProfile;
    });
  }

  Future<String> getToken() async {
    try {
      final token = await userLocalDataSource.getToken();
      return token!;
    } catch (e) {
      toLoginBack();
      throw Exception("User Missing Credentials");
    }
  }
}
