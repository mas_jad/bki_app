import '../../util/const.dart';

class Departemen {
  int? id;
  String? nama;

  @override
  toString() {
    return nama ?? empty;
  }

  Departemen({this.id, this.nama});

  Departemen.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nama = json['nama'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['nama'] = nama;
    return data;
  }
}
