import '../../util/const.dart';

class AlertRequestOliResponse {
  int? code;
  String? message;
  List<AlertRequestOli>? data;

  AlertRequestOliResponse({this.code, this.message, this.data});

  AlertRequestOliResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <AlertRequestOli>[];
      json['data'].forEach((v) {
        data!.add(AlertRequestOli.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AlertRequestOli {
  int? id;
  int? idPesawatPenggerak;
  String? namaPesawatPenggerak;
  int? idMasterBarang;
  int? idOliYgDigunakan;
  String? namaJenisOliYgDigunakan;
  String? merekOliYgDigunakan;
  String? batasMinimum;
  String? totalSaatIni;
  int? isRequestOli;
  String? message;

  @override
  String toString() {
    return namaJenisOliYgDigunakan ?? empty;
  }

  AlertRequestOli(
      {this.id,
      this.idPesawatPenggerak,
      this.namaPesawatPenggerak,
      this.idMasterBarang,
      this.idOliYgDigunakan,
      this.namaJenisOliYgDigunakan,
      this.merekOliYgDigunakan,
      this.batasMinimum,
      this.totalSaatIni,
      this.isRequestOli,
      this.message});

  AlertRequestOli.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idPesawatPenggerak = json['id_pesawat_penggerak'];
    namaPesawatPenggerak = json['nama_pesawat_penggerak'];
    idMasterBarang = json['id_master_barang'];
    idOliYgDigunakan = json['id_oli_yg_digunakan'];
    namaJenisOliYgDigunakan = json['nama_jenis_oli_yg_digunakan'];
    merekOliYgDigunakan = json['merek_oli_yg_digunakan'];
    batasMinimum = json['batas_minimum'];
    totalSaatIni = json['total_saat_ini'];
    isRequestOli = json['is_request_oli'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['id_pesawat_penggerak'] = idPesawatPenggerak;
    data['nama_pesawat_penggerak'] = namaPesawatPenggerak;
    data['id_master_barang'] = idMasterBarang;
    data['id_oli_yg_digunakan'] = idOliYgDigunakan;
    data['nama_jenis_oli_yg_digunakan'] = namaJenisOliYgDigunakan;
    data['merek_oli_yg_digunakan'] = merekOliYgDigunakan;
    data['batas_minimum'] = batasMinimum;
    data['total_saat_ini'] = totalSaatIni;
    data['is_request_oli'] = isRequestOli;
    data['message'] = message;
    return data;
  }
}
