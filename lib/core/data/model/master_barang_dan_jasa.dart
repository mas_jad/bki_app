import 'package:pms_pcm/core/util/const.dart';

class MasterBarangDanJasaResponse {
  bool? success;
  int? code;
  MasterBarangDanJasa? data;

  MasterBarangDanJasaResponse({this.success, this.code, this.data});

  MasterBarangDanJasaResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    code = json['code'];
    data = json['data'] != null
        ? MasterBarangDanJasa.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    data['code'] = code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class MasterBarangDanJasa {
  List<MasterBarang>? masterBarang;
  List<MasterJasa>? masterJasa;

  MasterBarangDanJasa({this.masterBarang, this.masterJasa});

  MasterBarangDanJasa.fromJson(Map<String, dynamic> json) {
    if (json['master_barang'] != null) {
      masterBarang = <MasterBarang>[];
      json['master_barang'].forEach((v) {
        masterBarang!.add(MasterBarang.fromJson(v));
      });
    }
    if (json['master_jasa'] != null) {
      masterJasa = <MasterJasa>[];
      json['master_jasa'].forEach((v) {
        masterJasa!.add(MasterJasa.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (masterBarang != null) {
      data['master_barang'] = masterBarang!.map((v) => v.toJson()).toList();
    }
    if (masterJasa != null) {
      data['master_jasa'] = masterJasa!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MasterBarang {
  int? barangId;
  int? barangIdDiKapal;
  String? tipe;
  String? kodefikasi;
  String? kodePart;
  String? namaBarang;
  String? spesifikasi;
  String? satuan;
  String? stok;
  String? jenis;

  @override
  String toString() {
    return namaBarang ?? empty;
  }

  MasterBarang(
      {this.barangId,
      this.barangIdDiKapal,
      this.tipe,
      this.kodefikasi,
      this.kodePart,
      this.namaBarang,
      this.spesifikasi,
      this.satuan,
      this.jenis,
      this.stok});

  MasterBarang.fromJson(Map<String, dynamic> json) {
    barangId = json['barang_id'];
    barangIdDiKapal = json['barang_id_di_kapal'];
    tipe = json['tipe'];
    kodefikasi = json['kodefikasi'];
    kodePart = json['kode_part'];
    namaBarang = json['nama_barang'];
    spesifikasi = json['spesifikasi'];
    satuan = json['satuan'];
    stok = json['stok'];
    jenis = json['jenis'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['barang_id'] = barangId;
    data['barang_id_di_kapal'] = barangIdDiKapal;
    data['tipe'] = tipe;
    data['kodefikasi'] = kodefikasi;
    data['kode_part'] = kodePart;
    data['jenis'] = jenis;
    data['nama_barang'] = namaBarang;
    data['spesifikasi'] = spesifikasi;
    data['satuan'] = satuan;
    data['stok'] = stok;
    return data;
  }
}

class MasterJasa {
  String? tipe;
  String? namaJasa;

  @override
  String toString() {
    return namaJasa ?? empty;
  }

  MasterJasa({this.tipe, this.namaJasa});

  MasterJasa.fromJson(Map<String, dynamic> json) {
    tipe = json['tipe'];
    namaJasa = json['nama_jasa'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['tipe'] = tipe;
    data['nama_jasa'] = namaJasa;
    return data;
  }
}
