import 'departemen.dart';

class DataKapal {
  int? idKapal;
  String? namaKapal;
  String? kategori;
  String? callSign;
  int? tahunPembuatan;
  String? imoNumber;
  String? bendera;
  int? gt;
  int? loa;
  String? image;
  List<Departemen>? departemen;
  String? status;

  DataKapal(
      {this.idKapal,
      this.namaKapal,
      this.kategori,
      this.callSign,
      this.tahunPembuatan,
      this.imoNumber,
      this.bendera,
      this.gt,
      this.loa,
      this.image,
      this.departemen,
      this.status});

  DataKapal.fromJson(Map<String, dynamic> json) {
    idKapal = json['id_kapal'];
    namaKapal = json['nama_kapal'];
    kategori = json['kategori'];
    callSign = json['call_sign'];
    tahunPembuatan = json['tahun_pembuatan'];
    imoNumber = json['imo_number'];
    bendera = json['bendera'];
    gt = json['gt'];
    loa = json['loa'];
    image = json['image'];
    if (json['departemen'] != null) {
      departemen = <Departemen>[];
      json['departemen'].forEach((v) {
        departemen!.add(Departemen.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id_kapal'] = idKapal;
    data['nama_kapal'] = namaKapal;
    data['kategori'] = kategori;
    data['call_sign'] = callSign;
    data['tahun_pembuatan'] = tahunPembuatan;
    data['imo_number'] = imoNumber;
    data['bendera'] = bendera;
    data['gt'] = gt;
    data['loa'] = loa;
    data['image'] = image;
    if (departemen != null) {
      data['departemen'] = departemen!.map((v) => v.toJson()).toList();
    }
    data['status'] = status;
    return data;
  }
}
