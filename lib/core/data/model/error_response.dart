class ErrorResponse {
  final String message;
  final int? statusCode;
  const ErrorResponse({required this.message, required this.statusCode});

  @override
  String toString() {
    return message;
  }
}
