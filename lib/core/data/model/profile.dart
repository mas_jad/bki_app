import 'data_kapal.dart';

class ProfileResponse {
  String? message;
  int? code;
  Profile? data;
  List<DataKapal>? dataKapal;
  String? date;

  ProfileResponse(
      {this.message, this.code, this.data, this.dataKapal, this.date});

  ProfileResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    data = json['data'] != null ? Profile.fromJson(json['data']) : null;
    if (json['data_kapal'] != null) {
      dataKapal = <DataKapal>[];
      json['data_kapal'].forEach((v) {
        dataKapal!.add(DataKapal.fromJson(v));
      });
    }
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    if (dataKapal != null) {
      data['data_kapal'] = dataKapal!.map((v) => v.toJson()).toList();
    }
    data['date'] = date;
    return data;
  }
}

class Profile {
  String? namaDepan;
  String? namaBelakang;
  String? jenisKelamin;
  String? phone;
  String? alamatRumah;
  String? tempatLahir;
  String? roleCode;
  String? roleName;

  Profile(
      {this.namaDepan,
      this.namaBelakang,
      this.jenisKelamin,
      this.phone,
      this.alamatRumah,
      this.tempatLahir,
      this.roleCode,
      this.roleName});

  Profile.fromJson(Map<String, dynamic> json) {
    namaDepan = json['nama_depan'];
    namaBelakang = json['nama_belakang'];
    jenisKelamin = json['jenis_kelamin'];
    phone = json['phone'];
    alamatRumah = json['alamat_rumah'];
    tempatLahir = json['tempat_lahir'];
    roleCode = json['role_code'];
    roleName = json['role_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['nama_depan'] = namaDepan;
    data['nama_belakang'] = namaBelakang;
    data['jenis_kelamin'] = jenisKelamin;
    data['phone'] = phone;
    data['alamat_rumah'] = alamatRumah;
    data['tempat_lahir'] = tempatLahir;
    data['role_code'] = roleCode;
    data['role_name'] = roleName;
    return data;
  }
}
