import 'data_kapal.dart';
import 'profile.dart';

class LoginResponse {
  String? message;
  int? code;
  Login? data;
  String? date;

  LoginResponse({this.message, this.code, this.data, this.date});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    code = json['code'];
    data = json['data'] != null ? Login.fromJson(json['data']) : null;
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['date'] = date;
    return data;
  }
}

class Login {
  String? email;
  String? token;
  Profile? profile;
  List<DataKapal>? dataKapal;

  Login({this.email, this.token, this.profile, this.dataKapal});

  Login.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    token = json['token'];
    profile =
        json['profile'] != null ? Profile.fromJson(json['profile']) : null;
    if (json['data_kapal'] != null) {
      dataKapal = <DataKapal>[];
      json['data_kapal'].forEach((v) {
        dataKapal!.add(DataKapal.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['token'] = token;
    if (profile != null) {
      data['profile'] = profile!.toJson();
    }
    if (dataKapal != null) {
      data['data_kapal'] = dataKapal!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
