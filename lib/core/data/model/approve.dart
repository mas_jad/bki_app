import 'package:pms_pcm/core/util/const.dart';

class Approve {
  String? status;
  String? approved1;
  String? approved1Date;
  String? approved2;
  String? approved2Date;
  String? approved3;
  String? approved3Date;
  String? approved4;
  String? approved4Date;
  String? approved5;
  String? approved5Date;
  String? approved6;
  String? approved6Date;

  Approve(
      {this.status,
      this.approved1,
      this.approved1Date,
      this.approved2,
      this.approved2Date,
      this.approved3,
      this.approved3Date,
      this.approved4,
      this.approved4Date,
      this.approved5,
      this.approved5Date,
      this.approved6,
      this.approved6Date});

  String? getName() {
    if (approved6 != null) {
      return "$approved6";
    } else if (approved5 != null) {
      return "$approved5";
    } else if (approved4 != null) {
      return "$approved4 (Office)";
    } else if (approved3 != null) {
      return "$approved3 (Port Engineer)";
    } else if (approved2 != null) {
      return "$approved2";
    } else if (approved1 != null) {
      return approved1;
    }
    return null;
  }

  String getStatus() {
    if (approved6 != null) {
      return ACCEPTED;
    } else if (approved5 != null) {
      return ONTHEWAY;
    } else if (approved4 != null) {
      return APPROVED;
    } else if (approved3 != null) {
      return APPROVED;
    } else if (approved2 != null) {
      return APPROVED;
    } else if (approved1 != null) {
      return SEND;
    } else {
      return DRAF;
    }
  }
}
