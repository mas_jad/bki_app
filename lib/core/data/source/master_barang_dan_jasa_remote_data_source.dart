import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/data/model/master_barang_dan_jasa.dart';
import 'package:pms_pcm/core/util/environment.dart';

import '../../util/locator.dart';

class MasterBarangDanJasaRemoteDataSource {
  Future<MasterBarangDanJasaResponse> getMasterBarangDanJasa(
      int kapalId, String token) async {
    final endpoint = dotenv.get(daftarBarangDanJasaEndpoint);
    final response = await locator<Dio>().get(
      "$endpoint/$kapalId",
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return MasterBarangDanJasaResponse.fromJson(response.data);
  }
}
