import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../util/const.dart';

class UserLocalDataSource {
  Future<void> saveToken(String token) async {
    const storage = FlutterSecureStorage();
    final key = dotenv.get(tokenKey);
    await storage.write(key: key, value: token);
  }

  Future<String?> getToken() async {
    const storage = FlutterSecureStorage();
    final key = dotenv.get(tokenKey);
    return await storage.read(key: key);
  }

  Future<void> deleteToken() async {
    const storage = FlutterSecureStorage();
    final key = dotenv.get(tokenKey);
    await storage.delete(key: key);
  }
}
