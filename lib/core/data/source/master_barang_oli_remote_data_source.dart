import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/util/environment.dart';

import '../../util/locator.dart';
import '../model/alert_request_oli.dart';

class MasterBarangOliRemoteDataSource {
  Future<AlertRequestOliResponse> getMasterBarangOli(
      int id, String token) async {
    final endpoint = dotenv.get(alertRequestOLIEndpoint);
    final response = await locator<Dio>().post(
      endpoint,
      data: {"kapal_id": id},
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );

    return AlertRequestOliResponse.fromJson(response.data);
  }
}
