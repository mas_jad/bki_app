import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pms_pcm/core/data/model/profile.dart';
import 'package:pms_pcm/core/util/environment.dart';
import 'package:pms_pcm/core/util/locator.dart';

import '../model/login.dart';

class UserRemoteDataSource {
  Future<LoginResponse> login(String email, String password) async {
    final endpoint = dotenv.get(loginEndpoint);
    final response = await locator<Dio>().post(endpoint, data: {
      "email": email,
      "password": password,
    });
    return LoginResponse.fromJson(response.data);
  }

  Future<ProfileResponse> getProfile(String token) async {
    final endpoint = dotenv.get(profileEndpoint);
    final response = await locator<Dio>().get(
      endpoint,
      options: Options(headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      }),
    );
    return ProfileResponse.fromJson(response.data);
  }
}
